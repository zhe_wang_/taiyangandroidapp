package com.tydp.sell.entity;

import com.bigkoo.pickerview.model.IPickerViewData;

public class CityModel implements IPickerViewData {
	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getPickerViewText() {
		return name;
	}
}
