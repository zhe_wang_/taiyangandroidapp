package com.tydp.sell.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tydp.sell.utils.HttpRequestListener;
import com.tydp.sell.utils.MyApplication;
import com.tydp.sell.utils.MySPEdit;
import com.tydp.sell.view.R;

/**
 * Created by admin on 2016/9/7.
 */
public abstract class BaseActivity extends AppCompatActivity implements HttpRequestListener{
    protected  LinearLayout back_layout;


    protected TextView tv_title;

    protected MySPEdit mySPEdit;
    protected MyApplication myApplication;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        mySPEdit=MySPEdit.getInstance(this);
        myApplication= MyApplication.getInstance();
        initBaseView();
    }

    protected  void initBaseView(){
        back_layout = (LinearLayout) findViewById(R.id.back_layout);
        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_title = (TextView) findViewById(R.id.tv_title);
    };


    public abstract int getLayout() ;
}
