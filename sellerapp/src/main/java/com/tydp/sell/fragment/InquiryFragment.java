package com.tydp.sell.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tydp.sell.view.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heng on 2016/9/5.
 * 询盘列表 fragment
 */
public class InquiryFragment extends Fragment{
    private View view;
    private TextView tv_title;
    private LinearLayout back_layout;

    private TabLayout tabLayout;
    private ViewPager mViewPager;
    private ViewAdapter mAdapter;
    private List<String> mTitleList;
    private  List<Fragment>  mFragmentList;
    private String title;
    private InquiryListFragment inquiryListFragment;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_inquiry,null);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        tv_title = (TextView) view.findViewById(R.id.tv_title);
        tv_title.setText("询盘列表");
        back_layout = (LinearLayout) view.findViewById(R.id.back_layout);
        back_layout.setVisibility(View.INVISIBLE);
        mViewPager= (ViewPager) view.findViewById(R.id.vp_view);
        tabLayout= (TabLayout) view.findViewById(R.id.tabs);
        mTitleList=new ArrayList<>();
        mTitleList.add("猪");
        mTitleList.add("羊");
        mTitleList.add("牛");
        mTitleList.add("水产");
        mTitleList.add("其它");
        mFragmentList=new ArrayList<>();
        for (int i=0;i<mTitleList.size();i++){
            inquiryListFragment=new InquiryListFragment();
            Bundle data=new Bundle();
            data.putString("position",""+i);
            inquiryListFragment.setArguments(data);
            mFragmentList.add(inquiryListFragment);
        }
        mAdapter=new ViewAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(mViewPager);//设置viewPaper和tabs关联
    }

    class  ViewAdapter extends FragmentPagerAdapter {

        public ViewAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitleList.get(position);
        }
        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }
    }
}
