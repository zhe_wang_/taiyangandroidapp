package com.tydp.sell.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.tydp.sell.view.R;
import com.tydp.sell.activity.DeclarationActivity;

/**
 * Created by heng on 2016/9/5.
 * 服务 fragment
 */
public class ServiceFragment extends Fragment implements View.OnClickListener {
    private View view;
    private RelativeLayout agency_layout;
    private RelativeLayout logistics_layout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_service, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        agency_layout = (RelativeLayout) view.findViewById(R.id.agency_layout);
        logistics_layout = (RelativeLayout) view.findViewById(R.id.logistics_layout);

        agency_layout.setOnClickListener(this);
        logistics_layout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.agency_layout: {
                Intent intent = new Intent(getActivity(), DeclarationActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.logistics_layout: {
//                Intent intent=new Intent(getActivity(),);
//                startActivity(intent);
            }
            break;
        }
    }
}
