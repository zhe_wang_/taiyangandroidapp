package com.tydp.sell.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.jdsjlzx.interfaces.OnItemClickListener;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.github.jdsjlzx.recyclerview.ProgressStyle;
import com.github.jdsjlzx.util.RecyclerViewStateUtils;
import com.github.jdsjlzx.view.LoadingFooter;
import com.tydp.sell.entity.ItemModel;
import com.tydp.sell.entity.ListBean;
import com.tydp.sell.utils.HttpRequestListener;
import com.tydp.sell.view.R;
import com.tydp.sell.base.ListBaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2016/9/7.
 */
public class InquiryListFragment extends Fragment implements HttpRequestListener{
    /**服务器端一共多少条数据*/
    private static final int TOTAL_COUNTER = 64;

    /**每一页展示多少条数据*/
    private static final int REQUEST_COUNT = 10;

    /**已经获取到多少条数据了*/
    private static int mCurrentCounter = 0;
    private View view;
    private LRecyclerView mRecyclerView;
    private LRecyclerViewAdapter mLRecyclerViewAdapter;
    private DataAdapter mDataAdapter;
//    private RecyclerView mRecyclerView;
//    private TextView position;
//    private String title;

    protected int mCurrentPage = 1;
    protected int totalPage = 0;
    private List<ListBean> mDatas;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       view=inflater.inflate(R.layout.fragment_inquiry_list,null);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mRecyclerView = (LRecyclerView) view.findViewById(R.id.recyclerview);
//        //init data
        ArrayList<ItemModel> dataList = new ArrayList<>();
        for (int i = 0; i < 11; i++) {

            ItemModel item = new ItemModel();
            item.id = i;
            item.title = "item" + i;
            dataList.add(item);
        }
        mCurrentCounter = dataList.size();
        mDataAdapter = new DataAdapter(getActivity());
        mDataAdapter.addAll(dataList);

        mLRecyclerViewAdapter = new LRecyclerViewAdapter(getActivity(), mDataAdapter);
        mRecyclerView.setAdapter(mLRecyclerViewAdapter);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mRecyclerView.setRefreshProgressStyle(ProgressStyle.BallSpinFadeLoader);
        mRecyclerView.setArrowImageView(R.drawable.ic_pulltorefresh_arrow);

//        RecyclerViewUtils.setHeaderView(mRecyclerView, new SampleHeader(getActivity()));

        mRecyclerView.setLScrollListener(new LRecyclerView.LScrollListener() {
            @Override
            public void onRefresh() {
                RecyclerViewStateUtils.setFooterViewState(mRecyclerView, LoadingFooter.State.Normal);
//                mDataAdapter.clear();
//                mCurrentCounter = 0;
//                isRefresh = true;
//                requestData();
            }

            @Override
            public void onScrollUp() {
            }

            @Override
            public void onScrollDown() {
            }

            @Override
            public void onBottom() {
                LoadingFooter.State state = RecyclerViewStateUtils.getFooterViewState(mRecyclerView);
                if(state == LoadingFooter.State.Loading) {
//                    Log.d(TAG, "the state is Loading, just wait..");
                    return;
                }

                if (mCurrentCounter < TOTAL_COUNTER) {
                    // loading more
                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, REQUEST_COUNT, LoadingFooter.State.Loading, null);
//                    requestData();
                } else {
                    //the end
                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, REQUEST_COUNT, LoadingFooter.State.TheEnd, null);

                }
            }

            @Override
            public void onScrolled(int distanceX, int distanceY) {
            }

        });
//        mRecyclerView.setRefreshing(true);

        mLRecyclerViewAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ItemModel item = mDataAdapter.getDataList().get(position);
//                AppToast.showShortText(EndlessLinearLayoutActivity.this, item.title);
            }

            @Override
            public void onItemLongClick(View view, int position) {
                ItemModel item = mDataAdapter.getDataList().get(position);
//                AppToast.showShortText(EndlessLinearLayoutActivity.this, "onItemLongClick - " + item.title);
            }
        });
    }
//全部订单{"sign":"ee7a84e07f027e9c4a2fe53ce2f8fcb2","action":"order_list","user_id":"11","model":"order","page":1,"token":"TbNS3ferz72vyRO7oqBhMIG75aVRE3no"}
//    private void requestData() {
//        Map<String, Object> params = new HashMap<>();
//        params.put("model", "order");
//        params.put("action", "order_list");
//        params.put("user_id", mySPEdit.getUserId());
//        params.put("token", mySPEdit.getToken());
//        params.put("page", mCurrentPage);
//        params.put("sign", MD5Utils.encode(MD5Utils.getSign("order", "order_list")));
//        L.e("全部订单" + JSON.toJSONString(params));
//        HttpUtils.sendPost(params, this, 100);
//    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }


    private class DataAdapter extends ListBaseAdapter<ItemModel> {

        private LayoutInflater mLayoutInflater;

        public DataAdapter(Context context) {
            mLayoutInflater = LayoutInflater.from(context);
            mContext = context;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(mLayoutInflater.inflate(R.layout.allinquiry_item, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//            ItemModel item = mDataList.get(position);
//
//            ViewHolder viewHolder = (ViewHolder) holder;
//            viewHolder.textView.setText(item.title);
        }


        private class ViewHolder extends RecyclerView.ViewHolder {

            private TextView textView;

            public ViewHolder(View itemView) {
                super(itemView);
//                textView = (TextView) itemView.findViewById(R.id.info_text);
            }
        }
    }
}
