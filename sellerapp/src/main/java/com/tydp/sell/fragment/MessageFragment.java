package com.tydp.sell.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.jdsjlzx.interfaces.OnItemClickListener;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.github.jdsjlzx.recyclerview.ProgressStyle;
import com.github.jdsjlzx.util.RecyclerViewStateUtils;
import com.github.jdsjlzx.view.LoadingFooter;
import com.tydp.sell.entity.ItemModel;
import com.tydp.sell.utils.HttpRequestListener;
import com.tydp.sell.view.R;
import com.tydp.sell.base.ListBaseAdapter;

import java.util.ArrayList;

/**
 * Created by heng on 2016/7/21.
 * 留言fragment
 */
public class MessageFragment extends Fragment implements HttpRequestListener {
    /*@InjectView(R.id.message_recyclerView)
//    XRecyclerView mRecyclerView;*/
//    private AfterSaleAdapter mAdapter;
//
//    private String title;
//
//    @InjectView(R.id.recycler_view)
//    CustRecyclerView mRecyclerView;
//    @InjectView(R.id.error_layout)
//    ErrorLayout mErrorLayout;
//
//    protected HeaderAndFooterRecyclerViewAdapter mRecyclerViewAdapter;

    /**服务器端一共多少条数据*/
    private static final int TOTAL_COUNTER = 64;

    /**每一页展示多少条数据*/
    private static final int REQUEST_COUNT = 10;

    /**已经获取到多少条数据了*/
    private static int mCurrentCounter = 0;
    private View view;
    private LRecyclerView mRecyclerView;
    private LRecyclerViewAdapter mLRecyclerViewAdapter;
    private DataAdapter mDataAdapter;

    protected int mCurrentPage = 1;
    protected int totalPage = 0;
    private String title;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.fragment_message, container, false);
//        ButterKnife.inject(this, view);
        title = getArguments().getString("title");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mRecyclerView = (LRecyclerView) view.findViewById(R.id.recyclerview);
//        //init data
        ArrayList<ItemModel> dataList = new ArrayList<>();
        for (int i = 0; i < 11; i++) {

            ItemModel item = new ItemModel();
            item.id = i;
            item.title = "item" + i;
            dataList.add(item);
        }
        mCurrentCounter = dataList.size();
        mDataAdapter = new DataAdapter(getActivity());
        mDataAdapter.addAll(dataList);

        mLRecyclerViewAdapter = new LRecyclerViewAdapter(getActivity(), mDataAdapter);
        mRecyclerView.setAdapter(mLRecyclerViewAdapter);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mRecyclerView.setRefreshProgressStyle(ProgressStyle.BallSpinFadeLoader);
        mRecyclerView.setArrowImageView(R.drawable.ic_pulltorefresh_arrow);

//        RecyclerViewUtils.setHeaderView(mRecyclerView, new SampleHeader(getActivity()));

        mRecyclerView.setLScrollListener(new LRecyclerView.LScrollListener() {
            @Override
            public void onRefresh() {
                RecyclerViewStateUtils.setFooterViewState(mRecyclerView, LoadingFooter.State.Normal);
//                mDataAdapter.clear();
//                mCurrentCounter = 0;
//                isRefresh = true;
//                requestData();
            }

            @Override
            public void onScrollUp() {
            }

            @Override
            public void onScrollDown() {
            }

            @Override
            public void onBottom() {
                LoadingFooter.State state = RecyclerViewStateUtils.getFooterViewState(mRecyclerView);
                if(state == LoadingFooter.State.Loading) {
//                    Log.d(TAG, "the state is Loading, just wait..");
                    return;
                }

                if (mCurrentCounter < TOTAL_COUNTER) {
                    // loading more
                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, REQUEST_COUNT, LoadingFooter.State.Loading, null);
//                    requestData();
                } else {
                    //the end
                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, REQUEST_COUNT, LoadingFooter.State.TheEnd, null);

                }
            }

            @Override
            public void onScrolled(int distanceX, int distanceY) {
            }

        });
//        mRecyclerView.setRefreshing(true);

        mLRecyclerViewAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ItemModel item = mDataAdapter.getDataList().get(position);
//                AppToast.showShortText(EndlessLinearLayoutActivity.this, item.title);
            }

            @Override
            public void onItemLongClick(View view, int position) {
                ItemModel item = mDataAdapter.getDataList().get(position);
//                AppToast.showShortText(EndlessLinearLayoutActivity.this, "onItemLongClick - " + item.title);
            }
        });
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

//    private MySPEdit mySPEdit;
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        mySPEdit = MySPEdit.getInstance(getActivity());
//        mCurrentPage = 1;
//        mDatas=new ArrayList<>();
//        mAdapter = new AfterSaleAdapter(getActivity());
//        mAdapter.setDataList(mDatas);
//        mRecyclerViewAdapter = new HeaderAndFooterRecyclerViewAdapter(getActivity(),mAdapter);
//        mRecyclerView.setAdapter(mRecyclerViewAdapter);
//        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
////        mRecyclerView.setHasFixedSize(true);
//
//        mRecyclerView.addOnScrollListener(mOnScrollListener);
//        mRecyclerView.setIsRefreshing(false);
//        mErrorLayout.setOnLayoutClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                mCurrentPage = 1;
//                mErrorLayout.setErrorType(ErrorLayout.NETWORK_LOADING);
//                requestData();
//            }
//        });
//        requestData();
//    }
//    private void requestData(){
//        if ("全部".equals(title)) {
//            showProgress(getActivity(),getString(R.string.jiazaizhong_dotdotdot));
//            Map<String, Object> params = new HashMap<>();
//            params.put("model", "user");
//            params.put("action", "message_list");
//            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "message_list")));
//            params.put("user_id", mySPEdit.getUserId());
//            params.put("msg_type","");
//            params.put("token", mySPEdit.getToken());
//            params.put("size", 10);
//            params.put("page", mCurrentPage);
//            L.e("全部"+JSON.toJSONString(params));
//            HttpUtils.sendPost(params, this, 100);
//        } else if ("留言".equals(title)) {
//            showProgress(getActivity(),getString(R.string.jiazaizhong_dotdotdot));
//            Map<String, Object> params = new HashMap<>();
//            params.put("model", "user");
//            params.put("action", "message_list");
//            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "message_list")));
//            params.put("user_id", mySPEdit.getUserId());
//            params.put("msg_type", "0");
//            params.put("token", mySPEdit.getToken());
//            params.put("size", 10);
//            params.put("page", mCurrentPage);
//            L.e(JSON.toJSONString(params));
//            L.e("留言"+JSON.toJSONString(params));
//            HttpUtils.sendPost(params, this, 101);
//        } else if ("投诉".equals(title)) {
//            showProgress(getActivity(),getString(R.string.jiazaizhong_dotdotdot));
//            Map<String, Object> params = new HashMap<>();
//            params.put("model", "user");
//            params.put("action", "message_list");
//            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "message_list")));
//            params.put("user_id", mySPEdit.getUserId());
//            params.put("msg_type", "1");
//            params.put("token", mySPEdit.getToken());
//            params.put("size", 10);
//            params.put("page", mCurrentPage);
//            L.e("投诉"+JSON.toJSONString(params));
//            HttpUtils.sendPost(params, this, 102);
//        } else {
//            showProgress(getActivity(),getString(R.string.jiazaizhong_dotdotdot));
//            Map<String, Object> params = new HashMap<>();
//            params.put("model", "user");
//            params.put("action", "message_list");
//            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "message_list")));
//            params.put("user_id", mySPEdit.getUserId());
//            params.put("msg_type", "2");
//            params.put("token", mySPEdit.getToken());
//            params.put("size", 10);
//            params.put("page", mCurrentPage);
//            L.e("售后"+JSON.toJSONString(params));
//            HttpUtils.sendPost(params, this, 103);
//        }
//    }
//    private MessageListModel messageListModel;
//    private List<MessageModel> mDatas;
//
//    @Override
//    public void success(String response, int id) {
//        mCurrentPage++;
//        switch (id) {
//            case 100:
//                dataSuccess(response);
//                break;
//            case 101:
//                dataSuccess(response);
//                break;
//            case 102:
//                dataSuccess(response);
//                break;
//            case 103:
//                dataSuccess(response);
//                break;
//        }
//    }
//
//    @Override
//    public void failByOther(String fail, int id) {
//        dismissProgress(getActivity());
//    }
//
//    @Override
//    public void fail(String fail, int id) {
//        dismissProgress(getActivity());
//        T.showShort(getActivity(),R.string.jianchawangluo);
//        if (mCurrentPage == 1) {
//            mErrorLayout.setErrorType(ErrorLayout.NETWORK_ERROR);
//        } else {
//
//            //在无网络时，滚动到底部时，mCurrentPage先自加了，然而在失败时却
//            //没有减回来，如果刻意在无网络的情况下上拉，可以出现漏页问题
//            //find by TopJohn
////            mCurrentPage--;
//
//            mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
//            RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, totalPage, LoadingFooter.State.NetWorkError, mFooterClick);
//            mAdapter.notifyDataSetChanged();
//        }
//    }
//    //數據請求 成功進行處理
//    private void dataSuccess(String response) {
//        dismissProgress(getActivity());
//        messageListModel = new Gson().fromJson(response, MessageListModel.class);
//        totalPage = messageListModel.getTotal().getPage_count();
//        mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
//        L.e("縂頁數:"+totalPage);
////                mRecyclerView.loadMoreComplete();
//        for (int i = 0; i < messageListModel.getList().size(); i++) {
//            mDatas.add(messageListModel.getList().get(i));
//        }
//        if (mCurrentPage == 1) {
//            L.e("数据的大小:"+mDatas.size());
//            mAdapter.setDataList(mDatas);
//        } else {
//            L.e("设置数据:");
//            RecyclerViewStateUtils.setFooterViewState(mRecyclerView, LoadingFooter.State.Normal);
////            mAdapter.addAll(mDatas);
//            mAdapter.setDataList(mDatas);
//        }
//        mRecyclerView.refreshComplete();
//        mAdapter.notifyDataSetChanged();
//    }
//    private View.OnClickListener mFooterClick = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
//            requestData();
//        }
//    };
//    protected RecyclerOnScrollListener mOnScrollListener = new RecyclerOnScrollListener() {
//
//        @Override
//        public void onScrolled(int dx, int dy) {
//            super.onScrolled(dx, dy);
//
////            if(null != headerView) {
////                if (dy == 0 || dy < headerView.getHeight()) {
////                    toTopBtn.setVisibility(View.GONE);
////                }
////            }
//
//
//        }
//
//        @Override
//        public void onScrollUp() {
//            // 滑动时隐藏float button
////            if (toTopBtn.getVisibility() == View.VISIBLE) {
////                toTopBtn.setVisibility(View.GONE);
////                animate(toTopBtn, R.anim.floating_action_button_hide);
////            }
//        }
//
//        @Override
//        public void onScrollDown() {
////            if (toTopBtn.getVisibility() != View.VISIBLE) {
////                toTopBtn.setVisibility(View.VISIBLE);
////                animate(toTopBtn, R.anim.floating_action_button_show);
////            }
//        }
//
//        @Override
//        public void onBottom() {
//            LoadingFooter.State state = RecyclerViewStateUtils.getFooterViewState(mRecyclerView);
//            if(state == LoadingFooter.State.Loading) {
//                return;
//            }
//            L.e("當前頁:"+mCurrentPage);
//            L.e("縂頁數:"+totalPage);
//            if (mCurrentPage <=totalPage) {
//                // loading more
//                RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
//                requestData();
//            } else {
//                //the end
//                /*if (totalPage > 1){
//                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, getPageSize(), LoadingFooter.State.TheEnd, null);
//                }else {
//                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, mListAdapter.getItemCount(), LoadingFooter.State.TheEnd, null);
//
//                }*/
//                RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, totalPage, LoadingFooter.State.TheEnd, null);
//            }
//        }
//
//
//    };

    private class DataAdapter extends ListBaseAdapter<ItemModel> {

        private LayoutInflater mLayoutInflater;

        public DataAdapter(Context context) {
            mLayoutInflater = LayoutInflater.from(context);
            mContext = context;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(mLayoutInflater.inflate(R.layout.item_message, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//            ItemModel item = mDataList.get(position);
//
//            ViewHolder viewHolder = (ViewHolder) holder;
//            viewHolder.textView.setText(item.title);
        }


        private class ViewHolder extends RecyclerView.ViewHolder {

            private TextView textView;

            public ViewHolder(View itemView) {
                super(itemView);
//                textView = (TextView) itemView.findViewById(R.id.info_text);
            }
        }
    }
}
