package com.tydp.sell.fragment;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.tydp.sell.utils.DensityUtil;
import com.tydp.sell.view.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heng on 2016/9/5.
 * 报盘管理 fragment
 */
public class OfferFragment extends Fragment{
    private View view;
    private TextView tv_title;
    private LinearLayout back_layout;
    private TabLayout tabLayout;
    private ViewPager mViewPager;
    private ViewAdapter mAdapter;
    private List<String> mTitleList;
    private  List<Fragment>  mFragmentList;
    private OfferListFragment offerListFragment;

    private TextView tv_all,tv_order,tv_withdraw;
    private PopupWindow popupWindow;
    private View layout;

    private LinearLayout ll_select;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_offer,container,false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tv_title = (TextView) view.findViewById(R.id.tv_title);
        tv_title.setText("报盘管理");
        back_layout = (LinearLayout) view.findViewById(R.id.back_layout);
        back_layout.setVisibility(View.INVISIBLE);
        mViewPager= (ViewPager) view.findViewById(R.id.vp_view);
        tabLayout= (TabLayout) view.findViewById(R.id.tabs);
        ll_select=(LinearLayout) view.findViewById(R.id.ll_select);

        ll_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupWindow(ll_select);
            }
        });
        mTitleList=new ArrayList<>();
        mTitleList.add("上架(15)");
        mTitleList.add("仓库(10)");
        mFragmentList=new ArrayList<>();
        for (int i=0;i<mTitleList.size();i++){
            offerListFragment=new OfferListFragment();
            Bundle data=new Bundle();
            data.putString("position",""+i);
            offerListFragment.setArguments(data);
            mFragmentList.add(offerListFragment);
        }
        mAdapter=new ViewAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(mViewPager);//设置viewPaper和tabs关联
    }

    public void showPopupWindow(View parent) {
        //加载布局
        layout = (LinearLayout) LayoutInflater.from(getActivity()).inflate(
                R.layout.dialog, null);
        //找到布局的控件
//        private TextView tv_all,tv_order,tv_withdraw;
        tv_all=(TextView) view.findViewById(R.id.tv_all);
        tv_order=(TextView) view.findViewById(R.id.tv_order);
        tv_withdraw=(TextView) view.findViewById(R.id.tv_withdraw);
//        tv_all.setOnClickListener(this);
//        tv_order.setOnClickListener(this);
//        tv_withdraw.setOnClickListener(this);
        // 实例化popupWindow
        popupWindow = new PopupWindow(layout, DensityUtil.dip2px(getActivity(),106) , DensityUtil.dip2px(getActivity(),131));
        //控制键盘是否可以获得焦点
        popupWindow.setFocusable(true);
        //设置popupWindow弹出窗体的背景
//        popupWindow.setBackgroundDrawable(new BitmapDrawable(null, ""));
//        popupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.persin_icon_bg));
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        WindowManager manager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        @SuppressWarnings("deprecation")
        //获取xoff
                int xpos = manager.getDefaultDisplay().getWidth() / 2 - popupWindow.getWidth() / 2;
        //xoff,yoff基于anchor的左下角进行偏移。
        popupWindow.showAsDropDown(parent, 0, 0);
    }

    class  ViewAdapter extends FragmentPagerAdapter {

        public ViewAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitleList.get(position);
        }
        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }
    }
}
