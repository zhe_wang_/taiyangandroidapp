package com.tydp.sell.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tydp.sell.view.CallMainActivity;
import com.tydp.sell.view.R;
import com.tydp.sell.activity.ChildAccountActivity;
import com.tydp.sell.activity.DickerActivity;
import com.tydp.sell.activity.EditSellerActivity;
import com.tydp.sell.activity.InquiryActivity;
import com.tydp.sell.activity.InviteFriendsActivity;
import com.tydp.sell.activity.ServiceActivity;
import com.tydp.sell.activity.ShipperManageActivity;
import com.tydp.sell.activity.ShopSettingActivity;
import com.tydp.sell.activity.WalletActivity;

/**
 * Created by heng on 2016/9/5.
 * 我的 fragment
 */
public class PersonalFragment extends Fragment implements View.OnClickListener {
    private View view;
    private RelativeLayout shopsetting_layout;
    private RelativeLayout wallet_layout;
    private RelativeLayout offer_layout;
    private LinearLayout dicker_layout;
    private RelativeLayout address_layout;

    private RelativeLayout service_layout;
    private RelativeLayout inquiry_layout;
    private RelativeLayout friend_layout;
    private LinearLayout child_layout;
    private TextView tv_edit;
    //fragment与MainActivity通信的 回调接口
    private CallMainActivity callMainActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_personal, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();

    }

    private void initView() {
        tv_edit = (TextView) view.findViewById(R.id.tv_edit);
        shopsetting_layout = (RelativeLayout) view.findViewById(R.id.shopsetting_layout);
        wallet_layout = (RelativeLayout) view.findViewById(R.id.wallet_layout);
        offer_layout = (RelativeLayout) view.findViewById(R.id.offer_layout);
        dicker_layout = (LinearLayout) view.findViewById(R.id.dicker_layout);
        address_layout = (RelativeLayout) view.findViewById(R.id.address_layout);
        service_layout = (RelativeLayout) view.findViewById(R.id.service_layout);
        inquiry_layout = (RelativeLayout) view.findViewById(R.id.inquiry_layout);
        friend_layout = (RelativeLayout) view.findViewById(R.id.friend_layout);
        child_layout = (LinearLayout) view.findViewById(R.id.child_layout);

        tv_edit.setOnClickListener(this);
        shopsetting_layout.setOnClickListener(this);
        wallet_layout.setOnClickListener(this);
        offer_layout.setOnClickListener(this);
        dicker_layout.setOnClickListener(this);
        address_layout.setOnClickListener(this);
        service_layout.setOnClickListener(this);
        inquiry_layout.setOnClickListener(this);
        friend_layout.setOnClickListener(this);
        child_layout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.wallet_layout: {
                //我的钱包
                Intent intent = new Intent(getActivity(), WalletActivity.class);
                startActivity(intent);
            }
            break;
            //店铺设置
            case R.id.shopsetting_layout: {
                Intent intent = new Intent(getActivity(), ShopSettingActivity.class);
//                startActivity(intent);
            }
            break;
            //编辑资料
            case R.id.tv_edit: {
                Intent intent = new Intent(getActivity(), EditSellerActivity.class);
                startActivity(intent);
            }
            break;

            case R.id.offer_layout: {
                //我的报盘
//                Intent intent=new Intent(getActivity(), InquiryActivity.class);
//                startActivity(intent);
                if (callMainActivity != null) {
                    callMainActivity.showOffer();
                }
            }
            break;
            case R.id.dicker_layout: {
                //还价通知
                Intent intent = new Intent(getActivity(), DickerActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.address_layout: {
                //发货人管理
                Intent intent = new Intent(getActivity(), ShipperManageActivity.class);
                intent.putExtra("my", "my");
                startActivity(intent);
            }
            break;
            case R.id.service_layout: {
                //联系客服
                Intent intent = new Intent(getActivity(), ServiceActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.inquiry_layout: {
                //我的询盘
                Intent intent = new Intent(getActivity(), InquiryActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.friend_layout: {
                //推荐好友
                Intent intent = new Intent(getActivity(), InviteFriendsActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.child_layout: {
                //子账号管理
                Intent intent = new Intent(getActivity(), ChildAccountActivity.class);
                startActivity(intent);
            }
            break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity != null) {
            callMainActivity = (CallMainActivity) activity;
        }
    }

}
