package com.tydp.sell.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.ConvertUtils;
import com.tydp.sell.utils.Constants;
import com.tydp.sell.utils.DensityUtil;
import com.tydp.sell.utils.L;
import com.tydp.sell.view.R;

import java.util.List;

/**
 * Created by heng on 2016/9/18.
 */
public class DickerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<String> mDatas;

    private boolean[] flag;
    private FooterViewHolder mFooterViewHolder;
    private ItemViewHolder mItemViewHolder;

    public DickerAdapter(Context mContext, List<String> mDatas) {
        this.mContext = mContext;
        this.mDatas = mDatas;
        flag = new boolean[7];
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Constants.TYPE_ITEM) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_dicker, parent, false);
            return new ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_footer, parent, false);
            return new FooterViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
//            ((ItemViewHolder) holder).tv_test.setText(mDatas.get(position));
            final ItemViewHolder mItemViewHolder = (ItemViewHolder) holder;
            mItemViewHolder.iv_show.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    L.e("是否为空hide_layout:" + (mItemViewHolder.show_layout == null));
//                    L.e("是否为空hide_layout:" + (mItemViewHolder.hide_layout == null));
                    mItemViewHolder.show_layout.setVisibility(View.GONE);
                    mItemViewHolder.hide_layout.setVisibility(View.VISIBLE);
                }
            });
            mItemViewHolder.iv_hide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemViewHolder.show_layout.setVisibility(View.VISIBLE);
                    mItemViewHolder.hide_layout.setVisibility(View.GONE);
                }
            });
            if (!flag[position]) {
                for (int i = 0; i < 3; i++) {
                    L.e("测试" + i);
//                viewHolder.dicker_layout.removeAllViews();
                    LinearLayout linearLayout = new LinearLayout(mContext);
                    TextView sortTextView = new TextView(mContext);
                    TextView price = new TextView(mContext);
                    TextView unit = new TextView(mContext);
                    price.setTextColor(mContext.getResources().getColor(R.color.common_red));

                    sortTextView.setBackgroundResource(R.drawable.dicker_bg);
                    int margintop = DensityUtil.dip2px(mContext, 17);
                    int margintopSize = ConvertUtils.dp2px(mContext, 15);
                    LinearLayout.LayoutParams sortParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                LinearLayout.LayoutParams sortParams= (LinearLayout.LayoutParams) viewHolder.ll_child.getLayoutParams();
                    sortParams.width = margintop;
                    sortParams.height = margintop;
                    sortParams.gravity = Gravity.CENTER;
                    sortTextView.setLayoutParams(sortParams);
                    int textSize = DensityUtil.sp2px(mContext, 14);
                    int textSize1 = ConvertUtils.sp2px(mContext, 14);
                    sortTextView.setGravity(Gravity.CENTER);
                    sortTextView.setTextSize(12);
                    sortTextView.setTextColor(mContext.getResources().getColor(R.color.white));
                    sortTextView.setText((i + 2) + "");


                    LinearLayout.LayoutParams priceParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    int marginLeft = DensityUtil.dip2px(mContext, 8);
                    priceParams.setMargins(marginLeft, 0, 0, 0);
                    price.setLayoutParams(priceParams);
                    price.setTextSize(14);
                    unit.setTextSize(14);
                    unit.setTextColor(mContext.getResources().getColor(R.color.common_textblackcolor));

                    price.setText("￥14800");
                    unit.setText("/吨");

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                LinearLayout.LayoutParams params=(LinearLayout.LayoutParams) viewHolder.ll_child.getLayoutParams();
                    params.setMargins(0, margintop, 0, 0);
                    linearLayout.setGravity(Gravity.CENTER_VERTICAL);
                    linearLayout.setLayoutParams(params);
                    linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                    linearLayout.addView(sortTextView);
                    linearLayout.addView(price);
                    linearLayout.addView(unit);
                    mItemViewHolder.dicker_hide_layout.addView(linearLayout);
                }
                flag[position] = true;
            }
        } else if (holder instanceof FooterViewHolder) {
            mFooterViewHolder = (FooterViewHolder) holder;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position + 1 == getItemCount()) {
            return Constants.TYPE_FOOTER;
        } else {
            return Constants.TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size() == 0 ? 0 : mDatas.size() + 1;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        LinearLayout dicker_hide_layout;
        LinearLayout ll_child;
        ImageView iv_show;
        ImageView iv_hide;
        RelativeLayout hide_layout;
        RelativeLayout show_layout;
        Button bt_user;

        public ItemViewHolder(View itemView) {
            super(itemView);
            dicker_hide_layout = (LinearLayout) itemView.findViewById(R.id.dicker_hide_layout);
            ll_child = (LinearLayout) itemView.findViewById(R.id.ll_child);
            iv_show = (ImageView) itemView.findViewById(R.id.iv_show);
            hide_layout = (RelativeLayout) itemView.findViewById(R.id.hide_layout);
            show_layout = (RelativeLayout) itemView.findViewById(R.id.show_layout);
            iv_hide = (ImageView) itemView.findViewById(R.id.iv_hide);
            bt_user = (Button) itemView.findViewById(R.id.bt_user);
        }

    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_footer;
        ProgressBar progressBar;
        TextView tv_statues;

        public FooterViewHolder(View view) {
            super(view);
            ll_footer = (LinearLayout) view.findViewById(R.id.ll_footer);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            tv_statues = (TextView) view.findViewById(R.id.tv_statues);
        }
    }

    public void setLoadingStatues(int statues) {
        switch (statues) {
            case Constants.LOAD_STATUES_LOADING:
                mFooterViewHolder.ll_footer.setVisibility(View.VISIBLE);
                mFooterViewHolder.progressBar.setVisibility(View.VISIBLE);
                mFooterViewHolder.tv_statues.setText(getString(R.string.jiazaizhong_dotdotdot));
                break;
            case Constants.LOAD_STATUES_COMPLETED:
                mFooterViewHolder.ll_footer.setVisibility(View.GONE);
                break;
            case Constants.LOAD_STATUES_NO_MORE_DATA:
                if (mFooterViewHolder!=null){
                    mFooterViewHolder.ll_footer.setVisibility(View.VISIBLE);
                    mFooterViewHolder.progressBar.setVisibility(View.GONE);
                    mFooterViewHolder.tv_statues.setText("数据全部加载完毕");
                }
                break;
        }
    }

    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);

//        void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
}
