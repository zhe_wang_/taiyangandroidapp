package com.tydp.sell.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.tydp.sell.utils.L;
import com.tydp.sell.view.R;

import java.util.HashMap;
import java.util.List;

/**
 * PersonInfoActivity  Adapter
 * Created by heng on 2016/7/14.
 */
public class ChildAccountAdapter extends RecyclerView.Adapter<ChildAccountAdapter.ViewHolder> {
    private List<String> mDatas;
    //    private int count = 4;
    private boolean flag = true;
    private Context mCtx;
    //用于记录每个RadioButton的状态，并保证只可选一个
    HashMap<String, Boolean> states = new HashMap<String, Boolean>();

    private boolean flagCheck;

    private Button confirm;

    private boolean intentFlag;

    public ChildAccountAdapter(Context mCtx, List<String> mDatas) {
        this.mDatas = mDatas;
        this.mCtx = mCtx;
        this.confirm = confirm;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_child, parent, false);

        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.iv_child_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = holder.getLayoutPosition();
                L.e("pos.....:" + pos);
                removeData(pos);
            }
        });
//        holder.tv_personinfoitem_name.setText(mDatas.get(position).getName());
//        holder.tv_personinfoitem_phone.setText(mDatas.get(position).getMobile());
//        String idNumber=mDatas.get(position).getId_number();
//        L.e("长度:"+idNumber.length());
//        holder.tv_personinfoitem_number.setText(idNumber.substring(0,5)+"****"+idNumber.substring(idNumber.length()-6,idNumber.length()));
//        if (flagCheck) {
//            holder.rb_personinfo_ischeck.setVisibility(View.GONE);
//        } else {
//            holder.rb_personinfo_ischeck.setTag(position);
//            if (mOnItemClickLitener != null) {
//                //单选收货地址
//                holder.rb_personinfo_ischeck.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        //重置，确保最多只有一项被选中
//                        for (String key : states.keySet()) {
//                            states.put(key, false);
//                        }
//                        states.put(String.valueOf(position), holder.rb_personinfo_ischeck.isChecked());
//                        if (holder.rb_personinfo_ischeck.isChecked() == true) {
//                            intentFlag = true;
//                            confirm.setBackgroundResource(R.mipmap.order_btn_ensure);
//                        } else {
//                            intentFlag = false;
//                            confirm.setBackgroundResource(R.mipmap.order_btn_pickup);
//                        }
//                        ChildAccountAdapter.this.notifyDataSetChanged();
//                    }
//                });
//
//                boolean res = false;
//                if (states.get(String.valueOf(position)) == null || states.get(String.valueOf(position)) == false) {
//                    res = false;
//                    states.put(String.valueOf(position), false);
////
////                    L.e("更新选择状态1");
//                } else {
//                    res = true;
////                    L.e("更新选择状态2");
////
//                }
//                holder.rb_personinfo_ischeck.setChecked(res);
//
//            }
//
//        }
//        //编辑收货地址
//        holder.iv_personinfo_edit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                    L.e("点击了编辑资料");
//                Intent i = new Intent(mCtx, EditPersonActivity.class);
//                i.putExtra("goodsPerson", (Serializable) mDatas.get(position));
//                mCtx.startActivity(i);
//            }
//        });
//        confirm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (intentFlag) {
////                    Intent a = new Intent(mCtx, AddPersonActivity.class);
////                    a.putExtra("address_id",mDatas.get(position).getId());
////                    setResult(1, a); //intent为A传来的带有Bundle的intent，当然也可以自己定义新的Bundle
////                    mCtx.finish();
//                    if(Activity.class.isInstance(mCtx))
//                    {
//                        // 转化为activity，然后finish就行了
//                        Activity activity = (Activity)mCtx;
//                        Intent a = new Intent(mCtx, AddPersonActivity.class);
//                        a.putExtra("address_id",mDatas.get(position).getId());
//                        activity.setResult(10,a);
//                        activity.finish();
//                    }
//                } else {
//                    Intent i = new Intent(mCtx, AddPersonActivity.class);
//                    mCtx.startActivity(i);
//                }
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
//        @InjectView(R.id.iv_personinfo_ischeck)
//        CheckBox rb_personinfo_ischeck;
//        @InjectView(R.id.iv_personinfo_edit)
//        ImageView iv_personinfo_edit;
//        @InjectView(R.id.tv_personinfoitem_name)
//        TextView tv_personinfoitem_name;
//        @InjectView(R.id.tv_personinfoitem_phone)
//        TextView tv_personinfoitem_phone;
//        @InjectView(R.id.tv_personinfoitem_number)
//        TextView tv_personinfoitem_number;

        ImageView iv_child_delete;

        public ViewHolder(final View itemView) {
            super(itemView);
//            ButterKnife.inject(this, itemView);
            iv_child_delete = (ImageView) itemView.findViewById(R.id.iv_child_delete);
        }

    }

    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
//      void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    public void setFlag(boolean flag) {
        this.flagCheck = flag;
    }

    public void removeData(int position) {
        mDatas.remove(position);
        notifyItemRemoved(position);
    }
}
