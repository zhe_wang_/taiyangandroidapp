package com.tydp.sell.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.tydp.sell.view.R;
import com.tydp.sell.activity.EditShipperActivity;

import java.util.HashMap;
import java.util.List;

/**
 * Created by heng on 2016/9/13.
 */
public class ShipperManageAdapter extends RecyclerView.Adapter<ShipperManageAdapter.ViewHolder> {
    private List<String> mDatas;
    //    private int count = 4;
    private boolean flag = true;
    private Context mCtx;
    //用于记录每个RadioButton的状态，并保证只可选一个
    HashMap<String, Boolean> states = new HashMap<String, Boolean>();

    private boolean flagCheck;

    private TextView confirm;

    private boolean intentFlag;

    public ShipperManageAdapter(Context mCtx,List<String> mDatas) {
        this.mDatas = mDatas;
        this.mCtx = mCtx;
//        this.confirm = confirm;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shiper, parent, false);

        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (flagCheck) {
            holder.iv_personinfo_ischeck.setVisibility(View.GONE);
        } else {
            holder.iv_personinfo_ischeck.setTag(position);
            if (mOnItemClickLitener != null) {
                //单选收货地址
                holder.iv_personinfo_ischeck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //重置，确保最多只有一项被选中
                        for (String key : states.keySet()) {
                            states.put(key, false);
                        }
                        states.put(String.valueOf(position), holder.iv_personinfo_ischeck.isChecked());
                        if (holder.iv_personinfo_ischeck.isChecked() == true) {
                            intentFlag = true;
//                            confirm.setBackgroundResource(R.mipmap.order_btn_ensure);
                        } else {
                            intentFlag = false;
//                            confirm.setBackgroundResource(R.mipmap.order_btn_pickup);
                        }
                        ShipperManageAdapter.this.notifyDataSetChanged();
                    }
                });

                boolean res = false;
                if (states.get(String.valueOf(position)) == null || states.get(String.valueOf(position)) == false) {
                    res = false;
                    states.put(String.valueOf(position), false);
//
//                    L.e("更新选择状态1");
                } else {
                    res = true;
//                    L.e("更新选择状态2");
//
                }
                holder.iv_personinfo_ischeck.setChecked(res);

            }

        }
        //编辑收货地址
        holder.iv_personinfo_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                    L.e("点击了编辑资料");
                Intent i = new Intent(mCtx, EditShipperActivity.class);
//                i.putExtra("goodsPerson", (Serializable) mDatas.get(position));
                mCtx.startActivity(i);
            }
        });
        /*confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (intentFlag) {
//                    Intent a = new Intent(mCtx, AddPersonActivity.class);
//                    a.putExtra("address_id",mDatas.get(position).getId());
//                    setResult(1, a); //intent为A传来的带有Bundle的intent，当然也可以自己定义新的Bundle
//                    mCtx.finish();
                    if(Activity.class.isInstance(mCtx))
                    {
                        // 转化为activity，然后finish就行了
//                        Activity activity = (Activity)mCtx;
//                        Intent a = new Intent(mCtx, AddPersonActivity.class);
//                        a.putExtra("address_id",mDatas.get(position).getId());
//                        activity.setResult(10,a);
//                        activity.finish();
                    }
                } else {
//                    Intent i = new Intent(mCtx, AddPersonActivity.class);
//                    mCtx.startActivity(i);
                }
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox iv_personinfo_ischeck;
        ImageView iv_personinfo_edit;
        TextView tv_personinfoitem_name;
        TextView tv_personinfoitem_phone;


        public ViewHolder(final View view) {
            super(view);
            iv_personinfo_ischeck=(CheckBox) view.findViewById(R.id.iv_personinfo_ischeck);
            iv_personinfo_edit=(ImageView) view.findViewById(R.id.iv_personinfo_edit);
            tv_personinfoitem_name=(TextView) view.findViewById(R.id.tv_personinfoitem_name);
            tv_personinfoitem_phone=(TextView) view.findViewById(R.id.tv_personinfoitem_phone);
        }

    }

    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
//      void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    public void setFlag(boolean flag) {
        this.flagCheck = flag;
    }
}
