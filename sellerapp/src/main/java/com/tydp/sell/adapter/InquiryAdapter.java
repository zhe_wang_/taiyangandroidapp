package com.tydp.sell.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tydp.sell.utils.Constants;
import com.tydp.sell.view.R;

import java.util.List;

/**
 * Created by heng on 2016/9/20.
 */
public class InquiryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<String> mDatas;

    private FooterViewHolder mFooterViewHolder;
    public InquiryAdapter(Context mContext,List<String> mDatas) {
        this.mDatas = mDatas;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Constants.TYPE_ITEM) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_inqury, parent, false);
            return new ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_footer, parent, false);
            return new FooterViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder){

        }else if (holder instanceof FooterViewHolder){
            mFooterViewHolder= (FooterViewHolder) holder;
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size() == 0 ? 0 : mDatas.size() + 1;
    }
    @Override
    public int getItemViewType(int position) {
        if (position + 1 == getItemCount()) {
            return Constants.TYPE_FOOTER;
        } else {
            return Constants.TYPE_ITEM;
        }
    }
    class ItemViewHolder extends RecyclerView.ViewHolder {
        LinearLayout dicker_hide_layout;
        LinearLayout ll_child;
        ImageView iv_show;
        ImageView iv_hide;
        RelativeLayout hide_layout;
        RelativeLayout show_layout;
        Button bt_user;

        public ItemViewHolder(View itemView) {
            super(itemView);
            dicker_hide_layout = (LinearLayout) itemView.findViewById(R.id.dicker_hide_layout);
            ll_child = (LinearLayout) itemView.findViewById(R.id.ll_child);
            iv_show = (ImageView) itemView.findViewById(R.id.iv_show);
            hide_layout = (RelativeLayout) itemView.findViewById(R.id.hide_layout);
            show_layout = (RelativeLayout) itemView.findViewById(R.id.show_layout);
            iv_hide = (ImageView) itemView.findViewById(R.id.iv_hide);
            bt_user = (Button) itemView.findViewById(R.id.bt_user);
        }

    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_footer;
        ProgressBar progressBar;
        TextView tv_statues;

        public FooterViewHolder(View view) {
            super(view);
            ll_footer = (LinearLayout) view.findViewById(R.id.ll_footer);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            tv_statues = (TextView) view.findViewById(R.id.tv_statues);
        }
    }

    public void setLoadingStatues(int statues) {
        switch (statues) {
            case Constants.LOAD_STATUES_LOADING:
                mFooterViewHolder.ll_footer.setVisibility(View.VISIBLE);
                mFooterViewHolder.progressBar.setVisibility(View.VISIBLE);
                mFooterViewHolder.tv_statues.setText(getString(R.string.jiazaizhong_dotdotdot));
                break;
            case Constants.LOAD_STATUES_COMPLETED:
                mFooterViewHolder.ll_footer.setVisibility(View.GONE);
                break;
            case Constants.LOAD_STATUES_NO_MORE_DATA:
                if (mFooterViewHolder!=null){
                    mFooterViewHolder.ll_footer.setVisibility(View.VISIBLE);
                    mFooterViewHolder.progressBar.setVisibility(View.GONE);
                    mFooterViewHolder.tv_statues.setText("数据全部加载完毕");
                }
                break;
        }
    }

    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);

//        void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
}
