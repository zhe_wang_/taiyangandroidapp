package com.tydp.sell.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tydp.sell.view.R;

import java.util.List;

/**
 * HomeFragment底部RecyClerView Adapter
 * Created by heng on 2016/7/14.
 */
public class HomeRvAdapter extends RecyclerView.Adapter<HomeRvAdapter.ViewHolder> {
    private List<String> mDatas;
    private Context mCtx;

    public HomeRvAdapter(Context mCtx,List<String> mDatas) {
        this.mDatas = mDatas;
        this.mCtx = mCtx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.allinquiry_item, parent, false);

        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

//        final String Goods_id = mList.get(position).getGoods_id();
//
//        if(mOnItemClickLitener != null){
//            holder.rl_home_item.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent i =new Intent(mCtx, OfferInfoActivity.class);
//                    i.putExtra("Goods_id",Goods_id);
//                    mCtx.startActivity(i);
//                }
//            });
//        }
//
////        DraweeController controller = Fresco.newDraweeControllerBuilder()
////                .setUri(mList.get(position).getGoods_thumb())
////                .build();
//        holder.imageView.setImageURI(Uri.parse(mList.get(position).getGoods_thumb()));
//
//        holder.textView_chanpin.setText(mList.get(position).getGoods_name());//名称
//        holder.tv_chanpin_number.setText(mList.get(position).getBrand_sn());//厂号
//        holder.tv_ilaitem_location.setText(mList.get(position).getGoods_local());//地点
//        holder.tv_ilaitem_price.setText(mList.get(position).getShop_price());//价格
//        holder.tv_ilaitem_unit.setText(mList.get(position).getUnit_name());//价格单位
//        holder.tv_ilaitem_price2.setText(mList.get(position).spec_1);//规格数量
//        holder.tv_ilaitem_unit2.setText(mList.get(position).spec_1_unit);//规格单位
//        //还价
//        if(mList.get(position).getOffer().equals("11")){
//            holder.im_ilaitem_1.setVisibility(View.VISIBLE);
//        }else{
//            holder.im_ilaitem_1.setVisibility(View.GONE);
//        }
//        //拼
//        if(mList.get(position).getIs_pin() ==1){
//            holder.im_ilaitem_2.setVisibility(View.VISIBLE);
//        }else{
//            holder.im_ilaitem_2.setVisibility(View.GONE);
//        }
//        //商品类型  6=期货7=现货
//        if(mList.get(position).getGoods_type().equals("6")){
//            holder.im_ilaitem_3.setImageResource(R.mipmap.list_icon_futures);
//        }else{
//            holder.im_ilaitem_3.setImageResource(R.mipmap.list_icon_cash);
//        }
//        //售卖类型 4零5整
//        if(mList.get(position).getSell_type().equals("4")){
//            holder.im_ilaitem_3.setImageResource(R.mipmap.list_icon_start);
//        }else{
//            holder.im_ilaitem_3.setImageResource(R.mipmap.list_icon_full);
//        }


    }

    @Override
    public int getItemCount() {

        return mDatas.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder{
//        @InjectView(R.id.imageView)
//        SimpleDraweeView imageView;
//        @InjectView(R.id.textView_chanpin)
//        TextView textView_chanpin;
//        @InjectView(R.id.tv_chanpin_number)
//        TextView tv_chanpin_number;
//        @InjectView(R.id.tv_ilaitem_price)
//        TextView tv_ilaitem_price;
//        @InjectView(R.id.tv_ilaitem_unit)
//        TextView tv_ilaitem_unit;
//        @InjectView(R.id.tv_ilaitem_price2)
//        TextView tv_ilaitem_price2;
//        @InjectView(R.id.tv_ilaitem_unit2)
//        TextView tv_ilaitem_unit2;
//        @InjectView(R.id.tv_ilaitem_location)
//        TextView tv_ilaitem_location;
//        @InjectView(R.id.im_ilaitem_1)
//        ImageView im_ilaitem_1;
//        @InjectView(R.id.im_ilaitem_2)
//        ImageView im_ilaitem_2;
//        @InjectView(R.id.im_ilaitem_3)
//        ImageView im_ilaitem_3;
//        @InjectView(R.id.im_ilaitem_4)
//        ImageView im_ilaitem_4;
//        @InjectView(R.id.rl_home_item)
//        RelativeLayout rl_home_item;


        public ViewHolder(final View itemView) {
            super(itemView);
//            ButterKnife.inject(this,itemView);

        }

    }



    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
//      void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
}
