package com.tydp.sell.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tydp.sell.utils.L;
import com.tydp.sell.view.R;

import java.util.List;

/**
 * 提现 Adapter
 * Created by heng on 2016/9/12.
 */
public class WithdrawAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public final static int TYPE_FOOTER = 4;//底部--往往是loading_more
    public final static int TYPE_NORMAL = 2; // 正常的一条数据

    private List<String> mDatas;
    private Context mContext;

    public WithdrawAdapter(Context mContext, List<String> mDatas) {
        this.mDatas = mDatas;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View view;
//        switch (viewType) {
//            case TYPE_NORMAL:
//                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_balance, parent, false);
//                vh = new Nor1malViewHolder(view);
//                return vh;
//
//            case TYPE_FOOTER:
//                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer, parent, false);
//                vh=new FooterViewHolder(view);
//                return vh;
//        }
        if (viewType==TYPE_NORMAL){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_balance, parent, false);
            vh = new NormalViewHolder(view);
            return vh;
        }else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer, parent, false);
            vh=new FooterViewHolder(view);
            return vh;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        //这时候 article是 null，先把 footer 处理了
        if (holder instanceof FooterViewHolder) {
//            ((FooterViewHolder) holder).progress;
            L.e("加载更多");
            return;
        }

    }


    @Override
    public int getItemCount() {

        return mDatas.size();
    }


    @Override
    public int getItemViewType(int position) {
//        L.e("position:"+position);
        String str=mDatas.get(position);
//        L.e("为空:"+(str==null));
        if (str==null){
            L.e("str为空");
            return TYPE_FOOTER;
        }else {
            return TYPE_NORMAL;
        }

    }

    class NormalViewHolder extends RecyclerView.ViewHolder{
        TextView tv_text;
        TextView tv_number;
        TextView tv_date;
        TextView tv_price;

        public NormalViewHolder(final View itemView) {
            super(itemView);
//            ButterKnife.inject(this,itemView);
            tv_text= (TextView) itemView.findViewById(R.id.tv_text);
            tv_number= (TextView) itemView.findViewById(R.id.tv_number);
            tv_date= (TextView) itemView.findViewById(R.id.tv_date);
            tv_price= (TextView) itemView.findViewById(R.id.tv_price);
        }

    }



    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
//      void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
    /**
     * 底部加载更多
     */
    class FooterViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progress;
        public FooterViewHolder(View itemView) {
            super(itemView);
//            ButterKnife.inject(this, itemView);
            progress = (ProgressBar) itemView.findViewById(R.id.progress);
        }
    }
}
