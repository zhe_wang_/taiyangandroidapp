package com.tydp.sell.utils;

/**
 * Created by admin on 2016/8/23.
 */
public class Constants {
    public static final int REQUESTCODE = 0;//请求码
    public static final int LOGOOUT = 200;//登出
    public static final int CHANGEPHONE = 201;//修改手机号
    public static final int BACKSELECT = 202;//跳转到报盘
    public static final String STROAGEACTIVITYACTION = "com.tydp.activity.StorageServicectivity";//仓储服务的action
    public static final String PUBLISHACTIVITYACTION = "com.tydp.activity.PublishActivity";//询盘服务的action


    //RecyclerView的适配器使用的 状态码
    public static final int TYPE_ITEM = 1;
    public static final int TYPE_FOOTER = 2;

    //加载状态
    public static final int LOAD_STATUES_LOADING = 3;
    public static final int LOAD_STATUES_COMPLETED = 4;
    public static final int LOAD_STATUES_NO_MORE_DATA = 5;
}
