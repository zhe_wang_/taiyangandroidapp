package com.tydp.sell.utils;

/**
 * Created by admin on 2016/7/28.
 */
public interface HttpRequestListener {
    void success(String response, int id);
    void failByOther(String fail, int id);//值不正确
    void fail(String fail, int id);//访问不到服务器
}
