package com.tydp.sell.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

@SuppressLint("CommitPrefEdits")
public class MySPEdit {
    private static SharedPreferences sPreferences;
    private SharedPreferences.Editor editor;
    private static MySPEdit _instancePublic = null;

    @SuppressLint("WorldWriteableFiles")
    private MySPEdit(Context context) {
        sPreferences = context.getSharedPreferences("MySharedPreferencesEdit",
                Context.MODE_MULTI_PROCESS);
        editor = sPreferences.edit();
    }

    /**
     * @param ct
     * @return
     */
    public static MySPEdit getInstance(Context ct) {
        if (_instancePublic == null)
            _instancePublic = new MySPEdit(ct);
        return _instancePublic;
    }
    public void setIsFirst(boolean isFirst) {
        editor.putBoolean("isFirst", isFirst).commit();
    }

    public boolean getIsFirst(){
        return sPreferences.getBoolean("isFirst", true);
    }
    public void setToken(String token) {
        editor.putString("AccessToken", token).commit();
    }

    public String getToken() {
        return sPreferences.getString("AccessToken", null);
    }

    public void setUserId(String user_id) {
        editor.putString("UserId", user_id).commit();
    }

    public String getUserId() {
        return sPreferences.getString("UserId", null);
    }

    public void setIsLogin(boolean isLogin) {
        editor.putBoolean("isLogin", isLogin).commit();
    }

    public boolean getIsLogin() {
        return sPreferences.getBoolean("isLogin", false);
    }

    public void setUserName(String userName) {
        editor.putString("userName", userName).commit();
    }

    public String getUserName() {
        return sPreferences.getString("userName", null);
    }

    public void setPass(String pass) {
        editor.putString("pass", pass).commit();
    }

    public String getPass() {
        return sPreferences.getString("pass", null);
    }

    public void setUserInfo(String userInfo) {
        editor.putString("userInfo", userInfo).commit();
    }

    public String getUserInfo() {
        return sPreferences.getString("userInfo", null);
    }

    public void setFirstOptions1(int  firstOptions1) {
        editor.putInt("firstOptions1", firstOptions1).commit();
    }

    public int getFirstOptions1() {
        return sPreferences.getInt("firstOptions1", 0);
    }

    public void setFirstOption2(int  firstOption2) {
        editor.putInt("firstOption2", firstOption2).commit();
    }

    public int getFirstOption2() {
        return sPreferences.getInt("firstOption2", 0);
    }

    public void setIsAddress(boolean hasAddress) {
        editor.putBoolean("hasAddress", hasAddress).commit();
    }

    public boolean getIsAddress(){
        return sPreferences.getBoolean("hasAddress", false);
    }
    public void setFirstLogin(String fistLogin) {
        editor.putString("fistLogin", fistLogin).commit();
    }

    public String getFirstLogin() {
        return sPreferences.getString("fistLogin", null);
    }
    public void setSecondLogin(String secondLogin) {
        editor.putString("secondLogin", secondLogin).commit();
    }

    public String getSecondLogin() {
        return sPreferences.getString("secondLogin", null);
    }
}
