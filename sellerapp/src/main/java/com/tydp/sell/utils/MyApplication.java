package com.tydp.sell.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.umeng.socialize.PlatformConfig;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by heng on 2016/7/15.
 */
public class MyApplication extends Application {

    private static Context context;//全局Context变量
    private static MyApplication myApplication;

    private boolean addressChange = true;//取货人信息是否修改
    private boolean personChange = true;//个人信息是否修改

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;
        myApplication = this;
        //初始化各个平台
        initShare();
        //图片加载
        Fresco.initialize(this);
        //网络请求
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
//              .addInterceptor(new LoggerInterceptor("TAG"))
                .connectTimeout(10000L, TimeUnit.MILLISECONDS)
                .readTimeout(10000L, TimeUnit.MILLISECONDS)
                //其他配置
                .build();

        OkHttpUtils.initClient(okHttpClient);
    }

    private void initShare() {
        //微信 wx12342956d1cab4f9,a5ae111de7d9ea137e88a5e02c07c94d
        PlatformConfig.setWeixin("wx967daebe835fbeac", "5bb696d9ccd75a38c8a0bfe0675559b3");
        //豆瓣RENREN平台目前只能在服务器端配置
        //新浪微博
        PlatformConfig.setSinaWeibo("3921700954", "04b48b094faeb16683c32669824ebdad");
        //易信
        PlatformConfig.setYixin("yxc0614e80c9304c11b0391514d09f13bf");
        PlatformConfig.setQQZone("100424468", "c7394704798a158208a74ab60104f0ba");
        PlatformConfig.setTwitter("3aIN7fuF685MuZ7jtXkQxalyi", "MK6FEYG63eWcpDFgRYw4w9puJhzDl0tyuqWjZ3M7XJuuG7mMbO");
        PlatformConfig.setAlipay("2015111700822536");
        PlatformConfig.setLaiwang("laiwangd497e70d4", "d497e70d4c3e4efeab1381476bac4c5e");
        PlatformConfig.setPinterest("1439206");
    }

    public static MyApplication getInstance() {
        return myApplication;
    }


    public static Context getContext() {
        return context;
    }


    private List<Activity> activityList = new ArrayList<>();

    //添加Activity 到容器中
    public void addActivity(Activity activity) {
        activityList.add(activity);
    }

    //移除容器中的Activity
    public void removeActivity(Activity activity) {
        activityList.remove(activity);
    }

    public void finishAll() {
        for (Activity activity : activityList) {
            if (!activity.isFinishing()) {
                activity.finish();
            }
        }
    }

    /**
     * 判断当前版本是否兼容目标版本的方法
     *
     * @param VersionCode
     * @return
     */
    public static boolean isMethodsCompat(int VersionCode) {
        int currentVersion = android.os.Build.VERSION.SDK_INT;
        return currentVersion >= VersionCode;

    }

    public boolean isAddressChange() {
        return addressChange;
    }

    public void setAddressChange(boolean addressChange) {
        this.addressChange = addressChange;
    }

    public boolean isPersonChange() {
        return personChange;
    }

    public void setPersonChange(boolean personChange) {
        this.personChange = personChange;
    }
}
