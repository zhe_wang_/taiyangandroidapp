package com.tydp.sell.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;

import com.tydp.sell.fragment.InquiryFragment;
import com.tydp.sell.fragment.OfferFragment;
import com.tydp.sell.fragment.PersonalFragment;
import com.tydp.sell.fragment.ServiceFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,CallMainActivity{
    private static final String savedTab = "savedTab";
//    @InjectView(R.id.fragment_container)
//    FrameLayout fragmentContainer;
//    @InjectView(R.id.rb_tab_inquiry)
    RadioButton rb_tab_inquiry;
//    @InjectView(R.id.rb_tab_offer)
    RadioButton rb_tab_offer;
//    @InjectView(R.id.rb_tab_order)
    RadioButton rb_tab_order;
//    @InjectView(R.id.rb_tab_service)
    RadioButton rb_tab_service;
//    @InjectView(R.id.rb_tab_my)
    RadioButton rb_tab_my;
//    @InjectView(R.id.main_radios)
//    RadioGroup main_radios;
    private int index=0;
    private InquiryFragment inquiryFragment;
    private OfferFragment offerFragment;
    private ServiceFragment serviceFragment;
    private PersonalFragment personalFragment;

    private FragmentTransaction transaction;
//    private FragmentPagerAdapter mFragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
//        @Override
//        public Fragment getItem(int position) {
//            if (position == R.id.rb_tab_inquiry) {
//                return new InquiryFragment();
//            } else if (position == R.id.rb_tab_offer) {
//                return new OfferFragment();
//            } else if (position==R.id.rb_tab_order){
//                return new OrderFragment();
//            }else if (position==R.id.rb_tab_service){
//                return new ServiceFragment();
//            }else {
//                return new PersonalFragment();
//            }
//        }
//        @Override
//        public int getCount() {
//            return 5;
//        }
//    };
//    //这儿是3个大的 fragment
//    @OnCheckedChanged({R.id.rb_tab_inquiry, R.id.rb_tab_offer, R.id.rb_tab_order,R.id.rb_tab_service,R.id.rb_tab_my})
//    public void onChecked(RadioButton rb) {
//        Boolean isChecked = rb.isChecked();
//        //检查是否选中
//        if (isChecked) {
//            int viewId = rb.getId();
//            //instantiateItem从FragmentManager中查找Fragment，找不到就getItem新建一个
//            Fragment fragment =
//                    (Fragment) mFragmentPagerAdapter.instantiateItem(fragmentContainer, viewId);
//            mFragmentPagerAdapter.setPrimaryItem(fragmentContainer, viewId, fragment);
//            mFragmentPagerAdapter.finishUpdate(fragmentContainer);
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        ButterKnife.inject(this);
//        rb_tab_inquiry=findViewByid(R.id.rb_tab_inquiry);
        //初始化视图
        initView();
//        rb_tab_inquiry= (RadioButton) findViewById(R.id.rb_tab_inquiry);

//        inquiryFragment=new InquiryFragment();
//        addFragment(inquiryFragment);
        select();

    }



    private void initView() {
        rb_tab_inquiry= (RadioButton) findViewById(R.id.rb_tab_inquiry);
        rb_tab_offer= (RadioButton) findViewById(R.id.rb_tab_offer);
        rb_tab_order= (RadioButton) findViewById(R.id.rb_tab_order);
        rb_tab_service= (RadioButton) findViewById(R.id.rb_tab_service);
        rb_tab_my= (RadioButton) findViewById(R.id.rb_tab_my);

        rb_tab_inquiry.setOnClickListener(this);
        rb_tab_offer.setOnClickListener(this);
        rb_tab_order.setOnClickListener(this);
        rb_tab_service.setOnClickListener(this);
        rb_tab_my.setOnClickListener(this);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        ButterKnife.reset(this);
    }

    //判断当前页面是否隐藏
    public void showFragment(Fragment fragment) {
        // TODO Auto-generated method stub
        transaction=getSupportFragmentManager().beginTransaction();
        if(inquiryFragment!=null){
            transaction.hide(inquiryFragment);
        }
        if(offerFragment!=null){
            transaction.hide(offerFragment);
        }

        if(serviceFragment!=null){
            transaction.hide(serviceFragment);
        }
        if(personalFragment!=null){
            transaction.hide(personalFragment);
        }
        transaction.show(fragment);
        transaction.commitAllowingStateLoss();
    }

    // 添加Fragment
    public void addFragment(Fragment fragment) {
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container, fragment);
        transaction.commit();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //询盘
            case R.id.rb_tab_inquiry:
                index=0;
                select();
                break;
            //报盘
            case R.id.rb_tab_offer:
                index=1;
              select();
                break;
            //订单
            case R.id.rb_tab_order:
                index=2;
                select();

                break;
            //服务
            case R.id.rb_tab_service:
                index=3;
                select();

                break;
            //我的
            case R.id.rb_tab_my:
                index=4;
                select();

                break;
        }
    }
    //选择切换哪个界面
    private void select() {
        if (index==0){
            //判断当前页面是否隐藏 ,隐藏给以显示
            if(inquiryFragment==null){
                inquiryFragment=new InquiryFragment();
                addFragment(inquiryFragment);
                showFragment(inquiryFragment);
            }else{
                if(inquiryFragment.isHidden()){
                    showFragment(inquiryFragment);
                }
            }
            rb_tab_inquiry.setChecked(true);
        }else if (index==1){
            if(offerFragment==null){
                offerFragment=new OfferFragment();
                addFragment(offerFragment);
                showFragment(offerFragment);
            }else{
                if(offerFragment.isHidden()){
                    showFragment(offerFragment);
                }
            }
            rb_tab_offer.setChecked(true);
        }else if (index==2){

        }else if (index==3){
            if(serviceFragment==null){
                serviceFragment=new ServiceFragment();
                addFragment(serviceFragment);
                showFragment(serviceFragment);
            }else{
                if(serviceFragment.isHidden()){
                    showFragment(serviceFragment);
                }
            }
            rb_tab_service.setChecked(true);
        }else if (index==4){
            if(personalFragment==null){
                personalFragment=new PersonalFragment();
                addFragment(personalFragment);
                showFragment(personalFragment);
            }else{
                if(personalFragment.isHidden()){
                    showFragment(personalFragment);
                }
            }
            rb_tab_my.setChecked(true);
        }
    }

    @Override
    public void showOffer() {
        index=1;
        select();
    }
}
