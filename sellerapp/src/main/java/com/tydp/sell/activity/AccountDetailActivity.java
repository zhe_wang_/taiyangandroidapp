package com.tydp.sell.activity;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.tydp.sell.adapter.WithdrawAdapter;
import com.tydp.sell.utils.DensityUtil;
import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class AccountDetailActivity extends BaseActivity implements View.OnClickListener{
    private TextView tv_right;
    private View view;
    private TextView tv_all,tv_order,tv_withdraw;
    private PopupWindow popupWindow;
    private RecyclerView mRecyclerView;
    private WithdrawAdapter mAdapter;
    private List<String> mDatas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_account_detail);
        tv_title.setText("账户明细");
        mDatas=new ArrayList<>();
        initView();
    }

    private void initView() {
        String rigth=getIntent().getStringExtra("right");
        tv_right = (TextView) findViewById(R.id.tv_right);
        tv_right.setVisibility(View.VISIBLE);
        tv_right.setText(rigth);

        mRecyclerView = (RecyclerView) findViewById(R.id.mrecyclerview);
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mAdapter=new WithdrawAdapter(this,mDatas);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager= (LinearLayoutManager) recyclerView.getLayoutManager();
                int totalItemCount=layoutManager.getItemCount();
                int lastVisibleItem=layoutManager.findLastVisibleItemPosition();
//                L.e("totalItemCount:"+totalItemCount);
//                L.e("lastVisibleItem:"+lastVisibleItem);
            }
        });

        tv_right.setOnClickListener(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_account_detail;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }
    public void showPopupWindow(View parent) {
        //加载布局
        view = (LinearLayout) LayoutInflater.from(AccountDetailActivity.this).inflate(
                R.layout.dialog, null);
        //找到布局的控件
//        private TextView tv_all,tv_order,tv_withdraw;
        tv_all=(TextView) view.findViewById(R.id.tv_all);
        tv_order=(TextView) view.findViewById(R.id.tv_order);
        tv_withdraw=(TextView) view.findViewById(R.id.tv_withdraw);
        tv_all.setOnClickListener(this);
        tv_order.setOnClickListener(this);
        tv_withdraw.setOnClickListener(this);
        // 实例化popupWindow
        popupWindow = new PopupWindow(view, DensityUtil.dip2px(this,106) , DensityUtil.dip2px(this,131));
        //控制键盘是否可以获得焦点
        popupWindow.setFocusable(true);
        //设置popupWindow弹出窗体的背景
//        popupWindow.setBackgroundDrawable(new BitmapDrawable(null, ""));
//        popupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.persin_icon_bg));
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        WindowManager manager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        @SuppressWarnings("deprecation")
        //获取xoff
                int xpos = manager.getDefaultDisplay().getWidth() / 2 - popupWindow.getWidth() / 2;
        //xoff,yoff基于anchor的左下角进行偏移。
        popupWindow.showAsDropDown(parent, xpos, 0);
    }

    @Override
    public void onClick(View v) {
//        private TextView tv_all,tv_order,tv_withdraw;
        switch (v.getId()){
            case R.id.tv_right:{
                showPopupWindow(tv_right);
            }
            break;
            case R.id.tv_all:{
                popupWindow.dismiss();
                tv_right.setText("全部");
            }
            break;
            case R.id.tv_order:{
                popupWindow.dismiss();
                tv_right.setText("订单");
            }
            break;
            case R.id.tv_withdraw:{
                popupWindow.dismiss();
                tv_right.setText("提现");
            }
            break;
        }
    }
}
