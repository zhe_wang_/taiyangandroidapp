package com.tydp.sell.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tydp.sell.view.R;

public class SelectTypeActivity extends AppCompatActivity implements View.OnClickListener{
    private LinearLayout title_layout;
    private ImageView iv_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_type);
        initView();
    }

    private void initView() {
        title_layout = (LinearLayout) findViewById(R.id.title_layout);
        title_layout.setVisibility(View.INVISIBLE);
        iv_back = (ImageView) findViewById(R.id.iv_back);

        iv_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:{
                finish();
            }
            break;
        }
    }
}
