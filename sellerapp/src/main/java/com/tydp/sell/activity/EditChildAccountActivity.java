package com.tydp.sell.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.tydp.sell.utils.L;
import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;
import com.tydp.sell.custom.MyDialog;

import java.util.ArrayList;
import java.util.List;

public class EditChildAccountActivity extends BaseActivity implements View.OnClickListener {
    private RelativeLayout authority_layout;
    private MyDialog myDialog;
    private View view;
    private ImageView iv_back;
    private RelativeLayout ll_content;
    private Bitmap bitmap;
    private int margin;
    private RelativeLayout contentParent_layout;

    private MyAdapter mAdapter;
    private List<String> mDatas;
    private GridView gridView;
    private Button bt_confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_edit_child_account);
        tv_title.setText("编辑子账号");
        initView();
    }

    private void initView() {
        mDatas = new ArrayList<>();
        mDatas.add("添加报盘");
        mDatas.add("审核订单");
        mDatas.add("添加报盘");
        mDatas.add("添加报盘");
        mDatas.add("审核订单");
        mDatas.add("添加报盘");
        mDatas.add("添加报盘");
        mDatas.add("审核订单");
        mDatas.add("添加报盘");
        initDialog();

    }

    private void initDialog() {
        bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.service_icon_close);
        authority_layout = (RelativeLayout) findViewById(R.id.authority_layout);
        view = LayoutInflater.from(this).inflate(R.layout.dialog_authority, null);
        iv_back = (ImageView) view.findViewById(R.id.iv_back);
        ll_content = (RelativeLayout) view.findViewById(R.id.ll_content);
        contentParent_layout = (RelativeLayout) view.findViewById(R.id.contentParent_layout);
        gridView = (GridView) view.findViewById(R.id.gridView);
        bt_confirm = (Button) view.findViewById(R.id.bt_confirm);
        authority_layout.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        bt_confirm.setOnClickListener(this);

        mAdapter = new MyAdapter(mDatas);
        gridView.setAdapter(mAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mAdapter.setPressPositon(position);
                mAdapter.notifyDataSetChanged();
            }
        });
        margin = bitmap.getHeight() / 2;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) ll_content.getLayoutParams();
        params.setMargins(margin, margin, margin, margin);
        ll_content.setLayoutParams(params);
        myDialog = new MyDialog(this, view, R.style.dialog);
        myDialog.setCanceledOnTouchOutside(false);// 设置点击屏幕Dialog不消失
    }


    @Override
    public int getLayout() {
        return R.layout.activity_edit_child_account;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.authority_layout: {
                myDialog.show();
            }
            break;
            case R.id.iv_back: {
                mAdapter.clearChecked();
                myDialog.dismiss();
            }
            break;
            case R.id.bt_confirm: {
                boolean[] flag = mAdapter.getChecked();
                for (int i = 0; i < flag.length; i++) {
                    if (flag[i]) {
                        L.e("选中了第" + i + "个按钮");
                    }
                }
                myDialog.dismiss();
            }
            break;
        }
    }

    //选择权限的 adapter
    class MyAdapter extends BaseAdapter {
        private List<String> mDatas;
        private int index = -1;
        private boolean[] flag;

        public MyAdapter(List<String> mDatas) {
            this.mDatas = mDatas;
            flag = new boolean[mDatas.size()];
        }

        @Override
        public int getCount() {
            return mDatas.size();
        }

        @Override
        public Object getItem(int position) {
            return mDatas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(EditChildAccountActivity.this).inflate(R.layout.item_authority, null);
                viewHolder.bt_authority = (Button) convertView.findViewById(R.id.bt_authority);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            if (flag[position]) {
                viewHolder.bt_authority.setBackgroundResource(R.drawable.my_bt_normal);
                viewHolder.bt_authority.setTextColor(getResources().getColor(R.color.white));
            } else {
                viewHolder.bt_authority.setBackgroundResource(R.drawable.my_bt_register_normal);
                viewHolder.bt_authority.setTextColor(getResources().getColor(R.color.common_bluecolor));
            }
            viewHolder.bt_authority.setText(mDatas.get(position));

            return convertView;
        }

        public void setPressPositon(int index) {
            this.index = index;
            flag[this.index] = flag[this.index] == true ? false : true;
        }

        //获取选中的
        public boolean[] getChecked() {
            return flag;
        }
        public void clearChecked(){
            for (int i=0;i<flag.length;i++){
                flag[i]=false;
            }
        }
    }

    class ViewHolder {
        Button bt_authority;
    }
}
