package com.tydp.sell.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tydp.sell.adapter.ShipperManageAdapter;
import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 发货人管理 activity
 */
public class ShipperManageActivity extends BaseActivity implements View.OnClickListener{
    private RecyclerView mRecyclerView;
    private ShipperManageAdapter mAdapter;
    private RelativeLayout add_layout;
    private TextView tv_add;
    private List<String> mDatas;

    private boolean flag;
    private String my;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_shipper_manage);
        tv_title.setText("发货人信息");
        initView();
    }

    private void initView() {
        mDatas=new ArrayList<>();
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        my = getIntent().getStringExtra("my");
        if (my != null) {
            flag = true;
        }
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        add_layout = (RelativeLayout) findViewById(R.id.add_layout);
        tv_add = (TextView) findViewById(R.id.tv_add);

        mAdapter=new ShipperManageAdapter(this,mDatas);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter.setFlag(flag);

        add_layout.setOnClickListener(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_shipper_manage;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.add_layout:{
                Intent intent=new Intent(ShipperManageActivity.this,EditShipperActivity.class);
                intent.putExtra("delete","delete");
                startActivity(intent);
            }
            break;
        }
    }
}
