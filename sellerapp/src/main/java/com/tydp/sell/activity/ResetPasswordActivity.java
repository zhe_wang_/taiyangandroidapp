package com.tydp.sell.activity;

import android.os.Bundle;

import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

public class ResetPasswordActivity extends BaseActivity {
    @Override
    public int getLayout() {
        return R.layout.activity_reset_password;
    }

//    @InjectView(R.id.et_code)
//    EditText etCode;
//    private String code;
//    @InjectView(R.id.et_password)
//    EditText etPassword;
//    private String password;
//
//    @InjectView(R.id.et_verfify)
//    EditText etVerfify;
//
//    @InjectView(R.id.et_phone_bg)
//    View phoneBg;
//    @InjectView(R.id.et_code_bg)
//    View codeBg;
//    @InjectView(R.id.et_password_bg)
//    View passwordBg;
//    @InjectView(R.id.et_verfify_bg)
//    View verfifyBg;
//    @InjectView(R.id.et_phone)
//    EditText etPhone;
//    private String phone;
//
//    private TimeCount time;
//    @InjectView(R.id.bt_code)
//    Button codeButton;
//
//    @OnClick(R.id.bt_code)
//    public void getCode(View v) {
//        phone = etPhone.getText().toString();
//        if (RegularUtils.isMobileExact(phone)) {
////            if (phone!=null){
//            time = new TimeCount(60000, 1000);
//            time.start();
//            Map<String, Object> params = new HashMap<>();
//            params.put("model", "user");
//            params.put("action", "send_mobile_code");
//            params.put("mobile", phone);
//            params.put("mobile_sign", MD5Utils.encode(MD5Utils.getMobileSign(phone)));
//            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "send_mobile_code")));
//            L.e(JSON.toJSONString(params));
//            HttpUtils.sendPost(params, this, 100);
//            //{"sign":"fe7f9534fe12ae60f5f6c94f01193056","action":"send_mobile_code","model":"user","mobile_sign":"6bcabdd95d3fc4baa2d22f1114f98a5c","mobile":"18500093343"}
//        } else {
//            T.showShort(this, "请输入正确的电话号码");
////            ToastUtil.showToast("请输入正确的电话号码");
////            {"error":"503","message":"\u53d1\u9001\u9a8c\u8bc1\u5931\u8d25","content":""}
////            {"error":"503","message":"\u53d1\u9001\u9a8c\u8bc1\u5931\u8d25","content":""}
////            {"error":"0","message":"\u9a8c\u8bc1\u7801\u53d1\u9001\u6210\u529f","content":""}
//        }
//
//    }
//
//    //重置完成
//    @InjectView(R.id.bt_complete)
//    Button bt_login;
//
//    @OnClick(R.id.bt_complete)
//    public void complete(View v) {
//        code = etCode.getText().toString().trim();
//        password = etPassword.getText().toString().trim();
//        if (!TextUtils.isEmpty(phone) && !TextUtils.isEmpty(code) && !TextUtils.isEmpty(password)) {
//            Map<String, Object> params = new HashMap<>();
//            params.put("model", "user");
//            params.put("action", "forget_password");
//            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "forget_password")));
//            params.put("mobile", phone);
//            params.put("password", password);
//            params.put("code", code);
//            HttpUtils.sendPost(params, this, 101);
//        }
//    }
//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_reset_password);
        tv_title.setText("重置密码");
//        initListener();
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }
//
//    @Override
//    protected int getLayout() {
//        return R.layout.activity_reset_password;
//    }
//
//    @Override
//    public void success(String response, int id) {
//        switch (id) {
//            case 101:
//                T.showShort(this, "重置密码成功,请登录");
//                finish();
//                break;
//        }
//
//    }
//
//    @Override
//    public void failByOther(String fail, int id) {
//        T.showShort(this, fail);
//    }
//
//    @Override
//    public void fail(String fail, int id) {
//        T.showShort(this, R.string.jianchawangluo);
//    }
//
//    private void initListener() {
//        etPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                changeBg(phoneBg, hasFocus);
//            }
//        });
//        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                changeBg(passwordBg, hasFocus);
//            }
//        });
//        etCode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                changeBg(codeBg, hasFocus);
//            }
//        });
//        etVerfify.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                changeBg(verfifyBg, hasFocus);
//            }
//        });
//    }
//
//    private void changeBg(View v, boolean hasFocus) {
//        if (hasFocus) {
//            v.setBackgroundColor(getResources().getColor(R.color.bg_bluecolor));
//        } else {
//            v.setBackgroundColor(getResources().getColor(R.color.bg_color));
//        }
//    }
//
//    class TimeCount extends CountDownTimer {
//        public TimeCount(long millisInFuture, long countDownInterval) {
//            super(millisInFuture, countDownInterval);
//        }
//
//        @Override
//        public void onFinish() {// 计时完毕
//            codeButton.setText("获取验证码");
//            codeButton.setClickable(true);
//        }
//
//        @Override
//        public void onTick(long millisUntilFinished) {// 计时过程
//            codeButton.setClickable(false);//防止重复点击
//            codeButton.setText(millisUntilFinished / 1000 + "s");
//        }
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        if (time != null) {
//            time.cancel();
//        }
//    }
}
