package com.tydp.sell.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

/**
 * 我的钱包activity
 */
public class WalletActivity extends BaseActivity implements View.OnClickListener{
   private Button bt_withdraw;
    private RelativeLayout balance_layout;
    private RelativeLayout withdraw_layout;
    private RelativeLayout withdrawcount_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_wallet);
        tv_title.setText("我的钱包");
        initView();
    }

    private void initView() {
        bt_withdraw = (Button) findViewById(R.id.bt_withdraw);
        balance_layout = (RelativeLayout) findViewById(R.id.balance_layout);
        withdraw_layout = (RelativeLayout) findViewById(R.id.withdraw_layout);
        withdrawcount_layout = (RelativeLayout) findViewById(R.id.withdrawcount_layout);


        bt_withdraw.setOnClickListener(this);
        balance_layout.setOnClickListener(this);
        withdraw_layout.setOnClickListener(this);
        withdrawcount_layout.setOnClickListener(this);
    }


    @Override
    public int getLayout() {
        return R.layout.activity_wallet;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //提现
            case R.id.bt_withdraw:{
                Intent inten=new Intent(WalletActivity.this,WithdrawActivity.class);
                startActivity(inten);
            }
            break;
            //结算中
            case R.id.balance_layout:{
                Intent inten=new Intent(WalletActivity.this,BalanceActivity.class);
                startActivity(inten);
            }
            break;
            //可提现
            case R.id.withdraw_layout: {
                Intent intent=new Intent(WalletActivity.this,AccountDetailActivity.class);
                intent.putExtra("right","全部");
                startActivity(intent);
            }
            break;
            //累计提现
            case R.id.withdrawcount_layout:{
                Intent intent=new Intent(WalletActivity.this,AccountDetailActivity.class);
                intent.putExtra("right","提现");
                startActivity(intent);
            }
            break;
        }
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }
}
