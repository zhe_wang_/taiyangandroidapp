package com.tydp.sell.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;
import com.tydp.sell.custom.MyListView;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.utils.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * 邀请好友activity
 */
public class InviteFriendsActivity extends BaseActivity implements View.OnClickListener {
    private MyListView fiends_listview;
    private List<String> mDatas;
    private MyAdapter mAdatper;
    private Button bt_invite;

    private TextView tv_email;
    private TextView tv_wechat;
    private TextView tv_friend;
    private TextView tv_message;


    //分享相关
    private SHARE_MEDIA share_media;
    private ShareAction shareAction;
    private PopupWindow window;
    private UMShareListener umShareListener = new UMShareListener() {
        @Override
        public void onResult(SHARE_MEDIA platform) {
            Log.d("plat", "platform" + platform);
            if (platform.name().equals("WEIXIN_FAVORITE")) {
                Toast.makeText(InviteFriendsActivity.this, platform + " 收藏成功啦", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(InviteFriendsActivity.this, platform + " 分享成功啦", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            Toast.makeText(InviteFriendsActivity.this, platform + " 分享失败啦", Toast.LENGTH_SHORT).show();
            if (t != null) {
                Log.d("throw", "throw:" + t.getMessage());
            }
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
            Toast.makeText(InviteFriendsActivity.this, platform + " 分享取消了", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_invite_friends);
        tv_title.setText("邀请好友");
        shareAction = new ShareAction(InviteFriendsActivity.this);
        initView();
    }

    private void initView() {
        fiends_listview = (MyListView) findViewById(R.id.fiends_listview);
        bt_invite = (Button) findViewById(R.id.bt_invite);

        mDatas = new ArrayList<>();
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mAdatper = new MyAdapter(mDatas);
        fiends_listview.setAdapter(mAdatper);
        bt_invite.setOnClickListener(this);
    }

    private void initPopwindow() {

        // 利用layoutInflater获得View
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popwindow_share, null);

        tv_email = (TextView) view.findViewById(R.id.tv_email);
        tv_wechat = (TextView) view.findViewById(R.id.tv_wechat);
        tv_friend = (TextView) view.findViewById(R.id.tv_friend);
        tv_message = (TextView) view.findViewById(R.id.tv_message);

        tv_email.setOnClickListener(this);
        tv_wechat.setOnClickListener(this);
        tv_friend.setOnClickListener(this);
        tv_message.setOnClickListener(this);
        // 下面是两种方法得到宽度和高度 getWindow().getDecorView().getWidth()

        window = new PopupWindow(view,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.alpha = 0.5f;

        getWindow().setAttributes(params);
        // 设置popWindow弹出窗体可点击，这句话必须添加，并且是true
        window.setFocusable(true);


        // 必须要给调用这个方法，否则点击popWindow以外部分，popWindow不会消失
        // window.setBackgroundDrawable(new BitmapDrawable());

        window.setBackgroundDrawable(new BitmapDrawable());
        // 在参照的View控件下方显示
        // window.showAsDropDown(MainActivity.this.findViewById(R.id.start));

        // 设置popWindow的显示和消失动画
        window.setAnimationStyle(R.style.mypopwindow_anim_style);
        // 在底部显示
        window.showAtLocation(InviteFriendsActivity.this.findViewById(R.id.bt_invite),
                Gravity.BOTTOM, 0, 0);

        // 这里检验popWindow里的button是否可以点击
        TextView first = (TextView) view.findViewById(R.id.first);
        first.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                window.dismiss();
            }
        });

        // popWindow消失监听方法
        window.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                System.out.println("popWindow消失");
                WindowManager.LayoutParams params = getWindow().getAttributes();
                params.alpha = 1f;
                getWindow().setAttributes(params);
            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.activity_invite_friends;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_invite: {
                initPopwindow();
            }
            break;
            case R.id.tv_email: {
                window.dismiss();
                share_media = SHARE_MEDIA.EMAIL;
                shareAction.setPlatform(share_media).setCallback(umShareListener).share();
            }
            break;
            case R.id.tv_wechat: {
                window.dismiss();
                share_media = SHARE_MEDIA.QQ;
                shareAction.setPlatform(share_media).setCallback(umShareListener).share();
            }
            break;
            case R.id.tv_friend: {

            }
            break;
            case R.id.tv_message: {
                window.dismiss();
                share_media = SHARE_MEDIA.SMS;
                shareAction.setPlatform(share_media).setCallback(umShareListener).share();
            }
            break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /** attention to this below ,must add this**/
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
        Log.d("result", "onActivityResult");
    }

    class MyAdapter extends BaseAdapter {
        private List<String> mDatas;

        public MyAdapter(List<String> mDatas) {
            this.mDatas = mDatas;
        }

        @Override
        public int getCount() {
            return mDatas.size();
        }

        @Override
        public Object getItem(int position) {
            return mDatas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (viewHolder == null) {
                viewHolder = new ViewHolder();
                convertView = getLayoutInflater().inflate(R.layout.item_friends_bargain, null);
                viewHolder.tv_friendname = (TextView) convertView.findViewById(R.id.tv_friendname);
                viewHolder.tv_date = (TextView) convertView.findViewById(R.id.tv_date);
                viewHolder.tv_count = (TextView) convertView.findViewById(R.id.tv_count);
            }
            return convertView;
        }

        class ViewHolder {
            TextView tv_friendname;
            TextView tv_date;
            TextView tv_count;
        }
    }

}
