package com.tydp.sell.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.tydp.sell.adapter.WithdrawAdapter;
import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 结算中界面
 */
public class BalanceActivity extends BaseActivity {
    private LinearLayout ll_count;
    private RecyclerView mRecyclerView;
    private WithdrawAdapter mAdapter;
    private List<String> mDatas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_balance);
        tv_title.setText("结算中");
        initView();


    }

    private void initView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        mDatas=new ArrayList<>();
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mAdapter=new WithdrawAdapter(this,mDatas);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_balance;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }
}
