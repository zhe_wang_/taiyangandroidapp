package com.tydp.sell.activity;

import android.os.Bundle;

import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

/**
 * 选择银行卡activity
 */
public class SelectBankActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_select_bank);
        tv_title.setText("选择银行卡");
    }

    @Override
    public int getLayout() {
        return R.layout.activity_select_bank;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }
}
