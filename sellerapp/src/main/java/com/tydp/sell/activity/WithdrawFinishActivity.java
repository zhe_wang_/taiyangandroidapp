package com.tydp.sell.activity;

import android.os.Bundle;

import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

/**
 * 提现详情
 */
public class WithdrawFinishActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_withdraw_finish);
        tv_title.setText("提现详情");
    }

    @Override
    public int getLayout() {
        return R.layout.activity_withdraw_finish;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }
}
