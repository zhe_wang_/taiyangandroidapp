package com.tydp.sell.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.tydp.sell.adapter.InquiryAdapter;
import com.tydp.sell.entity.ItemModel;
import com.tydp.sell.utils.Constants;
import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;
import com.tydp.sell.base.ListBaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的询盘activity
 */
public class InquiryActivity extends BaseActivity {
    /**
     * 服务器端一共多少条数据
     */
    private static final int TOTAL_COUNTER = 64;

    /**
     * 每一页展示多少条数据
     */
    private static final int REQUEST_COUNT = 10;

    /**
     * 已经获取到多少条数据了
     */
    private static int mCurrentCounter = 0;
    private View view;
    /*private LRecyclerView mRecyclerView;
    private LRecyclerViewAdapter mLRecyclerViewAdapter;
    private DataAdapter mDataAdapter;*/
    private RecyclerView mRecyclerView;
    private InquiryAdapter mAdapter;
    private List<String> mDatas;
    private LinearLayoutManager layoutManager;
    private int mCurrentPage = 1;
    private int mTotalPage = 1;
    private int lastVisibleItemPosition;

    private Handler mHandler = new Handler();

    private Button bt_add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_inquiry);
        tv_title.setText("我的询盘");
        initView();
    }

    private void initView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        bt_add = (Button) findViewById(R.id.bt_add);

        bt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InquiryActivity.this, PublishActivity.class);
                startActivity(intent);
            }
        });
        mDatas = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            mDatas.add("测试");
        }
        mAdapter = new InquiryAdapter(this, mDatas);
        mRecyclerView.setAdapter(mAdapter);
        layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        //RecyclerView设置监听事件
        initRecyclerViewListener();
    }

    private void initRecyclerViewListener() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mAdapter.getItemCount()) {
//                    Log.e("ceshi", "当前页:" + mCurrentPage);
                    if (mTotalPage == 1) {
                        Log.e("ceshi", "只有一页");
                        mAdapter.setLoadingStatues(Constants.LOAD_STATUES_NO_MORE_DATA);
                    } else if (mCurrentPage <= mTotalPage) {
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                requestData();
                            }
                        }, 2000);
                    } else {
                        mAdapter.setLoadingStatues(Constants.LOAD_STATUES_NO_MORE_DATA);
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                //此处的mCurrentPage 和mTotalPage根据实际情况进行判断
                if (mTotalPage == 1) {
                    mAdapter.setLoadingStatues(Constants.LOAD_STATUES_NO_MORE_DATA);
                }
            }

        });
    }

    //请求数据
    private void requestData() {
        for (int i = 0; i < 5; i++) {
            mDatas.add("测试" + i);
        }
        mCurrentPage++;
//        mSwipeRefreshLayout.setRefreshing(false);
        //禁止下拉刷新
//        mSwipeRefreshLayout.setEnabled(false);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_inquiry;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

    private class DataAdapter extends ListBaseAdapter<ItemModel> {

        private LayoutInflater mLayoutInflater;

        public DataAdapter(Context context) {
            mLayoutInflater = LayoutInflater.from(context);
            mContext = context;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(mLayoutInflater.inflate(R.layout.item_inqury, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//            ItemModel item = mDataList.get(position);
//
//            ViewHolder viewHolder = (ViewHolder) holder;
//            viewHolder.textView.setText(item.title);
        }


        private class ViewHolder extends RecyclerView.ViewHolder {

            private TextView textView;

            public ViewHolder(View itemView) {
                super(itemView);
//                textView = (TextView) itemView.findViewById(R.id.info_text);
            }
        }
    }
}
