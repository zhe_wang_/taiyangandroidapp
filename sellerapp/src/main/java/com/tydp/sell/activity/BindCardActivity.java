package com.tydp.sell.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

/**
 * 绑定银行卡界面
 */
public class BindCardActivity extends BaseActivity implements View.OnClickListener {
    private Button bt_bind_next;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_bind_card);
        tv_title.setText("绑定银行卡");
        initView();
    }

    private void initView() {
        bt_bind_next = (Button) findViewById(R.id.bt_bind_next);

        bt_bind_next.setOnClickListener(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_bind_card;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_bind_next:{

            }
            break;
        }
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }
}
