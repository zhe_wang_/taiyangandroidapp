package com.tydp.sell.activity;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.tydp.sell.adapter.DeclarationAdapter;
import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 代理报关、仓储服务 Activity
 */
public class DeclarationActivity extends BaseActivity implements View.OnClickListener {
    private RecyclerView mRecyclerView;
    private DeclarationAdapter mAdapter;
    private List<String> mDatas;
    private LinearLayout ll_location;
    private View view;
    private PopupWindow popupWindow;
    private ListView left_listView;
    private List<String> leftDatas;
    private LeftAdapter leftAdapter;
    //    private CustomPopwindow customPopwindow;
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_declaration);
        tv_title.setText("代理报关");
        mActivity = this;
        initView();
    }

    private void initView() {
        mDatas = new ArrayList<>();
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        mAdapter = new DeclarationAdapter(this, mDatas);
        mRecyclerView.setAdapter(mAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        ll_location = (LinearLayout) findViewById(R.id.ll_location);
        ll_location.setOnClickListener(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_declaration;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_location: {
                showPopupWindow(ll_location);
//                customPopwindow = new CustomPopwindow(mActivity,itemClickListener);
//                L.e("测试是否为空:"+(customPopwindow==null));
////                customPopwindow.showAtLocation(view,
////                        Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0);
//                customPopwindow.showAsDropDown(ll_location, 0, 0);
            }
            break;
        }
    }

    public void showPopupWindow(View parent) {
        leftDatas = new ArrayList<>();
        leftDatas.add("中国");
        leftDatas.add("天津");
        leftDatas.add("香港");
        leftDatas.add("大连");
        leftDatas.add("北京");
        //加载布局
        view = (LinearLayout) LayoutInflater.from(this).inflate(
                R.layout.popwindow_location_select, null);
        //找到布局的控件

        left_listView = (ListView) view.findViewById(R.id.left_listView);
        leftAdapter = new LeftAdapter(leftDatas);
        left_listView.setAdapter(leftAdapter);
        left_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    popupWindow.dismiss();
            }
        });


        popupWindow = new PopupWindow(view,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        //点击其它部分 popupWindow消失
        view.findViewById(R.id.popwin_supplier_list_bottom)
                .setOnClickListener(new View.OnClickListener() {
                    public void onClick(View arg0) {
                        popupWindow.dismiss();
                    }
                });
        // 设置动画效果
        popupWindow.setAnimationStyle(R.style.popwin_anim_style);
        // 实例化popupWindow
//        popupWindow = new PopupWindow(view, ScreenUtils.getScreenWidth(DeclarationActivity.this), DensityUtil.dip2px(DeclarationActivity.this,214));
//        popupWindow = new PopupWindow(view,
//                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //控制键盘是否可以获得焦点
        popupWindow.setFocusable(true);
        //设置popupWindow弹出窗体的背景
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //xoff,yoff基于anchor的左下角进行偏移。
        popupWindow.showAsDropDown(parent, 0, 2);
    }

    class LeftAdapter extends BaseAdapter {
        private List<String> leftDatas;

        public LeftAdapter(List<String> leftDatas) {
            this.leftDatas = leftDatas;
        }

        @Override
        public int getCount() {
            return leftDatas.size();
        }

        @Override
        public Object getItem(int position) {
            return leftDatas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LeftViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new LeftViewHolder();
                convertView = LayoutInflater.from(DeclarationActivity.this).inflate(R.layout.item_location_select, null);
                viewHolder.view_bg = convertView.findViewById(R.id.view_bg);
                viewHolder.tv_location = (TextView) convertView.findViewById(R.id.tv_location);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (LeftViewHolder) convertView.getTag();
            }
            viewHolder.tv_location.setText(leftDatas.get(position));
            return convertView;
        }
    }

    class LeftViewHolder {
        View view_bg;
        TextView tv_location;
    }

}
