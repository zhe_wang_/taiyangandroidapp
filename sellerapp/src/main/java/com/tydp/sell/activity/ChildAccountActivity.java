package com.tydp.sell.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.tydp.sell.adapter.ChildAccountAdapter;
import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 子账号管理activity
 */

public class ChildAccountActivity extends BaseActivity {
    private ChildAccountAdapter mAdapter;
    private List<String> mDatas;
    private RecyclerView recyclerview;
    private RelativeLayout add_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_child_account);
        tv_title.setText("子账号管理");
        initView();
    }

    private void initView() {
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        mDatas=new ArrayList<>();
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mDatas.add("1");
        mAdapter=new ChildAccountAdapter(this,mDatas);
        recyclerview.setAdapter(mAdapter);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        add_layout = (RelativeLayout) findViewById(R.id.add_layout);
        add_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ChildAccountActivity.this,EditChildAccountActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.activity_child_account;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }
}
