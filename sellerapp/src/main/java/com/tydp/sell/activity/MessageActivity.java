package com.tydp.sell.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;
import com.tydp.sell.fragment.MessageFragment;

import java.util.ArrayList;
import java.util.List;

public class MessageActivity extends BaseActivity {
    private TabLayout tabs;
    private ViewPager vp_view;
    private List<Fragment> mFragmentList;
    private ViewAdapter mAdapter;
    private List<String> mTitleList;
    private Button bt_commit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_message);
        tv_title.setText("会员留言");
        initView();
    }

    private void initView() {
        tabs = (TabLayout) findViewById(R.id.tabs);
        vp_view = (ViewPager) findViewById(R.id.vp_view);
        bt_commit = (Button) findViewById(R.id.bt_commit);

        bt_commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MessageActivity.this,CommitMessageActivity.class);
                startActivity(intent);
            }
        });

        mTitleList = new ArrayList<>();
        mTitleList.add("全部");
        mTitleList.add("留言");
        mTitleList.add("投诉");
        mTitleList.add("售后");
        mFragmentList = new ArrayList<>();
        for (int i=0;i<mTitleList.size();i++){
            MessageFragment messageFragment= new MessageFragment();
            Bundle data=new Bundle();
            data.putString("title",mTitleList.get(i));
            messageFragment.setArguments(data);
            mFragmentList.add(messageFragment);
        }
        mAdapter = new ViewAdapter(getSupportFragmentManager());
        vp_view.setAdapter(mAdapter);
        tabs.setupWithViewPager(vp_view);//设置viewPaper和tabs关联
    }

    @Override
    public int getLayout() {
        return R.layout.activity_message;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }
    class ViewAdapter extends FragmentPagerAdapter {

        public ViewAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitleList.get(position);
        }
    }
}
