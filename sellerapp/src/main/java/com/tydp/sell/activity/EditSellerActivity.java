package com.tydp.sell.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.view.ContextThemeWrapper;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tydp.sell.entity.CityModel;
import com.tydp.sell.entity.ProvinceModel;
import com.tydp.sell.utils.ACache;
import com.tydp.sell.utils.Constants;
import com.tydp.sell.utils.HttpUtils;
import com.tydp.sell.utils.L;
import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * 编辑卖家界面
 */
public class EditSellerActivity extends BaseActivity implements View.OnClickListener {
    //省市二级联动
    private ArrayList<ProvinceModel> options1Items = new ArrayList<>();
    private ArrayList<ArrayList<CityModel>> options2Items = new ArrayList<>();
    OptionsPickerView pvOptions;
    private List<CityModel> mCityList;
    private ArrayList<CityModel> options2Items_child;

    private int firstOptions1;
    private int firstOption2;
    //已经存在省份市区
    private String hasProvinceId;
    private String hasCityId;
    private String provice;
    private String city;

    private String provinceId;
    private String cityId;


    private RelativeLayout head_layout;
    private RelativeLayout address_layout;

    private boolean flag;//判断是否修改头像

    private String fileName;
    private static String path = "/sdcard/myHead/";// sd路径
    private Bitmap head;
    private File temp;

    SimpleDraweeView personLogo;

    private ACache mCache;//主要用于缓存地址

    private TextView tv_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tv_title.setText("编辑资料");
        mCache = ACache.get(this);

//        userModel = new Gson().fromJson(mySPEdit.getUserInfo(), UserModel.class);
//        hasProvinceId = userModel.getProvince();
//        hasCityId = userModel.getCity();
        initView();
        initData();
    }

    private void initView() {
        head_layout = (RelativeLayout) findViewById(R.id.head_layout);
        address_layout = (RelativeLayout) findViewById(R.id.address_layout);
        personLogo = (SimpleDraweeView) findViewById(R.id.im_personlogo);
        tv_location = (TextView) findViewById(R.id.tv_location);

        head_layout.setOnClickListener(this);
        address_layout.setOnClickListener(this);
    }

    private boolean first = true;
    //初始化数据
    private void initData() {
        //选项选择器
        pvOptions = new OptionsPickerView(this);
        if (TextUtils.isEmpty(mCache.getAsString("location"))){
            HttpUtils.sendPost(this, 100, first);
        }else {
            String location=mCache.getAsString("location");
//            L.e(location.substring(location.length()-100));
            initLocation(location);
        }


    }
    @Override
    public int getLayout() {
        return R.layout.activity_edit_seller;
    }

    @Override
    public void success(String response, int id) {
        if (id == 100) {
            mCache.put("location", response, 6 * ACache.TIME_DAY);
//            mCache.put("location",response,30);
            initLocation(response);

        }
        if (id == 101) {
//            if (TextUtils.isEmpty(response)) {
//                dismissProgress(this);
//                Toast.makeText(this, "修改成功", Toast.LENGTH_SHORT).show();
//                mySPEdit.setFirstOptions1(firstOptions1);
//                mySPEdit.setFirstOption2(firstOption2);
//
//                mySPEdit.setIsAddress(true);
//                if(fileName!=null)
//                {
//                    DataCleanManager.deleteFilesByDirectory(new File(fileName));
//                }
//
//                myApplication.setPersonChange(true);
//                finish();
//            } else {
//                userModel = new Gson().fromJson(response, UserModel.class);
//                initView();
//            }
        }
    }

    private void initLocation(String locationStr) {
        Type listType = new TypeToken<LinkedList<ProvinceModel>>() {
        }.getType();
        Gson gson = new Gson();
        LinkedList<ProvinceModel> provinceListModel = gson.fromJson(locationStr, listType);
        for (Iterator iterator = provinceListModel.iterator(); iterator.hasNext(); ) {
            ProvinceModel provinceModel = (ProvinceModel) iterator.next();
//                System.out.println(user.getName());
//                System.out.println(user.getId());
//            L.e("ce::::"+(provinceListModel==null));
//            if (hasProvinceId.equals(provinceModel.getId())) {
//                provice = provinceModel.getName();
//                mySPEdit.setFirstOptions1(firstOptions1);
//            }
            options1Items.add(provinceModel);
            mCityList = provinceModel.getCity_list();
            options2Items_child = new ArrayList<>();
            for (int i = 0; i < mCityList.size(); i++) {
                options2Items_child.add(mCityList.get(i));
//                if (hasCityId.equals(mCityList.get(i).getId())) {
//                    city = mCityList.get(i).getName();
//                    firstOption2 = i;
//                    mySPEdit.setFirstOption2(firstOption2);
//                }
            }
            options2Items.add(options2Items_child);
            firstOptions1++;
        }
        if (provice != null && city != null) {
            tv_location.setText(provice + city);
        }
//        initOrigalData();
//            L.e("获取的数据为空:" + (provinceListModel == null));
        //选项1
//            options1Items.add(new ProvinceBean(0, "广东", "广东省，以岭南东道、广南东路得名", "其他数据"));
//            options1Items.add(new ProvinceBean(1, "湖南", "湖南省地处中国中部、长江中游，因大部分区域处于洞庭湖以南而得名湖南", "芒果TV"));
//            options1Items.add(new ProvinceBean(3, "广西", "嗯～～", ""));


        //三级联动效果
//        pvOptions.setPicker(options1Items, options2Items, options3Items,true);
        pvOptions.setPicker(options1Items, options2Items, true);
        //设置选择的三级单位
//        pwOptions.setLabels("省", "市", "区");
        pvOptions.setTitle("选择城市");
        pvOptions.setCyclic(false);
        //设置默认选中的三级项目
        //监听确定选择按钮
        pvOptions.setSelectOptions(mySPEdit.getFirstOptions1(), mySPEdit.getFirstOption2());
        pvOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {

            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
//                    L.e("options1:"+options1);
//                    L.e("option2:"+option2);
                firstOptions1 = options1;
                firstOption2 = option2;
                //返回的分别是三个级别的选中位置
                String tx = options1Items.get(options1).getPickerViewText()
                        + options2Items.get(options1).get(option2).getPickerViewText();
                provinceId = options1Items.get(options1).getId();
                cityId = options2Items.get(options1).get(option2).getId();
//                    tvLocation.setText(tx+"//"+provinceId+"//"+cityId);
                tv_location.setText(tx);
            }
        });
    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.head_layout: {
                openPictureSelectDialog();
            }
            break;
            case R.id.address_layout: {
                pvOptions.show();
            }
            break;
        }
    }

    /**
     * 打开对话框
     **/
    private void openPictureSelectDialog() {
        //自定义Context,添加主题
        Context dialogContext = new ContextThemeWrapper(this, android.R.style.Theme_Light);
        String[] choiceItems = new String[2];
        choiceItems[0] = "相机拍摄";  //拍照
        choiceItems[1] = "本地相册";  //从相册中选择
        ListAdapter adapter = new ArrayAdapter<String>(dialogContext, android.R.layout.simple_list_item_1, choiceItems);
        //对话框建立在刚才定义好的上下文上
        AlertDialog.Builder builder = new AlertDialog.Builder(dialogContext);
        builder.setTitle("选择图片");
        builder.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:  //相机
//                        doTakePhoto();
                        Intent intent2 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent2.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "head.jpg")));
                        startActivityForResult(intent2, 2);// 采用ForResult打开
                        break;
                    case 1:  //从图库相册中选取
//                        doPickPhotoFromGallery();
                        Intent intent1 = new Intent(Intent.ACTION_PICK, null);
                        intent1.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                        startActivityForResult(intent1, 1);
                        break;
                }
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    cropPhoto(data.getData());// 裁剪图片
//                    File temp = new File(Environment.getExternalStorageDirectory()
//                            + "/head.jpg");
//                    cropPhoto(Uri.fromFile(temp));// 裁剪图片
                }
                break;
            case 2:
                if (resultCode == RESULT_OK) {
                    temp = new File(Environment.getExternalStorageDirectory()
                            + "/sellhead.jpg");
//                     temp=new File(path+"/head.jpg");
                    cropPhoto(Uri.fromFile(temp));// 裁剪图片
                }

                break;
            case 3:
                if (data != null) {
                    Bundle extras = data.getExtras();
//                    Uri uri = (Uri) extras.getParcelable(MediaStore.EXTRA_OUTPUT);
//                    Log.e("ceshi", "onActivityResult: "+(uri==null) );
                    head = extras.getParcelable("data");
//                    personLogo.setImageBitmap(head);

                    if (head != null) {
                        flag = true;
                        /**
                         * 上传服务器代码
                         */
//                        if (fileName != null) {
//                            delFile(fileName);
//                        }

                        setPicToView(head);// 保存在SD卡中
//                        ivHead.setImageBitmap(head);// 用ImageView显示出来
                        L.e(fileName);
                        Uri uri = Uri.fromFile(new File(fileName));
                        L.e("uri" + uri);
                        personLogo.setImageURI(uri);
                    }
                }
                break;
            case Constants.REQUESTCODE:
                if (resultCode == Constants.CHANGEPHONE) {
                    L.e("修改手机号成功");
                    String newphone = data.getStringExtra("newphone");
//                    hashPhone.setText(newphone);
                }
                break;
            default:
                break;

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 调用系统的裁剪
     *
     * @param uri
     */
    public void cropPhoto(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, 3);
    }

    //将图片存储至sd卡
    private void setPicToView(Bitmap mBitmap) {
        String sdStatus = Environment.getExternalStorageState();
        if (!sdStatus.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
            return;
        }
        FileOutputStream b = null;
        File file = new File(path);
        file.mkdirs();// 创建文件夹
        fileName = path + UUID.randomUUID() + ".jpg";// 图片名字
        try {
            b = new FileOutputStream(fileName);
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, b);// 把数据写入文件
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                // 关闭流
                b.flush();
                b.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (pvOptions.isShowing()) {
                pvOptions.dismiss();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}
