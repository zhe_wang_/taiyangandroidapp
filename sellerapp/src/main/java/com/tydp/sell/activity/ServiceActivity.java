package com.tydp.sell.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

/**
 * 客服界面
 */
public class ServiceActivity extends BaseActivity implements View.OnClickListener{
    private Button bt_opinion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_service);
        tv_title.setText("联系客服");
        initView();
    }

    private void initView() {
        bt_opinion = (Button) findViewById(R.id.bt_opinion);

        bt_opinion.setOnClickListener(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_service;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_opinion:
                Intent intent=new Intent(ServiceActivity.this,MessageActivity.class);
                startActivity(intent);
                break;
        }
    }
}
