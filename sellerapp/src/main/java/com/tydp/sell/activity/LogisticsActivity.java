package com.tydp.sell.activity;

import android.os.Bundle;

import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

public class LogisticsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logistics);
    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }
}
