package com.tydp.sell.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tydp.sell.utils.L;
import com.tydp.sell.utils.T;
import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

public class EditShipperActivity extends BaseActivity implements View.OnClickListener{
    private String delete;
    private Button bt_person_save;
    private Button bt_person_delete;
    private EditText et_editperson_name;
    private EditText et_editperson_phone;

    private boolean isNameEmpty;//名字是否为空
    private boolean isPhoneEmpty;//电话是否为空
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_edit_shipper);
        tv_title.setText("编辑发货人");
        delete=getIntent().getStringExtra("delete");
        initView();
    }

    private void initView() {
        bt_person_save = (Button) findViewById(R.id.bt_person_save);
        bt_person_delete = (Button) findViewById(R.id.bt_person_delete);
        if (!TextUtils.isEmpty(delete)){
            bt_person_delete.setVisibility(View.GONE);
        }
        bt_person_save.setOnClickListener(this);
        bt_person_delete.setOnClickListener(this);
        et_editperson_name= (EditText) findViewById(R.id.et_editperson_name);
        et_editperson_phone = (EditText) findViewById(R.id.et_editperson_phone);

        et_editperson_name.setText("测试姓名");
        et_editperson_phone.setText("13517275554");

        if (TextUtils.isEmpty(et_editperson_name.getText().toString().trim())){
            isNameEmpty=true;
        }
        if (TextUtils.isEmpty(et_editperson_phone.getText().toString().trim())){
            isPhoneEmpty=true;
        }
        if (!isNameEmpty&&!isPhoneEmpty){
            bt_person_delete.setEnabled(true);
            bt_person_delete.setBackgroundResource(R.drawable.bt_save_selector);
        }
        //监听et_editperson_name上文本内容的变化
        et_editperson_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()==0){
                    isNameEmpty=true;
                        bt_person_delete.setEnabled(false);
                        bt_person_delete.setBackgroundResource(R.drawable.bt_delete_selector);
                    L.e("删除按钮变色");
                }else {
                    isNameEmpty=false;
                    if (!isPhoneEmpty){
                        bt_person_delete.setEnabled(true);
                        bt_person_delete.setBackgroundResource(R.drawable.bt_save_selector);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //监听et_editperson_phone上文本内容的变化
        et_editperson_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()==0){
                    isPhoneEmpty=true;
                        bt_person_delete.setEnabled(false);
                        bt_person_delete.setBackgroundResource(R.drawable.bt_delete_selector);
                    L.e("删除按钮变色");
                }else {
                    isPhoneEmpty=false;
                    if (!isNameEmpty){
                        bt_person_delete.setEnabled(true);
                        bt_person_delete.setBackgroundResource(R.drawable.bt_save_selector);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.activity_edit_shipper;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //保存
            case R.id.bt_person_save:{
                String name=et_editperson_name.getText().toString().trim();
                String phone=et_editperson_phone.getText().toString().trim();
                if (!TextUtils.isEmpty(name)){
                    if (!TextUtils.isEmpty(phone)){

                    }else {
                        T.showShort(this,"请输入电话号码");
                    }
                }else {
                    T.showShort(this,"请输入姓名");
                }
            }
            break;

            case R.id.bt_person_delete:{

            }
            break;
        }
    }
}
