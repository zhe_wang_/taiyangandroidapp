package com.tydp.sell.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.ConvertUtils;
import com.tydp.sell.adapter.DickerAdapter;
import com.tydp.sell.entity.ItemModel;
import com.tydp.sell.utils.Constants;
import com.tydp.sell.utils.DensityUtil;
import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;
import com.tydp.sell.base.ListBaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的还价activity
 */
public class DickerActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private DickerAdapter mAdapter;
    private List<String> mDatas;
    private LinearLayoutManager layoutManager;
    private int mCurrentPage = 1;
    private int mTotalPage = 2;
    private int lastVisibleItemPosition;

    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_dicker);
        tv_title.setText("我的还价");
        initView();

    }

    private void initView() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.mSwipeRefreshLayout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.common_bluecolor);
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
//        //init data
        mDatas = new ArrayList<>();
        mAdapter = new DickerAdapter(this, mDatas);

        //模拟网络请求过程
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                requestData();
            }
        }, 4000);
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        /**
         * RecyclerView设置监听事件
         */
        initRecyclerViewListener();
    }

    private void initRecyclerViewListener() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mAdapter.getItemCount()) {
//                    Log.e("ceshi", "当前页:" + mCurrentPage);
                    if (mTotalPage == 1) {
                        Log.e("ceshi", "只有一页");
                        mAdapter.setLoadingStatues(Constants.LOAD_STATUES_NO_MORE_DATA);
                    } else if (mCurrentPage <= mTotalPage) {
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                requestData();
                            }
                        }, 2000);
                    } else {
                        mAdapter.setLoadingStatues(Constants.LOAD_STATUES_NO_MORE_DATA);
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                //此处的mCurrentPage 和mTotalPage根据实际情况进行判断
                if (mTotalPage == 1) {
                    mAdapter.setLoadingStatues(Constants.LOAD_STATUES_NO_MORE_DATA);
                }
            }

        });
    }

    //请求数据
    private void requestData() {
        for (int i = 0; i < 5; i++) {
            mDatas.add("测试" + i);
        }
        mCurrentPage++;
        mSwipeRefreshLayout.setRefreshing(false);
        //禁止下拉刷新
//        mSwipeRefreshLayout.setEnabled(false);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_dicker;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

    @Override
    public void onRefresh() {

    }


    /**
     * 此处适配器 无用 暂未删除
     */
    private class DataAdapter extends ListBaseAdapter<ItemModel> {
        private boolean isShow;
        private LayoutInflater mLayoutInflater;
        boolean flag[];

        //        private View view;
//        private LinearLayout dicker_layout;
        public DataAdapter(Context context) {
            mLayoutInflater = LayoutInflater.from(context);
            mContext = context;
            flag = new boolean[7];
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(mLayoutInflater.inflate(R.layout.item_dicker, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//            ItemModel item = mDataList.get(position);
//
            final ViewHolder viewHolder = (ViewHolder) holder;
//            viewHolder.textView.setText(item.title);
            viewHolder.iv_show.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewHolder.show_layout.setVisibility(View.GONE);
                    viewHolder.hide_layout.setVisibility(View.VISIBLE);
                }
            });
            viewHolder.iv_hide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewHolder.show_layout.setVisibility(View.VISIBLE);
                    viewHolder.hide_layout.setVisibility(View.GONE);
                }
            });
            if (!flag[position]) {
                for (int i = 0; i < 3; i++) {
//                L.e("测试"+i);
//                viewHolder.dicker_layout.removeAllViews();
                    LinearLayout linearLayout = new LinearLayout(mContext);
                    TextView sortTextView = new TextView(mContext);
                    TextView price = new TextView(mContext);
                    TextView unit = new TextView(mContext);
                    price.setTextColor(mContext.getResources().getColor(R.color.common_red));

                    sortTextView.setBackgroundResource(R.drawable.dicker_bg);
                    int margintop = DensityUtil.dip2px(mContext, 17);
                    int margintopSize = ConvertUtils.dp2px(mContext, 15);
                    LinearLayout.LayoutParams sortParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                LinearLayout.LayoutParams sortParams= (LinearLayout.LayoutParams) viewHolder.ll_child.getLayoutParams();
                    sortParams.width = margintop;
                    sortParams.height = margintop;
                    sortParams.gravity = Gravity.CENTER;
                    sortTextView.setLayoutParams(sortParams);
                    int textSize = DensityUtil.sp2px(mContext, 14);
                    int textSize1 = ConvertUtils.sp2px(mContext, 14);
                    sortTextView.setGravity(Gravity.CENTER);
                    sortTextView.setTextSize(12);
                    sortTextView.setTextColor(mContext.getResources().getColor(R.color.white));
                    sortTextView.setText((i + 2) + "");


                    LinearLayout.LayoutParams priceParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    int marginLeft = DensityUtil.dip2px(mContext, 8);
                    priceParams.setMargins(marginLeft, 0, 0, 0);
                    price.setLayoutParams(priceParams);
                    price.setTextSize(14);
                    unit.setTextSize(14);
                    unit.setTextColor(mContext.getResources().getColor(R.color.common_textblackcolor));

                    price.setText("￥14800");
                    unit.setText("/吨");

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                LinearLayout.LayoutParams params=(LinearLayout.LayoutParams) viewHolder.ll_child.getLayoutParams();
                    params.setMargins(0, margintop, 0, 0);
                    linearLayout.setGravity(Gravity.CENTER_VERTICAL);
                    linearLayout.setLayoutParams(params);
                    linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                    linearLayout.addView(sortTextView);
                    linearLayout.addView(price);
                    linearLayout.addView(unit);
                    viewHolder.dicker_hide_layout.addView(linearLayout);
                }
                flag[position] = true;
            }

        }


        private class ViewHolder extends RecyclerView.ViewHolder {

            LinearLayout dicker_hide_layout;
            LinearLayout ll_child;
            ImageView iv_show;
            ImageView iv_hide;
            RelativeLayout hide_layout;
            RelativeLayout show_layout;

            public ViewHolder(View itemView) {
                super(itemView);
//                textView = (TextView) itemView.findViewById(R.id.info_text);
                dicker_hide_layout = (LinearLayout) itemView.findViewById(R.id.dicker_hide_layout);
                ll_child = (LinearLayout) itemView.findViewById(R.id.ll_child);
                iv_show = (ImageView) itemView.findViewById(R.id.iv_show);
                hide_layout = (RelativeLayout) itemView.findViewById(R.id.hide_layout);
                show_layout = (RelativeLayout) itemView.findViewById(R.id.show_layout);
                iv_hide = (ImageView) itemView.findViewById(R.id.iv_hide);
            }
        }
    }
}
