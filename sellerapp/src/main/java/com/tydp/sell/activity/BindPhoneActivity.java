package com.tydp.sell.activity;

import android.os.Bundle;
import android.view.View;

import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

public class BindPhoneActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_bind_phone);
        tv_title.setText("绑定手机");
        back_layout.setVisibility(View.INVISIBLE);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_bind_phone;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }
}
