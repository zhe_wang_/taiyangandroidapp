package com.tydp.sell.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;

/**
 * 提现界面
 */
public class WithdrawActivity extends BaseActivity implements View.OnClickListener{
    private Button bt_withdraw_next;
    private TextView et_card;
    private boolean isHaseCard;
    private RelativeLayout bankcard_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_withdraw);
        tv_title.setText("提现");
        initView();
    }

    private void initView() {
        bt_withdraw_next = (Button) findViewById(R.id.bt_withdraw_next);
        et_card = (TextView) findViewById(R.id.et_card);
        bankcard_layout = (RelativeLayout) findViewById(R.id.bankcard_layout);

        bt_withdraw_next.setOnClickListener(this);
        bankcard_layout.setOnClickListener(this);
        if (!TextUtils.isEmpty(et_card.getText().toString())){
            isHaseCard=true;
        }
    }

    @Override
    public int getLayout() {
        return R.layout.activity_withdraw;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //下一步
            case R.id.bt_withdraw_next:{
                Intent intent=new Intent(WithdrawActivity.this,WithdrawFinishActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.bankcard_layout:{
                if (isHaseCard){
                    //有银行卡,去选择
                    Intent intent=new Intent(WithdrawActivity.this,SelectBankActivity.class);
                    startActivity(intent);
                }else {
                    //无银行卡 去绑定
                    Intent intent=new Intent(WithdrawActivity.this,BindCardActivity.class);
                    startActivity(intent);
                }

            }
            break;
        }
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }
}
