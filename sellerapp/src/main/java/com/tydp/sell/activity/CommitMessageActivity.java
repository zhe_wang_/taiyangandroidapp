package com.tydp.sell.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tydp.sell.utils.T;
import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;
import com.tydp.sell.custom.MyDialog;

/**
 * 提交留言界面
 */
public class CommitMessageActivity extends BaseActivity implements View.OnClickListener {
    private EditText et_title;
    private EditText et_content;
    //
    private RelativeLayout type_layout;

    //
    public void typeLayout(View v) {

    }

    private TextView mMessage, mComplain, maftersale;
    private ImageView iv_cancel;
    private MyDialog myDialog;
    TextView tv_messagetype;

    Button bt_commit;
    private int type;

//    public void commit(View v) {
//        String msg_type = tv_messagetype.getText().toString();
//        String msg_title = et_title.getText().toString();
//        String msg_content = et_content.getText().toString();
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tv_title.setText("添加留言");
//        order_sn = getIntent().getStringExtra("Order_sn");
//        if(order_sn!=null){
//            et_title.setText("[理赔] 订单号 : "+order_sn);
//        }
        initView();

    }

    private void initView() {
        type_layout = (RelativeLayout) findViewById(R.id.type_layout);
        et_title = (EditText) findViewById(R.id.et_title);
        et_content = (EditText) findViewById(R.id.et_content);
        tv_messagetype = (TextView) findViewById(R.id.tv_messagetype);
        bt_commit = (Button) findViewById(R.id.bt_commit);

        type_layout.setOnClickListener(this);
        bt_commit.setOnClickListener(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_commit_message;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_message:
                tv_messagetype.setText("留言");
                myDialog.dismiss();
                break;
            case R.id.tv_complain:
                tv_messagetype.setText("投诉");
                myDialog.dismiss();
                break;
            case R.id.tv_aftersale:
                tv_messagetype.setText("售后");
                myDialog.dismiss();
                break;
            case R.id.iv_cancel:
                myDialog.dismiss();
                break;
            case R.id.type_layout: {
                View view = LayoutInflater.from(this).inflate(R.layout.message_dialog, null);
                mMessage = (TextView) view.findViewById(R.id.tv_message);
                mComplain = (TextView) view.findViewById(R.id.tv_complain);
                maftersale = (TextView) view.findViewById(R.id.tv_aftersale);
                iv_cancel = (ImageView) view.findViewById(R.id.iv_cancel);
                mMessage.setOnClickListener(this);
                mComplain.setOnClickListener(this);
                maftersale.setOnClickListener(this);
                iv_cancel.setOnClickListener(this);
                myDialog = new MyDialog(this, view, R.style.dialog);
                myDialog.show();
            }
            break;
            case R.id.bt_commit:{
                String msg_type = tv_messagetype.getText().toString();
                String msg_title = et_title.getText().toString();
                String msg_content = et_content.getText().toString();

                finish();
            }
            break;
        }
    }

    @Override
    public void success(String response, int id) {
//        dismissProgress(this);
        T.showShort(this, "提交成功");
        finish();
    }

    @Override
    public void failByOther(String fail, int id) {
//        dismissProgress(this);
        T.showShort(this, "提交人为失败");
    }

    @Override
    public void fail(String fail, int id) {
//        dismissProgress(this);
        T.showShort(this, "提交失败");
    }
}
