package com.tydp.sell.activity;

import android.os.Bundle;

import com.tydp.sell.view.R;
import com.tydp.sell.base.BaseActivity;


/**
 * 登录Activity
 */

public class LoginActivity extends BaseActivity {
//    private UMShareAPI mShareAPI = null;//友盟登录的api
//
//    private String action;//用于登录成功后进入对应的界面
//    private String title;//用于仓储服务的标题
//    @InjectView(R.id.bt_register)
//    Button registerButton;
//    @InjectView(R.id.et_phone)
//    EditText etPhone;
//    @InjectView(R.id.et_password)
//    EditText etPassword;
//    @InjectView(R.id.et_phone_bg)
//    View phoneBg;
//    @InjectView(R.id.et_password_bg)
//    View passwordBg;
//    //QQ登录
//    @InjectView(R.id.tv_qq)
//    TextView tv_qq;
//    @OnClick(R.id.tv_qq)
//    public void loginByQQ(View v){
//        SHARE_MEDIA platform = SHARE_MEDIA.QQ;
//        L.e("platform:---"+platform);
//        mShareAPI.doOauthVerify(LoginActivity.this, platform, umAuthListener);
//    }
//    //微信登录
//    @InjectView(R.id.tv_wechat)
//    TextView tv_wechat;
//    @OnClick(R.id.tv_wechat)
//    public void loginByWeiChat(View v){
//        SHARE_MEDIA platform = SHARE_MEDIA.WEIXIN;
//        mShareAPI.doOauthVerify(LoginActivity.this, platform, umAuthListener);
//    }
//    @InjectView(R.id.tv_fpassword)
//    TextView tv_fpassword;
//    //忘记密码
//    @OnClick(R.id.tv_fpassword)
//    public void reset(View v) {
//        Intent intent = new Intent(this, ResetPasswordActivity.class);
//        startActivity(intent);
//    }
//    //注册
//    @OnClick(R.id.bt_register)
//    public void register(View v) {
//        Intent intent = new Intent(this, RegisterActivity.class);
//        startActivity(intent);
//    }
//
////    @InjectView(R.id.back_layout)
////    LinearLayout backLayout;
//
//    @OnClick(R.id.back_layout)
//    public void back(View v) {
////        finish();
//        if (my != null) {
//            endActivity(100);
//        }else {
//            finish();
//        }
//    }
//
//    private void endActivity(int code) {
//        //数据是使用Intent返回
//        Intent intent = new Intent();
//        //设置返回数据
//        LoginActivity.this.setResult(code, intent);
//        //关闭Activity
//        LoginActivity.this.finish();
//    }
//
//    private UserModel mUserModel;
//    private MyApplication application;
//    @InjectView(R.id.bt_login)
//    Button loginButton;
//
//    @OnClick(R.id.bt_login)
//    public void login(View v) {
//        userName = etPhone.getText().toString();
//        password = etPassword.getText().toString();
//        if (TextUtils.isEmpty(userName)) {
//            Toast.makeText(this, "请输入用户名", Toast.LENGTH_LONG).show();
//            return;
//        }
//        if (TextUtils.isEmpty(password)) {
//            Toast.makeText(this, "请输入密码", Toast.LENGTH_LONG).show();
//            return;
//        }
////        if (my != null) {
//        showProgress(LoginActivity.this, "登陆中");
//        Map<String, Object> params = new HashMap<>();
//        params.put("model", "user");
//        params.put("action", "login");
//        params.put("user_name", userName);
//        params.put("password", password);
//        params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "login")));
//        HttpUtils.sendPost(params, this, 100);
//
//    }
//
//    private String my;
//
//    private String userName;
//    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tv_title.setText("用户登录");
//        mTitle.setText("用户登录");
//        mShareAPI = UMShareAPI.get( this );
//        initData();
//        initListener();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

//    private void initData() {
//        application = MyApplication.getInstance();
//        application.addActivity(this);
//        my = getIntent().getStringExtra("my");
//        action = getIntent().getStringExtra("action");
//        title=getIntent().getStringExtra("title");
//    }
//
//    @Override
//    protected int getLayout() {
//        return R.layout.activity_login;
//    }
//
//    private void initListener() {
//        etPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    phoneBg.setBackgroundColor(getResources().getColor(R.color.bg_bluecolor));
//                } else {
//                    phoneBg.setBackgroundColor(getResources().getColor(R.color.bg_color));
//                }
//            }
//        });
//        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    passwordBg.setBackgroundColor(getResources().getColor(R.color.bg_bluecolor));
//                } else {
//                    passwordBg.setBackgroundColor(getResources().getColor(R.color.bg_color));
//                }
//            }
//        });
//    }
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == event.KEYCODE_BACK) {
//            if (my != null) {
//                endActivity(100);
//            }
//        }
//        return super.onKeyDown(keyCode, event);
//    }
//
//    @Override
//    public void success(String response, int id) {
////        dismissProgress(LoginActivity.this);
//        dismissProgress(LoginActivity.this);
//        mUserModel = new Gson().fromJson(response, UserModel.class);
//        Log.e("ceshi", "token:" + mUserModel.getToken());
//        Log.e("ceshi", "id:" + mUserModel.getUser_id());
//        mySPEdit.setIsLogin(true);
//        mySPEdit.setToken(mUserModel.getToken());
//        mySPEdit.setUserId(mUserModel.getUser_id());
//        mySPEdit.setUserName(userName);
//        mySPEdit.setPass(password);
//        application.setCurrentUser(mUserModel);
//        mySPEdit.setFirstLogin(mUserModel.getLast_login());
//        mySPEdit.setUserInfo(response);
////        Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_LONG).show();
//        T.showShort(this, "登录成功");
//        if (my != null) {
//            if (my.equals("我的")) {
//                endActivity(0);
//            } else if (my.equals("服务")) {
//                endActivity(1);
//            }
//        }
//        if (action != null) {
//            if (title!=null){
//                Intent intent = new Intent();
//                intent.setAction(action);
//                intent.putExtra("title",title);
//                startActivity(intent);
//                finish();
//            }
//
//        }
////        switch (id){
////            case 100:
////                L.e("第一次登陆成功");
////                break;
////            case 101:
////                L.e("第二次登陆成功");
////                break;
////            case 102:
////                L.e("第三次登陆成功");
////                break;
////        }
//
//    }
//
//    @Override
//    public void failByOther(String fail, int id) {
//        dismissProgress(LoginActivity.this);
////        Toast.makeText(this, fail, Toast.LENGTH_LONG).show();
//        T.showShort(this, fail);
//    }
//
//    @Override
//    public void fail(String fail, int id) {
//        dismissProgress(LoginActivity.this);
//        T.showShort(this, R.string.jianchawangluo);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//
//    }
//    /** auth callback interface**/
//    private UMAuthListener umAuthListener = new UMAuthListener() {
//        @Override
//        public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
////            Toast.makeText(getApplicationContext(), "Authorize succeed", Toast.LENGTH_SHORT).show();
////            L.e("onComplete  action:---"+action);
////            L.e("user info:"+data.toString());
//            /*action
//            0  授权登录
//            2  获取用户信息
//            */
//            switch (action){
//                case 0:
//                    mShareAPI.getPlatformInfo(LoginActivity.this, platform, umAuthListener);
//                    break;
//                case 2:
//                    L.e("用户信息:"+ JSON.toJSONString(data));
//                    break;
//            }
//        }
//
//        @Override
//        public void onError(SHARE_MEDIA platform, int action, Throwable t) {
//            Toast.makeText( getApplicationContext(), "Authorize fail", Toast.LENGTH_SHORT).show();
//        }
//
//        @Override
//        public void onCancel(SHARE_MEDIA platform, int action) {
//            L.e("onCancel  action:---"+action);
//            Toast.makeText( getApplicationContext(), "Authorize cancel", Toast.LENGTH_SHORT).show();
//        }
//    };
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        mShareAPI.onActivityResult(requestCode, resultCode, data);
//    }
}
