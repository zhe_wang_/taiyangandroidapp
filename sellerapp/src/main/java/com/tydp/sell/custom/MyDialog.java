package com.tydp.sell.custom;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * 自定义dialon
 * @author Administrator
 *
 */
public class MyDialog extends Dialog {

	private static int default_width = 160; // 默认宽度

	private static int default_height = 120;// 默认高度

	public MyDialog(Context context, View layout, int style) {
		this(context, default_width, default_height, layout, style);
	}

	public MyDialog(Context context, int width, int height, View layout, int style) {
		super(context, style);
		setContentView(layout);
		Window window = getWindow();
		WindowManager.LayoutParams params = window.getAttributes();
		params.gravity = Gravity.CENTER;
		window.setAttributes(params);

	}
	//调用方法
//	protected void showAlertDialog(String title) {
//		View alertView = (View) getLayoutInflater().inflate(R.layout.progress_hud, null);
//		TextView tvTitle = (TextView) alertView.findViewById(R.id.tv_msg);
//		tvTitle.setText(title);
//		TextView tvCancel = (TextView) alertView.findViewById(R.id.tv_cancel);
//		tvCancel.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				builder.dismiss();
//			}
//		});
//		TextView tvConfirm = (TextView) alertView.findViewById(R.id.tv_confirm);
//		tvConfirm.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				builder.dismiss();
//			}
//		});
//		builder = new Myd(MainActivity.this,0,0,alertView,R.style.dialog);
//		builder.show();
//	}

	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode || KeyEvent.KEYCODE_HOME== keyCode) {
        	 return false;
        }
        return false;
    }

}