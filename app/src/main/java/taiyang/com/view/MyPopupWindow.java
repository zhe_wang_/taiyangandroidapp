package taiyang.com.view;


import android.view.View;
import android.widget.PopupWindow;

/**
 * Created by admin on 2016/8/19.
 */
public class MyPopupWindow extends PopupWindow {
    public MyPopupWindow(View contentView, int width, int height) {
        super(contentView, width, height, false);
    }
    @Override
    public void dismiss() {
        if (mSendMessage!=null){
            mSendMessage.send();
        }
        super.dismiss();

    }
    public interface SendMessage{
        void send();
    }
    private SendMessage mSendMessage;
    public void setSendMessage(SendMessage mSendMessage){
        this.mSendMessage=mSendMessage;
    }
}
