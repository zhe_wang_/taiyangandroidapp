package taiyang.com.tydp_b;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.umeng.analytics.MobclickAgent;

import java.util.Map;

import cn.magicwindow.MLinkAPIFactory;
import cn.magicwindow.MWConfiguration;
import cn.magicwindow.MagicWindowSDK;
import cn.magicwindow.mlink.YYBCallback;
import cn.magicwindow.mlink.annotation.MLinkDefaultRouter;
import taiyang.com.activity.InfoActivity;
import taiyang.com.activity.LoginActivity;
import taiyang.com.activity.PublishActivity;
import taiyang.com.activity.SellActivity;
import taiyang.com.activity.SplashActivity;
import taiyang.com.entity.UserModel;
import taiyang.com.fragment.HomeFragment;
import taiyang.com.fragment.MyFragment;
import taiyang.com.fragment.OfferFragment;
import taiyang.com.fragment.ServiceFragment;
import taiyang.com.utils.Constants;
import taiyang.com.utils.DensityUtil;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.L;
import taiyang.com.utils.MyApplication;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.ToastUtil;
import taiyang.com.view.XCSlideView;

/**
 * 主页Activity
 */

@MLinkDefaultRouter
public class MainActivity extends AppCompatActivity implements View.OnClickListener, HttpRequestListener {

    private RadioButton rb_bottom_home, rb_bottom_offer, rb_bottom_my, rb_bottom_service;

    private int tab = 1;
    private FragmentManager fManager;
    private HomeFragment mHomeFragment;
    private OfferFragment mOfferFragment;
    private MyFragment mMyFragment;
    private ServiceFragment mServiceFragment;
    private String mResult;
    private Map<String, Object> params;
    private UserModel userModel;
    private XCSlideView mSlideViewLEFT;
    private View menuViewLeft;
    private int mScreenWidth = 0;
    private Context mContext;
    private MyApplication application;
    private MySPEdit mySPEdit;
    private SimpleDraweeView iv_left_icon;
    private TextView tv_left_name;
    private ImageView iv_left_callphone, rb_bottom_info;
    private long exitTime = 0;//退出程序间隔时间
    private PopupWindow window;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);


        MWConfiguration config = new MWConfiguration(this);
        config.setLogEnable(true);//打开魔窗Log信息
        MagicWindowSDK.initSDK(config);

        MLinkAPIFactory.createAPI(this).registerWithAnnotation(this);


        if (getIntent().getData()!=null) {
            MLinkAPIFactory.createAPI(this).router(getIntent().getData());
            //跳转后结束当前activity
            finish();
        } else {
            //TODO：动画等耗时操作结束后再调用checkYYB(),一般写在starActivity前即可
            MLinkAPIFactory.createAPI(this).checkYYB(MainActivity.this, new YYBCallback() {
                @Override
                public void onFailed(Context context) {
                }

                @Override
                public void onSuccess() {
                }
            });
        }
        //友盟统计
        MobclickAgent.setScenarioType(this, MobclickAgent.EScenarioType.E_UM_NORMAL);
        mySPEdit = MySPEdit.getInstance(this);
        application = MyApplication.getInstance();
        mySPEdit = MySPEdit.getInstance(this);
        initViews();
        initListener();
        //请求侧滑菜单数据
        //请求版本信息
        initVersionUpDate();

        // 默认切换 homeFragment
        tab = 1;
        changeToF1();
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (mResult != null && mResult.equals("result")) {
            // InquiryListActivity 退回切换 offerFragment
            tab = 2;
            changeToF2();
        }
        //加载侧滑头 数据
        //登录过
        if (MySPEdit.getInstance(this).getIsLogin()) {
            userModel = new Gson().fromJson(mySPEdit.getUserInfo(), UserModel.class);
            if (userModel != null) {
                tv_left_name.setText(userModel.getUser_name());
                if (userModel.getUser_face() != null) {
                    iv_left_icon.setImageURI(Uri.parse(userModel.getUser_face()));
                }
            }
        } else {
            //未登录
            //iv_left_icon.setBackgroundDrawable(getResources().getDrawable(R.mipmap.person_head_default));
            //iv_left_icon.setImageBitmap(new BitmapDrawable(R.mipmap.person_head_default));
            iv_left_icon.setImageResource(R.mipmap.person_head_default);
            tv_left_name.setText("请登录");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        //确保普通返回界面时 默认打开homeFragment
        mResult = null;
    }

    private void initViews() {
        Drawable drawable1;
        mContext = this;
        rb_bottom_home = (RadioButton) findViewById(R.id.rb_bottom_home);
        rb_bottom_offer = (RadioButton) findViewById(R.id.rb_bottom_offer);
        rb_bottom_service = (RadioButton) findViewById(R.id.rb_bottom_service);
        rb_bottom_my = (RadioButton) findViewById(R.id.rb_bottom_my);
        rb_bottom_info = (ImageView) findViewById(R.id.rb_bottom_info);


        drawable1 = getResources().getDrawable(R.drawable.bottom_home_pager);
        drawable1.setBounds(0, 0, 80, 80);//第一0是距左边距离，第二0是距上边距离，40分别是长宽
        rb_bottom_home.setCompoundDrawables(null, drawable1, null, null);

        drawable1 = getResources().getDrawable(R.drawable.bottom_offer_pager);
        drawable1.setBounds(0, 0, 80, 80);//第一0是距左边距离，第二0是距上边距离，40分别是长宽
        rb_bottom_offer.setCompoundDrawables(null, drawable1, null, null);

        drawable1 = getResources().getDrawable(R.drawable.bottom_service_pager);
        drawable1.setBounds(0, 0, 80, 80);//第一0是距左边距离，第二0是距上边距离，40分别是长宽
        rb_bottom_service.setCompoundDrawables(null, drawable1, null, null);

        drawable1 = getResources().getDrawable(R.drawable.bottom_my_pager);
        drawable1.setBounds(0, 0, 80, 80);//第一0是距左边距离，第二0是距上边距离，40分别是长宽
        rb_bottom_my.setCompoundDrawables(null, drawable1, null, null);

        //加载左侧viwe
        mScreenWidth = DensityUtil.getScreenWidthAndHeight(mContext)[0];
        menuViewLeft = LayoutInflater.from(mContext).inflate(R.layout.layout_slideview, null);
        iv_left_callphone = (ImageView) menuViewLeft.findViewById(R.id.iv_left_callphone);
        iv_left_icon = (SimpleDraweeView) menuViewLeft.findViewById(R.id.iv_left_icon);
        tv_left_name = (TextView) menuViewLeft.findViewById(R.id.tv_left_name);
        mSlideViewLEFT = XCSlideView.create(this, XCSlideView.Positon.LEFT);
        mSlideViewLEFT.setMenuView(MainActivity.this, menuViewLeft);
        mSlideViewLEFT.setMenuWidth(mScreenWidth * 75 / 100);

    }

    private void initListener() {
        rb_bottom_home.setOnClickListener(this);
        rb_bottom_service.setOnClickListener(this);
        rb_bottom_offer.setOnClickListener(this);
        rb_bottom_my.setOnClickListener(this);
        rb_bottom_info.setOnClickListener(this);
        iv_left_callphone.setOnClickListener(this);
    }


    //请求版本信息
    private void initVersionUpDate() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 2) {
            // InquiryListActivity 传递的值
            mResult = data.getStringExtra("result");

        }
        if (requestCode == Constants.REQUESTCODE && resultCode == Constants.LOGOOUT) {
            tab = 1;
            L.e("退出登录");
        }

//        if (requestCode == 0 && resultCode == 0) {
//            tab = 5;
////            changeToF5();
//        } else {
//            Log.e("ceshi", tab + "标签");
//
//        }
        if (requestCode == 1 && resultCode == 1) {
            tab = 4;
//            changeToF5();
        } else {
            Log.e("ceshi", tab + "标签");

        }
        if (requestCode == Constants.REQUESTCODE && resultCode == Constants.BACKSELECT) {
            tab = 2;
        }
        select(tab);
    }

    /**
     * 通过点击rb切换fragment
     *
     * @param v
     */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rb_bottom_home:
                tab = 1;
                changeToF1();
                break;
            case R.id.rb_bottom_offer:
                tab = 2;
                changeToF2();
                break;
            case R.id.rb_bottom_info:
                showPopWindow();
                break;
            case R.id.rb_bottom_service:
                if (mySPEdit.getToken() != null) {
                    tab = 4;
                    changeToF4();
                } else {
                    Intent intent = new Intent(this, LoginActivity.class);
                    intent.putExtra("my", "服务");
                    startActivityForResult(intent, 1);
                }

                break;
            case R.id.rb_bottom_my:
                if (mySPEdit.getToken() != null) {
                    tab = 5;
                    changeToF5();
                } else {
                    Intent intent = new Intent(this, LoginActivity.class);
                    intent.putExtra("my", "我的");
                    startActivityForResult(intent, 0);
                }
                break;

            case R.id.iv_left_callphone:

                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "13517278554"));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);
                mSlideViewLEFT.dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!BuildConfig.DEBUG) {
            MobclickAgent.onResume(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (!BuildConfig.DEBUG) {
            MobclickAgent.onResume(this);
        }
    }

    private void select(int tab) {
        if (tab == 1) {
            changeToF1();
        } else if (tab == 2) {
            changeToF2();
        } else if (tab == 3) {
            showPopWindow();
        } else if (tab == 4) {
            changeToF4();
        } else {
            changeToF5();
        }
    }

    /**
     * 切换homeFragment
     */
    private void changeToF1() {
        fManager = getSupportFragmentManager();
        FragmentTransaction transaction = fManager.beginTransaction();
        commonChange(transaction);

        rb_bottom_home.setChecked(true);

        if (mHomeFragment == null) {
            mHomeFragment = new HomeFragment();
            mHomeFragment.setView(mSlideViewLEFT);
            transaction.add(R.id.container, mHomeFragment, "f1");
        } else {
            transaction.show(mHomeFragment);

        }
        transaction.commitAllowingStateLoss();

    }

    /**
     * 切换offerFragment
     */
    private void changeToF2() {
        fManager = getSupportFragmentManager();
        FragmentTransaction transaction = fManager.beginTransaction();
        commonChange(transaction);
        rb_bottom_offer.setChecked(true);
        if (mOfferFragment == null) {
            transaction.add(R.id.container, new OfferFragment(), "f2");
        } else {
            transaction.show(mOfferFragment);
        }
        transaction.commitAllowingStateLoss();

    }

    /**
     * 切换infoFragment
     */
    private void showPopWindow() {
        initPopwindow();
    }

    private void initPopwindow() {

        // 利用layoutInflater获得View
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_custome_view, null);

        // 下面是两种方法得到宽度和高度 getWindow().getDecorView().getWidth()

        window = new PopupWindow(view,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.alpha = 0.5f;

        getWindow().setAttributes(params);
        // 设置popWindow弹出窗体可点击，这句话必须添加，并且是true
        window.setFocusable(true);


        // 必须要给调用这个方法，否则点击popWindow以外部分，popWindow不会消失
        // window.setBackgroundDrawable(new BitmapDrawable());

        window.setBackgroundDrawable(new BitmapDrawable());
        // 在参照的View控件下方显示
        // window.showAsDropDown(MainActivity.this.findViewById(R.id.start));

        // 设置popWindow的显示和消失动画
        window.setAnimationStyle(R.style.mypopwindow_anim_style);
        // 在底部显示
        window.showAtLocation(MainActivity.this.findViewById(R.id.rb_bottom_info),
                Gravity.BOTTOM, 0, 0);

        // 这里检验popWindow里的button是否可以点击
        view.findViewById(R.id.close_btn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                window.dismiss();
            }
        });


        view.findViewById(R.id.iv_publish_sell).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                window.dismiss();
                if (mySPEdit.getToken() != null) {
                    Intent i = new Intent(MainActivity.this, SellActivity.class);
                    startActivity(i);
                } else {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivityForResult(intent, 1);
                }
            }
        });
        view.findViewById(R.id.iv_publish_qiugou).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                window.dismiss();
                if (mySPEdit.getToken() != null) {
                    Intent i = new Intent(MainActivity.this, PublishActivity.class);
                    startActivity(i);
                } else {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivityForResult(intent, 1);
                }
            }
        });
        view.findViewById(R.id.iv_publish_info).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                window.dismiss();
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                startActivity(intent);
            }
        });


        // popWindow消失监听方法
        window.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                System.out.println("popWindow消失");
                WindowManager.LayoutParams params = getWindow().getAttributes();
                params.alpha = 1f;
                getWindow().setAttributes(params);
            }
        });
    }

    /**
     * 切换myFragment
     */
    private void changeToF4() {
        fManager = getSupportFragmentManager();
        FragmentTransaction transaction = fManager.beginTransaction();
        commonChange(transaction);
        rb_bottom_service.setChecked(true);
        if (mServiceFragment == null) {
            transaction.add(R.id.container, new ServiceFragment(), "f4");
        } else {
            transaction.show(mServiceFragment);
        }
        transaction.commitAllowingStateLoss();
    }

    private void changeToF5() {
        fManager = getSupportFragmentManager();
        FragmentTransaction transaction = fManager.beginTransaction();
        commonChange(transaction);
        rb_bottom_my.setChecked(true);
        if (mMyFragment == null) {
            transaction.add(R.id.container, new MyFragment(), "f5");
        } else {
            transaction.show(mMyFragment);
        }
        transaction.commitAllowingStateLoss();
    }


    /**
     * 每次切换时 隐藏以前打开的fragment 类似删除
     *
     * @param transaction
     */
    private void commonChange(FragmentTransaction transaction) {
        mHomeFragment = (HomeFragment) fManager.findFragmentByTag("f1");
        mOfferFragment = (OfferFragment) fManager.findFragmentByTag("f2");
        mServiceFragment = (ServiceFragment) fManager.findFragmentByTag("f4");
        mMyFragment = (MyFragment) fManager.findFragmentByTag("f5");


        if (mHomeFragment != null) {
            transaction.hide(mHomeFragment);
        }
        if (mServiceFragment != null) {
            transaction.hide(mServiceFragment);
        }
        if (mOfferFragment != null) {
            transaction.hide(mOfferFragment);
        }

        if (mMyFragment != null) {
            transaction.hide(mMyFragment);
        }
    }


    @Override
    public void success(String response, int id) {
    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

    //点击2次返回退出
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                ToastUtil.showToast("再按一次退出程序");
                exitTime = System.currentTimeMillis();
            } else {
                finish();
                System.exit(0);
            }
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

}
