package taiyang.com.tydp_b;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.kaopiz.kprogresshud.KProgressHUD;

/**
 * Created by admin on 2016/8/29.
 */
public class BaseFragment extends Fragment {
    KProgressHUD kProgressHUD;

    protected void showProgress(Context context, String message) {
        kProgressHUD = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(message)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    protected void dismissProgress(Context context) {
        if (kProgressHUD != null) {
            kProgressHUD.dismiss();
        }
    }
}
