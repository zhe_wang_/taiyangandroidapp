package taiyang.com.tydp_b;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by Administrator on 2016/9/6.
 */
public class KProgressActivity  extends AppCompatActivity {
    KProgressHUD kProgressHUD;

    protected void showProgress(Context context, String message) {
        kProgressHUD = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(message)
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    protected void dismissProgress(Context context) {
        if (kProgressHUD != null) {
            kProgressHUD.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!BuildConfig.DEBUG) {
            MobclickAgent.onResume(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (!BuildConfig.DEBUG) {
            MobclickAgent.onResume(this);
        }
    }

}
