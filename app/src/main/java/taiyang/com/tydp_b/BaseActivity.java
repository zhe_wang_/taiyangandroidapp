package taiyang.com.tydp_b;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.umeng.analytics.MobclickAgent;
import com.umeng.message.PushAgent;

import java.util.Locale;

import butterknife.ButterKnife;
import cn.magicwindow.Session;
import taiyang.com.activity.OfferInfoActivity;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.MyApplication;
import taiyang.com.utils.MySPEdit;

/**
 * Created by heng on 2016/7/25.
 */
public abstract class BaseActivity extends AppCompatActivity implements HttpRequestListener {
    KProgressHUD kProgressHUD;
//    @InjectView(R.id.back_layout)
  protected   LinearLayout backLayout;
//
//    @OnClick(R.id.back_layout)
//    public void back(View v) {
//        finish();
//    }
//
//    @InjectView(R.id.tv_title)
    protected TextView mTitle;

    protected MySPEdit mySPEdit;
    protected MyApplication myApplication;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PushAgent.getInstance(getApplicationContext()).onAppStart();
        setContentView(getLayout());
        ButterKnife.inject(this);
        backLayout= (LinearLayout) findViewById(R.id.back_layout);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mTitle= (TextView) findViewById(R.id.tv_title);
        mySPEdit=MySPEdit.getInstance(this);
        myApplication=MyApplication.getInstance();

        MobclickAgent.setScenarioType(this, MobclickAgent.EScenarioType.E_UM_NORMAL);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            //透明导航栏
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
//        }
//        // 创建状态栏的管理实例
//        SystemBarTintManager tintManager = new SystemBarTintManager(this);
//        // 激活状态栏设置
//        tintManager.setStatusBarTintEnabled(true);
//        // 激活导航栏设置
//        tintManager.setNavigationBarTintEnabled(true);
        // 设置一个颜色给系统栏
//        if (PrefUtils.isDarkMode()) {
//            tintManager.setTintColor(getResources().getColor(R.color.colorPrimaryDarkDarkTheme));
//        } else {
//            tintManager.setTintColor(getResources().getColor(R.color.colorPrimaryDark));
//        }
    }
    protected abstract int getLayout();
    private ProgressDialog dialog;


    protected boolean ch(){
        return Locale.getDefault().getLanguage().equals("ch");
    }
    protected void showProgress(Context context, String message) {
        kProgressHUD = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(message)
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    protected void dismissProgress(Context context) {
        if (kProgressHUD != null) {
            kProgressHUD.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Session.onResume(this);
        if (!BuildConfig.DEBUG) {
            MobclickAgent.onResume(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Session.onPause(this);
        if (!BuildConfig.DEBUG) {
            MobclickAgent.onResume(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }


}
