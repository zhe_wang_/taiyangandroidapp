package taiyang.com.entity;

import taiyang.com.adapter.Entity;

/**
 * Created by admin on 2016/8/2.
 */
public class ListBean extends Entity{
        private String order_id;
        private String order_sn;
        private String user_id;
        private String pay_status;
        private String add_time;
        private String total_fee;
        /**
         * rec_id : 115
         * order_id : 115
         * goods_id : 106
         * goods_name : 2号肉
         * goods_sn :
         * product_id : 0
         * goods_number : 40
         * market_price : 10000.00
         * goods_price : 10000.00
         * goods_attr :
         * send_number : 0
         * is_real : 1
         * extension_code :
         * parent_id : 0
         * is_gift : 0
         * goods_attr_id :
         * total_weight : 1.00
         * is_retail : 1
         * measure_unit : 件
         * storage : 300
         * sku : null
         * brand_sn : 001A
         * region_name : 【CA】加拿大
         * port : 0
         * goods_local : 天津
         * subtotal : 10000
         * part_number : 40
         * part_weight : 0
         * formated_goods_price : ￥10000.00
         * formated_subtotal : ￥10000.00
         * goods_thumb : http://tydpb2b.bizsoho.com/images/201606/thumb_img/37_thumb_G_14671394333243.jpg
         */

        private GoodsModel goods;
        private String order_status_format;
        private int show_pay;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getOrder_sn() {
            return order_sn;
        }

        public void setOrder_sn(String order_sn) {
            this.order_sn = order_sn;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getPay_status() {
            return pay_status;
        }

        public void setPay_status(String pay_status) {
            this.pay_status = pay_status;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getTotal_fee() {
            return total_fee;
        }

        public void setTotal_fee(String total_fee) {
            this.total_fee = total_fee;
        }

        public GoodsModel getGoods() {
            return goods;
        }

        public void setGoods(GoodsModel goods) {
            this.goods = goods;
        }

        public String getOrder_status_format() {
            return order_status_format;
        }

        public void setOrder_status_format(String order_status_format) {
            this.order_status_format = order_status_format;
        }

        public int getShow_pay() {
            return show_pay;
        }

        public void setShow_pay(int show_pay) {
            this.show_pay = show_pay;
        }


}
