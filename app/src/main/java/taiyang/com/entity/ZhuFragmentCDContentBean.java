package taiyang.com.entity;

import java.util.List;

/**
 * Created by Administrator on 2016/8/6.
 */
public class ZhuFragmentCDContentBean {

    /**
     * brand_id : 409
     * brand_sn : 2088118
     */

    private List<BrandListBean> brand_list;
    /**
     * cat_id : 83
     * cat_list : [{"goods_id":"26","goods_name":"猪半鼻"},{"goods_id":"10","goods_name":"猪整头"},{"goods_id":"11","goods_name":"猪半头"},{"goods_id":"12","goods_name":"猪嘴唇"},{"goods_id":"14","goods_name":"猪面具"},{"goods_id":"15","goods_name":"猪脑"},{"goods_id":"16","goods_name":"猪耳朵"},{"goods_id":"17","goods_name":"猪耳片"},{"goods_id":"19","goods_name":"猪耳根"},{"goods_id":"20","goods_name":"猪舌根"},{"goods_id":"23","goods_name":"猪舌"},{"goods_id":"24","goods_name":"猪整鼻"},{"goods_id":"1313","goods_name":"下颚去皮"},{"goods_id":"1317","goods_name":"猪头肉"},{"goods_id":"1327","goods_name":"带皮猪下颌"}]
     * cat_name : 猪头（头部）
     */

    private List<CatDataBean> cat_data;
    /**
     * id : 13
     * val : 上海
     */

    private List<SelListBean> sel_list;
    /**
     * id : 1
     * site_icon : 1465781373566000242.png
     * site_name : 【US】美国
     */

    private List<SiteListBean> site_list;
    /**
     * cat_id : 1
     * cat_name : 猪
     */

    private List<TopCategoryBean> top_category;

    public List<String> getGoods_local() {
        return goods_local;
    }

    public void setGoods_local(List<String> goods_local) {
        this.goods_local = goods_local;
    }

    private List<String> goods_local;

    public List<BrandListBean> getBrand_list() {
        return brand_list;
    }

    public void setBrand_list(List<BrandListBean> brand_list) {
        this.brand_list = brand_list;
    }

    public List<CatDataBean> getCat_data() {
        return cat_data;
    }

    public void setCat_data(List<CatDataBean> cat_data) {
        this.cat_data = cat_data;
    }

    public List<SelListBean> getSel_list() {
        return sel_list;
    }

    public void setSel_list(List<SelListBean> sel_list) {
        this.sel_list = sel_list;
    }

    public List<SiteListBean> getSite_list() {
        return site_list;
    }

    public void setSite_list(List<SiteListBean> site_list) {
        this.site_list = site_list;
    }

    public List<TopCategoryBean> getTop_category() {
        return top_category;
    }

    public void setTop_category(List<TopCategoryBean> top_category) {
        this.top_category = top_category;
    }

    public static class BrandListBean {
        private String brand_id;
        private String brand_sn;

        public String getBrand_id() {
            return brand_id;
        }

        public void setBrand_id(String brand_id) {
            this.brand_id = brand_id;
        }

        public String getBrand_sn() {
            return brand_sn;
        }

        public void setBrand_sn(String brand_sn) {
            this.brand_sn = brand_sn;
        }
    }

    public static class CatDataBean {
        private String cat_id;
        private String cat_name;
        /**
         * goods_id : 26
         * goods_name : 猪半鼻
         */

        private List<CatListBean> cat_list;

        public String getCat_id() {
            return cat_id;
        }

        public void setCat_id(String cat_id) {
            this.cat_id = cat_id;
        }

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        public List<CatListBean> getCat_list() {
            return cat_list;
        }

        public void setCat_list(List<CatListBean> cat_list) {
            this.cat_list = cat_list;
        }

        public static class CatListBean {
            private String goods_id;
            private String goods_name;

            public String getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(String goods_id) {
                this.goods_id = goods_id;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }
        }
    }

    public static class SelListBean {
        private String id;
        private String val;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVal() {
            return val;
        }

        public void setVal(String val) {
            this.val = val;
        }
    }

    public static class SiteListBean {
        private String id;
        private String site_icon;
        private String site_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSite_icon() {
            return site_icon;
        }

        public void setSite_icon(String site_icon) {
            this.site_icon = site_icon;
        }

        public String getSite_name() {
            return site_name;
        }

        public void setSite_name(String site_name) {
            this.site_name = site_name;
        }
    }

    public static class TopCategoryBean {
        private String cat_id;
        private String cat_name;

        public String getCat_id() {
            return cat_id;
        }

        public void setCat_id(String cat_id) {
            this.cat_id = cat_id;
        }

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }
    }
}
