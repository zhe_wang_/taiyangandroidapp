package taiyang.com.entity;

/**
 * Created by Administrator on 2016/8/9.
 * 产地 厂号 港口 模板Bean
 */
public class OfferFragmentTextBean {

    public OfferFragmentTextBean() {

    }

    public OfferFragmentTextBean(String id, String name) {
        this.id = id;
        this.name = name;
    }

        private String id;
        private String name;

        public String getid() {
            return id;
        }

        public void setBrand_id(String id) {
            this.id = id;
        }

        public String getname() {
            return name;
        }

        public void setname(String name) {
            this.name = name;
        }
//    }

}
