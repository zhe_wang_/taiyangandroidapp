package taiyang.com.entity;

/**
 * Created by Administrator on 2016/8/23.
 */
public class BrandListBean {

        private String brand_id;
        private String brand_sn;

        public String getBrand_id() {
            return brand_id;
        }

        public void setBrand_id(String brand_id) {
            this.brand_id = brand_id;
        }

        public String getBrand_sn() {
            return brand_sn;
        }

        public void setBrand_sn(String brand_sn) {
            this.brand_sn = brand_sn;
        }

}
