package taiyang.com.entity;

/**
 * Created by admin on 2016/8/2.
 */
public class GoodsModel {

    /**
     * rec_id : 1800
     * order_id : 1800
     * goods_id : 1834
     * goods_name : 2号肉
     * goods_sn :
     * product_id : 0
     * goods_number : 2
     * market_price : 14905.20
     * goods_price : 14905.20
     * goods_attr :
     * send_number : 0
     * is_real : 1
     * extension_code :
     * parent_id : 0
     * is_gift : 0
     * goods_attr_id :
     * total_weight : 1.00
     * is_retail : 1
     * measure_unit : 件
     * storage : 104
     * sku : N17092909985
     * brand_sn : 06-08/13-18
     * region_name : 【CL】智利
     * port : 0
     * goods_local : 山东
     * user_id : 1046
     * shop_price_unit : 件
     * spec_2 : 2
     * shop_price : 14905.20
     * goods_type : 7
     * good_face :
     * subtotal : 14905.2
     * part_number : 2
     * part_weight : 0
     * formated_goods_price : ￥14905.20
     * formated_subtotal : ￥14905.20
     * goods_thumb : http://test.taiyanggo.com/images/201610/thumb_img/37_thumb_G_14763791622004.jpg
     * formated_shop_price : ￥7453
     * en_dw : 2件/吨
     */
    public String getPart_unit() {
        return part_unit;
    }

    public void setPart_unit(String part_unit) {
        this.part_unit = part_unit;
    }

    private String part_unit;
    private String rec_id;
    private String order_id;
    private String goods_id;
    private String goods_name;
    private String goods_sn;
    private String product_id;
    private String goods_number;
    private String market_price;
    private String goods_price;
    private String goods_attr;
    private String send_number;
    private String is_real;
    private String extension_code;
    private String parent_id;
    private String is_gift;
    private String goods_attr_id;
    private String total_weight;
    private String is_retail;
    private String measure_unit;
    private String storage;
    private String sku;
    private String brand_sn;
    private String region_name;
    private String port;
    private String goods_local;
    private String user_id;
    private String shop_price_unit;
    private String spec_2;
    private String shop_price;
    private String goods_type;
    private String good_face;
    private double subtotal;
    private int part_number;
    private String part_weight;
    private String formated_goods_price;
    private String formated_subtotal;
    private String goods_thumb;
    private String formated_shop_price;
    private String en_dw;

    public String getRec_id() {
        return rec_id;
    }

    public void setRec_id(String rec_id) {
        this.rec_id = rec_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getGoods_sn() {
        return goods_sn;
    }

    public void setGoods_sn(String goods_sn) {
        this.goods_sn = goods_sn;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getGoods_number() {
        return goods_number;
    }

    public void setGoods_number(String goods_number) {
        this.goods_number = goods_number;
    }

    public String getMarket_price() {
        return market_price;
    }

    public void setMarket_price(String market_price) {
        this.market_price = market_price;
    }

    public String getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(String goods_price) {
        this.goods_price = goods_price;
    }

    public String getGoods_attr() {
        return goods_attr;
    }

    public void setGoods_attr(String goods_attr) {
        this.goods_attr = goods_attr;
    }

    public String getSend_number() {
        return send_number;
    }

    public void setSend_number(String send_number) {
        this.send_number = send_number;
    }

    public String getIs_real() {
        return is_real;
    }

    public void setIs_real(String is_real) {
        this.is_real = is_real;
    }

    public String getExtension_code() {
        return extension_code;
    }

    public void setExtension_code(String extension_code) {
        this.extension_code = extension_code;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getIs_gift() {
        return is_gift;
    }

    public void setIs_gift(String is_gift) {
        this.is_gift = is_gift;
    }

    public String getGoods_attr_id() {
        return goods_attr_id;
    }

    public void setGoods_attr_id(String goods_attr_id) {
        this.goods_attr_id = goods_attr_id;
    }

    public String getTotal_weight() {
        return total_weight;
    }

    public void setTotal_weight(String total_weight) {
        this.total_weight = total_weight;
    }

    public String getIs_retail() {
        return is_retail;
    }

    public void setIs_retail(String is_retail) {
        this.is_retail = is_retail;
    }

    public String getMeasure_unit() {
        return measure_unit;
    }

    public void setMeasure_unit(String measure_unit) {
        this.measure_unit = measure_unit;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getBrand_sn() {
        return brand_sn;
    }

    public void setBrand_sn(String brand_sn) {
        this.brand_sn = brand_sn;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getGoods_local() {
        return goods_local;
    }

    public void setGoods_local(String goods_local) {
        this.goods_local = goods_local;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getShop_price_unit() {
        return shop_price_unit;
    }

    public void setShop_price_unit(String shop_price_unit) {
        this.shop_price_unit = shop_price_unit;
    }

    public String getSpec_2() {
        return spec_2;
    }

    public void setSpec_2(String spec_2) {
        this.spec_2 = spec_2;
    }

    public String getShop_price() {
        return shop_price;
    }

    public void setShop_price(String shop_price) {
        this.shop_price = shop_price;
    }

    public String getGoods_type() {
        return goods_type;
    }

    public void setGoods_type(String goods_type) {
        this.goods_type = goods_type;
    }

    public String getGood_face() {
        return good_face;
    }

    public void setGood_face(String good_face) {
        this.good_face = good_face;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public int getPart_number() {
        return part_number;
    }

    public void setPart_number(int part_number) {
        this.part_number = part_number;
    }

    public String getPart_weight() {
        return part_weight;
    }

    public void setPart_weight(String part_weight) {
        this.part_weight = part_weight;
    }

    public String getFormated_goods_price() {
        return formated_goods_price;
    }

    public void setFormated_goods_price(String formated_goods_price) {
        this.formated_goods_price = formated_goods_price;
    }

    public String getFormated_subtotal() {
        return formated_subtotal;
    }

    public void setFormated_subtotal(String formated_subtotal) {
        this.formated_subtotal = formated_subtotal;
    }

    public String getGoods_thumb() {
        return goods_thumb;
    }

    public void setGoods_thumb(String goods_thumb) {
        this.goods_thumb = goods_thumb;
    }

    public String getFormated_shop_price() {
        return formated_shop_price;
    }

    public void setFormated_shop_price(String formated_shop_price) {
        this.formated_shop_price = formated_shop_price;
    }

    public String getEn_dw() {
        return en_dw;
    }

    public void setEn_dw(String en_dw) {
        this.en_dw = en_dw;
    }
}
