package taiyang.com.entity;

import java.util.List;

/**
 * Created by admin on 2016/8/17.
 */
public class MessageListModel {

    /**
     * record_count : 1
     * page_count : 1
     * page : 1
     */

    private PageModel total;
    /**
     * msg_content : 测试一下吧
     * msg_time : 2016-08-17 13:40:45
     * msg_type : 留言
     * msg_title : 测试
     * msg_id : 17
     */

    private List<MessageModel> list;

    public PageModel getTotal() {
        return total;
    }

    public void setTotal(PageModel total) {
        this.total = total;
    }

    public List<MessageModel> getList() {
        return list;
    }

    public void setList(List<MessageModel> list) {
        this.list = list;
    }



}
