package taiyang.com.entity;

import java.util.List;

import taiyang.com.adapter.Entity;

/**
 * Created by admin on 2016/8/16.
 * 报盘管理
 */
public class BankAccountModel {


    /**
     * shop : {"user_id":"4","bank_switch":"1","future":"15","spot":"15"}
     * bank_list : [{"id":"15","user_id":"4","username":"王尼玛呵呵","account":"2134564651231","deposit_bank":"招商银行","mobile":"18226613958","add_time":"2017-09-24 13:46:48","is_spot":"1","is_future":"1"},{"id":"16","user_id":"4","username":"仇尼玛","account":"8979456465466544566","deposit_bank":"中国工商银行分行","mobile":"18226613999","add_time":"2017-09-24 13:47:09","is_spot":"0","is_future":"0"},{"id":"17","user_id":"4","username":"wangz","account":"123123123","deposit_bank":"zhaoshang","mobile":"13295609110","add_time":"2017-09-24 15:53:26","is_spot":"0","is_future":"0"},{"id":"18","user_id":"4","username":"zhanghuxingming","account":"123123","deposit_bank":"kaihuhang","mobile":"13295609110","add_time":"2017-09-24 16:02:30","is_spot":"0","is_future":"0"},{"id":"19","user_id":"4","username":"xingming","account":"505512341234","deposit_bank":"yinhang","mobile":"13312341234","add_time":"2017-09-24 16:14:58","is_spot":"0","is_future":"0"},{"id":"21","user_id":"4","username":"qiutao","account":"12345678909876","deposit_bank":"hefeiyinhang","mobile":"18709869677","add_time":"2017-10-11 09:36:08","is_spot":"0","is_future":"0"},{"id":"28","user_id":"4","username":"1111王尼玛","account":"2134564651231","deposit_bank":"招商银行","mobile":"18226613958","add_time":"2017-10-14 10:43:54","is_spot":"0","is_future":"0"},{"id":"29","user_id":"4","username":"王尼玛22222222","account":"2134564651231","deposit_bank":"招商银行","mobile":"18226613958","add_time":"2017-10-14 10:44:29","is_spot":"0","is_future":"0"}]
     * bank_count : 8
     * goods_amount_count : 25
     */

    private ShopBean shop;
    private int bank_count;
    private String goods_amount_count;
    private List<BankListBean> bank_list;

    public ShopBean getShop() {
        return shop;
    }

    public void setShop(ShopBean shop) {
        this.shop = shop;
    }

    public int getBank_count() {
        return bank_count;
    }

    public void setBank_count(int bank_count) {
        this.bank_count = bank_count;
    }

    public String getGoods_amount_count() {
        return goods_amount_count;
    }

    public void setGoods_amount_count(String goods_amount_count) {
        this.goods_amount_count = goods_amount_count;
    }

    public List<BankListBean> getBank_list() {
        return bank_list;
    }

    public void setBank_list(List<BankListBean> bank_list) {
        this.bank_list = bank_list;
    }

    public static class ShopBean {
        /**
         * user_id : 4
         * bank_switch : 1
         * future : 15
         * spot : 15
         */

        private String user_id;
        private String bank_switch;
        private String future;
        private String spot;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getBank_switch() {
            return bank_switch;
        }

        public void setBank_switch(String bank_switch) {
            this.bank_switch = bank_switch;
        }

        public String getFuture() {
            return future;
        }

        public void setFuture(String future) {
            this.future = future;
        }

        public String getSpot() {
            return spot;
        }

        public void setSpot(String spot) {
            this.spot = spot;
        }
    }

    public static class BankListBean extends Entity{
        /**
         * id : 15
         * user_id : 4
         * username : 王尼玛呵呵
         * account : 2134564651231
         * deposit_bank : 招商银行
         * mobile : 18226613958
         * add_time : 2017-09-24 13:46:48
         * is_spot : 1
         * is_future : 1
         */

        private String id;
        private String user_id;
        private String username;
        private String account;
        private String deposit_bank;
        private String mobile;
        private String add_time;
        private String is_spot;
        private String is_future;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getDeposit_bank() {
            return deposit_bank;
        }

        public void setDeposit_bank(String deposit_bank) {
            this.deposit_bank = deposit_bank;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getIs_spot() {
            return is_spot;
        }

        public void setIs_spot(String is_spot) {
            this.is_spot = is_spot;
        }

        public String getIs_future() {
            return is_future;
        }

        public void setIs_future(String is_future) {
            this.is_future = is_future;
        }
    }
}
