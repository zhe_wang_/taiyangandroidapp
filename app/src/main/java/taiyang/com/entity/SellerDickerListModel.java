package taiyang.com.entity;

import java.util.List;

import taiyang.com.adapter.Entity;

/**
 * Created by admin on 2016/8/16.
 * 我的还价
 */
public class SellerDickerListModel {


    /**
     * list : [{"goods_id":"1672","goods_name":"猪整头","shop_price":"￥123123","brand_sn":"350","base_id":"10","base_ids":"","huckster":"2","good_face":"","goods_type":"7","sell_type":"4","offer":"11","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/10_thumb_G_14763233280018.jpg","region_name":"【AR】阿根廷","port_name":"","goods_type_name":"现货","offer_name":"还价报盘","sell_type_name":"零售","huck_list":[{"last_price":"100000000.00","message":"123123","price":"123123.00","id":"560","created_at":"2017-04-27 09:51"}]}]
     * total : {"record_count":"3","page_count":1,"page":1}
     */

    private TotalBean total;
    private List<ListBean> list;

    public TotalBean getTotal() {
        return total;
    }

    public void setTotal(TotalBean total) {
        this.total = total;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class TotalBean {
        /**
         * record_count : 3
         * page_count : 1
         * page : 1
         */

        private String record_count;
        private int page_count;
        private int page;

        public String getRecord_count() {
            return record_count;
        }

        public void setRecord_count(String record_count) {
            this.record_count = record_count;
        }

        public int getPage_count() {
            return page_count;
        }

        public void setPage_count(int page_count) {
            this.page_count = page_count;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }
    }

    public static class ListBean extends Entity {
        /**
         * goods_id : 1672
         * goods_name : 猪整头
         * shop_price : ￥123123
         * brand_sn : 350
         * base_id : 10
         * base_ids :
         * huckster : 2
         * good_face :
         * goods_type : 7
         * sell_type : 4
         * offer : 11
         * goods_thumb : http://test.taiyanggo.com/images/201610/thumb_img/10_thumb_G_14763233280018.jpg
         * region_name : 【AR】阿根廷
         * port_name :
         * goods_type_name : 现货
         * offer_name : 还价报盘
         * sell_type_name : 零售
         * huck_list : [{"last_price":"100000000.00","message":"123123","price":"123123.00","id":"560","created_at":"2017-04-27 09:51"}]
         */

        private String goods_id;
        private String goods_name;
        private String shop_price;
        private String brand_sn;
        private String base_id;
        private String base_ids;
        private String huckster;
        private String good_face;
        private String goods_type;
        private String sell_type;
        private String offer;
        private String goods_thumb;
        private String region_name;
        private String port_name;
        private String goods_type_name;
        private String offer_name;
        private String sell_type_name;
        private List<HuckListBean> huck_list;

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getShop_price() {
            return shop_price;
        }

        public void setShop_price(String shop_price) {
            this.shop_price = shop_price;
        }

        public String getBrand_sn() {
            return brand_sn;
        }

        public void setBrand_sn(String brand_sn) {
            this.brand_sn = brand_sn;
        }

        public String getBase_id() {
            return base_id;
        }

        public void setBase_id(String base_id) {
            this.base_id = base_id;
        }

        public String getBase_ids() {
            return base_ids;
        }

        public void setBase_ids(String base_ids) {
            this.base_ids = base_ids;
        }

        public String getHuckster() {
            return huckster;
        }

        public void setHuckster(String huckster) {
            this.huckster = huckster;
        }

        public String getGood_face() {
            return good_face;
        }

        public void setGood_face(String good_face) {
            this.good_face = good_face;
        }

        public String getGoods_type() {
            return goods_type;
        }

        public void setGoods_type(String goods_type) {
            this.goods_type = goods_type;
        }

        public String getSell_type() {
            return sell_type;
        }

        public void setSell_type(String sell_type) {
            this.sell_type = sell_type;
        }

        public String getOffer() {
            return offer;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

        public String getGoods_thumb() {
            return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
            this.goods_thumb = goods_thumb;
        }

        public String getRegion_name() {
            return region_name;
        }

        public void setRegion_name(String region_name) {
            this.region_name = region_name;
        }

        public String getPort_name() {
            return port_name;
        }

        public void setPort_name(String port_name) {
            this.port_name = port_name;
        }

        public String getGoods_type_name() {
            return goods_type_name;
        }

        public void setGoods_type_name(String goods_type_name) {
            this.goods_type_name = goods_type_name;
        }

        public String getOffer_name() {
            return offer_name;
        }

        public void setOffer_name(String offer_name) {
            this.offer_name = offer_name;
        }

        public String getSell_type_name() {
            return sell_type_name;
        }

        public void setSell_type_name(String sell_type_name) {
            this.sell_type_name = sell_type_name;
        }

        public List<HuckListBean> getHuck_list() {
            return huck_list;
        }

        public void setHuck_list(List<HuckListBean> huck_list) {
            this.huck_list = huck_list;
        }

        public static class HuckListBean {
            /**
             * last_price : 100000000.00
             * message : 123123
             * price : 123123.00
             * id : 560
             * created_at : 2017-04-27 09:51
             */

            private String last_price;
            private String message;
            private String price;
            private String id;
            private String created_at;

            public String getLast_price() {
                return last_price;
            }

            public void setLast_price(String last_price) {
                this.last_price = last_price;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }
        }
    }
}
