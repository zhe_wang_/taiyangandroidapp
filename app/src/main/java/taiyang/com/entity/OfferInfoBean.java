package taiyang.com.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2016/8/9.
 */
public class OfferInfoBean implements Serializable {

        public String goods_id;
        public String shop_id;
        private String brand_sn;
        private String formated_arrive_date;
        private String formated_lading_date;
        private String goods_local;
        private String goods_name;
        private String goods_number;
        private String currency;
        private String shop_price_formated;
        private String formated_shop_price_am;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getShop_price_formated() {
        return shop_price_formated;
    }

    public void setShop_price_formated(String shop_price_formated) {
        this.shop_price_formated = shop_price_formated;
    }

    public String getFormated_shop_price_am() {
        return formated_shop_price_am;
    }

    public void setFormated_shop_price_am(String formated_shop_price_am) {
        this.formated_shop_price_am = formated_shop_price_am;
    }

    public String getFormated_shop_price_eur() {
        return formated_shop_price_eur;
    }

    public void setFormated_shop_price_eur(String formated_shop_price_eur) {
        this.formated_shop_price_eur = formated_shop_price_eur;
    }

    private String formated_shop_price_eur;


        private String goods_thumb;

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    private String last_update;
        private String goods_txt;
        private String goods_type;

    public OfferInfoBean() {
    }

    public String getShop_price_unit() {
        return shop_price_unit;
    }

    public void setShop_price_unit(String shop_price_unit) {
        this.shop_price_unit = shop_price_unit;
    }

    private String shop_price_unit;
        private String goods_weight;
        private int is_pin;
        private String offer;
        private String offer_type;
        private String pack_name;

        private String port_name;
        private String prepay_name;
        public String prepay_type;
        public String prepay_num;
        private String region_name;


    public String getMake_date() {
        return make_date;
    }

    public void setMake_date(String make_date) {
        this.make_date = make_date;
    }

    private String make_date;
        private String sell_type;
        private String shop_name;
        private float shop_price;
        private String shop_price_am;
        private String sku;
        private String spec_1;
        private String spec_1_unit;
        private String spec_2;
    /**
     * prepay_name : 100
     * shop_price : 7452.60
     * shop_price_unit : 吨
     * prepay_type : 0
     * prepay_num : 0
     * goods_base : [{"region":"25","region_name":"【ID】印尼","brand_ids":"692","brand_name":"100/200","brand_sn":"100/200","brand_logo":"http://test.taiyanggo.com/images/no_picture.gif","base_ids":"146","base_ids_name":null,"goods_names":"鲍鱼","goods_images":"http://test.taiyanggo.com/Array","specs_1":"100","specs_1_unit":"KG/件","specs_2":"10","specs_2_unit":"件/吨","specs_3":"1","specs_3_unit":"吨/整柜","packs":"8","packs_name":"定装","make_date":"","spec_txts":null,"cat_ids":"227","cat_id_first":"5"},{"region":"15","region_name":"【AU】澳大利亚","brand_ids":"252","brand_name":"218","brand_sn":"218","brand_logo":"http://test.taiyanggo.com/images/no_picture.gif","base_ids":"159","base_ids_name":null,"goods_names":"田螺","goods_images":"http://test.taiyanggo.com/images/no_picture.gif","specs_1":"100","specs_1_unit":"KG/件","specs_2":"10","specs_2_unit":"件/吨","specs_3":"1","specs_3_unit":"吨/整柜","packs":"8","packs_name":"定装","make_date":"","spec_txts":null,"cat_ids":"227","cat_id_first":"5"},{"region":"5","region_name":"【HUN】匈牙利","brand_ids":"556","brand_name":"HU131EK","brand_sn":"HU131EK","brand_logo":"http://test.taiyanggo.com/images/no_picture.gif","base_ids":"162","base_ids_name":null,"goods_names":"马蹄螺","goods_images":"http://test.taiyanggo.com/images/no_picture.gif","specs_1":"100","specs_1_unit":"KG/件","specs_2":"10","specs_2_unit":"件/吨","specs_3":"1","specs_3_unit":"吨/整柜","packs":"8","packs_name":"定装","make_date":"","spec_txts":null,"cat_ids":"227","cat_id_first":"5"},{"region":"17","region_name":"【MN】蒙古","brand_ids":"405","brand_name":"5470099","brand_sn":"5470099","brand_logo":"http://test.taiyanggo.com/images/no_picture.gif","base_ids":"199","base_ids_name":null,"goods_names":"中国龙虾","goods_images":"http://test.taiyanggo.com/images/no_picture.gif","specs_1":"100","specs_1_unit":"KG/件","specs_2":"10","specs_2_unit":"件/吨","specs_3":"1","specs_3_unit":"吨/整柜","packs":"8","packs_name":"定装","make_date":"","spec_txts":null,"cat_ids":"230","cat_id_first":"5"}]
     * picture_list : []
     * bp_list : []
     * shop_goods_num : 28
     * shop_order_num : 0
     * shop_logo :
     * follow_count : 0
     * goods_base_list : [{"goods_id":"1725","goods_name":"","goods_number":"12","shop_price":"￥29800.00","goods_type":"7","goods_thumb":"http://test.taiyanggo.com/images/no_picture.gif","base_id":"0","shop_price_unit":"吨","currency":"1","shop_price_eur":"0.00","shop_price_am":"4492.82","spec_1":"","spec_2":""},{"goods_id":"1719","goods_name":"","goods_number":"165","shop_price":"\u20ac1000","goods_type":"6","goods_thumb":"http://test.taiyanggo.com/images/no_picture.gif","base_id":"0","shop_price_unit":"吨","currency":"3","shop_price_eur":"1000.00","shop_price_am":"0.00","spec_1":"","spec_2":""},{"goods_id":"1722","goods_name":"羊脖肉","goods_number":"256","shop_price":"$2000","goods_type":"6","goods_thumb":"http://test.taiyanggo.com/images/no_picture.gif","base_id":"1112","shop_price_unit":"件","currency":"2","shop_price_eur":"0.00","shop_price_am":"10000.00","spec_1":"200","spec_2":"5"}]
     */

    private String shop_goods_num;
    private String shop_order_num;

    public String getShop_goods_num() {
        return shop_goods_num;
    }

    public String getShop_order_num() {
        return shop_order_num;
    }

    public String getShop_logo() {
        return shop_logo;
    }

    public String getFollow_count() {
        return follow_count;
    }

    private String shop_logo;
    private String follow_count;

    public String getFormated_shop_price() {
        return formated_shop_price;
    }

    private String formated_shop_price;
        private String spec_2_unit;
        private List<BpListBean> bp_list;
        private List<PictureListBean> picture_list;
        private List<HuckListBean> huck_list;
    private List<GoodsBaseListBean> goods_base_list;
    private List<GoodsBaseBean> goods_base;

    public List<GoodsBaseListBean> getGoods_base_list() {
        return goods_base_list;
    }

    public void setGoods_base_list(List<GoodsBaseListBean> goods_base_list) {
        this.goods_base_list = goods_base_list;
    }

    public List<GoodsBaseBean> getGoods_base() {
        return goods_base;
    }

    public void setGoods_base(List<GoodsBaseBean> goods_base) {
        this.goods_base = goods_base;
    }

    public static class HuckListBean {
        private String created_at;
        private String formated_price;
        private String user_name;
        public String unit;

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getFormated_price() {
            return formated_price;
        }

        public void setFormated_price(String formated_price) {
            this.formated_price = formated_price;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }
    }

        public String getBrand_sn() {
            return brand_sn;
        }

        public void setBrand_sn(String brand_sn) {
            this.brand_sn = brand_sn;
        }

        public String getFormated_arrive_date() {
            return formated_arrive_date;
        }

        public void setFormated_arrive_date(String formated_arrive_date) {
            this.formated_arrive_date = formated_arrive_date;
        }

        public String getFormated_lading_date() {
            return formated_lading_date;
        }

        public void setFormated_lading_date(String formated_lading_date) {
            this.formated_lading_date = formated_lading_date;
        }

        public String getGoods_local() {
            return goods_local;
        }

        public void setGoods_local(String goods_local) {
            this.goods_local = goods_local;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_number() {
            return goods_number;
        }

        public void setGoods_number(String goods_number) {
            this.goods_number = goods_number;
        }

        public String getGoods_thumb() {
            return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
            this.goods_thumb = goods_thumb;
        }

        public String getGoods_txt() {
            return goods_txt;
        }

        public void setGoods_txt(String goods_txt) {
            this.goods_txt = goods_txt;
        }

        public String getGoods_type() {
            return goods_type;
        }

        public void setGoods_type(String goods_type) {
            this.goods_type = goods_type;
        }


        public String getGoods_weight() {
            return goods_weight;
        }

        public void setGoods_weight(String goods_weight) {
            this.goods_weight = goods_weight;
        }

        public int getIs_pin() {
            return is_pin;
        }

        public void setIs_pin(int is_pin) {
            this.is_pin = is_pin;
        }

        public String getOffer() {
            return offer;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

        public String getOffer_type() {
            return offer_type;
        }

        public void setOffer_type(String offer_type) {
            this.offer_type = offer_type;
        }

        public String getPack_name() {
            return pack_name;
        }

        public void setPack_name(String pack_name) {
            this.pack_name = pack_name;
        }

        public String getPort_name() {
            return port_name;
        }

        public void setPort_name(String port_name) {
            this.port_name = port_name;
        }

        public String getPrepay_name() {
            return prepay_name;
        }

        public void setPrepay_name(String prepay_name) {
            this.prepay_name = prepay_name;
        }

        public String getRegion_name() {
            return region_name;
        }

        public void setRegion_name(String region_name) {
            this.region_name = region_name;
        }

        public String getSell_type() {
            return sell_type;
        }

        public void setSell_type(String sell_type) {
            this.sell_type = sell_type;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public float getShop_price() {
            return shop_price;
        }

        public void setShop_price(float shop_price) {
            this.shop_price = shop_price;
        }

        public String getShop_price_am() {
            return shop_price_am;
        }

        public void setShop_price_am(String shop_price_am) {
            this.shop_price_am = shop_price_am;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getSpec_1() {
            return spec_1;
        }

        public void setSpec_1(String spec_1) {
            this.spec_1 = spec_1;
        }

        public String getSpec_1_unit() {
            return spec_1_unit;
        }

        public void setSpec_1_unit(String spec_1_unit) {
            this.spec_1_unit = spec_1_unit;
        }

        public String getSpec_2() {
            return spec_2;
        }

        public void setSpec_2(String spec_2) {
            this.spec_2 = spec_2;
        }

        public String getSpec_2_unit() {
            return spec_2_unit;
        }

        public void setSpec_2_unit(String spec_2_unit) {
            this.spec_2_unit = spec_2_unit;
        }

        public List<BpListBean> getBp_list() {
            return bp_list;
        }

        public void setBp_list(List<BpListBean> bp_list) {
            this.bp_list = bp_list;
        }

        public List<HuckListBean> getHuck_list() {
            return huck_list;
        }

        public List<PictureListBean> getPicture_list() {
            return picture_list;
        }

        public void setPicture_list(List<PictureListBean> picture_list) {
            this.picture_list = picture_list;
        }

    public static class PictureListBean implements Serializable{
        private String id;
        private String thumb_url;
        private String img_url;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getThumb_url() {
            return thumb_url;
        }

        public void setThumb_url(String goods_price) {
            this.thumb_url = thumb_url;
        }

        public String getImg_url() {
            return img_url;
        }

        public void setImg_url(String img_url) {
            this.img_url = img_url;
        }

    }

        public static class BpListBean implements Serializable{
            private String add_time;
            private String goods_amount;
            private int rate;

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public String getGoods_price() {
                return goods_amount;
            }

            public void setGoods_price(String goods_price) {
                this.goods_amount = goods_price;
            }

            public int getRate() {
                return rate;
            }

            public void setRate(int rate) {
                this.rate = rate;
            }

        }


    public static class GoodsBaseListBean {
        /**
         * goods_id : 1725
         * goods_name :
         * goods_number : 12
         * shop_price : ￥29800
         * goods_type : 7
         * goods_thumb : http://test.taiyanggo.com/images/no_picture.gif
         * base_id : 0
         */

        @SerializedName("goods_id")
        private String goods_idX;
        @SerializedName("goods_name")
        private String goods_nameX;
        @SerializedName("goods_number")
        private String goods_numberX;
        @SerializedName("shop_price")
        private String shop_priceX;
        @SerializedName("goods_type")
        private String goods_typeX;
        @SerializedName("goods_thumb")
        private String goods_thumbX;
        private String base_id;

        public String getShop_price_unit() {
            return shop_price_unit;
        }

        private String shop_price_unit;
        public String getGoods_idX() {
            return goods_idX;
        }

        public void setGoods_idX(String goods_idX) {
            this.goods_idX = goods_idX;
        }

        public String getGoods_nameX() {
            return goods_nameX;
        }

        public void setGoods_nameX(String goods_nameX) {
            this.goods_nameX = goods_nameX;
        }

        public String getGoods_numberX() {
            return goods_numberX;
        }

        public void setGoods_numberX(String goods_numberX) {
            this.goods_numberX = goods_numberX;
        }

        public String getShop_priceX() {
            return shop_priceX;
        }

        public void setShop_priceX(String shop_priceX) {
            this.shop_priceX = shop_priceX;
        }

        public String getGoods_typeX() {
            return goods_typeX;
        }

        public void setGoods_typeX(String goods_typeX) {
            this.goods_typeX = goods_typeX;
        }

        public String getGoods_thumbX() {
            return goods_thumbX;
        }

        public void setGoods_thumbX(String goods_thumbX) {
            this.goods_thumbX = goods_thumbX;
        }

        public String getBase_id() {
            return base_id;
        }

        public void setBase_id(String base_id) {
            this.base_id = base_id;
        }
    }

    public static class GoodsBaseBean {
        /**
         * region : 25
         * region_name : 【ID】印尼
         * brand_ids : 692
         * brand_name : 100/200
         * brand_sn : 100/200
         * brand_logo : http://test.taiyanggo.com/images/no_picture.gif
         * base_ids : 146
         * base_ids_name : null
         * goods_names : 鲍鱼
         * goods_images : http://test.taiyanggo.com/Array
         * specs_1 : 100
         * specs_1_unit : KG/件
         * specs_2 : 10
         * specs_2_unit : 件/吨
         * specs_3 : 1
         * specs_3_unit : 吨/整柜
         * packs : 8
         * packs_name : 定装
         * make_date :
         * spec_txts : null
         * cat_ids : 227
         * cat_id_first : 5
         */

        private String region;
        @SerializedName("region_name")
        private String region_nameX;
        private String brand_ids;
        private String brand_name;
        @SerializedName("brand_sn")
        private String brand_snX;
        private String brand_logo;
        private String base_ids;
        private Object base_ids_name;
        private String goods_names;
        private String goods_images;
        private String specs_1;
        private String specs_1_unit;
        private String specs_2;
        private String specs_2_unit;
        private String formated_make_date;

        public String getFormated_make_date() {
            return formated_make_date;
        }

        private String specs_3;
        private String specs_3_unit;
        private String packs;
        private String packs_name;
        private String make_date;
        private Object spec_txts;
        private String cat_ids;
        private String cat_id_first;

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getRegion_nameX() {
            return region_nameX;
        }

        public void setRegion_nameX(String region_nameX) {
            this.region_nameX = region_nameX;
        }

        public String getBrand_ids() {
            return brand_ids;
        }

        public void setBrand_ids(String brand_ids) {
            this.brand_ids = brand_ids;
        }

        public String getBrand_name() {
            return brand_name;
        }

        public void setBrand_name(String brand_name) {
            this.brand_name = brand_name;
        }

        public String getBrand_snX() {
            return brand_snX;
        }

        public void setBrand_snX(String brand_snX) {
            this.brand_snX = brand_snX;
        }

        public String getBrand_logo() {
            return brand_logo;
        }

        public void setBrand_logo(String brand_logo) {
            this.brand_logo = brand_logo;
        }

        public String getBase_ids() {
            return base_ids;
        }

        public void setBase_ids(String base_ids) {
            this.base_ids = base_ids;
        }

        public Object getBase_ids_name() {
            return base_ids_name;
        }

        public void setBase_ids_name(Object base_ids_name) {
            this.base_ids_name = base_ids_name;
        }

        public String getGoods_names() {
            return goods_names;
        }

        public void setGoods_names(String goods_names) {
            this.goods_names = goods_names;
        }

        public String getGoods_images() {
            return goods_images;
        }

        public void setGoods_images(String goods_images) {
            this.goods_images = goods_images;
        }

        public String getSpecs_1() {
            return specs_1;
        }

        public void setSpecs_1(String specs_1) {
            this.specs_1 = specs_1;
        }

        public String getSpecs_1_unit() {
            return specs_1_unit;
        }

        public void setSpecs_1_unit(String specs_1_unit) {
            this.specs_1_unit = specs_1_unit;
        }

        public String getSpecs_2() {
            return specs_2;
        }

        public void setSpecs_2(String specs_2) {
            this.specs_2 = specs_2;
        }

        public String getSpecs_2_unit() {
            return specs_2_unit;
        }

        public void setSpecs_2_unit(String specs_2_unit) {
            this.specs_2_unit = specs_2_unit;
        }

        public String getSpecs_3() {
            return specs_3;
        }

        public void setSpecs_3(String specs_3) {
            this.specs_3 = specs_3;
        }

        public String getSpecs_3_unit() {
            return specs_3_unit;
        }

        public void setSpecs_3_unit(String specs_3_unit) {
            this.specs_3_unit = specs_3_unit;
        }

        public String getPacks() {
            return packs;
        }

        public void setPacks(String packs) {
            this.packs = packs;
        }

        public String getPacks_name() {
            return packs_name;
        }

        public void setPacks_name(String packs_name) {
            this.packs_name = packs_name;
        }

        public String getMake_date() {
            return make_date;
        }

        public void setMake_date(String make_date) {
            this.make_date = make_date;
        }

        public Object getSpec_txts() {
            return spec_txts;
        }

        public void setSpec_txts(Object spec_txts) {
            this.spec_txts = spec_txts;
        }

        public String getCat_ids() {
            return cat_ids;
        }

        public void setCat_ids(String cat_ids) {
            this.cat_ids = cat_ids;
        }

        public String getCat_id_first() {
            return cat_id_first;
        }

        public void setCat_id_first(String cat_id_first) {
            this.cat_id_first = cat_id_first;
        }
    }
}
