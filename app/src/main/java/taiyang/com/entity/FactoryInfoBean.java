package taiyang.com.entity;

/**
 * Created by Administrator on 2016/8/16.
 */
public class FactoryInfoBean {

        private String brand_content;
        private String brand_desc;
        private String brand_id;
        private String brand_logo;
        private String brand_name;
        private String brand_sn;
        private String id;
        private String is_show;
        private String sid;
        private String site_icon;
        private String site_name;
        private String site_name_en;
        private String site_url;
        private String sort_order;

        public String getBrand_content() {
            return brand_content;
        }

        public void setBrand_content(String brand_content) {
            this.brand_content = brand_content;
        }

        public String getBrand_desc() {
            return brand_desc;
        }

        public void setBrand_desc(String brand_desc) {
            this.brand_desc = brand_desc;
        }

        public String getBrand_id() {
            return brand_id;
        }

        public void setBrand_id(String brand_id) {
            this.brand_id = brand_id;
        }

        public String getBrand_logo() {
            return brand_logo;
        }

        public void setBrand_logo(String brand_logo) {
            this.brand_logo = brand_logo;
        }

        public String getBrand_name() {
            return brand_name;
        }

        public void setBrand_name(String brand_name) {
            this.brand_name = brand_name;
        }

        public String getBrand_sn() {
            return brand_sn;
        }

        public void setBrand_sn(String brand_sn) {
            this.brand_sn = brand_sn;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIs_show() {
            return is_show;
        }

        public void setIs_show(String is_show) {
            this.is_show = is_show;
        }

        public String getSid() {
            return sid;
        }

        public void setSid(String sid) {
            this.sid = sid;
        }

        public String getSite_icon() {
            return site_icon;
        }

        public void setSite_icon(String site_icon) {
            this.site_icon = site_icon;
        }

        public String getSite_name() {
            return site_name;
        }

        public void setSite_name(String site_name) {
            this.site_name = site_name;
        }

        public String getSite_name_en() {
            return site_name_en;
        }

        public void setSite_name_en(String site_name_en) {
            this.site_name_en = site_name_en;
        }

        public String getSite_url() {
            return site_url;
        }

        public void setSite_url(String site_url) {
            this.site_url = site_url;
        }

        public String getSort_order() {
            return sort_order;
        }

        public void setSort_order(String sort_order) {
            this.sort_order = sort_order;
        }
}
