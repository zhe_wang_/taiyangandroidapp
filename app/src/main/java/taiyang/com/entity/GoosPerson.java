package taiyang.com.entity;

import java.io.Serializable;

/**
 * Created by admin on 2016/8/23.
 * 收货人
 */
public class GoosPerson implements Serializable {

    /**
     * created : 2016-08-23 00:00:00
     * id : 11
     * id_number : 220284198905212311
     * mobile : 2147483647
     * name : 吕庆峰
     * type : 0
     * user_id : 11
     */

    private String created;
    private String id;
    private String id_number;
    private String mobile;
    private String name;
    private String type;
    private String user_id;

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
