package taiyang.com.entity;

import com.bigkoo.pickerview.model.IPickerViewData;

import java.util.List;

public class ProvinceModel implements IPickerViewData {
    /**
     * id : 2
     * name : 北京
     * city_list : [{"id":"52","name":"北京"}]
     */

    private String id;
    private String name;
    /**
     * id : 52
     * name : 北京
     */

    private List<CityModel> city_list;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CityModel> getCity_list() {
        return city_list;
    }

    public void setCity_list(List<CityModel> city_list) {
        this.city_list = city_list;
    }

    @Override
    public String getPickerViewText() {
        return name;
    }
}
