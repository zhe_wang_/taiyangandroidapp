package taiyang.com.entity;

/**
 * Created by heng on 2016/8/19.
 */
public class FactorGoodsModel {

        public String spec_1;
        public String spec_1_unit;
        private String goods_number;
        private String goods_id;
        private String goods_name;
        private String brand_sn;
        private String shop_price;
        private String shop_price_fake;
        private String goods_local;
        private String goods_type;
        private String sell_type;
        private String offer;
    public String getArrive_count() {
        return arrive_count;
    }

    public void setArrive_count(String arrive_count) {
        this.arrive_count = arrive_count;
    }

    private String arrive_count;
        private String goods_thumb;
        private int is_pin;
        private String region_name_ch;
        private String unit_name;
        private String picture;

        public String getRegion_name() {
            return region_name;
        }

        public void setRegion_name(String region_name) {
            this.region_name = region_name;
        }

        private String region_name;
        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_number() {
            return goods_number;
        }

        public void setGoods_number(String goods_number) {
            this.goods_number = goods_number;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getBrand_sn() {
            return brand_sn;
        }

        public void setBrand_sn(String brand_sn) {
            this.brand_sn = brand_sn;
        }

        public String getShop_price() {
            return shop_price;
        }

        public void setShop_price(String shop_price) {
            this.shop_price = shop_price;
        }

        public String getGoods_local() {
            return goods_local;
        }

        public void setGoods_local(String goods_local) {
            this.goods_local = goods_local;
        }

        public String getGoods_type() {
            return goods_type;
        }

        public void setGoods_type(String goods_type) {
            this.goods_type = goods_type;
        }

        public String getSell_type() {
            return sell_type;
        }

        public void setSell_type(String sell_type) {
            this.sell_type = sell_type;
        }

        public String getOffer() {
            return offer;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

        public String getGoods_thumb() {
            return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
            this.goods_thumb = goods_thumb;
        }

        public int getIs_pin() {
            return is_pin;
        }

        public void setIs_pin(int is_pin) {
            this.is_pin = is_pin;
        }

        public String getRegion_name_ch() {
            return region_name_ch;
        }

        public void setRegion_name_ch(String region_name_ch) {
            this.region_name_ch = region_name_ch;
        }

        public String getUnit_name() {
            return unit_name;
        }

        public void setUnit_name(String unit_name) {
            this.unit_name = unit_name;
        }

        public String getShop_price_fake() {
            return shop_price_fake;
        }

        public void setShop_price_fake(String shop_price_fake) {
            this.shop_price_fake = shop_price_fake;
        }

            /*public List<PictureBean> getPicture() {
                return picture;
            }

            public void setPicture(List<PictureBean> picture) {
                this.picture = picture;
            }*/

        public static class PictureBean {
            private String img_id;
            private String img_url;
            private String thumb_url;
            private String img_desc;

            public String getImg_id() {
                return img_id;
            }

            public void setImg_id(String img_id) {
                this.img_id = img_id;
            }

            public String getImg_url() {
                return img_url;
            }

            public void setImg_url(String img_url) {
                this.img_url = img_url;
            }

            public String getThumb_url() {
                return thumb_url;
            }

            public void setThumb_url(String thumb_url) {
                this.thumb_url = thumb_url;
            }

            public String getImg_desc() {
                return img_desc;
            }

            public void setImg_desc(String img_desc) {
                this.img_desc = img_desc;
            }
        }
}
