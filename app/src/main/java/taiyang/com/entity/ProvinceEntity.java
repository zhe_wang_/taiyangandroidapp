package taiyang.com.entity;

import java.util.List;

/**
 * Created by admin on 2016/8/12.
 */
public class ProvinceEntity {
    private List<ProvinceModel> provinceModels;

    public List<ProvinceModel> getProvinceModels() {
        return provinceModels;
    }

    public void setProvinceModels(List<ProvinceModel> provinceModels) {
        this.provinceModels = provinceModels;
    }
}
