package taiyang.com.entity;

/**
 * Created by admin on 2016/8/16.
 * 报盘管理
 */
public class SellerOrderModel {


    public GetAddressBean getSend_address() {
        return send_address;
    }

    public void setSend_address(GetAddressBean send_address) {
        this.send_address = send_address;
    }

    public GetAddressBean getGet_address() {
        return get_address;
    }

    public void setGet_address(GetAddressBean get_address) {
        this.get_address = get_address;
    }

    /**
     * order_id : 1708
     * order_sn : 2017061388374
     * user_id : 911
     * order_status : 0
     * shipping_status : 0
     * pay_status : 0
     * consignee :
     * province : 0
     * city : 0
     * district : 0
     * address :
     * zipcode :
     * tel :
     * mobile :
     * email :
     * postscript :
     * shipping_id : 0
     * shipping_name :
     * pay_id : 2
     * pay_name : 银行转帐
     * inv_payee :
     * inv_content :
     * goods_amount : 136.53
     * shipping_fee : 0.00
     * insure_fee : 0.00
     * pay_fee : 0.00
     * money_paid : 0.00
     * surplus : 0.00
     * integral : 0
     * integral_money : 0.00
     * bonus : 0.00
     * order_amount : 40.96
     * amount_last : 95.57
     * add_time : 1497310862
     * confirm_time : 1497310862
     * pay_time : 0
     * shipping_time : 0
     * bonus_id : 0
     * invoice_no :
     * extension_code :
     * extension_id : 0
     * to_buyer :
     * pay_note :
     * inv_type :
     * tax : 0.00
     * parent_id : 0
     * discount : 0.00
     * pay_order_id :
     * order_from : 1
     * action_user :
     * goods_sn :
     * get_address_id : 0
     * pay_type_id : 0
     * send_address_id : 0
     * is_future : 0
     * is_prepay : 1
     * prepay_ratio : 30
     * pay_check : 0
     * is_entrust : 0
     * entrust_order_id : 0
     * entrust_money : 0.00
     * entrust_before : 0
     * buyer_name :
     * buyer_card :
     * buyer_phone :
     * seller_name :
     * seller_phone :
     * stock_status : 0
     * stock_remark :
     * remark :
     * logistics : 0
     * business_status : 0
     * business_reason :
     * total_fee : 136.53
     * pay_total : 0
     * formated_pay_total : ￥0.00
     * pay_total_ratio : 0.00
     * formated_pay_total_ratio : 0%
     * formated_entrust_money : ￥0.00
     * formated_goods_amount : ￥136.53
     * formated_discount : ￥0.00
     * formated_tax : ￥0.00
     * formated_shipping_fee : ￥0.00
     * formated_insure_fee : ￥0.00
     * formated_pay_fee : ￥0.00
     * formated_total_fee : ￥136.53
     * formated_money_paid : ￥0.00
     * formated_bonus : ￥0.00
     * formated_integral_money : ￥0.00
     * formated_surplus : ￥0.00
     * formated_order_amount : ￥40.96
     * formated_add_time : 2017-06-13 15:41:02
     * user_info : {"mobile":"18722651772","name":"小邢","user_name":"18722651772"}
     * order_status_name : 待付款
     * goods_info : {"rec_id":"1708","order_id":"1708","goods_id":"1682","goods_name":"牛脖骨","goods_sn":"","product_id":"0","goods_number":"9","market_price":"123.00","goods_price":"123.00","goods_attr":"","send_number":"0","is_real":"1","extension_code":"","parent_id":"0","is_gift":"0","goods_attr_id":"","total_weight":"1.11","is_retail":"1","measure_unit":"Ctn","storage":"123","sku":"N17052008745","brand_sn":"67","region_name":"【AU】澳大利亚","port":"0","goods_local":"123123","user_id":"4","shop_price_unit":"吨","spec_2":"9","shop_price":"123.00","goods_type":"7","good_face":"images/201705/1495210138607021789.jpg","subtotal":136.53,"part_number":9,"part_weight":"0","formated_goods_price":"￥123.00","formated_subtotal":"￥136.53","goods_thumb":"http://test.taiyanggo.com/images/201705/1495210138607021789.jpg","en_dw":"9件/吨"}
     */

    private GetAddressBean send_address;
    private GetAddressBean get_address;

    public String getBuyer_seller_edit() {
        return buyer_seller_edit;
    }

    public void setBuyer_seller_edit(String buyer_seller_edit) {
        this.buyer_seller_edit = buyer_seller_edit;
    }

    private String buyer_seller_edit;
    public static class GetAddressBean {
        /**
         * name : 哈哈
         * mobile : 18226613958
         * id_number : 545125454541545
         */

        private String name;
        private String mobile;
        private String id_number;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getId_number() {
            return id_number;
        }

        public void setId_number(String id_number) {
            this.id_number = id_number;
        }
    }
    private String order_id;
    private String order_sn;
    private String user_id;
    private String order_status;
    private String shipping_status;
    private String pay_status;
    private String consignee;
    private String province;
    private String city;
    private String district;
    private String address;
    private String zipcode;
    private String tel;
    private String mobile;
    private String email;
    private String postscript;
    private String shipping_id;
    private String shipping_name;
    private String pay_id;
    private String pay_name;
    private String inv_payee;
    private String inv_content;
    private String goods_amount;
    private String shipping_fee;
    private String insure_fee;
    private String pay_fee;
    private String money_paid;
    private String surplus;
    private String integral;
    private String integral_money;
    private String bonus;
    private String order_amount;
    private String amount_last;
    private String add_time;
    private String confirm_time;
    private String pay_time;
    private String shipping_time;
    private String bonus_id;
    private String invoice_no;
    private String extension_code;
    private String extension_id;
    private String to_buyer;
    private String pay_note;
    private String inv_type;
    private String tax;
    private String parent_id;
    private String discount;
    private String pay_order_id;
    private String order_from;
    private String action_user;
    private String goods_sn;
    private String get_address_id;
    private String pay_type_id;
    private String send_address_id;
    private String is_future;
    private String is_prepay;
    private String prepay_ratio;
    private String pay_check;
    private String is_entrust;
    private String entrust_order_id;
    private String entrust_money;
    private String entrust_before;
    private String buyer_name;
    private String buyer_card;
    private String buyer_phone;
    private String seller_name;
    private String seller_phone;
    private String stock_status;
    private String stock_remark;
    private String remark;
    private String logistics;
    private String business_status;
    private String business_reason;
    private String total_fee;
    private String formated_pay_total;
    private String pay_total_ratio;
    private String formated_pay_total_ratio;
    private String formated_entrust_money;
    private String formated_goods_amount;
    private String formated_discount;
    private String formated_tax;
    private String formated_shipping_fee;
    private String formated_insure_fee;
    private String formated_pay_fee;
    private String formated_total_fee;
    private String formated_money_paid;
    private String formated_bonus;
    private String formated_integral_money;
    private String formated_surplus;
    private String formated_order_amount;
    private String formated_add_time;
    private UserInfoBean user_info;
    private String order_status_name;
    private GoodsInfoBean goods_info;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_sn() {
        return order_sn;
    }

    public void setOrder_sn(String order_sn) {
        this.order_sn = order_sn;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getShipping_status() {
        return shipping_status;
    }

    public void setShipping_status(String shipping_status) {
        this.shipping_status = shipping_status;
    }

    public String getPay_status() {
        return pay_status;
    }

    public void setPay_status(String pay_status) {
        this.pay_status = pay_status;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostscript() {
        return postscript;
    }

    public void setPostscript(String postscript) {
        this.postscript = postscript;
    }

    public String getShipping_id() {
        return shipping_id;
    }

    public void setShipping_id(String shipping_id) {
        this.shipping_id = shipping_id;
    }

    public String getShipping_name() {
        return shipping_name;
    }

    public void setShipping_name(String shipping_name) {
        this.shipping_name = shipping_name;
    }

    public String getPay_id() {
        return pay_id;
    }

    public void setPay_id(String pay_id) {
        this.pay_id = pay_id;
    }

    public String getPay_name() {
        return pay_name;
    }

    public void setPay_name(String pay_name) {
        this.pay_name = pay_name;
    }

    public String getInv_payee() {
        return inv_payee;
    }

    public void setInv_payee(String inv_payee) {
        this.inv_payee = inv_payee;
    }

    public String getInv_content() {
        return inv_content;
    }

    public void setInv_content(String inv_content) {
        this.inv_content = inv_content;
    }

    public String getGoods_amount() {
        return goods_amount;
    }

    public void setGoods_amount(String goods_amount) {
        this.goods_amount = goods_amount;
    }

    public String getShipping_fee() {
        return shipping_fee;
    }

    public void setShipping_fee(String shipping_fee) {
        this.shipping_fee = shipping_fee;
    }

    public String getInsure_fee() {
        return insure_fee;
    }

    public void setInsure_fee(String insure_fee) {
        this.insure_fee = insure_fee;
    }

    public String getPay_fee() {
        return pay_fee;
    }

    public void setPay_fee(String pay_fee) {
        this.pay_fee = pay_fee;
    }

    public String getMoney_paid() {
        return money_paid;
    }

    public void setMoney_paid(String money_paid) {
        this.money_paid = money_paid;
    }

    public String getSurplus() {
        return surplus;
    }

    public void setSurplus(String surplus) {
        this.surplus = surplus;
    }

    public String getIntegral() {
        return integral;
    }

    public void setIntegral(String integral) {
        this.integral = integral;
    }

    public String getIntegral_money() {
        return integral_money;
    }

    public void setIntegral_money(String integral_money) {
        this.integral_money = integral_money;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    public String getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(String order_amount) {
        this.order_amount = order_amount;
    }

    public String getAmount_last() {
        return amount_last;
    }

    public void setAmount_last(String amount_last) {
        this.amount_last = amount_last;
    }

    public String getAdd_time() {
        return add_time;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }

    public String getConfirm_time() {
        return confirm_time;
    }

    public void setConfirm_time(String confirm_time) {
        this.confirm_time = confirm_time;
    }

    public String getPay_time() {
        return pay_time;
    }

    public void setPay_time(String pay_time) {
        this.pay_time = pay_time;
    }

    public String getShipping_time() {
        return shipping_time;
    }

    public void setShipping_time(String shipping_time) {
        this.shipping_time = shipping_time;
    }

    public String getBonus_id() {
        return bonus_id;
    }

    public void setBonus_id(String bonus_id) {
        this.bonus_id = bonus_id;
    }

    public String getInvoice_no() {
        return invoice_no;
    }

    public void setInvoice_no(String invoice_no) {
        this.invoice_no = invoice_no;
    }

    public String getExtension_code() {
        return extension_code;
    }

    public void setExtension_code(String extension_code) {
        this.extension_code = extension_code;
    }

    public String getExtension_id() {
        return extension_id;
    }

    public void setExtension_id(String extension_id) {
        this.extension_id = extension_id;
    }

    public String getTo_buyer() {
        return to_buyer;
    }

    public void setTo_buyer(String to_buyer) {
        this.to_buyer = to_buyer;
    }

    public String getPay_note() {
        return pay_note;
    }

    public void setPay_note(String pay_note) {
        this.pay_note = pay_note;
    }

    public String getInv_type() {
        return inv_type;
    }

    public void setInv_type(String inv_type) {
        this.inv_type = inv_type;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPay_order_id() {
        return pay_order_id;
    }

    public void setPay_order_id(String pay_order_id) {
        this.pay_order_id = pay_order_id;
    }

    public String getOrder_from() {
        return order_from;
    }

    public void setOrder_from(String order_from) {
        this.order_from = order_from;
    }

    public String getAction_user() {
        return action_user;
    }

    public void setAction_user(String action_user) {
        this.action_user = action_user;
    }

    public String getGoods_sn() {
        return goods_sn;
    }

    public void setGoods_sn(String goods_sn) {
        this.goods_sn = goods_sn;
    }

    public String getGet_address_id() {
        return get_address_id;
    }

    public void setGet_address_id(String get_address_id) {
        this.get_address_id = get_address_id;
    }

    public String getPay_type_id() {
        return pay_type_id;
    }

    public void setPay_type_id(String pay_type_id) {
        this.pay_type_id = pay_type_id;
    }

    public String getSend_address_id() {
        return send_address_id;
    }

    public void setSend_address_id(String send_address_id) {
        this.send_address_id = send_address_id;
    }

    public String getIs_future() {
        return is_future;
    }

    public void setIs_future(String is_future) {
        this.is_future = is_future;
    }

    public String getIs_prepay() {
        return is_prepay;
    }

    public void setIs_prepay(String is_prepay) {
        this.is_prepay = is_prepay;
    }

    public String getPrepay_ratio() {
        return prepay_ratio;
    }

    public void setPrepay_ratio(String prepay_ratio) {
        this.prepay_ratio = prepay_ratio;
    }

    public String getPay_check() {
        return pay_check;
    }

    public void setPay_check(String pay_check) {
        this.pay_check = pay_check;
    }

    public String getIs_entrust() {
        return is_entrust;
    }

    public void setIs_entrust(String is_entrust) {
        this.is_entrust = is_entrust;
    }

    public String getEntrust_order_id() {
        return entrust_order_id;
    }

    public void setEntrust_order_id(String entrust_order_id) {
        this.entrust_order_id = entrust_order_id;
    }

    public String getEntrust_money() {
        return entrust_money;
    }

    public void setEntrust_money(String entrust_money) {
        this.entrust_money = entrust_money;
    }

    public String getEntrust_before() {
        return entrust_before;
    }

    public void setEntrust_before(String entrust_before) {
        this.entrust_before = entrust_before;
    }

    public String getBuyer_name() {
        return buyer_name;
    }

    public void setBuyer_name(String buyer_name) {
        this.buyer_name = buyer_name;
    }

    public String getBuyer_card() {
        return buyer_card;
    }

    public void setBuyer_card(String buyer_card) {
        this.buyer_card = buyer_card;
    }

    public String getBuyer_phone() {
        return buyer_phone;
    }

    public void setBuyer_phone(String buyer_phone) {
        this.buyer_phone = buyer_phone;
    }

    public String getSeller_name() {
        return seller_name;
    }

    public void setSeller_name(String seller_name) {
        this.seller_name = seller_name;
    }

    public String getSeller_phone() {
        return seller_phone;
    }

    public void setSeller_phone(String seller_phone) {
        this.seller_phone = seller_phone;
    }

    public String getStock_status() {
        return stock_status;
    }

    public void setStock_status(String stock_status) {
        this.stock_status = stock_status;
    }

    public String getStock_remark() {
        return stock_remark;
    }

    public void setStock_remark(String stock_remark) {
        this.stock_remark = stock_remark;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getLogistics() {
        return logistics;
    }

    public void setLogistics(String logistics) {
        this.logistics = logistics;
    }

    public String getBusiness_status() {
        return business_status;
    }

    public void setBusiness_status(String business_status) {
        this.business_status = business_status;
    }

    public String getBusiness_reason() {
        return business_reason;
    }

    public void setBusiness_reason(String business_reason) {
        this.business_reason = business_reason;
    }

    public String getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(String total_fee) {
        this.total_fee = total_fee;
    }


    public String getFormated_pay_total() {
        return formated_pay_total;
    }

    public void setFormated_pay_total(String formated_pay_total) {
        this.formated_pay_total = formated_pay_total;
    }

    public String getPay_total_ratio() {
        return pay_total_ratio;
    }

    public void setPay_total_ratio(String pay_total_ratio) {
        this.pay_total_ratio = pay_total_ratio;
    }

    public String getFormated_pay_total_ratio() {
        return formated_pay_total_ratio;
    }

    public void setFormated_pay_total_ratio(String formated_pay_total_ratio) {
        this.formated_pay_total_ratio = formated_pay_total_ratio;
    }

    public String getFormated_entrust_money() {
        return formated_entrust_money;
    }

    public void setFormated_entrust_money(String formated_entrust_money) {
        this.formated_entrust_money = formated_entrust_money;
    }

    public String getFormated_goods_amount() {
        return formated_goods_amount;
    }

    public void setFormated_goods_amount(String formated_goods_amount) {
        this.formated_goods_amount = formated_goods_amount;
    }

    public String getFormated_discount() {
        return formated_discount;
    }

    public void setFormated_discount(String formated_discount) {
        this.formated_discount = formated_discount;
    }

    public String getFormated_tax() {
        return formated_tax;
    }

    public void setFormated_tax(String formated_tax) {
        this.formated_tax = formated_tax;
    }

    public String getFormated_shipping_fee() {
        return formated_shipping_fee;
    }

    public void setFormated_shipping_fee(String formated_shipping_fee) {
        this.formated_shipping_fee = formated_shipping_fee;
    }

    public String getFormated_insure_fee() {
        return formated_insure_fee;
    }

    public void setFormated_insure_fee(String formated_insure_fee) {
        this.formated_insure_fee = formated_insure_fee;
    }

    public String getFormated_pay_fee() {
        return formated_pay_fee;
    }

    public void setFormated_pay_fee(String formated_pay_fee) {
        this.formated_pay_fee = formated_pay_fee;
    }

    public String getFormated_total_fee() {
        return formated_total_fee;
    }

    public void setFormated_total_fee(String formated_total_fee) {
        this.formated_total_fee = formated_total_fee;
    }

    public String getFormated_money_paid() {
        return formated_money_paid;
    }

    public void setFormated_money_paid(String formated_money_paid) {
        this.formated_money_paid = formated_money_paid;
    }

    public String getFormated_bonus() {
        return formated_bonus;
    }

    public void setFormated_bonus(String formated_bonus) {
        this.formated_bonus = formated_bonus;
    }

    public String getFormated_integral_money() {
        return formated_integral_money;
    }

    public void setFormated_integral_money(String formated_integral_money) {
        this.formated_integral_money = formated_integral_money;
    }

    public String getFormated_surplus() {
        return formated_surplus;
    }

    public void setFormated_surplus(String formated_surplus) {
        this.formated_surplus = formated_surplus;
    }

    public String getFormated_order_amount() {
        return formated_order_amount;
    }

    public void setFormated_order_amount(String formated_order_amount) {
        this.formated_order_amount = formated_order_amount;
    }

    public String getFormated_add_time() {
        return formated_add_time;
    }

    public void setFormated_add_time(String formated_add_time) {
        this.formated_add_time = formated_add_time;
    }

    public UserInfoBean getUser_info() {
        return user_info;
    }

    public void setUser_info(UserInfoBean user_info) {
        this.user_info = user_info;
    }

    public String getOrder_status_name() {
        return order_status_name;
    }

    public void setOrder_status_name(String order_status_name) {
        this.order_status_name = order_status_name;
    }

    public GoodsInfoBean getGoods_info() {
        return goods_info;
    }

    public void setGoods_info(GoodsInfoBean goods_info) {
        this.goods_info = goods_info;
    }

    public static class UserInfoBean {
        /**
         * mobile : 18722651772
         * name : 小邢
         * user_name : 18722651772
         */

        private String mobile;
        private String name;
        private String user_name;

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }
    }

    public static class GoodsInfoBean {
        /**
         * rec_id : 1708
         * order_id : 1708
         * goods_id : 1682
         * goods_name : 牛脖骨
         * goods_sn :
         * product_id : 0
         * goods_number : 9
         * market_price : 123.00
         * goods_price : 123.00
         * goods_attr :
         * send_number : 0
         * is_real : 1
         * extension_code :
         * parent_id : 0
         * is_gift : 0
         * goods_attr_id :
         * total_weight : 1.11
         * is_retail : 1
         * measure_unit : Ctn
         * storage : 123
         * sku : N17052008745
         * brand_sn : 67
         * region_name : 【AU】澳大利亚
         * port : 0
         * goods_local : 123123
         * user_id : 4
         * shop_price_unit : 吨
         * spec_2 : 9
         * shop_price : 123.00
         * goods_type : 7
         * good_face : images/201705/1495210138607021789.jpg
         * subtotal : 136.53
         * part_number : 9
         * part_weight : 0
         * formated_goods_price : ￥123.00
         * formated_subtotal : ￥136.53
         * goods_thumb : http://test.taiyanggo.com/images/201705/1495210138607021789.jpg
         * en_dw : 9件/吨
         */

        public String getPart_unit() {
            return part_unit;
        }

        public void setPart_unit(String part_unit) {
            this.part_unit = part_unit;
        }

        private String part_unit;
        private String rec_id;
        private String order_id;
        private String goods_id;
        private String goods_name;
        private String goods_sn;
        private String product_id;
        private String goods_number;
        private String market_price;
        private String goods_price;

        public String getSpec_1_unit() {
            return spec_1_unit;
        }

        public void setSpec_1_unit(String spec_1_unit) {
            this.spec_1_unit = spec_1_unit;
        }

        private String spec_1_unit;
        private String goods_attr;
        private String send_number;
        private String is_real;
        private String extension_code;
        private String parent_id;
        private String is_gift;
        private String goods_attr_id;
        private String total_weight;
        private String is_retail;
        private String measure_unit;
        private String storage;
        private String sku;
        private String brand_sn;
        private String region_name;
        private String port;
        private String goods_local;
        private String user_id;
        private String shop_price_unit;
        private String spec_2;
        private String shop_price;
        private String goods_type;
        private String good_face;
        private double subtotal;
        private int part_number;
        private String part_weight;
        private String formated_goods_price;
        private String formated_subtotal;
        private String goods_thumb;
        private String en_dw;

        public String getRec_id() {
            return rec_id;
        }

        public void setRec_id(String rec_id) {
            this.rec_id = rec_id;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_sn() {
            return goods_sn;
        }

        public void setGoods_sn(String goods_sn) {
            this.goods_sn = goods_sn;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getGoods_number() {
            return goods_number;
        }

        public void setGoods_number(String goods_number) {
            this.goods_number = goods_number;
        }

        public String getMarket_price() {
            return market_price;
        }

        public void setMarket_price(String market_price) {
            this.market_price = market_price;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public String getGoods_attr() {
            return goods_attr;
        }

        public void setGoods_attr(String goods_attr) {
            this.goods_attr = goods_attr;
        }

        public String getSend_number() {
            return send_number;
        }

        public void setSend_number(String send_number) {
            this.send_number = send_number;
        }

        public String getIs_real() {
            return is_real;
        }

        public void setIs_real(String is_real) {
            this.is_real = is_real;
        }

        public String getExtension_code() {
            return extension_code;
        }

        public void setExtension_code(String extension_code) {
            this.extension_code = extension_code;
        }

        public String getParent_id() {
            return parent_id;
        }

        public void setParent_id(String parent_id) {
            this.parent_id = parent_id;
        }

        public String getIs_gift() {
            return is_gift;
        }

        public void setIs_gift(String is_gift) {
            this.is_gift = is_gift;
        }

        public String getGoods_attr_id() {
            return goods_attr_id;
        }

        public void setGoods_attr_id(String goods_attr_id) {
            this.goods_attr_id = goods_attr_id;
        }

        public String getTotal_weight() {
            return total_weight;
        }

        public void setTotal_weight(String total_weight) {
            this.total_weight = total_weight;
        }

        public String getIs_retail() {
            return is_retail;
        }

        public void setIs_retail(String is_retail) {
            this.is_retail = is_retail;
        }

        public String getMeasure_unit() {
            return measure_unit;
        }

        public void setMeasure_unit(String measure_unit) {
            this.measure_unit = measure_unit;
        }

        public String getStorage() {
            return storage;
        }

        public void setStorage(String storage) {
            this.storage = storage;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getBrand_sn() {
            return brand_sn;
        }

        public void setBrand_sn(String brand_sn) {
            this.brand_sn = brand_sn;
        }

        public String getRegion_name() {
            return region_name;
        }

        public void setRegion_name(String region_name) {
            this.region_name = region_name;
        }

        public String getPort() {
            return port;
        }

        public void setPort(String port) {
            this.port = port;
        }

        public String getGoods_local() {
            return goods_local;
        }

        public void setGoods_local(String goods_local) {
            this.goods_local = goods_local;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getShop_price_unit() {
            return shop_price_unit;
        }

        public void setShop_price_unit(String shop_price_unit) {
            this.shop_price_unit = shop_price_unit;
        }

        public String getSpec_2() {
            return spec_2;
        }

        public void setSpec_2(String spec_2) {
            this.spec_2 = spec_2;
        }

        public String getShop_price() {
            return shop_price;
        }

        public void setShop_price(String shop_price) {
            this.shop_price = shop_price;
        }

        public String getGoods_type() {
            return goods_type;
        }

        public void setGoods_type(String goods_type) {
            this.goods_type = goods_type;
        }

        public String getGood_face() {
            return good_face;
        }

        public void setGood_face(String good_face) {
            this.good_face = good_face;
        }

        public double getSubtotal() {
            return subtotal;
        }

        public void setSubtotal(double subtotal) {
            this.subtotal = subtotal;
        }

        public int getPart_number() {
            return part_number;
        }

        public void setPart_number(int part_number) {
            this.part_number = part_number;
        }

        public String getPart_weight() {
            return part_weight;
        }

        public void setPart_weight(String part_weight) {
            this.part_weight = part_weight;
        }

        public String getFormated_goods_price() {
            return formated_goods_price;
        }

        public void setFormated_goods_price(String formated_goods_price) {
            this.formated_goods_price = formated_goods_price;
        }

        public String getFormated_subtotal() {
            return formated_subtotal;
        }

        public void setFormated_subtotal(String formated_subtotal) {
            this.formated_subtotal = formated_subtotal;
        }

        public String getGoods_thumb() {
            return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
            this.goods_thumb = goods_thumb;
        }

        public String getEn_dw() {
            return en_dw;
        }

        public void setEn_dw(String en_dw) {
            this.en_dw = en_dw;
        }
    }
}
