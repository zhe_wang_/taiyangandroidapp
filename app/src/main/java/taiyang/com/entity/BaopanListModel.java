package taiyang.com.entity;

import java.util.List;

import taiyang.com.adapter.Entity;

/**
 * Created by admin on 2016/8/16.
 * 报盘管理
 */
public class BaopanListModel {

    /**
     * list : [{"goods_id":"1865","goods_name":"鸡脖子","goods_number":"99947","brand_sn":"1111","shop_price":"￥3","goods_local":"版本","goods_type":"7","sell_type":"4","offer":"11","region_name":"【CN】中国","goods_thumb":"http://test.taiyanggo.com/images/201710/1507236235745971080.jpg","spec_1":"999","spec_1_unit":"件/吨","spec_2":"2","spec_2_unit":"件/吨","goods_weight":"999.00","good_face":"images/201710/1507236235745971080.jpg","shop_price_unit":"1","currency":"1","shop_price_eur":"0.00","shop_price_am":"0.75","measure_unit":"件","sku":"N17100612998","is_check":"1","is_on_sale":"1","is_pin":0,"unit_name":"件","region_icon":"","last_update_time":"2017-10-06 12:43"},{"goods_id":"1864","goods_name":"鸡翅中","goods_number":"9","brand_sn":"1111","shop_price":"￥0","goods_local":"好","goods_type":"7","sell_type":"4","offer":"11","region_name":"【CN】中国","goods_thumb":"http://test.taiyanggo.com/images/201710/1507236109371546262.jpg","spec_1":"8","spec_1_unit":"件/吨","spec_2":"125","spec_2_unit":"件/吨","goods_weight":"8.00","good_face":"images/201710/1507236109371546262.jpg","shop_price_unit":"1","currency":"1","shop_price_eur":"0.00","shop_price_am":"0.75","measure_unit":"件","sku":"N17100612611","is_check":"1","is_on_sale":"1","is_pin":0,"unit_name":"件","region_icon":"","last_update_time":"2017-10-06 12:41"},{"goods_id":"1827","goods_name":"猪脑","goods_number":"20","brand_sn":"7","shop_price":"￥40000.00","goods_local":"大连","goods_type":"7","sell_type":"4","offer":"11","region_name":"【CA】加拿大","goods_thumb":"http://test.taiyanggo.com/images/201709/1504605714630865742.jpg","spec_1":"200","spec_1_unit":"件/吨","spec_2":"5","spec_2_unit":"件/吨","goods_weight":"200.00","good_face":"images/201709/1504605714630865742.jpg","shop_price_unit":"0","currency":"1","shop_price_eur":"0.00","shop_price_am":"6030.64","measure_unit":"件","sku":"N17090602433","is_check":"1","is_on_sale":"1","is_pin":0,"unit_name":"吨","region_icon":"http://test.taiyanggo.com/config/country/2.png","last_update_time":"2017-09-06 02:01"},{"goods_id":"1791","goods_name":"猪后筒骨","goods_number":"12","brand_sn":"350","shop_price":"$200","goods_local":"测试","goods_type":"7","sell_type":"4","offer":"11","region_name":"【AR】阿根廷","goods_thumb":"http://test.taiyanggo.com/images/201709/1504215893509551758.jpg","spec_1":"1000","spec_1_unit":"件/吨","spec_2":"1","spec_2_unit":"件/吨","goods_weight":"1000.00","good_face":"images/201709/1504215893509551758.jpg","shop_price_unit":"1","currency":"2","shop_price_eur":"0.00","shop_price_am":"200.00","measure_unit":"件","sku":"N17090113337","is_check":"1","is_on_sale":"1","is_pin":0,"unit_name":"件","region_icon":"http://test.taiyanggo.com/config/country/6.png","last_update_time":"2017-09-01 13:44"},{"goods_id":"1789","goods_name":"肘子","goods_number":"2000","brand_sn":"350","shop_price":"￥200.00","goods_local":"北京","goods_type":"7","sell_type":"4","offer":"11","region_name":"【AR】阿根廷","goods_thumb":"http://test.taiyanggo.com/images/201701/thumb_img/1349_thumb_G_14842627701505.jpg","spec_1":"1000","spec_1_unit":"件/吨","spec_2":"1","spec_2_unit":"件/吨","goods_weight":"1000.00","good_face":"","shop_price_unit":"0","currency":"1","shop_price_eur":"0.00","shop_price_am":"30.15","measure_unit":"件","sku":"N17083110258","is_check":"1","is_on_sale":"1","is_pin":0,"unit_name":"吨","region_icon":"http://test.taiyanggo.com/config/country/6.png","last_update_time":"2017-08-31 10:03"},{"goods_id":"1773","goods_name":"牡蛎","goods_number":"11","brand_sn":"12","shop_price":"￥1","goods_local":"111","goods_type":"7","sell_type":"4","offer":"11","region_name":"【CA】加拿大","goods_thumb":"http://test.taiyanggo.com/images/201708/1503785614775378999.jpg","spec_1":"111","spec_1_unit":"件/吨","spec_2":"10","spec_2_unit":"件/吨","goods_weight":"111.00","good_face":"images/201708/1503785614775378999.jpg","shop_price_unit":"1","currency":"1","shop_price_eur":"0.00","shop_price_am":"1.66","measure_unit":"件","sku":"N17082714890","is_check":"1","is_on_sale":"1","is_pin":0,"unit_name":"件","region_icon":"http://test.taiyanggo.com/config/country/2.png","last_update_time":"2017-08-27 14:13"},{"goods_id":"1682","goods_name":"牛脖骨","goods_number":"114","brand_sn":"67","shop_price":"￥119.00","goods_local":"123123","goods_type":"7","sell_type":"4","offer":"11","region_name":"【AU】澳大利亚","goods_thumb":"http://test.taiyanggo.com/images/201705/1495210138607021789.jpg","spec_1":"123","spec_1_unit":"Ctn/Ton","spec_2":"9","spec_2_unit":"Ctn/Ton","goods_weight":"123.00","good_face":"images/201705/1495210138607021789.jpg","shop_price_unit":"0","currency":"1","shop_price_eur":"0.00","shop_price_am":"18.54","measure_unit":"Ctn","sku":"N17052008745","is_check":"1","is_on_sale":"1","is_pin":0,"unit_name":"吨","region_icon":"http://test.taiyanggo.com/config/country/15.png","last_update_time":"2017-08-16 14:29"},{"goods_id":"1672","goods_name":"猪整头","goods_number":"43","brand_sn":"350","shop_price":"￥33.00","goods_local":"NYC","goods_type":"7","sell_type":"4","offer":"11","region_name":"【AR】阿根廷","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/10_thumb_G_14763233280018.jpg","spec_1":"999","spec_1_unit":"件/吨","spec_2":"2","spec_2_unit":"件/吨","goods_weight":"999.00","good_face":"","shop_price_unit":"0","currency":"1","shop_price_eur":"0.00","shop_price_am":"15076588.92","measure_unit":"件","sku":"N17042705219","is_check":"1","is_on_sale":"1","is_pin":0,"unit_name":"吨","region_icon":"http://test.taiyanggo.com/config/country/6.png","last_update_time":"2017-08-16 14:29"},{"goods_id":"1671","goods_name":"牛前胸","goods_number":"9999","brand_sn":"国产","shop_price":"$9999999","goods_local":"温哥华","goods_type":"7","sell_type":"4","offer":"11","region_name":"【CN】中国","goods_thumb":"http://test.taiyanggo.com/images/201701/thumb_img/1357_thumb_G_14842582810099.jpg","spec_1":"99","spec_1_unit":"件/吨","spec_2":"11","spec_2_unit":"件/吨","goods_weight":"99.00","good_face":"","shop_price_unit":"0","currency":"2","shop_price_eur":"0.00","shop_price_am":"9999999.00","measure_unit":"件","sku":"N17042705997","is_check":"1","is_on_sale":"1","is_pin":0,"unit_name":"吨","region_icon":"","last_update_time":"2017-08-16 14:29"},{"goods_id":"1669","goods_name":"虾仁","goods_number":"8","brand_sn":"19246","shop_price":"￥1.00","goods_local":"NYC","goods_type":"7","sell_type":"4","offer":"11","region_name":"【US】美国","goods_thumb":"http://test.taiyanggo.com/images/no_picture.gif","spec_1":"1000","spec_1_unit":"件/吨","spec_2":"1","spec_2_unit":"件/吨","goods_weight":"1000.00","good_face":"","shop_price_unit":"0","currency":"1","shop_price_eur":"0.00","shop_price_am":"0.15","measure_unit":"件","sku":"N17042101683","is_check":"1","is_on_sale":"1","is_pin":0,"unit_name":"吨","region_icon":"","last_update_time":"2017-08-16 14:29"}]
     * total : {"record_count":10,"page_count":1,"page":1}
     */

    private TotalBean total;
    private List<ListBean> list;

    public TotalBean getTotal() {
        return total;
    }

    public void setTotal(TotalBean total) {
        this.total = total;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class TotalBean {
        /**
         * record_count : 10
         * page_count : 1
         * page : 1
         */

        private int record_count;
        private int page_count;
        private int page;

        public int getRecord_count() {
            return record_count;
        }

        public void setRecord_count(int record_count) {
            this.record_count = record_count;
        }

        public int getPage_count() {
            return page_count;
        }

        public void setPage_count(int page_count) {
            this.page_count = page_count;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }
    }

    public static class ListBean extends Entity{
        /**
         * goods_id : 1865
         * goods_name : 鸡脖子
         * goods_number : 99947
         * brand_sn : 1111
         * shop_price : ￥3
         * goods_local : 版本
         * goods_type : 7
         * sell_type : 4
         * offer : 11
         * region_name : 【CN】中国
         * goods_thumb : http://test.taiyanggo.com/images/201710/1507236235745971080.jpg
         * spec_1 : 999
         * spec_1_unit : 件/吨
         * spec_2 : 2
         * spec_2_unit : 件/吨
         * goods_weight : 999.00
         * good_face : images/201710/1507236235745971080.jpg
         * shop_price_unit : 1
         * currency : 1
         * shop_price_eur : 0.00
         * shop_price_am : 0.75
         * measure_unit : 件
         * sku : N17100612998
         * is_check : 1
         * is_on_sale : 1
         * is_pin : 0
         * unit_name : 件
         * region_icon :
         * last_update_time : 2017-10-06 12:43
         */

        private String goods_id;
        private String goods_name;
        private String goods_number;
        private String brand_sn;
        private String shop_price;
        private String goods_local;
        private String goods_type;
        private String sell_type;
        private String offer;
        private String region_name;
        private String goods_thumb;
        private String spec_1;
        private String spec_1_unit;
        private String spec_2;
        private String spec_2_unit;
        private String goods_weight;
        private String good_face;
        private String shop_price_unit;
        private String currency;
        private String shop_price_eur;
        private String shop_price_am;
        private String measure_unit;
        private String sku;
        private String is_check;
        private String is_on_sale;
        private int is_pin;
        private String unit_name;
        private String region_icon;
        private String last_update_time;

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_number() {
            return goods_number;
        }

        public void setGoods_number(String goods_number) {
            this.goods_number = goods_number;
        }

        public String getBrand_sn() {
            return brand_sn;
        }

        public void setBrand_sn(String brand_sn) {
            this.brand_sn = brand_sn;
        }

        public String getShop_price() {
            return shop_price;
        }

        public void setShop_price(String shop_price) {
            this.shop_price = shop_price;
        }

        public String getGoods_local() {
            return goods_local;
        }

        public void setGoods_local(String goods_local) {
            this.goods_local = goods_local;
        }

        public String getGoods_type() {
            return goods_type;
        }

        public void setGoods_type(String goods_type) {
            this.goods_type = goods_type;
        }

        public String getSell_type() {
            return sell_type;
        }

        public void setSell_type(String sell_type) {
            this.sell_type = sell_type;
        }

        public String getOffer() {
            return offer;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

        public String getRegion_name() {
            return region_name;
        }

        public void setRegion_name(String region_name) {
            this.region_name = region_name;
        }

        public String getGoods_thumb() {
            return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
            this.goods_thumb = goods_thumb;
        }

        public String getSpec_1() {
            return spec_1;
        }

        public void setSpec_1(String spec_1) {
            this.spec_1 = spec_1;
        }

        public String getSpec_1_unit() {
            return spec_1_unit;
        }

        public void setSpec_1_unit(String spec_1_unit) {
            this.spec_1_unit = spec_1_unit;
        }

        public String getSpec_2() {
            return spec_2;
        }

        public void setSpec_2(String spec_2) {
            this.spec_2 = spec_2;
        }

        public String getSpec_2_unit() {
            return spec_2_unit;
        }

        public void setSpec_2_unit(String spec_2_unit) {
            this.spec_2_unit = spec_2_unit;
        }

        public String getGoods_weight() {
            return goods_weight;
        }

        public void setGoods_weight(String goods_weight) {
            this.goods_weight = goods_weight;
        }

        public String getGood_face() {
            return good_face;
        }

        public void setGood_face(String good_face) {
            this.good_face = good_face;
        }

        public String getShop_price_unit() {
            return shop_price_unit;
        }

        public void setShop_price_unit(String shop_price_unit) {
            this.shop_price_unit = shop_price_unit;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getShop_price_eur() {
            return shop_price_eur;
        }

        public void setShop_price_eur(String shop_price_eur) {
            this.shop_price_eur = shop_price_eur;
        }

        public String getShop_price_am() {
            return shop_price_am;
        }

        public void setShop_price_am(String shop_price_am) {
            this.shop_price_am = shop_price_am;
        }

        public String getMeasure_unit() {
            return measure_unit;
        }

        public void setMeasure_unit(String measure_unit) {
            this.measure_unit = measure_unit;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getIs_check() {
            return is_check;
        }

        public void setIs_check(String is_check) {
            this.is_check = is_check;
        }

        public String getIs_on_sale() {
            return is_on_sale;
        }

        public void setIs_on_sale(String is_on_sale) {
            this.is_on_sale = is_on_sale;
        }

        public int getIs_pin() {
            return is_pin;
        }

        public void setIs_pin(int is_pin) {
            this.is_pin = is_pin;
        }

        public String getUnit_name() {
            return unit_name;
        }

        public void setUnit_name(String unit_name) {
            this.unit_name = unit_name;
        }

        public String getRegion_icon() {
            return region_icon;
        }

        public void setRegion_icon(String region_icon) {
            this.region_icon = region_icon;
        }

        public String getLast_update_time() {
            return last_update_time;
        }

        public void setLast_update_time(String last_update_time) {
            this.last_update_time = last_update_time;
        }
    }
}
