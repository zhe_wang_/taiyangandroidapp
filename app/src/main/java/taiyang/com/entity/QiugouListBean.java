package taiyang.com.entity;

import java.util.List;

/**
 * Created by zhew on 9/9/17.
 * Copyright © 2006-2016 365jia.cn 安徽大尺度网络传媒有限公司版权所有
 **/

public class QiugouListBean
{
    /**
     * list : [{"id":"200","title":"哈哈哈","goods_name":"不不不","goods_num":"12","price_low":"166","price_up":"2000","address":"哈哈哈","user_name":"18500083614","user_phone":"18500083614","user_id":"1046","created_at":"1503632491","memo":"规律哈哈","type":"海鲜","sku":"QG082511683","user_face":""},{"id":"199","title":"带鱼","goods_name":"带鱼","goods_num":"1","price_low":"7500","price_up":"10000","address":"北京","user_name":"张春生","user_phone":"18500083614","user_id":"1046","created_at":"1502850715","memo":"","type":"水产","sku":"QG081610418","user_face":""},{"id":"198","title":"123123","goods_name":"123123","goods_num":"123","price_low":"123","price_up":"234","address":"123123","user_name":"wangxiao123","user_phone":"18565827222","user_id":"4","created_at":"1502597594","memo":"123123123","type":"海鲜","sku":"QG081312188","user_face":"config/head_img/1503446777036401557.jpg"},{"id":"195","title":"急需禽类","goods_name":"鸡翅","goods_num":"5","price_low":"5060","price_up":"8000","address":"上海","user_name":"张春生","user_phone":"18519519744","user_id":"1032","created_at":"1497332332","memo":"美丽的小鸟快乐的成长美丽的小鸟快乐的成长","type":"禽类","sku":"QG061313386","user_face":null},{"id":"194","title":"急需水产","goods_name":"北极虾","goods_num":"1.8","price_low":"9000","price_up":"16000","address":"漯河","user_name":"张春生","user_phone":"18519519744","user_id":"1032","created_at":"1497332326","memo":"美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长","type":"水产","sku":"QG061313459","user_face":null},{"id":"193","title":"急需牛腿","goods_name":"牛后腿","goods_num":"1.8","price_low":"9000","price_up":"12000","address":"三亚","user_name":"张春生","user_phone":"18519519744","user_id":"1032","created_at":"1497332295","memo":"美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长","type":"牛","sku":"QG061313244","user_face":null},{"id":"192","title":"急需羊头","goods_name":"羊半头","goods_num":"1.5","price_low":"7000","price_up":"10000","address":"内蒙古","user_name":"张春生","user_phone":"18519519744","user_id":"1032","created_at":"1497332276","memo":"美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长美丽的小鸟快乐的成长","type":"羊","sku":"QG061313565","user_face":null},{"id":"191","title":"急需猪肉","goods_name":"猪半头","goods_num":"1.2","price_low":"5000","price_up":"11000","address":"哈尔冰","user_name":"张春生","user_phone":"18519519744","user_id":"1032","created_at":"1497332261","memo":"美丽的小鸟快乐的成长美丽的小鸟快乐的成长","type":"猪","sku":"QG061313483","user_face":null},{"id":"190","title":"555","goods_name":"5","goods_num":"55","price_low":"5","price_up":"55","address":"55555555555","user_name":"王小玮","user_phone":"18565827222","user_id":"4","created_at":"1497320350","memo":"55","type":"水产","sku":"QG061310588","user_face":"config/head_img/1503446777036401557.jpg"},{"id":"179","title":"急需","goods_name":"羊腿","goods_num":"2.3","price_low":"5000","price_up":"40000","address":"测试的","user_name":"张春生","user_phone":"18519519744","user_id":"1029","created_at":"1497235271","memo":"","type":"羊","sku":"QG061210509","user_face":null}]
     * total : {"record_count":47,"page_count":5,"page":1}
     */

    private TotalBean total;
    private List<ListBean> list;

    public TotalBean getTotal() {
        return total;
    }

    public void setTotal(TotalBean total) {
        this.total = total;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class TotalBean {
        /**
         * record_count : 47
         * page_count : 5
         * page : 1
         */

        private int record_count;
        private int page_count;
        private int page;

        public int getRecord_count() {
            return record_count;
        }

        public void setRecord_count(int record_count) {
            this.record_count = record_count;
        }

        public int getPage_count() {
            return page_count;
        }

        public void setPage_count(int page_count) {
            this.page_count = page_count;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }
    }

    public static class ListBean {
        /**
         * id : 200
         * title : 哈哈哈
         * goods_name : 不不不
         * goods_num : 12
         * price_low : 166
         * price_up : 2000
         * address : 哈哈哈
         * user_name : 18500083614
         * user_phone : 18500083614
         * user_id : 1046
         * created_at : 1503632491
         * memo : 规律哈哈
         * type : 海鲜
         * sku : QG082511683
         * user_face :
         */

        private String id;
        private String title;
        private String goods_name;
        private String goods_num;
        private String price_low;
        private String price_up;
        private String address;
        private String user_name;
        private String user_phone;
        private String user_id;
        private String created_at;
        private String memo;
        private String type;
        private String sku;
        private String user_face;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_num() {
            return goods_num;
        }

        public void setGoods_num(String goods_num) {
            this.goods_num = goods_num;
        }

        public String getPrice_low() {
            return price_low;
        }

        public void setPrice_low(String price_low) {
            this.price_low = price_low;
        }

        public String getPrice_up() {
            return price_up;
        }

        public void setPrice_up(String price_up) {
            this.price_up = price_up;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getUser_phone() {
            return user_phone;
        }

        public void setUser_phone(String user_phone) {
            this.user_phone = user_phone;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getUser_face() {
            return user_face;
        }

        public void setUser_face(String user_face) {
            this.user_face = user_face;
        }
    }
}
