package taiyang.com.entity;

import java.util.List;

/**
 * Created by zhew on 9/9/17.
 * Copyright © 2006-2016 365jia.cn 安徽大尺度网络传媒有限公司版权所有
 **/

public class CommentListBean {
    private List<CommentBean> comment_list;

    public List<CommentBean> getComment_list() {
        return comment_list;
    }

    public void setComment_list(List<CommentBean> comment_list) {
        this.comment_list = comment_list;
    }

    public static class CommentBean {
        /**
         * id : 1
         * user_id : 4
         * p_id : 191
         * content : test
         * status : 1
         * add_time : 2017-09-09 03:46
         * user_name : wangxiao123
         * user_face : http://test.taiyanggo.com/config/head_img/1503446777036401557.jpg
         */

        private String id;
        private String user_id;
        private String p_id;
        private String content;
        private String status;
        private String add_time;
        private String user_name;
        private String user_face;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getP_id() {
            return p_id;
        }

        public void setP_id(String p_id) {
            this.p_id = p_id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getUser_face() {
            return user_face;
        }

        public void setUser_face(String user_face) {
            this.user_face = user_face;
        }
    }
}
