package taiyang.com.entity;

import java.util.List;

/**
 * Created by heng on 2016/8/19.
 */
public class FactorGoodsListModel {

    /**
     * record_count : 3
     * page_count : 1
     * page : 1
     */

    private PageModel total;
    /**
     * goods_id : 16
     * goods_name : 前筒骨
     * brand_sn : 513
     * shop_price : ￥9800
     * goods_local : 河北唐山盛华冷库
     * goods_type : 7
     * sell_type : 4
     * offer : 10
     * goods_thumb : http://tydpb2b.bizsoho.com/images/201606/thumb_img/101_thumb_G_14671369817065.jpg
     * spec_1 : 10
     * spec_1_unit : 件/吨
     * spec_2 : 100
     * spec_2_unit : 件/吨
     * goods_weight : 10.00
     * is_pin : 0
     * unit_name : 元/吨
     * region_icon :
     */

    private List<FactorGoodsModel> list;

    public PageModel getTotal() {
        return total;
    }

    public void setTotal(PageModel total) {
        this.total = total;
    }

    public List<FactorGoodsModel> getList() {
        return list;
    }

    public void setList(List<FactorGoodsModel> list) {
        this.list = list;
    }



}
