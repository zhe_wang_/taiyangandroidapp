package taiyang.com.entity;

import java.util.List;

/**
 * Created by admin on 2016/8/12.
 */
public class ProvinceListModel {

    private List<ProvinceModel> provinceList;

    public List<ProvinceModel> getProvinceList() {
        return provinceList;
    }

    public void setProvinceList(List<ProvinceModel> provinceList) {
        this.provinceList = provinceList;
    }
}
