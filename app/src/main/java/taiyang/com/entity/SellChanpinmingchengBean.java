package taiyang.com.entity;

import java.util.List;

/**
 * Created by zhew on 8/22/17.
 * Copyright © 2006-2016 365jia.cn 安徽大尺度网络传媒有限公司版权所有
 **/


public class SellChanpinmingchengBean {


    private List<GoodsNameListBean> goods_name_list;

    public List<GoodsNameListBean> getGoods_name_list() {
        return goods_name_list;
    }

    public void setGoods_name_list(List<GoodsNameListBean> goods_name_list) {
        this.goods_name_list = goods_name_list;
    }

    public static class GoodsNameListBean {
        /**
         * goods_id : 1008
         * goods_name : 猪碎肉90/10
         * goods_name_en : Pork Trimmings90/10
         */

        private String goods_id;
        private String goods_name;
        private String goods_name_en;

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_name_en() {
            return goods_name_en;
        }

        public void setGoods_name_en(String goods_name_en) {
            this.goods_name_en = goods_name_en;
        }
    }
}

