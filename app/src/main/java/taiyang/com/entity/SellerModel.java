package taiyang.com.entity;

/**
 * Created by admin on 2016/8/19.
 */
public class SellerModel {


    /**
     * user_id : 1046
     * user_face : http://test.taiyanggo.com/images/user_face.png
     * shop_name : 哈哈小店
     * shop_info :
     * province :
     * city :
     * follow_count : 1
     * address : 北京丰台
     * created : 2017-08-16
     * order_num : 0
     * is_follow : 0
     */

    private String user_id;
    private String user_face;
    private String shop_name;
    private String shop_info;
    private String province;

    public String getService_hotline() {
        return service_hotline;
    }

    public void setService_hotline(String service_hotline) {
        this.service_hotline = service_hotline;
    }

    private String service_hotline;
    private String city;
    private String follow_count;
    private String address;
    private String created;
    private String order_num;
    private int is_follow;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_face() {
        return user_face;
    }

    public void setUser_face(String user_face) {
        this.user_face = user_face;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_info() {
        return shop_info;
    }

    public void setShop_info(String shop_info) {
        this.shop_info = shop_info;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFollow_count() {
        return follow_count;
    }

    public void setFollow_count(String follow_count) {
        this.follow_count = follow_count;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getOrder_num() {
        return order_num;
    }

    public void setOrder_num(String order_num) {
        this.order_num = order_num;
    }

    public int getIs_follow() {
        return is_follow;
    }

    public void setIs_follow(int is_follow) {
        this.is_follow = is_follow;
    }
}
