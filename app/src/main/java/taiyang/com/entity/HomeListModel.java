package taiyang.com.entity;

/**
 * Created by Administrator on 2016/8/6.
 */
public class HomeListModel {

    /**
     * error : 0
     * message :
     * content : {"app_slide":[{"pic":"/config/afficheimg/1469240070803262041.jpg","title":"","link":""},{"pic":"/config/afficheimg/1469240070128628366.jpg","title":"","link":""},{"pic":"/config/afficheimg/1469240070745654255.jpg","title":"","link":""},{"pic":"/config/afficheimg/1469405524553575740.jpg","title":"","link":""}],"app_index_pic":{"url":"/config/afficheimg/1469405694949857791.png","link":"","width":"","height":"","text":""},"best_goods":[{"goods_id":"2","goods_name":"","brand_sn":"DE202","shop_price":"￥18000","goods_local":"天津","goods_type":"7","sell_type":"5","offer":"10","goods_thumb":"images/201606/thumb_img/19_thumb_G_14671385976437.jpg","is_pin":0,"region_name_ch":"德国","unit_name":"元/吨","picture":[{"img_id":"27","img_url":"images/201608/1470351289156530176.png","thumb_url":"images/201608/1470351289923478282.jpg","img_desc":""}]},{"goods_id":"1","goods_name":"猪整头","brand_sn":"80","shop_price":"￥13000","goods_local":"上海","goods_type":"7","sell_type":"4","offer":"11","goods_thumb":"images/201606/thumb_img/10_thumb_G_14671375640788.jpg","is_pin":0,"region_name_ch":"加拿大","unit_name":"元/吨","picture":[{"img_id":"27","img_url":"images/201608/1470351289156530176.png","thumb_url":"images/201608/1470351289923478282.jpg","img_desc":""}]}]}
     */

    private String error;
    private String message;
    /**
     * app_slide : [{"pic":"/config/afficheimg/1469240070803262041.jpg","title":"","link":""},{"pic":"/config/afficheimg/1469240070128628366.jpg","title":"","link":""},{"pic":"/config/afficheimg/1469240070745654255.jpg","title":"","link":""},{"pic":"/config/afficheimg/1469405524553575740.jpg","title":"","link":""}]
     * app_index_pic : {"url":"/config/afficheimg/1469405694949857791.png","link":"","width":"","height":"","text":""}
     * best_goods : [{"goods_id":"2","goods_name":"","brand_sn":"DE202","shop_price":"￥18000","goods_local":"天津","goods_type":"7","sell_type":"5","offer":"10","goods_thumb":"images/201606/thumb_img/19_thumb_G_14671385976437.jpg","is_pin":0,"region_name_ch":"德国","unit_name":"元/吨","picture":[{"img_id":"27","img_url":"images/201608/1470351289156530176.png","thumb_url":"images/201608/1470351289923478282.jpg","img_desc":""}]},{"goods_id":"1","goods_name":"猪整头","brand_sn":"80","shop_price":"￥13000","goods_local":"上海","goods_type":"7","sell_type":"4","offer":"11","goods_thumb":"images/201606/thumb_img/10_thumb_G_14671375640788.jpg","is_pin":0,"region_name_ch":"加拿大","unit_name":"元/吨","picture":[{"img_id":"27","img_url":"images/201608/1470351289156530176.png","thumb_url":"images/201608/1470351289923478282.jpg","img_desc":""}]}]
     */

    private ContentBean content;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ContentBean getContent() {
        return content;
    }

    public void setContent(ContentBean content) {
        this.content = content;
    }


}
