package taiyang.com.entity;

import java.util.List;

import taiyang.com.adapter.Entity;

/**
 * Created by admin on 2016/8/16.
 * 报盘管理
 */
public class SellerBaopanListModel {


    /**
     * list : [{"order_id":"1708","order_sn":"2017061388374","user_id":"911","shipping_status":"0","pay_status":"0","add_time":"2017-06-13 15:41:02","agency_id":"4","order_status":"0","stock_status":"0","total_fee":"￥136.53","goods":{"rec_id":"1708","order_id":"1708","goods_id":"1682","goods_name":"牛脖骨","goods_sn":"","goods_number":"9","market_price":"123.00","goods_price":"123.00","total_weight":"1.11","is_retail":"1","measure_unit":"Ctn","storage":"123","sku":"N17052008745","brand_sn":"67","region_name":"【AU】澳大利亚","port":"0","goods_local":"123123","user_id":"4","shop_price_unit":"吨","spec_2":"9","shop_price":"123.00","goods_type":"7","good_face":"images/201705/1495210138607021789.jpg","subtotal":136.53,"part_number":9,"part_weight":"0","formated_goods_price":"￥123.00","formated_subtotal":"￥136.53","goods_thumb":"http://test.taiyanggo.com/images/201705/1495210138607021789.jpg","en_dw":"9件/吨","region_name_ch_l":1,"region_name_ch":"澳大利亚","spec_1":"123","spec_1_unit":"KG/Ctn","offer":"11","sell_type":"4"},"order_status_format":"待付款","tag_status":"确认库存"},{"order_id":"1707","order_sn":"2017061344083","user_id":"911","shipping_status":"0","pay_status":"0","add_time":"2017-06-13 15:39:56","agency_id":"4","order_status":"0","stock_status":"0","total_fee":"￥136.53","goods":{"rec_id":"1707","order_id":"1707","goods_id":"1682","goods_name":"牛脖骨","goods_sn":"","goods_number":"9","market_price":"123.00","goods_price":"123.00","total_weight":"1.11","is_retail":"1","measure_unit":"Ctn","storage":"123","sku":"N17052008745","brand_sn":"67","region_name":"【AU】澳大利亚","port":"0","goods_local":"123123","user_id":"4","shop_price_unit":"吨","spec_2":"9","shop_price":"123.00","goods_type":"7","good_face":"images/201705/1495210138607021789.jpg","subtotal":136.53,"part_number":9,"part_weight":"0","formated_goods_price":"￥123.00","formated_subtotal":"￥136.53","goods_thumb":"http://test.taiyanggo.com/images/201705/1495210138607021789.jpg","en_dw":"9件/吨","region_name_ch_l":1,"region_name_ch":"澳大利亚","spec_1":"123","spec_1_unit":"KG/Ctn","offer":"11","sell_type":"4"},"order_status_format":"待付款","tag_status":"确认库存"},{"order_id":"1662","order_sn":"2017050666759","user_id":"981","shipping_status":"0","pay_status":"0","add_time":"2017-05-06 08:09:21","agency_id":"4","order_status":"0","stock_status":"0","total_fee":"￥99999999.99","goods":{"rec_id":"1662","order_id":"1662","goods_id":"1672","goods_name":"猪整头","goods_sn":"","goods_number":"2","market_price":"99999999.00","goods_price":"99999999.00","total_weight":"2.00","is_retail":"1","measure_unit":"件","storage":"99","sku":"N17042705219","brand_sn":"350","region_name":"【AR】阿根廷","port":"0","goods_local":"NYC","user_id":"4","shop_price_unit":"吨","spec_2":"2","shop_price":"23.00","goods_type":"7","good_face":"","subtotal":199999998,"part_number":1,"part_weight":"0","formated_goods_price":"￥99999999.00","formated_subtotal":"￥199999998.00","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/10_thumb_G_14763233280018.jpg","en_dw":"2件/吨","region_name_ch_l":1,"region_name_ch":"阿根廷","spec_1":"999","spec_1_unit":"KG/件","offer":"11","sell_type":"4"},"order_status_format":"待付款","tag_status":"确认库存"},{"order_id":"1645","order_sn":"2017042768713","user_id":"981","shipping_status":"0","pay_status":"0","add_time":"2017-04-27 09:51:52","agency_id":"4","order_status":"0","stock_status":"0","total_fee":"￥99999999.99","goods":{"rec_id":"1645","order_id":"1645","goods_id":"1672","goods_name":"猪整头","goods_sn":"","goods_number":"15","market_price":"99999999.00","goods_price":"99999999.00","total_weight":"14.99","is_retail":"1","measure_unit":"件","storage":"99","sku":"N17042705219","brand_sn":"350","region_name":"【AR】阿根廷","port":"0","goods_local":"NYC","user_id":"4","shop_price_unit":"吨","spec_2":"2","shop_price":"23.00","goods_type":"7","good_face":"","subtotal":1.49899998501E9,"part_number":2,"part_weight":"0","formated_goods_price":"￥99999999.00","formated_subtotal":"￥1498999985.01","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/10_thumb_G_14763233280018.jpg","en_dw":"2件/吨","region_name_ch_l":1,"region_name_ch":"阿根廷","spec_1":"999","spec_1_unit":"KG/件","offer":"11","sell_type":"4"},"order_status_format":"待付款","tag_status":"确认库存"},{"order_id":"1644","order_sn":"2017042798739","user_id":"981","shipping_status":"0","pay_status":"0","add_time":"2017-04-27 05:37:26","agency_id":"4","order_status":"0","stock_status":"0","total_fee":"￥99999999.99","goods":{"rec_id":"1644","order_id":"1644","goods_id":"1672","goods_name":"猪整头","goods_sn":"","goods_number":"2","market_price":"99999999.00","goods_price":"99999999.00","total_weight":"2.00","is_retail":"1","measure_unit":"件","storage":"99","sku":"N17042705219","brand_sn":"350","region_name":"【AR】阿根廷","port":"0","goods_local":"NYC","user_id":"4","shop_price_unit":"吨","spec_2":"2","shop_price":"23.00","goods_type":"7","good_face":"","subtotal":199999998,"part_number":1,"part_weight":"0","formated_goods_price":"￥99999999.00","formated_subtotal":"￥199999998.00","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/10_thumb_G_14763233280018.jpg","en_dw":"2件/吨","region_name_ch_l":1,"region_name_ch":"阿根廷","spec_1":"999","spec_1_unit":"KG/件","offer":"11","sell_type":"4"},"order_status_format":"待付款","tag_status":"确认库存"},{"order_id":"1633","order_sn":"2017042631987","user_id":"981","shipping_status":"0","pay_status":"0","add_time":"2017-04-26 07:03:33","agency_id":"4","order_status":"0","stock_status":"0","total_fee":"￥900000.00","goods":{"rec_id":"1633","order_id":"1633","goods_id":"51","goods_name":"猪整头","goods_sn":"","goods_number":"2","market_price":"18000.00","goods_price":"18000.00","total_weight":"50.00","is_retail":"0","measure_unit":"柜","storage":"2","sku":"N16082502177","brand_sn":"80","region_name":"【CA】加拿大","port":"0","goods_local":"北京","user_id":"4","shop_price_unit":"吨","spec_2":"","shop_price":"7777.00","goods_type":"7","good_face":"images/201706/1497529373960244582.jpeg","subtotal":900000,"part_number":"0","part_weight":"25.00","formated_goods_price":"￥18000.00","formated_subtotal":"￥900000.00","goods_thumb":"http://test.taiyanggo.com/images/201706/1497529373960244582.jpeg","en_dw":"50吨/柜","region_name_ch_l":1,"region_name_ch":"加拿大","spec_1":"","spec_1_unit":"","offer":"11","sell_type":"5"},"order_status_format":"待付款","tag_status":"确认库存"},{"order_id":"1632","order_sn":"2017042656670","user_id":"981","shipping_status":"0","pay_status":"0","add_time":"2017-04-26 07:02:27","agency_id":"4","order_status":"0","stock_status":"0","total_fee":"￥450000.00","goods":{"rec_id":"1632","order_id":"1632","goods_id":"51","goods_name":"猪整头","goods_sn":"","goods_number":"1","market_price":"18000.00","goods_price":"18000.00","total_weight":"25.00","is_retail":"0","measure_unit":"柜","storage":"2","sku":"N16082502177","brand_sn":"80","region_name":"【CA】加拿大","port":"0","goods_local":"北京","user_id":"4","shop_price_unit":"吨","spec_2":"","shop_price":"7777.00","goods_type":"7","good_face":"images/201706/1497529373960244582.jpeg","subtotal":450000,"part_number":"0","part_weight":"25.00","formated_goods_price":"￥18000.00","formated_subtotal":"￥450000.00","goods_thumb":"http://test.taiyanggo.com/images/201706/1497529373960244582.jpeg","en_dw":"25吨/柜","region_name_ch_l":1,"region_name_ch":"加拿大","spec_1":"","spec_1_unit":"","offer":"11","sell_type":"5"},"order_status_format":"待付款","tag_status":"确认库存"},{"order_id":"1631","order_sn":"2017042689454","user_id":"981","shipping_status":"0","pay_status":"0","add_time":"2017-04-26 06:22:20","agency_id":"4","order_status":"0","stock_status":"0","total_fee":"￥450000.00","goods":{"rec_id":"1631","order_id":"1631","goods_id":"51","goods_name":"猪整头","goods_sn":"","goods_number":"1","market_price":"18000.00","goods_price":"18000.00","total_weight":"25.00","is_retail":"0","measure_unit":"柜","storage":"2","sku":"N16082502177","brand_sn":"80","region_name":"【CA】加拿大","port":"0","goods_local":"北京","user_id":"4","shop_price_unit":"吨","spec_2":"","shop_price":"7777.00","goods_type":"7","good_face":"images/201706/1497529373960244582.jpeg","subtotal":450000,"part_number":"0","part_weight":"25.00","formated_goods_price":"￥18000.00","formated_subtotal":"￥450000.00","goods_thumb":"http://test.taiyanggo.com/images/201706/1497529373960244582.jpeg","en_dw":"25吨/柜","region_name_ch_l":1,"region_name_ch":"加拿大","spec_1":"","spec_1_unit":"","offer":"11","sell_type":"5"},"order_status_format":"待付款","tag_status":"确认库存"},{"order_id":"1578","order_sn":"2017040526727","user_id":"312","shipping_status":"0","pay_status":"0","add_time":"2017-04-05 01:16:34","agency_id":"4","order_status":"0","stock_status":"1","total_fee":"￥475000.00","goods":{"rec_id":"1578","order_id":"1578","goods_id":"51","goods_name":"猪整头","goods_sn":"","goods_number":"1","market_price":"19000.00","goods_price":"19000.00","total_weight":"25.00","is_retail":"0","measure_unit":"柜","storage":"2","sku":"N16082502177","brand_sn":"80","region_name":"【CA】加拿大","port":"0","goods_local":"北京","user_id":"4","shop_price_unit":"吨","spec_2":"","shop_price":"7777.00","goods_type":"7","good_face":"images/201706/1497529373960244582.jpeg","subtotal":475000,"part_number":"0","part_weight":"25.00","formated_goods_price":"￥19000.00","formated_subtotal":"￥475000.00","goods_thumb":"http://test.taiyanggo.com/images/201706/1497529373960244582.jpeg","en_dw":"25吨/柜","region_name_ch_l":1,"region_name_ch":"加拿大","spec_1":"","spec_1_unit":"","offer":"11","sell_type":"5"},"order_status_format":"待付款","tag_status":"详细信息"},{"order_id":"1021","order_sn":"2016101588322","user_id":"312","shipping_status":"0","pay_status":"0","add_time":"2016-10-15 08:37:23","agency_id":"4","order_status":"1","stock_status":"0","total_fee":"￥475000.00","goods":{"rec_id":"1021","order_id":"1021","goods_id":"51","goods_name":"Whole Pig Head","goods_sn":"","goods_number":"1","market_price":"19000.00","goods_price":"19000.00","total_weight":"25.00","is_retail":"0","measure_unit":"柜","storage":"2","sku":"N16082502177","brand_sn":"80","region_name":"【CA】加拿大","port":"0","goods_local":"北京","user_id":"4","shop_price_unit":"吨","spec_2":"","shop_price":"7777.00","goods_type":"7","good_face":"images/201706/1497529373960244582.jpeg","subtotal":475000,"part_number":"0","part_weight":"25.00","formated_goods_price":"￥19000.00","formated_subtotal":"￥475000.00","goods_thumb":"http://test.taiyanggo.com/images/201706/1497529373960244582.jpeg","en_dw":"25吨/柜","region_name_ch_l":1,"region_name_ch":"加拿大","spec_1":"","spec_1_unit":"","offer":"11","sell_type":"5"},"order_status_format":"已付款","tag_status":"详细信息"}]
     * total : {"record_count":14,"page_count":2,"page":1}
     */

    private TotalBean total;
    private List<ListBean> list;

    public TotalBean getTotal() {
        return total;
    }

    public void setTotal(TotalBean total) {
        this.total = total;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class TotalBean {
        /**
         * record_count : 14
         * page_count : 2
         * page : 1
         */

        private int record_count;
        private int page_count;
        private int page;

        public int getRecord_count() {
            return record_count;
        }

        public void setRecord_count(int record_count) {
            this.record_count = record_count;
        }

        public int getPage_count() {
            return page_count;
        }

        public void setPage_count(int page_count) {
            this.page_count = page_count;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }
    }

    public static class ListBean extends Entity{
        /**
         * order_id : 1708
         * order_sn : 2017061388374
         * user_id : 911
         * shipping_status : 0
         * pay_status : 0
         * add_time : 2017-06-13 15:41:02
         * agency_id : 4
         * order_status : 0
         * stock_status : 0
         * total_fee : ￥136.53
         * goods : {"rec_id":"1708","order_id":"1708","goods_id":"1682","goods_name":"牛脖骨","goods_sn":"","goods_number":"9","market_price":"123.00","goods_price":"123.00","total_weight":"1.11","is_retail":"1","measure_unit":"Ctn","storage":"123","sku":"N17052008745","brand_sn":"67","region_name":"【AU】澳大利亚","port":"0","goods_local":"123123","user_id":"4","shop_price_unit":"吨","spec_2":"9","shop_price":"123.00","goods_type":"7","good_face":"images/201705/1495210138607021789.jpg","subtotal":136.53,"part_number":9,"part_weight":"0","formated_goods_price":"￥123.00","formated_subtotal":"￥136.53","goods_thumb":"http://test.taiyanggo.com/images/201705/1495210138607021789.jpg","en_dw":"9件/吨","region_name_ch_l":1,"region_name_ch":"澳大利亚","spec_1":"123","spec_1_unit":"KG/Ctn","offer":"11","sell_type":"4"}
         * order_status_format : 待付款
         * tag_status : 确认库存
         */

        private String order_id;
        private String order_sn;
        private String user_id;
        private String shipping_status;
        private String pay_status;
        private String add_time;
        private String agency_id;
        private String order_status;
        private String stock_status;
        private String total_fee;
        private GoodsBean goods;
        private String order_status_format;
        private String tag_status;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getOrder_sn() {
            return order_sn;
        }

        public void setOrder_sn(String order_sn) {
            this.order_sn = order_sn;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getShipping_status() {
            return shipping_status;
        }

        public void setShipping_status(String shipping_status) {
            this.shipping_status = shipping_status;
        }

        public String getPay_status() {
            return pay_status;
        }

        public void setPay_status(String pay_status) {
            this.pay_status = pay_status;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getAgency_id() {
            return agency_id;
        }

        public void setAgency_id(String agency_id) {
            this.agency_id = agency_id;
        }

        public String getOrder_status() {
            return order_status;
        }

        public void setOrder_status(String order_status) {
            this.order_status = order_status;
        }

        public String getStock_status() {
            return stock_status;
        }

        public void setStock_status(String stock_status) {
            this.stock_status = stock_status;
        }

        public String getTotal_fee() {
            return total_fee;
        }

        public void setTotal_fee(String total_fee) {
            this.total_fee = total_fee;
        }

        public GoodsBean getGoods() {
            return goods;
        }

        public void setGoods(GoodsBean goods) {
            this.goods = goods;
        }

        public String getOrder_status_format() {
            return order_status_format;
        }

        public void setOrder_status_format(String order_status_format) {
            this.order_status_format = order_status_format;
        }

        public String getTag_status() {
            return tag_status;
        }

        public void setTag_status(String tag_status) {
            this.tag_status = tag_status;
        }

        public static class GoodsBean {
            /**
             * rec_id : 1708
             * order_id : 1708
             * goods_id : 1682
             * goods_name : 牛脖骨
             * goods_sn :
             * goods_number : 9
             * market_price : 123.00
             * goods_price : 123.00
             * total_weight : 1.11
             * is_retail : 1
             * measure_unit : Ctn
             * storage : 123
             * sku : N17052008745
             * brand_sn : 67
             * region_name : 【AU】澳大利亚
             * port : 0
             * goods_local : 123123
             * user_id : 4
             * shop_price_unit : 吨
             * spec_2 : 9
             * shop_price : 123.00
             * goods_type : 7
             * good_face : images/201705/1495210138607021789.jpg
             * subtotal : 136.53
             * part_number : 9
             * part_weight : 0
             * formated_goods_price : ￥123.00
             * formated_subtotal : ￥136.53
             * goods_thumb : http://test.taiyanggo.com/images/201705/1495210138607021789.jpg
             * en_dw : 9件/吨
             * region_name_ch_l : 1
             * region_name_ch : 澳大利亚
             * spec_1 : 123
             * spec_1_unit : KG/Ctn
             * offer : 11
             * sell_type : 4
             */

            private String rec_id;
            private String order_id;
            private String goods_id;
            private String goods_name;
            private String goods_sn;
            private String goods_number;
            private String market_price;
            private String goods_price;
            private String total_weight;
            private String is_retail;
            private String measure_unit;
            private String storage;
            private String sku;
            private String brand_sn;
            private String region_name;
            private String port;
            private String goods_local;
            private String user_id;
            private String shop_price_unit;
            private String spec_2;
            private String shop_price;
            private String goods_type;
            private String good_face;

            public int getIs_pin() {
                return is_pin;
            }

            public void setIs_pin(int is_pin) {
                this.is_pin = is_pin;
            }

            private int is_pin;
            private double subtotal;
            private int part_number;
            private String part_weight;

            public String getPart_unit() {
                return part_unit;
            }

            public void setPart_unit(String part_unit) {
                this.part_unit = part_unit;
            }

            private String part_unit;
            private String formated_goods_price;
            private String formated_subtotal;
            private String goods_thumb;
            private String en_dw;
            private int region_name_ch_l;
            private String region_name_ch;
            private String spec_1;
            private String spec_1_unit;
            private String offer;
            private String sell_type;

            public String getRec_id() {
                return rec_id;
            }

            public void setRec_id(String rec_id) {
                this.rec_id = rec_id;
            }

            public String getOrder_id() {
                return order_id;
            }

            public void setOrder_id(String order_id) {
                this.order_id = order_id;
            }

            public String getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(String goods_id) {
                this.goods_id = goods_id;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getGoods_sn() {
                return goods_sn;
            }

            public void setGoods_sn(String goods_sn) {
                this.goods_sn = goods_sn;
            }

            public String getGoods_number() {
                return goods_number;
            }

            public void setGoods_number(String goods_number) {
                this.goods_number = goods_number;
            }

            public String getMarket_price() {
                return market_price;
            }

            public void setMarket_price(String market_price) {
                this.market_price = market_price;
            }

            public String getGoods_price() {
                return goods_price;
            }

            public void setGoods_price(String goods_price) {
                this.goods_price = goods_price;
            }

            public String getTotal_weight() {
                return total_weight;
            }

            public void setTotal_weight(String total_weight) {
                this.total_weight = total_weight;
            }

            public String getIs_retail() {
                return is_retail;
            }

            public void setIs_retail(String is_retail) {
                this.is_retail = is_retail;
            }

            public String getMeasure_unit() {
                return measure_unit;
            }

            public void setMeasure_unit(String measure_unit) {
                this.measure_unit = measure_unit;
            }

            public String getStorage() {
                return storage;
            }

            public void setStorage(String storage) {
                this.storage = storage;
            }

            public String getSku() {
                return sku;
            }

            public void setSku(String sku) {
                this.sku = sku;
            }

            public String getBrand_sn() {
                return brand_sn;
            }

            public void setBrand_sn(String brand_sn) {
                this.brand_sn = brand_sn;
            }

            public String getRegion_name() {
                return region_name;
            }

            public void setRegion_name(String region_name) {
                this.region_name = region_name;
            }

            public String getPort() {
                return port;
            }

            public void setPort(String port) {
                this.port = port;
            }

            public String getGoods_local() {
                return goods_local;
            }

            public void setGoods_local(String goods_local) {
                this.goods_local = goods_local;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getShop_price_unit() {
                return shop_price_unit;
            }

            public void setShop_price_unit(String shop_price_unit) {
                this.shop_price_unit = shop_price_unit;
            }

            public String getSpec_2() {
                return spec_2;
            }

            public void setSpec_2(String spec_2) {
                this.spec_2 = spec_2;
            }

            public String getShop_price() {
                return shop_price;
            }

            public void setShop_price(String shop_price) {
                this.shop_price = shop_price;
            }

            public String getGoods_type() {
                return goods_type;
            }

            public void setGoods_type(String goods_type) {
                this.goods_type = goods_type;
            }

            public String getGood_face() {
                return good_face;
            }

            public void setGood_face(String good_face) {
                this.good_face = good_face;
            }

            public double getSubtotal() {
                return subtotal;
            }

            public void setSubtotal(double subtotal) {
                this.subtotal = subtotal;
            }

            public int getPart_number() {
                return part_number;
            }

            public void setPart_number(int part_number) {
                this.part_number = part_number;
            }

            public String getPart_weight() {
                return part_weight;
            }

            public void setPart_weight(String part_weight) {
                this.part_weight = part_weight;
            }

            public String getFormated_goods_price() {
                return formated_goods_price;
            }

            public void setFormated_goods_price(String formated_goods_price) {
                this.formated_goods_price = formated_goods_price;
            }

            public String getFormated_subtotal() {
                return formated_subtotal;
            }

            public void setFormated_subtotal(String formated_subtotal) {
                this.formated_subtotal = formated_subtotal;
            }

            public String getGoods_thumb() {
                return goods_thumb;
            }

            public void setGoods_thumb(String goods_thumb) {
                this.goods_thumb = goods_thumb;
            }

            public String getEn_dw() {
                return en_dw;
            }

            public void setEn_dw(String en_dw) {
                this.en_dw = en_dw;
            }

            public int getRegion_name_ch_l() {
                return region_name_ch_l;
            }

            public void setRegion_name_ch_l(int region_name_ch_l) {
                this.region_name_ch_l = region_name_ch_l;
            }

            public String getRegion_name_ch() {
                return region_name_ch;
            }

            public void setRegion_name_ch(String region_name_ch) {
                this.region_name_ch = region_name_ch;
            }

            public String getSpec_1() {
                return spec_1;
            }

            public void setSpec_1(String spec_1) {
                this.spec_1 = spec_1;
            }

            public String getSpec_1_unit() {
                return spec_1_unit;
            }

            public void setSpec_1_unit(String spec_1_unit) {
                this.spec_1_unit = spec_1_unit;
            }

            public String getOffer() {
                return offer;
            }

            public void setOffer(String offer) {
                this.offer = offer;
            }

            public String getSell_type() {
                return sell_type;
            }

            public void setSell_type(String sell_type) {
                this.sell_type = sell_type;
            }
        }
    }
}
