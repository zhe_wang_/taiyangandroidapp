package taiyang.com.entity;

import java.util.List;

/**
 * Created by admin on 2016/8/16.
 * 我的还价
 */
public class DickerListModel {


    /**
     * record_count : 2
     * page_count : 1
     * page : 1
     */

    private PageModel total;
    /**
     * id : 16
     * goods_id : 15
     * last_price : 15000.00
     * price : 14800.00
     * goods_user_id : 6
     * user_id : 11
     * created_at : 2016-08-16 14:43
     * status : 0
     * goods_name : 猪手
     * goods_thumb : http://tydpb2b.bizsoho.com/images/201606/thumb_img/33_thumb_G_14671367925364.jpg
     * brand_sn : 31559
     * formated_shop_price : ￥15000.00
     * unit : 吨
     * sell_type : 4
     * goods_type : 7
     * is_pin : 0
     * offer : 11
     */

    private List<DickerModel> list;

    public PageModel getTotal() {
        return total;
    }

    public void setTotal(PageModel total) {
        this.total = total;
    }

    public List<DickerModel> getList() {
        return list;
    }

    public void setList(List<DickerModel> list) {
        this.list = list;
    }


}
