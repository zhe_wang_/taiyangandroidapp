package taiyang.com.entity;

import java.util.List;

import taiyang.com.adapter.Entity;

/**
 * Created by Administrator on 2016/8/15.
 */
public class InquiryListMoreContentBean {


    /**
     * error : 0
     * message :
     * content : {"type":"hot","action":"productList","model":"other","goods_list":[{"goods_id":"1671","goods_name":"牛前胸","goods_number":9999,"brand_sn":"国产","shop_price":"￥66327993","goods_local":"温哥华","goods_type":"7","sell_type":"4","offer":"11","region_name":"中国","goods_thumb":"http://test.taiyanggo.com/images/201701/thumb_img/1357_thumb_G_14842582810099.jpg","spec_1":"99","spec_1_unit":"件/吨","spec_2":"11","spec_2_unit":"件/吨","goods_weight":"99.00","shop_price_fake":"999","is_pin":0,"unit_name":"元/吨","region_icon":"","picture":""},{"goods_id":"1665","goods_name":"海月水母","goods_number":364,"brand_sn":"06-02","shop_price":"￥33164","goods_local":"天津","goods_type":"6","sell_type":"4","offer":"11","region_name":"智利","goods_thumb":"http://test.taiyanggo.com/images/no_picture.gif","spec_1":"100","spec_1_unit":"件/吨","spec_2":"10","spec_2_unit":"件/吨","goods_weight":"100.00","shop_price_fake":"879","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/14.png","picture":""},{"goods_id":"1686","goods_name":"泥蚶(又名血蚶）","goods_number":154,"brand_sn":"1000837","shop_price":"￥22358","goods_local":"兰州","goods_type":"7","sell_type":"4","offer":"11","region_name":"西班牙","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/23_thumb_G_14763787119593.jpg","spec_1":"125","spec_1_unit":"件/吨","spec_2":"8","spec_2_unit":"件/吨","goods_weight":"125.00","shop_price_fake":"99","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/9.png","picture":""},{"goods_id":"1695","goods_name":"虾仁/竹蛏","goods_number":55,"brand_sn":"135/1/171/1","shop_price":"￥19800","goods_local":"宁波","goods_type":"6","sell_type":"5","offer":"10","region_name":"比利时/比利时","goods_thumb":"http://test.taiyanggo.com/images/no_picture.gif","spec_1":"3.00","spec_1_unit":"吨/柜","spec_2":"","spec_2_unit":"","goods_weight":"3.00","shop_price_fake":"0","is_pin":1,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/22.png","picture":""},{"goods_id":"1674","goods_name":"澳洲鲭鲐","goods_number":23,"brand_sn":"171/1","shop_price":"￥111111","goods_local":"北京","goods_type":"6","sell_type":"5","offer":"11","region_name":"比利时","goods_thumb":"http://test.taiyanggo.com/images/no_picture.gif","spec_1":"25.00","spec_1_unit":"吨/柜","spec_2":"","spec_2_unit":"","goods_weight":"25.00","shop_price_fake":"0","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/22.png","picture":""},{"goods_id":"1669","goods_name":"虾仁","goods_number":8,"brand_sn":"19246","shop_price":"￥1","goods_local":"NYC","goods_type":"7","sell_type":"4","offer":"11","region_name":"美国","goods_thumb":"http://test.taiyanggo.com/images/no_picture.gif","spec_1":"1000","spec_1_unit":"件/吨","spec_2":"1","spec_2_unit":"件/吨","goods_weight":"1000.00","shop_price_fake":"0","is_pin":0,"unit_name":"元/吨","region_icon":"","picture":""},{"goods_id":"1692","goods_name":"牛后胸","goods_number":55,"brand_sn":"85084001","shop_price":"￥37263","goods_local":"香港","goods_type":"6","sell_type":"5","offer":"11","region_name":"法国","goods_thumb":"http://test.taiyanggo.com/images/201701/thumb_img/1358_thumb_G_14842581334650.jpg","spec_1":"2.00","spec_1_unit":"吨/柜","spec_2":"","spec_2_unit":"","goods_weight":"2.00","shop_price_fake":"0","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/11.png","picture":""}]}
     */

    private String error;
    private String message;
    private ContentBean content;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ContentBean getContent() {
        return content;
    }

    public void setContent(ContentBean content) {
        this.content = content;
    }

    public static class ContentBean {
        /**
         * type : hot
         * action : productList
         * model : other
         * goods_list : [{"goods_id":"1671","goods_name":"牛前胸","goods_number":9999,"brand_sn":"国产","shop_price":"￥66327993","goods_local":"温哥华","goods_type":"7","sell_type":"4","offer":"11","region_name":"中国","goods_thumb":"http://test.taiyanggo.com/images/201701/thumb_img/1357_thumb_G_14842582810099.jpg","spec_1":"99","spec_1_unit":"件/吨","spec_2":"11","spec_2_unit":"件/吨","goods_weight":"99.00","shop_price_fake":"999","is_pin":0,"unit_name":"元/吨","region_icon":"","picture":""},{"goods_id":"1665","goods_name":"海月水母","goods_number":364,"brand_sn":"06-02","shop_price":"￥33164","goods_local":"天津","goods_type":"6","sell_type":"4","offer":"11","region_name":"智利","goods_thumb":"http://test.taiyanggo.com/images/no_picture.gif","spec_1":"100","spec_1_unit":"件/吨","spec_2":"10","spec_2_unit":"件/吨","goods_weight":"100.00","shop_price_fake":"879","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/14.png","picture":""},{"goods_id":"1686","goods_name":"泥蚶(又名血蚶）","goods_number":154,"brand_sn":"1000837","shop_price":"￥22358","goods_local":"兰州","goods_type":"7","sell_type":"4","offer":"11","region_name":"西班牙","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/23_thumb_G_14763787119593.jpg","spec_1":"125","spec_1_unit":"件/吨","spec_2":"8","spec_2_unit":"件/吨","goods_weight":"125.00","shop_price_fake":"99","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/9.png","picture":""},{"goods_id":"1695","goods_name":"虾仁/竹蛏","goods_number":55,"brand_sn":"135/1/171/1","shop_price":"￥19800","goods_local":"宁波","goods_type":"6","sell_type":"5","offer":"10","region_name":"比利时/比利时","goods_thumb":"http://test.taiyanggo.com/images/no_picture.gif","spec_1":"3.00","spec_1_unit":"吨/柜","spec_2":"","spec_2_unit":"","goods_weight":"3.00","shop_price_fake":"0","is_pin":1,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/22.png","picture":""},{"goods_id":"1674","goods_name":"澳洲鲭鲐","goods_number":23,"brand_sn":"171/1","shop_price":"￥111111","goods_local":"北京","goods_type":"6","sell_type":"5","offer":"11","region_name":"比利时","goods_thumb":"http://test.taiyanggo.com/images/no_picture.gif","spec_1":"25.00","spec_1_unit":"吨/柜","spec_2":"","spec_2_unit":"","goods_weight":"25.00","shop_price_fake":"0","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/22.png","picture":""},{"goods_id":"1669","goods_name":"虾仁","goods_number":8,"brand_sn":"19246","shop_price":"￥1","goods_local":"NYC","goods_type":"7","sell_type":"4","offer":"11","region_name":"美国","goods_thumb":"http://test.taiyanggo.com/images/no_picture.gif","spec_1":"1000","spec_1_unit":"件/吨","spec_2":"1","spec_2_unit":"件/吨","goods_weight":"1000.00","shop_price_fake":"0","is_pin":0,"unit_name":"元/吨","region_icon":"","picture":""},{"goods_id":"1692","goods_name":"牛后胸","goods_number":55,"brand_sn":"85084001","shop_price":"￥37263","goods_local":"香港","goods_type":"6","sell_type":"5","offer":"11","region_name":"法国","goods_thumb":"http://test.taiyanggo.com/images/201701/thumb_img/1358_thumb_G_14842581334650.jpg","spec_1":"2.00","spec_1_unit":"吨/柜","spec_2":"","spec_2_unit":"","goods_weight":"2.00","shop_price_fake":"0","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/11.png","picture":""}]
         */

        private String type;
        private String action;
        private String model;
        private List<GoodsListBean> goods_list;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public List<GoodsListBean> getGoods_list() {
            return goods_list;
        }

        public void setGoods_list(List<GoodsListBean> goods_list) {
            this.goods_list = goods_list;
        }

        public static class GoodsListBean extends Entity{
            /**
             * goods_id : 1671
             * goods_name : 牛前胸
             * goods_number : 9999
             * brand_sn : 国产
             * shop_price : ￥66327993
             * goods_local : 温哥华
             * goods_type : 7
             * sell_type : 4
             * offer : 11
             * region_name : 中国
             * goods_thumb : http://test.taiyanggo.com/images/201701/thumb_img/1357_thumb_G_14842582810099.jpg
             * spec_1 : 99
             * spec_1_unit : 件/吨
             * spec_2 : 11
             * spec_2_unit : 件/吨
             * goods_weight : 99.00
             * shop_price_fake : 999
             * is_pin : 0
             * unit_name : 元/吨
             * region_icon :
             * picture :
             */

            private String goods_id;
            private String goods_name;
            private int goods_number;
            private String brand_sn;
            private String shop_price;
            private String goods_local;
            private String goods_type;
            private String sell_type;
            private String offer;
            private String region_name;
            private String goods_thumb;
            private String spec_1;
            private String spec_1_unit;
            private String spec_2;
            private String spec_2_unit;
            private String goods_weight;
            private String shop_price_fake;
            private int is_pin;
            private String unit_name;
            private String region_icon;
            private String picture;

            public String getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(String goods_id) {
                this.goods_id = goods_id;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public int getGoods_number() {
                return goods_number;
            }

            public void setGoods_number(int goods_number) {
                this.goods_number = goods_number;
            }

            public String getBrand_sn() {
                return brand_sn;
            }

            public void setBrand_sn(String brand_sn) {
                this.brand_sn = brand_sn;
            }

            public String getShop_price() {
                return shop_price;
            }

            public void setShop_price(String shop_price) {
                this.shop_price = shop_price;
            }

            public String getGoods_local() {
                return goods_local;
            }

            public void setGoods_local(String goods_local) {
                this.goods_local = goods_local;
            }

            public String getGoods_type() {
                return goods_type;
            }

            public void setGoods_type(String goods_type) {
                this.goods_type = goods_type;
            }

            public String getSell_type() {
                return sell_type;
            }

            public void setSell_type(String sell_type) {
                this.sell_type = sell_type;
            }

            public String getOffer() {
                return offer;
            }

            public void setOffer(String offer) {
                this.offer = offer;
            }

            public String getRegion_name() {
                return region_name;
            }

            public void setRegion_name(String region_name) {
                this.region_name = region_name;
            }

            public String getGoods_thumb() {
                return goods_thumb;
            }

            public void setGoods_thumb(String goods_thumb) {
                this.goods_thumb = goods_thumb;
            }

            public String getSpec_1() {
                return spec_1;
            }

            public void setSpec_1(String spec_1) {
                this.spec_1 = spec_1;
            }

            public String getSpec_1_unit() {
                return spec_1_unit;
            }

            public void setSpec_1_unit(String spec_1_unit) {
                this.spec_1_unit = spec_1_unit;
            }

            public String getSpec_2() {
                return spec_2;
            }

            public void setSpec_2(String spec_2) {
                this.spec_2 = spec_2;
            }

            public String getSpec_2_unit() {
                return spec_2_unit;
            }

            public void setSpec_2_unit(String spec_2_unit) {
                this.spec_2_unit = spec_2_unit;
            }

            public String getGoods_weight() {
                return goods_weight;
            }

            public void setGoods_weight(String goods_weight) {
                this.goods_weight = goods_weight;
            }

            public String getShop_price_fake() {
                return shop_price_fake;
            }

            public void setShop_price_fake(String shop_price_fake) {
                this.shop_price_fake = shop_price_fake;
            }

            public int getIs_pin() {
                return is_pin;
            }

            public void setIs_pin(int is_pin) {
                this.is_pin = is_pin;
            }

            public String getUnit_name() {
                return unit_name;
            }

            public void setUnit_name(String unit_name) {
                this.unit_name = unit_name;
            }

            public String getRegion_icon() {
                return region_icon;
            }

            public void setRegion_icon(String region_icon) {
                this.region_icon = region_icon;
            }

            public String getPicture() {
                return picture;
            }

            public void setPicture(String picture) {
                this.picture = picture;
            }
        }
    }
}
