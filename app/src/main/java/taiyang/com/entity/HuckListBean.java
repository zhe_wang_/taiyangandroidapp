package taiyang.com.entity;

/**
 * Created by Administrator on 2016/9/18.
 * 报盘信息 出价列表模板实体
 */
public class HuckListBean {

    public HuckListBean() {

    }

    public HuckListBean(String created_at, String formated_price, String unit, String user_name) {
        this.created_at = created_at;
        this.formated_price = formated_price;
        this.unit = unit;
        this.user_name = user_name;
    }

    private String created_at;
    private String formated_price;
    private String unit;
    private String user_name;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getFormated_price() {
        return formated_price;
    }

    public void setFormated_price(String formated_price) {
        this.formated_price = formated_price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}
