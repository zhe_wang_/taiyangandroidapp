package taiyang.com.entity;

import java.util.List;

import taiyang.com.adapter.Entity;

/**
 * Created by Administrator on 2016/8/9.
 */
public class ChangeOfferInfoBean  {


    /**
     * goods_id : 1801
     * goods_sn : 123
     * cat_id : 0
     * cat_ids : /#/1/#/3/#/227/#/
     * base_id : 0
     * base_ids : /#/1336/#/1347/#/146/#/
     * region_id : 0
     * region : /#/6/#/12/#/21/#/
     * region_name : 【AR】阿根廷/【BR】巴西/【CR】哥斯达黎加
     * brand_id : 0
     * brand_ids : /#/91/#/142/#/719/#/
     * brand_sn : 350/SIF103/112
     * goods_name :
     * goods_name_en :
     * is_test : 0
     * goods_names : /#/猪后筒骨/#/羊排/#/鲍鱼/#/
     * goods_name_style : +
     * desc_title :
     * click_count : 1
     * provider_name :
     * goods_number : 12
     * goods_weight : 527.00
     * market_price : 0.00
     * shop_price : 663.28
     * promote_price : 0.00
     * promote_start_date : 0
     * promote_end_date : 0
     * promote_number : 0
     * warn_number : 1
     * keywords :
     * goods_brief :
     * goods_desc :
     * goods_desc_mobile :
     * goods_thumb :
     * goods_img :
     * original_img :
     * goods_image :
     * goods_images : /#//#//#//#/
     * is_real : 1
     * extension_code :
     * is_on_sale : 0
     * is_alone_sale : 1
     * is_shipping : 0
     * integral : 0
     * add_time : 1504479720
     * sort_order : 100
     * is_delete : 0
     * is_best : 0
     * is_new : 0
     * is_hot : 0
     * is_promote : 0
     * bonus_type_id : 0
     * last_update : 1504479720
     * goods_type : 6
     * seller_note :
     * give_integral : -1
     * rank_integral : -1
     * suppliers_id : null
     * is_check : 1
     * check_msg :
     * measure_unit : 柜
     * sale_num : 0
     * comment_num : 0
     * offer_type : 1
     * sell_type : 5
     * offer : 11
     * arrive_date : 2017-09-04
     * lading_date : 2017-09-04
     * port : 29
     * prepay : 17
     * spec_1 :
     * spec_1_unit :
     * spec_2 :
     * spec_2_unit :
     * specs_1 : /#/12/#/25/#/123/#/
     * specs_1_unit : /#/KG/件/#/KG/件/#/KG/件/#/
     * specs_2 : /#/1000/#/1000/#/1000/#/
     * specs_2_unit : /#/件/吨/#/件/吨/#/件/吨/#/
     * specs_3 : /#/13/#/58/#/456/#/
     * specs_3_unit : /#/吨/整柜/#/吨/整柜/#/吨/整柜/#/
     * pack : 0
     * packs : /#/8/#/8/#/8/#/
     * goods_txt : 呵呵测试
     * user_id : 4
     * make_date : 0
     * make_dates : /#/1504454400/#/1504454400/#/1504454400/#/
     * goods_local :
     * huckster : 0
     * is_entrust : 0
     * entrust_order_id : 0
     * picture_id : 518,519,520
     * prepay_num : 0
     * sku : N17090415231
     * shop_price_am : 100.00
     * shop_price_eur : 0.00
     * currency : 2
     * currency_money : 100.00
     * spec_txt :
     * spec_txts : /#/23/#/测试/#/测/#/
     * number_zero_time : 0
     * shop_price_unit : 0
     * good_face : images/201709/1504479718738859443.jpg
     * arrive_m1 : 0
     * arrive_m2 : 0
     * lading_m1 : 0
     * lading_m2 : 0
     * huckster_time : 0
     * shop_price_fake : 0
     * goods_base : [{"region":"6","region_name":"【AR】阿根廷","brand_list":{"736":"9999","131":"2062","130":"1918","129":"189","128":"4750","127":"4725","126":"4720","125":"4641","124":"4073","123":"4069","122":"3675","121":"2091","132":"1399/1399/4641","133":"1989/1989/4641","134":"2025/391/391","735":"8888","734":"88888","732":"520","718":"111","684":"78899","673":"888","139":"2035/2035/1113","138":"4720/4720/4750","137":"4069/4069/4750","136":"13/13/4725","135":"4073/3675/3675","120":"2082","119":"2035","118":"2025","102":"1631","101":"1621","100":"3811","99":"2589","98":"1774","97":"1683","96":"1610","95":"1550","94":"1325","93":"1310","92":"1304","103":"1646","104":"3976","105":"4491","117":"1989","116":"1970","115":"1930","114":"1920","113":"1399","112":"1352","111":"1130","109":"1014","108":"249","107":"13","106":"3649","91":"350"},"brand_ids":"91","brand_name":"350","brand_sn":"350","brand_logo":"images/no_picture.gif","base_ids":"1336","base_ids_name":null,"goods_names":"猪后筒骨","goods_images":null,"specs_1":"12","specs_1_unit":"KG/件","specs_2":"1000","specs_2_unit":"件/吨","specs_3":"13","specs_3_unit":"吨/整柜","packs":"8","packs_name":"定装","make_date":"2017-09-04","spec_txts":"23","cat_ids":"1","cat_id_first":"1"},{"region":"12","region_name":"【BR】巴西","brand_list":{"730":"333","191":"SIF2051","190":"SIF232","189":"SIF177","188":"SIF4507","187":"SIF2543","186":"SIF2007","185":"SIF458","184":"SIF421","183":"SIF385","182":"SIF337","181":"SIF42","180":"SIF1001","179":"SIF68","178":"SIF4430","177":"SIF1672","192":"SIF2121","193":"SIF2924","194":"SIF2960","687":"1010790","685":"2146/915","207":"SIF160","206":"SIF1156","205":"SIF915","204":"SIF3237","203":"SIF784","202":"SIF3681","201":"SIF2146","200":"SIF377","199":"SIF1184","198":"SIF3548","197":"SIF3392","196":"SIF3225","195":"SIF504","176":"SIF3571","175":"SIF725","174":"SIF2014","155":"SIF2032","154":"SIF2022","153":"SIF1798","152":"SIF1661","151":"SIF1215","150":"SIF1155","149":"SIF922","148":"SIF601","147":"SIF576","146":"SIF544","145":"SIF530","144":"SIF516","143":"SIF121","142":"SIF103","141":"SIF18","156":"SIF2435","157":"SIF2485","158":"SIF3125","173":"SIF4017","172":"SIF716","171":"SIF797","170":"SIF4166","169":"SIF3404","168":"SIF3409","167":"SIF2172","166":"SIF1194","165":"SIF490","164":"SIF87","163":"SIF4444","162":"SIF3887","161":"SIF3742","160":"SIF3595","159":"SIF3300","140":"SIF1"},"brand_ids":"142","brand_name":"SIF103","brand_sn":"SIF103","brand_logo":"images/no_picture.gif","base_ids":"1347","base_ids_name":null,"goods_names":"羊排","goods_images":null,"specs_1":"25","specs_1_unit":"KG/件","specs_2":"1000","specs_2_unit":"件/吨","specs_3":"58","specs_3_unit":"吨/整柜","packs":"8","packs_name":"定装","make_date":"2017-09-04","spec_txts":"测试","cat_ids":"3","cat_id_first":"3"},{"region":"21","region_name":"【CR】哥斯达黎加","brand_list":{"719":"112","737":"586"},"brand_ids":"719","brand_name":"112","brand_sn":"112","brand_logo":"images/no_picture.gif","base_ids":"146","base_ids_name":null,"goods_names":"鲍鱼","goods_images":null,"specs_1":"123","specs_1_unit":"KG/件","specs_2":"1000","specs_2_unit":"件/吨","specs_3":"456","specs_3_unit":"吨/整柜","packs":"8","packs_name":"定装","make_date":"2017-09-04","spec_txts":"测","cat_ids":"227","cat_id_first":"5"}]
     * picture_list : [{"id":"518","thumb_url":"images/201709/1504479719462733043.jpg","img_url":"images/201709/1504479719462733043.jpg"},{"id":"519","thumb_url":"images/201709/1504479719455432486.jpg","img_url":"images/201709/1504479719455432486.jpg"},{"id":"520","thumb_url":"images/201709/1504479720666174796.jpg","img_url":"images/201709/1504479720666174796.jpg"}]
     * cat_id_first : 0
     * currency_money_input : 100.00
     */

    private String goods_id;
    private String goods_sn;
    private String cat_id;
    private String cat_ids;
    private String base_id;
    private String base_ids;
    private String region_id;
    private String region;
    private String region_name;
    private String brand_id;
    private String brand_ids;
    private String brand_sn;
    private String goods_name;
    private String goods_name_en;
    private String is_test;
    private String goods_names;
    private String goods_name_style;
    private String desc_title;
    private String click_count;
    private String provider_name;
    private String goods_number;
    private String goods_weight;
    private String market_price;
    private String shop_price;
    private String promote_price;
    private String promote_start_date;
    private String promote_end_date;
    private String promote_number;
    private String warn_number;
    private String keywords;
    private String goods_brief;
    private String goods_desc;
    private String goods_desc_mobile;
    private String goods_thumb;
    private String goods_img;
    private String original_img;
    private String goods_image;
    private String goods_images;
    private String is_real;
    private String extension_code;
    private String is_on_sale;
    private String is_alone_sale;
    private String is_shipping;
    private String integral;
    private String add_time;
    private String sort_order;
    private String is_delete;
    private String is_best;
    private String is_new;
    private String is_hot;
    private String is_promote;
    private String bonus_type_id;
    private String last_update;
    private String goods_type;
    private String seller_note;
    private String give_integral;
    private String rank_integral;
    private Object suppliers_id;
    private String is_check;
    private String check_msg;
    private String measure_unit;
    private String sale_num;
    private String comment_num;
    private String offer_type;
    private String sell_type;
    private String offer;
    private String arrive_date;
    private String lading_date;
    private String port;
    private String prepay;
    private String spec_1;
    private String spec_1_unit;
    private String spec_2;
    private String spec_2_unit;
    private String specs_1;
    private String specs_1_unit;
    private String specs_2;
    private String specs_2_unit;
    private String specs_3;
    private String specs_3_unit;
    private String pack;
    private String packs;
    private String goods_txt;
    private String user_id;
    private String make_date;
    private String make_dates;
    private String goods_local;
    private String huckster;
    private String is_entrust;
    private String entrust_order_id;
    private String picture_id;
    private String prepay_num;
    private String sku;
    private String shop_price_am;
    private String shop_price_eur;
    private String currency;
    private String currency_money;
    private String spec_txt;
    private String spec_txts;
    private String number_zero_time;
    private String shop_price_unit;
    private String good_face;
    private String arrive_m1;
    private String arrive_m2;
    private String lading_m1;
    private String lading_m2;
    private String huckster_time;
    private String shop_price_fake;
    private String cat_id_first;
    private String currency_money_input;
    private List<GoodsBaseBean> goods_base;
    private List<PictureListBean> picture_list;

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getGoods_sn() {
        return goods_sn;
    }

    public void setGoods_sn(String goods_sn) {
        this.goods_sn = goods_sn;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_ids() {
        return cat_ids;
    }

    public void setCat_ids(String cat_ids) {
        this.cat_ids = cat_ids;
    }

    public String getBase_id() {
        return base_id;
    }

    public void setBase_id(String base_id) {
        this.base_id = base_id;
    }

    public String getBase_ids() {
        return base_ids;
    }

    public void setBase_ids(String base_ids) {
        this.base_ids = base_ids;
    }

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getBrand_ids() {
        return brand_ids;
    }

    public void setBrand_ids(String brand_ids) {
        this.brand_ids = brand_ids;
    }

    public String getBrand_sn() {
        return brand_sn;
    }

    public void setBrand_sn(String brand_sn) {
        this.brand_sn = brand_sn;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getGoods_name_en() {
        return goods_name_en;
    }

    public void setGoods_name_en(String goods_name_en) {
        this.goods_name_en = goods_name_en;
    }

    public String getIs_test() {
        return is_test;
    }

    public void setIs_test(String is_test) {
        this.is_test = is_test;
    }

    public String getGoods_names() {
        return goods_names;
    }

    public void setGoods_names(String goods_names) {
        this.goods_names = goods_names;
    }

    public String getGoods_name_style() {
        return goods_name_style;
    }

    public void setGoods_name_style(String goods_name_style) {
        this.goods_name_style = goods_name_style;
    }

    public String getDesc_title() {
        return desc_title;
    }

    public void setDesc_title(String desc_title) {
        this.desc_title = desc_title;
    }

    public String getClick_count() {
        return click_count;
    }

    public void setClick_count(String click_count) {
        this.click_count = click_count;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public void setProvider_name(String provider_name) {
        this.provider_name = provider_name;
    }

    public String getGoods_number() {
        return goods_number;
    }

    public void setGoods_number(String goods_number) {
        this.goods_number = goods_number;
    }

    public String getGoods_weight() {
        return goods_weight;
    }

    public void setGoods_weight(String goods_weight) {
        this.goods_weight = goods_weight;
    }

    public String getMarket_price() {
        return market_price;
    }

    public void setMarket_price(String market_price) {
        this.market_price = market_price;
    }

    public String getShop_price() {
        return shop_price;
    }

    public void setShop_price(String shop_price) {
        this.shop_price = shop_price;
    }

    public String getPromote_price() {
        return promote_price;
    }

    public void setPromote_price(String promote_price) {
        this.promote_price = promote_price;
    }

    public String getPromote_start_date() {
        return promote_start_date;
    }

    public void setPromote_start_date(String promote_start_date) {
        this.promote_start_date = promote_start_date;
    }

    public String getPromote_end_date() {
        return promote_end_date;
    }

    public void setPromote_end_date(String promote_end_date) {
        this.promote_end_date = promote_end_date;
    }

    public String getPromote_number() {
        return promote_number;
    }

    public void setPromote_number(String promote_number) {
        this.promote_number = promote_number;
    }

    public String getWarn_number() {
        return warn_number;
    }

    public void setWarn_number(String warn_number) {
        this.warn_number = warn_number;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getGoods_brief() {
        return goods_brief;
    }

    public void setGoods_brief(String goods_brief) {
        this.goods_brief = goods_brief;
    }

    public String getGoods_desc() {
        return goods_desc;
    }

    public void setGoods_desc(String goods_desc) {
        this.goods_desc = goods_desc;
    }

    public String getGoods_desc_mobile() {
        return goods_desc_mobile;
    }

    public void setGoods_desc_mobile(String goods_desc_mobile) {
        this.goods_desc_mobile = goods_desc_mobile;
    }

    public String getGoods_thumb() {
        return goods_thumb;
    }

    public void setGoods_thumb(String goods_thumb) {
        this.goods_thumb = goods_thumb;
    }

    public String getGoods_img() {
        return goods_img;
    }

    public void setGoods_img(String goods_img) {
        this.goods_img = goods_img;
    }

    public String getOriginal_img() {
        return original_img;
    }

    public void setOriginal_img(String original_img) {
        this.original_img = original_img;
    }

    public String getGoods_image() {
        return goods_image;
    }

    public void setGoods_image(String goods_image) {
        this.goods_image = goods_image;
    }

    public String getGoods_images() {
        return goods_images;
    }

    public void setGoods_images(String goods_images) {
        this.goods_images = goods_images;
    }

    public String getIs_real() {
        return is_real;
    }

    public void setIs_real(String is_real) {
        this.is_real = is_real;
    }

    public String getExtension_code() {
        return extension_code;
    }

    public void setExtension_code(String extension_code) {
        this.extension_code = extension_code;
    }

    public String getIs_on_sale() {
        return is_on_sale;
    }

    public void setIs_on_sale(String is_on_sale) {
        this.is_on_sale = is_on_sale;
    }

    public String getIs_alone_sale() {
        return is_alone_sale;
    }

    public void setIs_alone_sale(String is_alone_sale) {
        this.is_alone_sale = is_alone_sale;
    }

    public String getIs_shipping() {
        return is_shipping;
    }

    public void setIs_shipping(String is_shipping) {
        this.is_shipping = is_shipping;
    }

    public String getIntegral() {
        return integral;
    }

    public void setIntegral(String integral) {
        this.integral = integral;
    }

    public String getAdd_time() {
        return add_time;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }

    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }

    public String getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(String is_delete) {
        this.is_delete = is_delete;
    }

    public String getIs_best() {
        return is_best;
    }

    public void setIs_best(String is_best) {
        this.is_best = is_best;
    }

    public String getIs_new() {
        return is_new;
    }

    public void setIs_new(String is_new) {
        this.is_new = is_new;
    }

    public String getIs_hot() {
        return is_hot;
    }

    public void setIs_hot(String is_hot) {
        this.is_hot = is_hot;
    }

    public String getIs_promote() {
        return is_promote;
    }

    public void setIs_promote(String is_promote) {
        this.is_promote = is_promote;
    }

    public String getBonus_type_id() {
        return bonus_type_id;
    }

    public void setBonus_type_id(String bonus_type_id) {
        this.bonus_type_id = bonus_type_id;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getGoods_type() {
        return goods_type;
    }

    public void setGoods_type(String goods_type) {
        this.goods_type = goods_type;
    }

    public String getSeller_note() {
        return seller_note;
    }

    public void setSeller_note(String seller_note) {
        this.seller_note = seller_note;
    }

    public String getGive_integral() {
        return give_integral;
    }

    public void setGive_integral(String give_integral) {
        this.give_integral = give_integral;
    }

    public String getRank_integral() {
        return rank_integral;
    }

    public void setRank_integral(String rank_integral) {
        this.rank_integral = rank_integral;
    }

    public Object getSuppliers_id() {
        return suppliers_id;
    }

    public void setSuppliers_id(Object suppliers_id) {
        this.suppliers_id = suppliers_id;
    }

    public String getIs_check() {
        return is_check;
    }

    public void setIs_check(String is_check) {
        this.is_check = is_check;
    }

    public String getCheck_msg() {
        return check_msg;
    }

    public void setCheck_msg(String check_msg) {
        this.check_msg = check_msg;
    }

    public String getMeasure_unit() {
        return measure_unit;
    }

    public void setMeasure_unit(String measure_unit) {
        this.measure_unit = measure_unit;
    }

    public String getSale_num() {
        return sale_num;
    }

    public void setSale_num(String sale_num) {
        this.sale_num = sale_num;
    }

    public String getComment_num() {
        return comment_num;
    }

    public void setComment_num(String comment_num) {
        this.comment_num = comment_num;
    }

    public String getOffer_type() {
        return offer_type;
    }

    public void setOffer_type(String offer_type) {
        this.offer_type = offer_type;
    }

    public String getSell_type() {
        return sell_type;
    }

    public void setSell_type(String sell_type) {
        this.sell_type = sell_type;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getArrive_date() {
        return arrive_date;
    }

    public void setArrive_date(String arrive_date) {
        this.arrive_date = arrive_date;
    }

    public String getLading_date() {
        return lading_date;
    }

    public void setLading_date(String lading_date) {
        this.lading_date = lading_date;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPrepay() {
        return prepay;
    }

    public void setPrepay(String prepay) {
        this.prepay = prepay;
    }

    public String getSpec_1() {
        return spec_1;
    }

    public void setSpec_1(String spec_1) {
        this.spec_1 = spec_1;
    }

    public String getSpec_1_unit() {
        return spec_1_unit;
    }

    public void setSpec_1_unit(String spec_1_unit) {
        this.spec_1_unit = spec_1_unit;
    }

    public String getSpec_2() {
        return spec_2;
    }

    public void setSpec_2(String spec_2) {
        this.spec_2 = spec_2;
    }

    public String getSpec_2_unit() {
        return spec_2_unit;
    }

    public void setSpec_2_unit(String spec_2_unit) {
        this.spec_2_unit = spec_2_unit;
    }

    public String getSpecs_1() {
        return specs_1;
    }

    public void setSpecs_1(String specs_1) {
        this.specs_1 = specs_1;
    }

    public String getSpecs_1_unit() {
        return specs_1_unit;
    }

    public void setSpecs_1_unit(String specs_1_unit) {
        this.specs_1_unit = specs_1_unit;
    }

    public String getSpecs_2() {
        return specs_2;
    }

    public void setSpecs_2(String specs_2) {
        this.specs_2 = specs_2;
    }

    public String getSpecs_2_unit() {
        return specs_2_unit;
    }

    public void setSpecs_2_unit(String specs_2_unit) {
        this.specs_2_unit = specs_2_unit;
    }

    public String getSpecs_3() {
        return specs_3;
    }

    public void setSpecs_3(String specs_3) {
        this.specs_3 = specs_3;
    }

    public String getSpecs_3_unit() {
        return specs_3_unit;
    }

    public void setSpecs_3_unit(String specs_3_unit) {
        this.specs_3_unit = specs_3_unit;
    }

    public String getPack() {
        return pack;
    }

    public void setPack(String pack) {
        this.pack = pack;
    }

    public String getPacks() {
        return packs;
    }

    public void setPacks(String packs) {
        this.packs = packs;
    }

    public String getGoods_txt() {
        return goods_txt;
    }

    public void setGoods_txt(String goods_txt) {
        this.goods_txt = goods_txt;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMake_date() {
        return make_date;
    }

    public void setMake_date(String make_date) {
        this.make_date = make_date;
    }

    public String getMake_dates() {
        return make_dates;
    }

    public void setMake_dates(String make_dates) {
        this.make_dates = make_dates;
    }

    public String getGoods_local() {
        return goods_local;
    }

    public void setGoods_local(String goods_local) {
        this.goods_local = goods_local;
    }

    public String getHuckster() {
        return huckster;
    }

    public void setHuckster(String huckster) {
        this.huckster = huckster;
    }

    public String getIs_entrust() {
        return is_entrust;
    }

    public void setIs_entrust(String is_entrust) {
        this.is_entrust = is_entrust;
    }

    public String getEntrust_order_id() {
        return entrust_order_id;
    }

    public void setEntrust_order_id(String entrust_order_id) {
        this.entrust_order_id = entrust_order_id;
    }

    public String getPicture_id() {
        return picture_id;
    }

    public void setPicture_id(String picture_id) {
        this.picture_id = picture_id;
    }

    public String getPrepay_num() {
        return prepay_num;
    }

    public void setPrepay_num(String prepay_num) {
        this.prepay_num = prepay_num;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getShop_price_am() {
        return shop_price_am;
    }

    public void setShop_price_am(String shop_price_am) {
        this.shop_price_am = shop_price_am;
    }

    public String getShop_price_eur() {
        return shop_price_eur;
    }

    public void setShop_price_eur(String shop_price_eur) {
        this.shop_price_eur = shop_price_eur;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency_money() {
        return currency_money;
    }

    public void setCurrency_money(String currency_money) {
        this.currency_money = currency_money;
    }

    public String getSpec_txt() {
        return spec_txt;
    }

    public void setSpec_txt(String spec_txt) {
        this.spec_txt = spec_txt;
    }

    public String getSpec_txts() {
        return spec_txts;
    }

    public void setSpec_txts(String spec_txts) {
        this.spec_txts = spec_txts;
    }

    public String getNumber_zero_time() {
        return number_zero_time;
    }

    public void setNumber_zero_time(String number_zero_time) {
        this.number_zero_time = number_zero_time;
    }

    public String getShop_price_unit() {
        return shop_price_unit;
    }

    public void setShop_price_unit(String shop_price_unit) {
        this.shop_price_unit = shop_price_unit;
    }

    public String getGood_face() {
        return good_face;
    }

    public void setGood_face(String good_face) {
        this.good_face = good_face;
    }

    public String getArrive_m1() {
        return arrive_m1;
    }

    public void setArrive_m1(String arrive_m1) {
        this.arrive_m1 = arrive_m1;
    }

    public String getArrive_m2() {
        return arrive_m2;
    }

    public void setArrive_m2(String arrive_m2) {
        this.arrive_m2 = arrive_m2;
    }

    public String getLading_m1() {
        return lading_m1;
    }

    public void setLading_m1(String lading_m1) {
        this.lading_m1 = lading_m1;
    }

    public String getLading_m2() {
        return lading_m2;
    }

    public void setLading_m2(String lading_m2) {
        this.lading_m2 = lading_m2;
    }

    public String getHuckster_time() {
        return huckster_time;
    }

    public void setHuckster_time(String huckster_time) {
        this.huckster_time = huckster_time;
    }

    public String getShop_price_fake() {
        return shop_price_fake;
    }

    public void setShop_price_fake(String shop_price_fake) {
        this.shop_price_fake = shop_price_fake;
    }

    public String getCat_id_first() {
        return cat_id_first;
    }

    public void setCat_id_first(String cat_id_first) {
        this.cat_id_first = cat_id_first;
    }

    public String getCurrency_money_input() {
        return currency_money_input;
    }

    public void setCurrency_money_input(String currency_money_input) {
        this.currency_money_input = currency_money_input;
    }

    public List<GoodsBaseBean> getGoods_base() {
        return goods_base;
    }

    public void setGoods_base(List<GoodsBaseBean> goods_base) {
        this.goods_base = goods_base;
    }

    public List<PictureListBean> getPicture_list() {
        return picture_list;
    }

    public void setPicture_list(List<PictureListBean> picture_list) {
        this.picture_list = picture_list;
    }

    public static class GoodsBaseBean extends Entity{
        /**
         * region : 6
         * region_name : 【AR】阿根廷
         * brand_list : {"736":"9999","131":"2062","130":"1918","129":"189","128":"4750","127":"4725","126":"4720","125":"4641","124":"4073","123":"4069","122":"3675","121":"2091","132":"1399/1399/4641","133":"1989/1989/4641","134":"2025/391/391","735":"8888","734":"88888","732":"520","718":"111","684":"78899","673":"888","139":"2035/2035/1113","138":"4720/4720/4750","137":"4069/4069/4750","136":"13/13/4725","135":"4073/3675/3675","120":"2082","119":"2035","118":"2025","102":"1631","101":"1621","100":"3811","99":"2589","98":"1774","97":"1683","96":"1610","95":"1550","94":"1325","93":"1310","92":"1304","103":"1646","104":"3976","105":"4491","117":"1989","116":"1970","115":"1930","114":"1920","113":"1399","112":"1352","111":"1130","109":"1014","108":"249","107":"13","106":"3649","91":"350"}
         * brand_ids : 91
         * brand_name : 350
         * brand_sn : 350
         * brand_logo : images/no_picture.gif
         * base_ids : 1336
         * base_ids_name : null
         * goods_names : 猪后筒骨
         * goods_images : null
         * specs_1 : 12
         * specs_1_unit : KG/件
         * specs_2 : 1000
         * specs_2_unit : 件/吨
         * specs_3 : 13
         * specs_3_unit : 吨/整柜
         * packs : 8
         * packs_name : 定装
         * make_date : 2017-09-04
         * spec_txts : 23
         * cat_ids : 1
         * cat_id_first : 1
         */

        private String region;
        private String region_name;
        private String brand_ids;
        private String brand_name;
        private String brand_sn;
        private String brand_logo;
        private String base_ids;
        private String base_ids_name;
        private String goods_names;
        private Object goods_images;
        private String specs_1;
        private String specs_1_unit;
        private String specs_2;
        private String specs_2_unit;
        private String specs_3;
        private String specs_3_unit;
        private String packs;
        private String packs_name;
        private String make_date;
        private String spec_txts;
        private String cat_ids;
        private String cat_id_first;

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getRegion_name() {
            return region_name;
        }

        public void setRegion_name(String region_name) {
            this.region_name = region_name;
        }

        public String getBrand_ids() {
            return brand_ids;
        }

        public void setBrand_ids(String brand_ids) {
            this.brand_ids = brand_ids;
        }

        public String getBrand_name() {
            return brand_name;
        }

        public void setBrand_name(String brand_name) {
            this.brand_name = brand_name;
        }

        public String getBrand_sn() {
            return brand_sn;
        }

        public void setBrand_sn(String brand_sn) {
            this.brand_sn = brand_sn;
        }

        public String getBrand_logo() {
            return brand_logo;
        }

        public void setBrand_logo(String brand_logo) {
            this.brand_logo = brand_logo;
        }

        public String getBase_ids() {
            return base_ids;
        }

        public void setBase_ids(String base_ids) {
            this.base_ids = base_ids;
        }

        public String getBase_ids_name() {
            return base_ids_name;
        }

        public void setBase_ids_name(String base_ids_name) {
            this.base_ids_name = base_ids_name;
        }

        public String getGoods_names() {
            return goods_names;
        }

        public void setGoods_names(String goods_names) {
            this.goods_names = goods_names;
        }

        public Object getGoods_images() {
            return goods_images;
        }

        public void setGoods_images(Object goods_images) {
            this.goods_images = goods_images;
        }

        public String getSpecs_1() {
            return specs_1;
        }

        public void setSpecs_1(String specs_1) {
            this.specs_1 = specs_1;
        }

        public String getSpecs_1_unit() {
            return specs_1_unit;
        }

        public void setSpecs_1_unit(String specs_1_unit) {
            this.specs_1_unit = specs_1_unit;
        }

        public String getSpecs_2() {
            return specs_2;
        }

        public void setSpecs_2(String specs_2) {
            this.specs_2 = specs_2;
        }

        public String getSpecs_2_unit() {
            return specs_2_unit;
        }

        public void setSpecs_2_unit(String specs_2_unit) {
            this.specs_2_unit = specs_2_unit;
        }

        public String getSpecs_3() {
            return specs_3;
        }

        public void setSpecs_3(String specs_3) {
            this.specs_3 = specs_3;
        }

        public String getSpecs_3_unit() {
            return specs_3_unit;
        }

        public void setSpecs_3_unit(String specs_3_unit) {
            this.specs_3_unit = specs_3_unit;
        }

        public String getPacks() {
            return packs;
        }

        public void setPacks(String packs) {
            this.packs = packs;
        }

        public String getPacks_name() {
            return packs_name;
        }

        public void setPacks_name(String packs_name) {
            this.packs_name = packs_name;
        }

        public String getMake_date() {
            return make_date;
        }

        public void setMake_date(String make_date) {
            this.make_date = make_date;
        }

        public String getSpec_txts() {
            return spec_txts;
        }

        public void setSpec_txts(String spec_txts) {
            this.spec_txts = spec_txts;
        }

        public String getCat_ids() {
            return cat_ids;
        }

        public void setCat_ids(String cat_ids) {
            this.cat_ids = cat_ids;
        }

        public String getCat_id_first() {
            return cat_id_first;
        }

        public void setCat_id_first(String cat_id_first) {
            this.cat_id_first = cat_id_first;
        }

    }

    public static class PictureListBean {
        /**
         * id : 518
         * thumb_url : images/201709/1504479719462733043.jpg
         * img_url : images/201709/1504479719462733043.jpg
         */

        private String id;
        private String thumb_url;
        private String img_url;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getThumb_url() {
            return thumb_url;
        }

        public void setThumb_url(String thumb_url) {
            this.thumb_url = thumb_url;
        }

        public String getImg_url() {
            return img_url;
        }

        public void setImg_url(String img_url) {
            this.img_url = img_url;
        }
    }
}
