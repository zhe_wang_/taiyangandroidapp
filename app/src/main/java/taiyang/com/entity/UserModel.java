package taiyang.com.entity;

import java.io.Serializable;

/**
 * Created by admin on 2016/7/21.
 */
public class UserModel implements Serializable {

    /**
     * user_id : 11
     * email : lvonfeng@qq.com
     * user_name : lvonfeng1
     * sex : 1
     * birthday : 1956-01-01
     * user_money : 0.00
     * frozen_money : 0.00
     * pay_points : 0
     * rank_points : 0
     * address_id : 0
     * reg_time : 1467850181
     * last_login : 1469636164
     * last_ip : 211.100.57.196
     * visit_count : 20
     * user_rank : 0
     * parent_id : 0
     * msn :
     * qq :
     * office_phone :
     * home_phone :
     * mobile_phone : 18500093343
     * is_validated : 0
     * credit_line : 0.00
     * user_face : http://tydpb2b.bizsoho.com/ /config/head_img/1468883349236364168.jpg
     * kefu_id : 0
     * shop_name : 哈哈
     * address : 北京
     * formated_reg_time : 2016-07-07
     * formated_last_login : 2016-07-28
     * formated_user_money : ￥0.00
     * formated_frozen_money : ￥0.00
     * rank_name :
     * bonus_number : 0
     * token : QBddouSe25J48T7mRoUi33iRmCF9HSxm
     * alias:大大
     */

    private String customer_service;
    private String user_id;
    private String email;
    private String user_name;
    private String sex;
    private String birthday;
    private String user_money;
    private String huck_num;
    private String frozen_money;
    private String pay_points;
    private String rank_points;
    private int address_id;
    private String reg_time;
    private String last_login;
    private String last_ip;

    public String getHuck_num() {
        return huck_num;
    }

    public String getJoin() {
        return join;
    }

    public void setJoin(String join) {
        this.join = join;
    }

    private String join;
    private String visit_count;
    private String user_rank;
    private String parent_id;
    private String msn;
    private String qq;
    private String office_phone;
    private String home_phone;
    private String mobile_phone;
    private String is_validated;
    private String credit_line;
    private String user_face;
    private String kefu_id;
    private String shop_name;

    public String getShop_info() {
        return shop_info;
    }

    public void setShop_info(String shop_info) {
        this.shop_info = shop_info;
    }

    private String shop_info;
//    private String address;
    private String formated_reg_time;
    private String formated_last_login;
    private String formated_user_money;
    private String formated_frozen_money;
    private String rank_name;
    private String bonus_number;
    private String token;
    private String alias;
    private String province;
    private String city;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getUser_money() {
        return user_money;
    }

    public void setUser_money(String user_money) {
        this.user_money = user_money;
    }

    public String getFrozen_money() {
        return frozen_money;
    }

    public void setFrozen_money(String frozen_money) {
        this.frozen_money = frozen_money;
    }

    public String getPay_points() {
        return pay_points;
    }

    public void setPay_points(String pay_points) {
        this.pay_points = pay_points;
    }

    public String getRank_points() {
        return rank_points;
    }

    public void setRank_points(String rank_points) {
        this.rank_points = rank_points;
    }

    public int getAddress_id() {
        return address_id;
    }

    public void setAddress_id(int address_id) {
        this.address_id = address_id;
    }

    public String getReg_time() {
        return reg_time;
    }

    public void setReg_time(String reg_time) {
        this.reg_time = reg_time;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getLast_ip() {
        return last_ip;
    }

    public void setLast_ip(String last_ip) {
        this.last_ip = last_ip;
    }

    public String getVisit_count() {
        return visit_count;
    }

    public void setVisit_count(String visit_count) {
        this.visit_count = visit_count;
    }

    public String getUser_rank() {
        return user_rank;
    }

    public void setUser_rank(String user_rank) {
        this.user_rank = user_rank;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getMsn() {
        return msn;
    }

    public void setMsn(String msn) {
        this.msn = msn;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getOffice_phone() {
        return office_phone;
    }

    public void setOffice_phone(String office_phone) {
        this.office_phone = office_phone;
    }

    public String getHome_phone() {
        return home_phone;
    }

    public void setHome_phone(String home_phone) {
        this.home_phone = home_phone;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public void setMobile_phone(String mobile_phone) {
        this.mobile_phone = mobile_phone;
    }

    public String getIs_validated() {
        return is_validated;
    }

    public void setIs_validated(String is_validated) {
        this.is_validated = is_validated;
    }

    public String getCredit_line() {
        return credit_line;
    }

    public void setCredit_line(String credit_line) {
        this.credit_line = credit_line;
    }

    public String getUser_face() {
        return user_face;
    }

    public void setUser_face(String user_face) {
        this.user_face = user_face;
    }

    public String getKefu_id() {
        return kefu_id;
    }

    public void setKefu_id(String kefu_id) {
        this.kefu_id = kefu_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

//    public String getAddress() {
//        return address;
//    }
//
//    public void setAddress(String address) {
//        this.address = address;
//    }

    public String getFormated_reg_time() {
        return formated_reg_time;
    }

    public void setFormated_reg_time(String formated_reg_time) {
        this.formated_reg_time = formated_reg_time;
    }

    public String getFormated_last_login() {
        return formated_last_login;
    }

    public void setFormated_last_login(String formated_last_login) {
        this.formated_last_login = formated_last_login;
    }

    public String getFormated_user_money() {
        return formated_user_money;
    }

    public void setFormated_user_money(String formated_user_money) {
        this.formated_user_money = formated_user_money;
    }

    public String getFormated_frozen_money() {
        return formated_frozen_money;
    }

    public void setFormated_frozen_money(String formated_frozen_money) {
        this.formated_frozen_money = formated_frozen_money;
    }

    public String getRank_name() {
        return rank_name;
    }

    public void setRank_name(String rank_name) {
        this.rank_name = rank_name;
    }

    public String getBonus_number() {
        return bonus_number;
    }

    public void setBonus_number(String bonus_number) {
        this.bonus_number = bonus_number;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCustomer_service() {
        return customer_service;
    }

    public void setCustomer_service(String customer_service) {
        this.customer_service = customer_service;
    }
}
