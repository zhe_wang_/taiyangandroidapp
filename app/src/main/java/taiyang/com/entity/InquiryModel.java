package taiyang.com.entity;

import taiyang.com.adapter.Entity;

/**
 * Created by admin on 2016/8/17.
 */
public class InquiryModel extends Entity{
        private String id;
        private String title;
        private String goods_name;
        private String goods_num;
        private String price_low;
        private String price_up;
        private String address;
        private String user_name;
        private String user_phone;
        private String user_id;
        private String created_at;
        private String memo;
        private String type;
        private Object sku;
        private String created;
        private int memo_len;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_num() {
            return goods_num;
        }

        public void setGoods_num(String goods_num) {
            this.goods_num = goods_num;
        }

        public String getPrice_low() {
            return price_low;
        }

        public void setPrice_low(String price_low) {
            this.price_low = price_low;
        }

        public String getPrice_up() {
            return price_up;
        }

        public void setPrice_up(String price_up) {
            this.price_up = price_up;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getUser_phone() {
            return user_phone;
        }

        public void setUser_phone(String user_phone) {
            this.user_phone = user_phone;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Object getSku() {
            return sku;
        }

        public void setSku(Object sku) {
            this.sku = sku;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public int getMemo_len() {
            return memo_len;
        }

        public void setMemo_len(int memo_len) {
            this.memo_len = memo_len;
        }
}
