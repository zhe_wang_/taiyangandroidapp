package taiyang.com.entity;

import java.util.List;

/**
 * Created by zhew on 8/22/17.
 * Copyright © 2006-2016 365jia.cn 安徽大尺度网络传媒有限公司版权所有
 **/


public class SellIndexBean {


    /**
     * user_info : {"user_id":"4","email":"www@123.com","user_name":"wangxiao123","password":"98f42922441d3565a586403f73eff93d","sex":"1","birthday":"1957-01-01","user_money":"755000.00","frozen_money":"0.00","pay_points":"0","rank_points":"0","address_id":"0","reg_time":"1467081480","last_login":"1503349012","last_time":"0000-00-00 00:00:00","last_ip":"220.178.14.98","visit_count":"7366","user_rank":"2","is_special":"0","ec_salt":"6846","salt":"0","parent_id":"0","flag":"0","alias":"王小玮","msn":"","qq":"","office_phone":"","home_phone":"","mobile_phone":"18565827222","is_validated":"0","credit_line":"1000000.00","passwd_question":"","passwd_answer":"","user_face":"config/head_img/1469583863661868669.png","city":"52","province":"2","kefu_id":"2","admin":"1","shop_name":"一家肉铺","address":"","shop_info":"","remarks":"","sale_id":"0","formated_user_money":"￥755000.00","formated_frozen_money":"￥0.00"}
     * site_list : [{"id":"6","site_name":"【AR】阿根廷"},{"id":"15","site_name":"【AU】澳大利亚"},{"id":"22","site_name":"【BE】比利时"},{"id":"12","site_name":"【BR】巴西"},{"id":"2","site_name":"【CA】加拿大"},{"id":"14","site_name":"【CL】智利"},{"id":"27","site_name":"【CN】中国"},{"id":"21","site_name":"【CR】哥斯达黎加"},{"id":"3","site_name":"【DE】德国"},{"id":"10","site_name":"【DK】丹麦"},{"id":"9","site_name":"【ES】西班牙"},{"id":"11","site_name":"【FRA】法国"},{"id":"5","site_name":"【HUN】匈牙利"},{"id":"25","site_name":"【ID】印尼"},{"id":"18","site_name":"【IE】爱尔兰"},{"id":"19","site_name":"【ITA】意大利"},{"id":"17","site_name":"【MN】蒙古"},{"id":"23","site_name":"【MX】墨西哥"},{"id":"8","site_name":"【NL】荷兰"},{"id":"16","site_name":"【NZL】新西兰"},{"id":"26","site_name":"【PK】巴基斯坦"},{"id":"20","site_name":"【PL】波兰"},{"id":"24","site_name":"【RO】罗马尼亚"},{"id":"7","site_name":"【UK】英国"},{"id":"1","site_name":"【US】美国"},{"id":"13","site_name":"【UY】乌拉圭"}]
     * cat_list : [{"cat_id":"1","cat_name":"猪"},{"cat_id":"2","cat_name":"牛"},{"cat_id":"3","cat_name":"羊"},{"cat_id":"289","cat_name":"禽类"},{"cat_id":"5","cat_name":"水产"},{"cat_id":"290","cat_name":"其它"}]
     * offer_type : [{"id":"1","val":"CIF"},{"id":"2","val":"FOB"},{"id":"3","val":"DDP"},{"id":"34","val":"CFR"}]
     * port : [{"id":"13","val":"上海"},{"id":"15","val":"南沙"},{"id":"22","val":"北京"},{"id":"25","val":"香港"},{"id":"26","val":"天津"},{"id":"29","val":"宁波"},{"id":"30","val":"青岛"},{"id":"31","val":"大连"},{"id":"32","val":"丹东"},{"id":"33","val":"张家港"},{"id":"35","val":"港口自定义"}]
     * prepay : [{"id":"16","val":"20%"},{"id":"23","val":"30%"},{"id":"17","val":"50%"},{"id":"18","val":"80%"},{"id":"21","val":"100%"}]
     */

    private UserInfoBean user_info;
    private List<SiteListBean> site_list;
    private List<CatListBean> cat_list;
    private List<OfferTypeBean> offer_type;
    private List<PortBean> port;
    private List<PrepayBean> prepay;

    public UserInfoBean getUser_info() {
        return user_info;
    }

    public void setUser_info(UserInfoBean user_info) {
        this.user_info = user_info;
    }

    public List<SiteListBean> getSite_list() {
        return site_list;
    }

    public void setSite_list(List<SiteListBean> site_list) {
        this.site_list = site_list;
    }

    public List<CatListBean> getCat_list() {
        return cat_list;
    }

    public void setCat_list(List<CatListBean> cat_list) {
        this.cat_list = cat_list;
    }

    public List<OfferTypeBean> getOffer_type() {
        return offer_type;
    }

    public void setOffer_type(List<OfferTypeBean> offer_type) {
        this.offer_type = offer_type;
    }

    public List<PortBean> getPort() {
        return port;
    }

    public void setPort(List<PortBean> port) {
        this.port = port;
    }

    public List<PrepayBean> getPrepay() {
        return prepay;
    }

    public void setPrepay(List<PrepayBean> prepay) {
        this.prepay = prepay;
    }

    public static class UserInfoBean {
        /**
         * user_id : 4
         * email : www@123.com
         * user_name : wangxiao123
         * password : 98f42922441d3565a586403f73eff93d
         * sex : 1
         * birthday : 1957-01-01
         * user_money : 755000.00
         * frozen_money : 0.00
         * pay_points : 0
         * rank_points : 0
         * address_id : 0
         * reg_time : 1467081480
         * last_login : 1503349012
         * last_time : 0000-00-00 00:00:00
         * last_ip : 220.178.14.98
         * visit_count : 7366
         * user_rank : 2
         * is_special : 0
         * ec_salt : 6846
         * salt : 0
         * parent_id : 0
         * flag : 0
         * alias : 王小玮
         * msn :
         * qq :
         * office_phone :
         * home_phone :
         * mobile_phone : 18565827222
         * is_validated : 0
         * credit_line : 1000000.00
         * passwd_question :
         * passwd_answer :
         * user_face : config/head_img/1469583863661868669.png
         * city : 52
         * province : 2
         * kefu_id : 2
         * admin : 1
         * shop_name : 一家肉铺
         * address :
         * shop_info :
         * remarks :
         * sale_id : 0
         * formated_user_money : ￥755000.00
         * formated_frozen_money : ￥0.00
         */

        private String user_id;
        private String email;
        private String user_name;
        private String password;
        private String sex;
        private String birthday;
        private String user_money;
        private String frozen_money;
        private String pay_points;
        private String rank_points;
        private String address_id;
        private String reg_time;
        private String last_login;
        private String last_time;
        private String last_ip;
        private String visit_count;
        private String user_rank;
        private String is_special;
        private String ec_salt;
        private String salt;
        private String parent_id;
        private String flag;
        private String alias;
        private String msn;
        private String qq;
        private String office_phone;
        private String home_phone;
        private String mobile_phone;
        private String is_validated;
        private String credit_line;
        private String passwd_question;
        private String passwd_answer;
        private String user_face;
        private String city;
        private String province;
        private String kefu_id;
        private String admin;
        private String shop_name;
        private String address;
        private String shop_info;
        private String remarks;
        private String sale_id;
        private String formated_user_money;
        private String formated_frozen_money;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getBirthday() {
            return birthday;
        }

        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }

        public String getUser_money() {
            return user_money;
        }

        public void setUser_money(String user_money) {
            this.user_money = user_money;
        }

        public String getFrozen_money() {
            return frozen_money;
        }

        public void setFrozen_money(String frozen_money) {
            this.frozen_money = frozen_money;
        }

        public String getPay_points() {
            return pay_points;
        }

        public void setPay_points(String pay_points) {
            this.pay_points = pay_points;
        }

        public String getRank_points() {
            return rank_points;
        }

        public void setRank_points(String rank_points) {
            this.rank_points = rank_points;
        }

        public String getAddress_id() {
            return address_id;
        }

        public void setAddress_id(String address_id) {
            this.address_id = address_id;
        }

        public String getReg_time() {
            return reg_time;
        }

        public void setReg_time(String reg_time) {
            this.reg_time = reg_time;
        }

        public String getLast_login() {
            return last_login;
        }

        public void setLast_login(String last_login) {
            this.last_login = last_login;
        }

        public String getLast_time() {
            return last_time;
        }

        public void setLast_time(String last_time) {
            this.last_time = last_time;
        }

        public String getLast_ip() {
            return last_ip;
        }

        public void setLast_ip(String last_ip) {
            this.last_ip = last_ip;
        }

        public String getVisit_count() {
            return visit_count;
        }

        public void setVisit_count(String visit_count) {
            this.visit_count = visit_count;
        }

        public String getUser_rank() {
            return user_rank;
        }

        public void setUser_rank(String user_rank) {
            this.user_rank = user_rank;
        }

        public String getIs_special() {
            return is_special;
        }

        public void setIs_special(String is_special) {
            this.is_special = is_special;
        }

        public String getEc_salt() {
            return ec_salt;
        }

        public void setEc_salt(String ec_salt) {
            this.ec_salt = ec_salt;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public String getParent_id() {
            return parent_id;
        }

        public void setParent_id(String parent_id) {
            this.parent_id = parent_id;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }

        public String getMsn() {
            return msn;
        }

        public void setMsn(String msn) {
            this.msn = msn;
        }

        public String getQq() {
            return qq;
        }

        public void setQq(String qq) {
            this.qq = qq;
        }

        public String getOffice_phone() {
            return office_phone;
        }

        public void setOffice_phone(String office_phone) {
            this.office_phone = office_phone;
        }

        public String getHome_phone() {
            return home_phone;
        }

        public void setHome_phone(String home_phone) {
            this.home_phone = home_phone;
        }

        public String getMobile_phone() {
            return mobile_phone;
        }

        public void setMobile_phone(String mobile_phone) {
            this.mobile_phone = mobile_phone;
        }

        public String getIs_validated() {
            return is_validated;
        }

        public void setIs_validated(String is_validated) {
            this.is_validated = is_validated;
        }

        public String getCredit_line() {
            return credit_line;
        }

        public void setCredit_line(String credit_line) {
            this.credit_line = credit_line;
        }

        public String getPasswd_question() {
            return passwd_question;
        }

        public void setPasswd_question(String passwd_question) {
            this.passwd_question = passwd_question;
        }

        public String getPasswd_answer() {
            return passwd_answer;
        }

        public void setPasswd_answer(String passwd_answer) {
            this.passwd_answer = passwd_answer;
        }

        public String getUser_face() {
            return user_face;
        }

        public void setUser_face(String user_face) {
            this.user_face = user_face;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getKefu_id() {
            return kefu_id;
        }

        public void setKefu_id(String kefu_id) {
            this.kefu_id = kefu_id;
        }

        public String getAdmin() {
            return admin;
        }

        public void setAdmin(String admin) {
            this.admin = admin;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getShop_info() {
            return shop_info;
        }

        public void setShop_info(String shop_info) {
            this.shop_info = shop_info;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getSale_id() {
            return sale_id;
        }

        public void setSale_id(String sale_id) {
            this.sale_id = sale_id;
        }

        public String getFormated_user_money() {
            return formated_user_money;
        }

        public void setFormated_user_money(String formated_user_money) {
            this.formated_user_money = formated_user_money;
        }

        public String getFormated_frozen_money() {
            return formated_frozen_money;
        }

        public void setFormated_frozen_money(String formated_frozen_money) {
            this.formated_frozen_money = formated_frozen_money;
        }
    }

    public static class SiteListBean extends SelecterBean{
        /**
         * id : 6
         * site_name : 【AR】阿根廷
         */

        private String id;
        private String site_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        @Override
        public String getVal() {
            return site_name;
        }

        @Override
        public void setVal(String val) {

            this.site_name = site_name;
        }

        public String getSite_name() {
            return site_name;
        }

        public void setSite_name(String site_name) {
            this.site_name = site_name;
        }
    }

    public static class CatListBean extends SelecterBean{
        /**
         * cat_id : 1
         * cat_name : 猪
         */

        private String cat_id;
        private String cat_name;

        public String getCat_id() {
            return cat_id;
        }

        public void setCat_id(String cat_id) {
            this.cat_id = cat_id;
        }

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        @Override
        public String getId() {
            return cat_id;
        }

        @Override
        public void setId(String id) {
            this.cat_id = cat_id;
        }

        @Override
        public String getVal() {
            return cat_name;
        }

        @Override
        public void setVal(String val) {
            this.cat_name = cat_name;
        }
    }

    public static class OfferTypeBean extends SelecterBean{
        /**
         * id : 1
         * val : CIF
         */

        private String id;
        private String val;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVal() {
            return val;
        }

        public void setVal(String val) {
            this.val = val;
        }
    }

    public static class PortBean extends SelecterBean{
        /**
         * id : 13
         * val : 上海
         */

        private String id;
        private String val;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVal() {
            return val;
        }

        public void setVal(String val) {
            this.val = val;
        }
    }

    public static class PrepayBean extends SelecterBean{
        /**
         * id : 16
         * val : 20%
         */

        private String id;
        private String val;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVal() {
            return val;
        }

        public void setVal(String val) {
            this.val = val;
        }
    }


    public static class SelecterBean {

        public String getId() {
            return null;
        }

        public void setId(String id) {
        }

        public String getVal() {
            return null;
        }

        public void setVal(String val) {
        }
    }
}

