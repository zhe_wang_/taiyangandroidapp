package taiyang.com.entity;

/**
 * Created by zhew on 9/9/17.
 * Copyright © 2006-2016 365jia.cn 安徽大尺度网络传媒有限公司版权所有
 **/

public class QiugouInfoBean

{
    /**
     * id : 191
     * title : 急需猪肉
     * goods_name : 猪半头
     * goods_num : 1.2
     * price_low : 5000
     * price_up : 11000
     * address : 哈尔冰
     * user_name : 张春生
     * user_phone : 18519519744
     * user_id : 1032
     * created_at : 1497332261
     * memo : 美丽的小鸟快乐的成长美丽的小鸟快乐的成长
     * type : 猪
     * sku : QG061313483
     */

    private String id;

    public String getUser_face() {
        return user_face;
    }

    public void setUser_face(String user_face) {
        this.user_face = user_face;
    }

    private String user_face;
    private String title;
    private String goods_name;
    private String goods_num;
    private String price_low;
    private String price_up;
    private String address;
    private String user_name;
    private String user_phone;
    private String user_id;
    private String created_at;
    private String memo;
    private String type;
    private String sku;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getGoods_num() {
        return goods_num;
    }

    public void setGoods_num(String goods_num) {
        this.goods_num = goods_num;
    }

    public String getPrice_low() {
        return price_low;
    }

    public void setPrice_low(String price_low) {
        this.price_low = price_low;
    }

    public String getPrice_up() {
        return price_up;
    }

    public void setPrice_up(String price_up) {
        this.price_up = price_up;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }
}
