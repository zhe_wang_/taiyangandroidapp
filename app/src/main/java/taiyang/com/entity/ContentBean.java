package taiyang.com.entity;

import java.util.List;

/**
 * Created by Administrator on 2016/8/6.
 */
public class ContentBean {

    private AppIndexPicBean app_index_pic;
    private List<AppSlideBean> app_slide;
    private List<BestGoodsBean> best_goods;
    private List<BestGoodsBean> miaosha_goods;
    private List<QiuGou> purchase_list;
    private List<BestGoodsBean> tejia_goods;

    public List<QiuGou> getPurchase_list() {
        return purchase_list;
    }

    public void setPurchase_list(List<QiuGou> purchase_list) {
        this.purchase_list = purchase_list;
    }

    public List<BestGoodsBean> getMiaosha_goods() {
        return miaosha_goods;
    }

    public void setMiaosha_goods(List<BestGoodsBean> miaosha_goods) {
        this.miaosha_goods = miaosha_goods;
    }

    public List<BestGoodsBean> getTejia_goods() {
        return tejia_goods;
    }

    public void setTejia_goods(List<BestGoodsBean> tejia_goods) {
        this.tejia_goods = tejia_goods;
    }

    public AppIndexPicBean getApp_index_pic() {
        return app_index_pic;
    }

    public void setApp_index_pic(AppIndexPicBean app_index_pic) {
        this.app_index_pic = app_index_pic;
    }

    public List<AppSlideBean> getApp_slide() {
        return app_slide;
    }

    public void setApp_slide(List<AppSlideBean> app_slide) {
        this.app_slide = app_slide;
    }

    public List<BestGoodsBean> getBest_goods() {
        return best_goods;
    }

    public void setBest_goods(List<BestGoodsBean> best_goods) {
        this.best_goods = best_goods;
    }

    public static class AppIndexPicBean {
        private String url;
        private String link;
        private String width;
        private String height;
        private String text;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }

    public static class AppSlideBean {
        private String pic;
        private String title;
        private String link;

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }

    public static class BestGoodsBean {
        public String spec_1;
        public String spec_1_unit;
        private String goods_number;
        private String goods_id;
        private String goods_name;
        private String brand_sn;
        private String shop_price;
        private String shop_price_fake;
        private String goods_local;
        public String getArrive_count() {
            return arrive_count;
        }

        public void setArrive_count(String arrive_count) {
            this.arrive_count = arrive_count;
        }

        private String arrive_count;
        private String goods_type;
        private String sell_type;
        private String offer;
        private String goods_thumb;
        private int is_pin;
        private String region_name_ch;
        private String unit_name;
        private String picture;

        public String getRegion_name() {
            return region_name;
        }

        public void setRegion_name(String region_name) {
            this.region_name = region_name;
        }

        private String region_name;
        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_number() {
            return goods_number;
        }

        public void setGoods_number(String goods_number) {
            this.goods_number = goods_number;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getBrand_sn() {
            return brand_sn;
        }

        public void setBrand_sn(String brand_sn) {
            this.brand_sn = brand_sn;
        }

        public String getShop_price() {
            return shop_price;
        }

        public void setShop_price(String shop_price) {
            this.shop_price = shop_price;
        }

        public String getGoods_local() {
            return goods_local;
        }

        public void setGoods_local(String goods_local) {
            this.goods_local = goods_local;
        }

        public String getGoods_type() {
            return goods_type;
        }

        public void setGoods_type(String goods_type) {
            this.goods_type = goods_type;
        }

        public String getSell_type() {
            return sell_type;
        }

        public void setSell_type(String sell_type) {
            this.sell_type = sell_type;
        }

        public String getOffer() {
            return offer;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

        public String getGoods_thumb() {
            return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
            this.goods_thumb = goods_thumb;
        }

        public int getIs_pin() {
            return is_pin;
        }

        public void setIs_pin(int is_pin) {
            this.is_pin = is_pin;
        }

        public String getRegion_name_ch() {
            return region_name_ch;
        }

        public void setRegion_name_ch(String region_name_ch) {
            this.region_name_ch = region_name_ch;
        }

        public String getUnit_name() {
            return unit_name;
        }

        public void setUnit_name(String unit_name) {
            this.unit_name = unit_name;
        }

        public String getShop_price_fake() {
            return shop_price_fake;
        }

        public void setShop_price_fake(String shop_price_fake) {
            this.shop_price_fake = shop_price_fake;
        }

            /*public List<PictureBean> getPicture() {
                return picture;
            }

            public void setPicture(List<PictureBean> picture) {
                this.picture = picture;
            }*/

        public static class PictureBean {
            private String img_id;
            private String img_url;
            private String thumb_url;
            private String img_desc;

            public String getImg_id() {
                return img_id;
            }

            public void setImg_id(String img_id) {
                this.img_id = img_id;
            }

            public String getImg_url() {
                return img_url;
            }

            public void setImg_url(String img_url) {
                this.img_url = img_url;
            }

            public String getThumb_url() {
                return thumb_url;
            }

            public void setThumb_url(String thumb_url) {
                this.thumb_url = thumb_url;
            }

            public String getImg_desc() {
                return img_desc;
            }

            public void setImg_desc(String img_desc) {
                this.img_desc = img_desc;
            }
        }
    }

    public static class QiuGou {

        private String id;
        private String title;
        private String goods_name;
        private String goods_num;
        private String price_low;
        private String price_up;
        private String address;
        private String user_name;
        private String user_phone;
        private String user_id;
        private String created_at;
        private String memo;
        private String type;
        private String sku;
        private String user_face;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_num() {
            return goods_num;
        }

        public void setGoods_num(String goods_num) {
            this.goods_num = goods_num;
        }

        public String getPrice_low() {
            return price_low;
        }

        public void setPrice_low(String price_low) {
            this.price_low = price_low;
        }

        public String getPrice_up() {
            return price_up;
        }

        public void setPrice_up(String price_up) {
            this.price_up = price_up;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getUser_phone() {
            return user_phone;
        }

        public void setUser_phone(String user_phone) {
            this.user_phone = user_phone;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getUser_face() {
            return user_face;
        }

        public void setUser_face(String user_face) {
            this.user_face = user_face;
        }

    }
}
