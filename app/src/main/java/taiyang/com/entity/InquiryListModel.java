package taiyang.com.entity;

import java.util.List;

/**
 * Created by admin on 2016/8/17.
 */
public class InquiryListModel {

    /**
     * record_count : 8
     * page_count : 1
     * page : 1
     */

    private PageModel total;
    /**
     * id : 16
F     * goods_name : 1
     * goods_num : 1
     * price_low : 1
     * price_up : 1
     * address : 1
     * user_name : lvonfeng1
     * user_phone : 185000
     * user_id : 11
     * created_at : 1471317960
     * memo : 1
     * type : 猪
     * sku : null
     * created : 2016-08-16 11:26
     * memo_len : 1
     */

    private List<InquiryModel> list;

    public PageModel getTotal() {
        return total;
    }

    public void setTotal(PageModel total) {
        this.total = total;
    }

    public List<InquiryModel> getList() {
        return list;
    }

    public void setList(List<InquiryModel> list) {
        this.list = list;
    }



}
