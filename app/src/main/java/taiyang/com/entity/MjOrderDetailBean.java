package taiyang.com.entity;

import java.util.List;

/**
 * Created by Administrator on 2016/9/2.
 */
public class MjOrderDetailBean {

    private String amount_last;
    private String buyer_seller_edit;
    private int can_pay;
    private String formated_add_time;
    private String goods_amount;
    private GoodsInfoBean goods_info;
    private String order_amount;
    private String order_id;
    private String order_sn;
    private String order_status;
    private String order_status_name;
    private String pay_check;
    private String pay_name;
    private String pay_status_name;

    private PaymentDescBean payment_desc;
    private String user_id;
    private Get_addressBean get_address;
    private List<?> send_address;

    public String getAmount_last() {
        return amount_last;
    }

    public void setAmount_last(String amount_last) {
        this.amount_last = amount_last;
    }

    public String getBuyer_seller_edit() {
        return buyer_seller_edit;
    }

    public void setBuyer_seller_edit(String buyer_seller_edit) {
        this.buyer_seller_edit = buyer_seller_edit;
    }

    public int getCan_pay() {
        return can_pay;
    }

    public void setCan_pay(int can_pay) {
        this.can_pay = can_pay;
    }

    public String getFormated_add_time() {
        return formated_add_time;
    }

    public void setFormated_add_time(String formated_add_time) {
        this.formated_add_time = formated_add_time;
    }

    public String getGoods_amount() {
        return goods_amount;
    }

    public void setGoods_amount(String goods_amount) {
        this.goods_amount = goods_amount;
    }

    public GoodsInfoBean getGoods_info() {
        return goods_info;
    }

    public void setGoods_info(GoodsInfoBean goods_info) {
        this.goods_info = goods_info;
    }

    public String getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(String order_amount) {
        this.order_amount = order_amount;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_sn() {
        return order_sn;
    }

    public void setOrder_sn(String order_sn) {
        this.order_sn = order_sn;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getOrder_status_name() {
        return order_status_name;
    }

    public void setOrder_status_name(String order_status_name) {
        this.order_status_name = order_status_name;
    }

    public String getPay_check() {
        return pay_check;
    }

    public void setPay_check(String pay_check) {
        this.pay_check = pay_check;
    }

    public String getPay_name() {
        return pay_name;
    }

    public void setPay_name(String pay_name) {
        this.pay_name = pay_name;
    }

    public String getPay_status_name() {
        return pay_status_name;
    }

    public void setPay_status_name(String pay_status_name) {
        this.pay_status_name = pay_status_name;
    }

    public PaymentDescBean getPayment_desc() {
        return payment_desc;
    }

    public void setPayment_desc(PaymentDescBean payment_desc) {
        this.payment_desc = payment_desc;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Get_addressBean getGet_address() {
        return get_address;
    }

    public List<?> getSend_address() {
        return send_address;
    }

    public void setSend_address(List<?> send_address) {
        this.send_address = send_address;
    }

    public static class Get_addressBean {
        public String id_number;
        public String mobile;
        public String name;
    }

    public static class GoodsInfoBean {
        private String goods_id;
        private String goods_name;
        private String goods_number;
        private String goods_price;
        private String goods_thumb;
        private String measure_unit;
        private String part_number;
        private String part_unit;

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_number() {
            return goods_number;
        }

        public void setGoods_number(String goods_number) {
            this.goods_number = goods_number;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public String getGoods_thumb() {
            return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
            this.goods_thumb = goods_thumb;
        }

        public String getMeasure_unit() {
            return measure_unit;
        }

        public void setMeasure_unit(String measure_unit) {
            this.measure_unit = measure_unit;
        }

        public String getPart_number() {
            return part_number;
        }

        public void setPart_number(String part_number) {
            this.part_number = part_number;
        }

        public String getPart_unit() {
            return part_unit;
        }

        public void setPart_unit(String part_unit) {
            this.part_unit = part_unit;
        }
    }

    public static class PaymentDescBean {
        private String payment_name;

        private SellerBean seller;

        private List<PaymentListBean> payment_list;

        public String getPayment_name() {
            return payment_name;
        }

        public void setPayment_name(String payment_name) {
            this.payment_name = payment_name;
        }

        public SellerBean getSeller() {
            return seller;
        }

        public void setSeller(SellerBean seller) {
            this.seller = seller;
        }

        public List<PaymentListBean> getPayment_list() {
            return payment_list;
        }

        public void setPayment_list(List<PaymentListBean> payment_list) {
            this.payment_list = payment_list;
        }

        public static class SellerBean {
            private String mobile;
            private String name;
            private String user_id;

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }
        }

        public static class PaymentListBean {
            private String name;
            private String value;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }
}
