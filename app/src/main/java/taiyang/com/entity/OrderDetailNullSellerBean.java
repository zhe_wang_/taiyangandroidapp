package taiyang.com.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2016/9/7.
 */
public class OrderDetailNullSellerBean {


    /**
     * order_id : 1754
     * order_sn : 2017100329906
     * user_id : 1047
     * order_status : 0
     * order_status_name : 待付款
     * pay_id : 2
     * pay_name : 银行转帐
     * goods_amount : ￥5000.00
     * order_amount : ￥4000.00
     * amount_last : 1000.00
     * formated_add_time : 2017-10-03 14:56:44
     * send_address : []
     * get_address : []
     * pay_check : 0
     * pay_status_name :
     * buyer_seller_edit :
     * goods_info : {"goods_id":"1832","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/31_thumb_G_14763925972936.jpg","goods_name":"B级猪手","goods_number":"5","goods_price":"￥5000.00","shop_price_unit":"件","part_number":5,"part_unit":"件/吨"}
     * pay_type_id : 0
     * stock_remark :
     * stock_status : 0
     * can_pay : 1
     * bank : {"username":"于明昊","account":"6228 4800 1871 2997 271 ","deposit_bank":"中国农业银行北京英家坟支行"}
     * payment_list : {"1":{"pay_id":"2","pay_code":"bank_mobile","pay_name":"银行转帐"}}
     * user_info : {"mobile":"13295609110","name":"xingming","user_name":"王半仙"}
     * stock_status_tag : 未确认
     */

    private String order_id;
    private String order_sn;
    private String user_id;
    private String order_status;
    private String order_status_name;
    private String pay_id;


    public String getPay_online() {
        return pay_online;
    }

    public void setPay_online(String pay_online) {
        this.pay_online = pay_online;
    }

    private String pay_online;
    private String pay_name;


    public String getApply_pay() {
        return apply_pay;
    }

    public void setApply_pay(String apply_pay) {
        this.apply_pay = apply_pay;
    }

    private String apply_pay;
    private String goods_amount;


    public String getMoney_paid() {
        return money_paid;
    }

    public void setMoney_paid(String money_paid) {
        this.money_paid = money_paid;
    }

    private String money_paid;
    private String order_amount;
    private String amount_last;
    private String formated_add_time;
    private String pay_check;
    private String pay_status_name;
    private String buyer_seller_edit;
    private GoodsInfoBean goods_info;
    private String pay_type_id;
    private String stock_remark;
    private String stock_status;
    private int can_pay;
    private BankBean bank;
    private UserInfoBean user_info;
    private String stock_status_tag;
    private GetAddressBean send_address;
    private GetAddressBean get_address;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_sn() {
        return order_sn;
    }

    public void setOrder_sn(String order_sn) {
        this.order_sn = order_sn;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getOrder_status_name() {
        return order_status_name;
    }

    public void setOrder_status_name(String order_status_name) {
        this.order_status_name = order_status_name;
    }

    public String getPay_id() {
        return pay_id;
    }

    public void setPay_id(String pay_id) {
        this.pay_id = pay_id;
    }

    public String getPay_name() {
        return pay_name;
    }

    public void setPay_name(String pay_name) {
        this.pay_name = pay_name;
    }

    public String getGoods_amount() {
        return goods_amount;
    }

    public void setGoods_amount(String goods_amount) {
        this.goods_amount = goods_amount;
    }

    public String getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(String order_amount) {
        this.order_amount = order_amount;
    }

    public String getAmount_last() {
        return amount_last;
    }

    public void setAmount_last(String amount_last) {
        this.amount_last = amount_last;
    }

    public String getFormated_add_time() {
        return formated_add_time;
    }

    public void setFormated_add_time(String formated_add_time) {
        this.formated_add_time = formated_add_time;
    }

    public String getPay_check() {
        return pay_check;
    }

    public void setPay_check(String pay_check) {
        this.pay_check = pay_check;
    }

    public String getPay_status_name() {
        return pay_status_name;
    }

    public void setPay_status_name(String pay_status_name) {
        this.pay_status_name = pay_status_name;
    }

    public String getBuyer_seller_edit() {
        return buyer_seller_edit;
    }

    public void setBuyer_seller_edit(String buyer_seller_edit) {
        this.buyer_seller_edit = buyer_seller_edit;
    }

    public GoodsInfoBean getGoods_info() {
        return goods_info;
    }

    public void setGoods_info(GoodsInfoBean goods_info) {
        this.goods_info = goods_info;
    }

    public String getPay_type_id() {
        return pay_type_id;
    }

    public void setPay_type_id(String pay_type_id) {
        this.pay_type_id = pay_type_id;
    }

    public String getStock_remark() {
        return stock_remark;
    }

    public void setStock_remark(String stock_remark) {
        this.stock_remark = stock_remark;
    }

    public String getStock_status() {
        return stock_status;
    }

    public void setStock_status(String stock_status) {
        this.stock_status = stock_status;
    }

    public int getCan_pay() {
        return can_pay;
    }

    public void setCan_pay(int can_pay) {
        this.can_pay = can_pay;
    }

    public BankBean getBank() {
        return bank;
    }

    public void setBank(BankBean bank) {
        this.bank = bank;
    }


    public UserInfoBean getUser_info() {
        return user_info;
    }

    public void setUser_info(UserInfoBean user_info) {
        this.user_info = user_info;
    }

    public String getStock_status_tag() {
        return stock_status_tag;
    }

    public void setStock_status_tag(String stock_status_tag) {
        this.stock_status_tag = stock_status_tag;
    }

    public GetAddressBean getSend_address() {
        return send_address;
    }

    public void setSend_address(GetAddressBean get_address) {
        this.send_address = send_address;
    }

    public GetAddressBean getGet_address() {
        return get_address;
    }

    public void setGet_address(GetAddressBean get_address) {
        this.get_address = get_address;
    }

    public static class GoodsInfoBean {
        public String getPart_weight() {
            return part_weight;
        }

        public void setPart_weight(String part_weight) {
            this.part_weight = part_weight;
        }

        /**
         * goods_id : 1832
         * goods_thumb : http://test.taiyanggo.com/images/201610/thumb_img/31_thumb_G_14763925972936.jpg
         * goods_name : B级猪手
         * goods_number : 5
         * goods_price : ￥5000.00
         * shop_price_unit : 件
         * part_number : 5
         * part_unit : 件/吨
         */

        private String part_weight;
        private String is_retail;

        public String getIs_retail() {
            return is_retail;
        }

        public void setIs_retail(String is_retail) {
            this.is_retail = is_retail;
        }

        public String getMeasure_unit() {
            return measure_unit;
        }

        public void setMeasure_unit(String measure_unit) {
            this.measure_unit = measure_unit;
        }

        public void setPart_number(double part_number) {
            this.part_number = part_number;
        }

        private String measure_unit;
        private String goods_id;
        private String goods_thumb;
        private String goods_name;

        private String goods_number;
        private String goods_price;
        private String shop_price_unit;

        public String getBrand_sn() {
            return brand_sn;
        }

        public void setBrand_sn(String brand_sn) {
            this.brand_sn = brand_sn;
        }

        private String brand_sn;
        private double part_number;
        private String part_unit;

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_thumb() {
            return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
            this.goods_thumb = goods_thumb;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_number() {
            return goods_number;
        }

        public void setGoods_number(String goods_number) {
            this.goods_number = goods_number;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public String getShop_price_unit() {
            return shop_price_unit;
        }

        public void setShop_price_unit(String shop_price_unit) {
            this.shop_price_unit = shop_price_unit;
        }

        public double getPart_number() {
            return part_number;
        }

        public void setPart_number(int part_number) {
            this.part_number = part_number;
        }

        public String getPart_unit() {
            return part_unit;
        }

        public void setPart_unit(String part_unit) {
            this.part_unit = part_unit;
        }
    }

    public static class BankBean {
        /**
         * username : 于明昊
         * account : 6228 4800 1871 2997 271
         * deposit_bank : 中国农业银行北京英家坟支行
         */

        private String username;
        private String account;
        private String deposit_bank;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getDeposit_bank() {
            return deposit_bank;
        }

        public void setDeposit_bank(String deposit_bank) {
            this.deposit_bank = deposit_bank;
        }
    }

    public static class PaymentListBean {
        /**
         * 1 : {"pay_id":"2","pay_code":"bank_mobile","pay_name":"银行转帐"}
         */

        @SerializedName("1")
        private _$1Bean _$1;

        public _$1Bean get_$1() {
            return _$1;
        }

        public void set_$1(_$1Bean _$1) {
            this._$1 = _$1;
        }

        public static class _$1Bean {
            /**
             * pay_id : 2
             * pay_code : bank_mobile
             * pay_name : 银行转帐
             */

            private String pay_id;
            private String pay_code;
            private String pay_name;

            public String getPay_id() {
                return pay_id;
            }

            public void setPay_id(String pay_id) {
                this.pay_id = pay_id;
            }

            public String getPay_code() {
                return pay_code;
            }

            public void setPay_code(String pay_code) {
                this.pay_code = pay_code;
            }

            public String getPay_name() {
                return pay_name;
            }

            public void setPay_name(String pay_name) {
                this.pay_name = pay_name;
            }
        }
    }

    public static class UserInfoBean {
        /**
         * mobile : 13295609110
         * name : xingming
         * user_name : 王半仙
         */

        private String mobile;
        private String name;
        private String user_name;

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }
    }

    public static class GetAddressBean {
        /**
         * name : 哈哈
         * mobile : 18226613958
         * id_number : 545125454541545
         */

        private String name;
        private String mobile;
        private String id_number;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getId_number() {
            return id_number;
        }

        public void setId_number(String id_number) {
            this.id_number = id_number;
        }
    }
}
