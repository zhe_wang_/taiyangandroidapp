package taiyang.com.entity;

import java.util.List;

import taiyang.com.adapter.Entity;

/**
 * Created by Administrator on 2016/8/15.
 */
public class InquiryListContentBean {


    private TotalBean total;
    /**
     * brand_sn : 1002794
     * goods_id : 3
     * goods_local : 上海
     * goods_name :
     * goods_thumb : http://tydpb2b.bizsoho.com/images/201606/thumb_img/23_thumb_G_14671368261233.jpg
     * goods_type : 6
     * goods_weight : 26.00
     * is_pin : 0
     * offer : 10
     * region_icon :
     * sell_type : 5
     * shop_price : ￥17500
     * spec_1 : 26.00
     * spec_1_unit : 吨/柜
     * spec_2 :
     * spec_2_unit :
     * unit_name : 元/吨
     */

    private List<ListBean> list;

    private List<Local> local;

    public TotalBean getTotal() {
        return total;
    }

    public void setTotal(TotalBean total) {
        this.total = total;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class TotalBean {
        private int page;
        private int page_count;
        private int record_count;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getPage_count() {
            return page_count;
        }

        public void setPage_count(int page_count) {
            this.page_count = page_count;
        }

        public int getRecord_count() {
            return record_count;
        }

        public void setRecord_count(int record_count) {
            this.record_count = record_count;
        }
    }

    public static class ListBean extends Entity{

        private String brand_sn;
        private String goods_number;
        private String goods_id;
        private String goods_local;
        private String goods_name;
        private String goods_thumb;

        public String getArrive_count() {
            return arrive_count;
        }

        public void setArrive_count(String arrive_count) {
            this.arrive_count = arrive_count;
        }

        private String arrive_count;
        private String goods_type;
        private String goods_weight;
        private int is_pin;
        private String offer;
        private String region_icon;

        public String getRegion_name_ch() {
            return region_name_ch;
        }

        public void setRegion_name_ch(String region_name_ch) {
            this.region_name_ch = region_name_ch;
        }

        private String region_name_ch;
        private String sell_type;
        private String shop_price;
        private String spec_1;
        private String spec_1_unit;
        private String spec_2;
        private String spec_2_unit;
        private String unit_name;

        public String getBrand_sn() {
            return brand_sn;
        }

        public String getGoods_number() {
            return goods_number;
        }

        public void setBrand_sn(String brand_sn) {
            this.brand_sn = brand_sn;
        }

        public void setGoods_number(String goods_number) {
            this.goods_number = goods_number;
        }

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_local() {
            return goods_local;
        }

        public void setGoods_local(String goods_local) {
            this.goods_local = goods_local;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_thumb() {
            return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
            this.goods_thumb = goods_thumb;
        }

        public String getGoods_type() {
            return goods_type;
        }

        public void setGoods_type(String goods_type) {
            this.goods_type = goods_type;
        }

        public String getGoods_weight() {
            return goods_weight;
        }

        public void setGoods_weight(String goods_weight) {
            this.goods_weight = goods_weight;
        }

        public int getIs_pin() {
            return is_pin;
        }

        public void setIs_pin(int is_pin) {
            this.is_pin = is_pin;
        }

        public String getOffer() {
            return offer;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

        public String getRegion_icon() {
            return region_icon;
        }

        public void setRegion_icon(String region_icon) {
            this.region_icon = region_icon;
        }

        public String getSell_type() {
            return sell_type;
        }

        public void setSell_type(String sell_type) {
            this.sell_type = sell_type;
        }

        public String getShop_price() {
            return shop_price;
        }

        public void setShop_price(String shop_price) {
            this.shop_price = shop_price;
        }

        public String getSpec_1() {
            return spec_1;
        }

        public void setSpec_1(String spec_1) {
            this.spec_1 = spec_1;
        }

        public String getSpec_1_unit() {
            return spec_1_unit;
        }

        public void setSpec_1_unit(String spec_1_unit) {
            this.spec_1_unit = spec_1_unit;
        }

        public String getSpec_2() {
            return spec_2;
        }

        public void setSpec_2(String spec_2) {
            this.spec_2 = spec_2;
        }

        public String getSpec_2_unit() {
            return spec_2_unit;
        }

        public void setSpec_2_unit(String spec_2_unit) {
            this.spec_2_unit = spec_2_unit;
        }

        public String getUnit_name() {
            return unit_name;
        }

        public void setUnit_name(String unit_name) {
            this.unit_name = unit_name;
        }
    }

    public static class Local {
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public List<Local> getLocaList() {
        return local;
    }

    public void setLocaList(List<Local> local) {
        this.local = local;
    }
}
