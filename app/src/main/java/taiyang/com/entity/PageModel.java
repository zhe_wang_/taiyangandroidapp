package taiyang.com.entity;

/**
 * Created by admin on 2016/8/17.
 */
public class PageModel {
        private String record_count;
        private int page_count;
        private int page;

        public String getRecord_count() {
            return record_count;
        }

        public void setRecord_count(String record_count) {
            this.record_count = record_count;
        }

        public int getPage_count() {
            return page_count;
        }

        public void setPage_count(int page_count) {
            this.page_count = page_count;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }
}
