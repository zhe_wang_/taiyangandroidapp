package taiyang.com.entity;

import java.util.List;

/**
 * Created by admin on 2016/8/10.
 */
public class InfoModel{

    /**
     * end : 0
     * category : [{"cat_name":"公告","cat_id":"4"},{"cat_name":"市场信息","cat_id":"5"}]
     * list : [{"id":"52","title":"英国退欧，肉类贸易面临\u201c三年不稳定期\u201d","description":"","arc_img":"config/article/14691262596982.png","short_title":"英国退欧，肉类贸易面临\u201c三年不稳...","author":"远洋冻品","url":"article.php?id=52","add_time":"2016-07-19","is_new":"1"},{"id":"51","title":"肉类产品检验检疫准入名单-2016年6月30日","description":"","arc_img":"config/article/14691269635813.png","short_title":"肉类产品检验检疫准入名单-201...","author":"远洋冻品","url":"article.php?id=51","add_time":"2016-07-19","is_new":"1"},{"id":"50","title":"俄罗斯牛肉也要进中国？俄再次请求中国取消猪牛肉进口限制","description":"","arc_img":"config/article/14691270632162.png","short_title":"俄罗斯牛肉也要进中国？俄再次请求...","author":"远洋冻品","url":"article.php?id=50","add_time":"2016-07-19","is_new":"1"},{"id":"49","title":"7月6日新增报盘2","description":"","arc_img":"config/article/14691273909696.png","short_title":"7月6日新增报盘2","author":"远洋冻品","url":"article.php?id=49","add_time":"2016-07-19","is_new":"1"},{"id":"48","title":"7月6日新增报盘","description":"","arc_img":"config/article/14691274038967.png","short_title":"7月6日新增报盘","author":"远洋冻品","url":"article.php?id=48","add_time":"2016-07-19","is_new":"1"},{"id":"47","title":"2016年7月7日国内市场零售价格","description":"","arc_img":"config/article/14691279965896.png","short_title":"2016年7月7日国内市场零售价...","author":"远洋冻品","url":"article.php?id=47","add_time":"2016-07-19","is_new":"1"},{"id":"46","title":"2016年7月6日国内市场零售价格","description":"","arc_img":"config/article/14691280128122.png","short_title":"2016年7月6日国内市场零售价...","author":"远洋冻品","url":"article.php?id=46","add_time":"2016-07-19","is_new":"1"},{"id":"45","title":"2016年7月6日电商平台部分订单成交价格","description":"","arc_img":"config/article/14691280301325.png","short_title":"2016年7月6日电商平台部分订...","author":"远洋冻品","url":"article.php?id=45","add_time":"2016-07-19","is_new":"1"},{"id":"44","title":"2016年7月2日国内市场产品零售价格","description":"","arc_img":"config/article/14691280457929.png","short_title":"2016年7月2日国内市场产品零...","author":"远洋冻品","url":"article.php?id=44","add_time":"2016-07-19","is_new":"1"},{"id":"43","title":"2016年7月2日电商平台订单部分成交价格","description":"","arc_img":"config/article/14691280662064.png","short_title":"2016年7月2日电商平台订单部...","author":"远洋冻品","url":"article.php?id=43","add_time":"2016-07-19","is_new":"1"}]
     */

    private int end;
    /**
     * cat_name : 公告
     * cat_id : 4
     */

    private List<CategoryModel> category;
    /**
     * id : 52
     * title : 英国退欧，肉类贸易面临“三年不稳定期”
     * description :
     * arc_img : config/article/14691262596982.png
     * short_title : 英国退欧，肉类贸易面临“三年不稳...
     * author : 远洋冻品
     * url : article.php?id=52
     * add_time : 2016-07-19
     * is_new : 1
     */

    private List<NewsModel> list;

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public List<CategoryModel> getCategory() {
        return category;
    }

    public void setCategory(List<CategoryModel> category) {
        this.category = category;
    }

    public List<NewsModel> getList() {
        return list;
    }

    public void setList(List<NewsModel> list) {
        this.list = list;
    }


}