package taiyang.com.entity;

import java.util.List;

/**
 * Created by Administrator on 2016/8/31.
 */
public class OrderCofirmOKBean {

    private String formated_order_amount;
    private String order_id;
    private String order_sn;
    private String order_status;
    private String payment;
    private Payment_desc payment_desc;

    public Payment_desc getPayment_desc() {
        return payment_desc;
    }

    public void setPayment_desc(Payment_desc payment_desc) {
        this.payment_desc = payment_desc;
    }

    public static class Payment_desc {
        private List<Payment_list> payment_list;
        private String payment;

        public String getPayment() {
            return payment;
        }

        public void setPayment(String payment) {
            this.payment = payment;
        }

        public static class Payment_list {
            private String name;
            private String value;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }

        public List<Payment_list> getPayment_list() {
            return payment_list;
        }

        public void setPayment_list(List<Payment_list> payment_list) {
            this.payment_list = payment_list;
        }
    }

    public String getFormated_order_amount() {
        return formated_order_amount;
    }

    public void setFormated_order_amount(String formated_order_amount) {
        this.formated_order_amount = formated_order_amount;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_sn() {
        return order_sn;
    }

    public void setOrder_sn(String order_sn) {
        this.order_sn = order_sn;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }
}
