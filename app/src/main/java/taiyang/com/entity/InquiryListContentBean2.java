package taiyang.com.entity;

import java.util.List;

import taiyang.com.adapter.Entity;

/**
 * Created by Administrator on 2016/8/15.
 */
public class InquiryListContentBean2 {


    /**
     * list : [{"goods_id":"51","goods_name":"猪整头","goods_number":2,"brand_sn":"80","shop_price":"￥7777","goods_local":"北京","goods_type":"7","sell_type":"5","offer":"11","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/10_thumb_G_14763233280018.jpg","spec_1":"25.00","spec_1_unit":"吨/柜","spec_2":"","spec_2_unit":"","goods_weight":"25.00","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/2.png"},{"goods_id":"1498","goods_name":"猪整头","goods_number":1,"brand_sn":"642","shop_price":"￥9400","goods_local":"天津","goods_type":"7","sell_type":"5","offer":"11","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/10_thumb_G_14763233280018.jpg","spec_1":"27.00","spec_1_unit":"吨/柜","spec_2":"","spec_2_unit":"","goods_weight":"27.00","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/2.png"},{"goods_id":"1690","goods_name":"翅根","goods_number":572,"brand_sn":"80","shop_price":"￥33164","goods_local":"宁波","goods_type":"6","sell_type":"4","offer":"11","goods_thumb":"http://test.taiyanggo.com/images/no_picture.gif","spec_1":"25","spec_1_unit":"件/吨","spec_2":"40","spec_2_unit":"件/吨","goods_weight":"25.00","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/2.png"},{"goods_id":"1651","goods_name":"猪脑/猪耳朵","goods_number":9,"brand_sn":"363L/14","shop_price":"￥1","goods_local":"香港","goods_type":"7","sell_type":"5","offer":"11","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/15_thumb_G_14763785231264.jpg","spec_1":"3.00","spec_1_unit":"吨/柜","spec_2":"","spec_2_unit":"","goods_weight":"3.00","is_pin":1,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/19.png"},{"goods_id":"1572","goods_name":"猪手","goods_number":1600,"brand_sn":"129","shop_price":"￥23500","goods_local":"天津","goods_type":"7","sell_type":"4","offer":"11","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/33_thumb_G_14763790516516.jpg","spec_1":"22.68","spec_1_unit":"件/吨","spec_2":"45","spec_2_unit":"件/吨","goods_weight":"22.68","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/2.png"},{"goods_id":"1571","goods_name":"猪手","goods_number":500,"brand_sn":"129","shop_price":"￥23500","goods_local":"天津","goods_type":"7","sell_type":"4","offer":"11","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/33_thumb_G_14763790516516.jpg","spec_1":"22.68","spec_1_unit":"件/吨","spec_2":"45","spec_2_unit":"件/吨","goods_weight":"22.68","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/2.png"},{"goods_id":"1570","goods_name":"猪手","goods_number":500,"brand_sn":"147","shop_price":"￥23500","goods_local":"天津","goods_type":"7","sell_type":"4","offer":"11","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/33_thumb_G_14763790516516.jpg","spec_1":"22.68","spec_1_unit":"件/吨","spec_2":"45","spec_2_unit":"件/吨","goods_weight":"22.68","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/2.png"},{"goods_id":"1568","goods_name":"猪手","goods_number":200,"brand_sn":"147","shop_price":"￥23500","goods_local":"天津","goods_type":"7","sell_type":"4","offer":"11","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/33_thumb_G_14763790516516.jpg","spec_1":"22.68","spec_1_unit":"件/吨","spec_2":"45","spec_2_unit":"件/吨","goods_weight":"22.68","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/2.png"},{"goods_id":"1567","goods_name":"猪手","goods_number":200,"brand_sn":"129","shop_price":"￥23500","goods_local":"天津","goods_type":"7","sell_type":"4","offer":"11","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/33_thumb_G_14763790516516.jpg","spec_1":"22.68","spec_1_unit":"件/吨","spec_2":"45","spec_2_unit":"件/吨","goods_weight":"22.68","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/2.png"},{"goods_id":"1566","goods_name":"猪手","goods_number":22,"brand_sn":"129","shop_price":"￥23500","goods_local":"天津","goods_type":"7","sell_type":"4","offer":"11","goods_thumb":"http://test.taiyanggo.com/images/201610/thumb_img/33_thumb_G_14763790516516.jpg","spec_1":"22.68","spec_1_unit":"件/吨","spec_2":"45","spec_2_unit":"件/吨","goods_weight":"22.68","is_pin":0,"unit_name":"元/吨","region_icon":"http://test.taiyanggo.com/config/country/2.png"}]
     * local : [{"id":"1682","name":"123123"},{"id":"410","name":"HongKong"},{"id":"1669","name":"NYC"},{"id":"687","name":"shanghai"},{"id":"1330","name":"Tianjin"},{"id":"1663","name":"三亚"},{"id":"37","name":"上海"},{"id":"38","name":"上海大宛冷库"},{"id":"1662","name":"上海开发区"},{"id":"107","name":"上海港"},{"id":"532","name":"上海～天津"},{"id":"1369","name":"上海～苏州"},{"id":"1667","name":"兰州"},{"id":"29","name":"北京"},{"id":"203","name":"北京13311362664"},{"id":"282","name":"北京西南郊"},{"id":"483","name":"南沙"},{"id":"1655","name":"厦门"},{"id":"497","name":"唐山"},{"id":"9","name":"大连"},{"id":"30","name":"天津"},{"id":"1451","name":"天津/上海"},{"id":"1487","name":"天津价"},{"id":"144","name":"天津港"},{"id":"621","name":"天津港金汇"},{"id":"642","name":"天津现货"},{"id":"196","name":"宁波"},{"id":"928","name":"山东"},{"id":"1683","name":"山东济南"},{"id":"705","name":"山东滨州"},{"id":"1684","name":"广东"},{"id":"596","name":"广州"},{"id":"305","name":"广州盐田"},{"id":"527","name":"张家港"},{"id":"1071","name":"当地"},{"id":"182","name":"新港"},{"id":"321","name":"昆山"},{"id":"581","name":"杭州"},{"id":"174","name":"沈阳"},{"id":"969","name":"沈阳于洪于洪市场"},{"id":"366","name":"沈阳于洪市场"},{"id":"206","name":"沈阳于洪水产"},{"id":"1459","name":"河北唐山"},{"id":"14","name":"河北唐山盛华冷库"},{"id":"53","name":"河南沈丘县"},{"id":"1657","name":"海南"},{"id":"843","name":"深圳"},{"id":"1671","name":"温哥华"}]
     * total : {"record_count":48,"page_count":5,"page":1}
     */

    private TotalBean total;
    private List<ListBean> list;
    private List<LocalBean> local;

    public TotalBean getTotal() {
        return total;
    }

    public void setTotal(TotalBean total) {
        this.total = total;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public List<LocalBean> getLocal() {
        return local;
    }

    public void setLocal(List<LocalBean> local) {
        this.local = local;
    }

    public static class TotalBean {
        /**
         * record_count : 48
         * page_count : 5
         * page : 1
         */

        private int record_count;
        private int page_count;
        private int page;

        public int getRecord_count() {
            return record_count;
        }

        public void setRecord_count(int record_count) {
            this.record_count = record_count;
        }

        public int getPage_count() {
            return page_count;
        }

        public void setPage_count(int page_count) {
            this.page_count = page_count;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }
    }

    public static class ListBean extends Entity{
        /**
         * goods_id : 51
         * goods_name : 猪整头
         * goods_number : 2
         * brand_sn : 80
         * shop_price : ￥7777
         * goods_local : 北京
         * goods_type : 7
         * sell_type : 5
         * offer : 11
         * goods_thumb : http://test.taiyanggo.com/images/201610/thumb_img/10_thumb_G_14763233280018.jpg
         * spec_1 : 25.00
         * spec_1_unit : 吨/柜
         * spec_2 :
         * spec_2_unit :
         * goods_weight : 25.00
         * is_pin : 0
         * unit_name : 元/吨
         * region_icon : http://test.taiyanggo.com/config/country/2.png
         */

        private String goods_id;
        private String goods_name;
        private int goods_number;
        private String brand_sn;
        private String shop_price;
        private String goods_local;
        private String goods_type;
        private String sell_type;
        private String offer;
        private String goods_thumb;
        private String spec_1;
        private String spec_1_unit;
        private String spec_2;
        private String spec_2_unit;
        private String goods_weight;
        private int is_pin;
        private String unit_name;
        private String region_icon;

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public int getGoods_number() {
            return goods_number;
        }

        public void setGoods_number(int goods_number) {
            this.goods_number = goods_number;
        }

        public String getBrand_sn() {
            return brand_sn;
        }

        public void setBrand_sn(String brand_sn) {
            this.brand_sn = brand_sn;
        }

        public String getShop_price() {
            return shop_price;
        }

        public void setShop_price(String shop_price) {
            this.shop_price = shop_price;
        }

        public String getGoods_local() {
            return goods_local;
        }

        public void setGoods_local(String goods_local) {
            this.goods_local = goods_local;
        }

        public String getGoods_type() {
            return goods_type;
        }

        public void setGoods_type(String goods_type) {
            this.goods_type = goods_type;
        }

        public String getSell_type() {
            return sell_type;
        }

        public void setSell_type(String sell_type) {
            this.sell_type = sell_type;
        }

        public String getOffer() {
            return offer;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

        public String getGoods_thumb() {
            return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
            this.goods_thumb = goods_thumb;
        }

        public String getSpec_1() {
            return spec_1;
        }

        public void setSpec_1(String spec_1) {
            this.spec_1 = spec_1;
        }

        public String getSpec_1_unit() {
            return spec_1_unit;
        }

        public void setSpec_1_unit(String spec_1_unit) {
            this.spec_1_unit = spec_1_unit;
        }

        public String getSpec_2() {
            return spec_2;
        }

        public void setSpec_2(String spec_2) {
            this.spec_2 = spec_2;
        }

        public String getSpec_2_unit() {
            return spec_2_unit;
        }

        public void setSpec_2_unit(String spec_2_unit) {
            this.spec_2_unit = spec_2_unit;
        }

        public String getGoods_weight() {
            return goods_weight;
        }

        public void setGoods_weight(String goods_weight) {
            this.goods_weight = goods_weight;
        }

        public int getIs_pin() {
            return is_pin;
        }

        public void setIs_pin(int is_pin) {
            this.is_pin = is_pin;
        }

        public String getUnit_name() {
            return unit_name;
        }

        public void setUnit_name(String unit_name) {
            this.unit_name = unit_name;
        }

        public String getRegion_icon() {
            return region_icon;
        }

        public void setRegion_icon(String region_icon) {
            this.region_icon = region_icon;
        }
    }

    public static class LocalBean {
        /**
         * id : 1682
         * name : 123123
         */

        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
