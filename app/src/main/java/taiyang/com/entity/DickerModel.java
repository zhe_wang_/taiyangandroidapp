package taiyang.com.entity;

import taiyang.com.adapter.Entity;

/**
 * Created by admin on 2016/8/17.
 */
public class DickerModel extends Entity{
        private String id;
        private String goods_id;
        private String last_price;
        private String price;
        private String goods_user_id;
        private String user_id;
        private String created_at;
        private String status;
        private String goods_name;
        private String goods_thumb;
        private String brand_sn;
        private String formated_shop_price;
        private String unit;
        private String sell_type;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;
        private String goods_type;
        private int is_pin;
        private String offer;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getLast_price() {
            return last_price;
        }

        public void setLast_price(String last_price) {
            this.last_price = last_price;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getGoods_user_id() {
            return goods_user_id;
        }

        public void setGoods_user_id(String goods_user_id) {
            this.goods_user_id = goods_user_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_thumb() {
            return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
            this.goods_thumb = goods_thumb;
        }

        public String getBrand_sn() {
            return brand_sn;
        }

        public void setBrand_sn(String brand_sn) {
            this.brand_sn = brand_sn;
        }

        public String getFormated_shop_price() {
            return formated_shop_price;
        }

        public void setFormated_shop_price(String formated_shop_price) {
            this.formated_shop_price = formated_shop_price;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getSell_type() {
            return sell_type;
        }

        public void setSell_type(String sell_type) {
            this.sell_type = sell_type;
        }

        public String getGoods_type() {
            return goods_type;
        }

        public void setGoods_type(String goods_type) {
            this.goods_type = goods_type;
        }

        public int getIs_pin() {
            return is_pin;
        }

        public void setIs_pin(int is_pin) {
            this.is_pin = is_pin;
        }

        public String getOffer() {
            return offer;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

}
