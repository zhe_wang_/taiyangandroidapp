package taiyang.com.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.umeng.analytics.MobclickAgent;
import com.umeng.message.PushAgent;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.utils.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import cn.magicwindow.mlink.annotation.MLinkRouter;
import taiyang.com.adapter.OfferDTLBAdapter;
import taiyang.com.adapter.OfferInfoHistoryRvAdapter;
import taiyang.com.adapter.OfferInfoRootAdapter;
import taiyang.com.entity.HuckListBean;
import taiyang.com.entity.OfferInfoBean;
import taiyang.com.entity.OfferInfoHuckListBean;
import taiyang.com.tydp_b.KProgressActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.Constants;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.LogUtil;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MyApplication;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.SoftKeyboardStateHelper;
import taiyang.com.utils.T;
import taiyang.com.utils.ToastUtil;

/**
 * 报盘详情Activity
 */

@MLinkRouter(keys={"taiyanggo_goods"})
public class OfferInfoActivity extends KProgressActivity implements View.OnClickListener {



    @InjectView(R.id.rv_offerinfohistory_list)
    RecyclerView rv_offerinfohistory_list;

    //-----------------------------------------
    @InjectView(R.id.ll_miaosha1)
    LinearLayout ll_miaosha1;

    @InjectView(R.id.riv_miaosha1)
    SimpleDraweeView riv_miaosha1;

    @InjectView(R.id.tv_miaosha1_name)
    TextView tv_miaosha1_name;

    @InjectView(R.id.tv_miaosha1_price)
    TextView tv_miaosha1_price;

    @InjectView(R.id.tv_miaosha1_fake_price)
    TextView tv_miaosha1_fake_price;
    @InjectView(R.id.ll_miaosha2)
    LinearLayout ll_miaosha2;
    @InjectView(R.id.riv_miaosha2)
    SimpleDraweeView riv_miaosha2;

    @InjectView(R.id.tv_miaosha2_name)
    TextView tv_miaosha2_name;

    @InjectView(R.id.tv_miaosha2_price)
    TextView tv_miaosha2_price;

    @InjectView(R.id.tv_miaosha2_fake_price)
    TextView tv_miaosha2_fake_price;

    @InjectView(R.id.ll_miaosha3)
    LinearLayout ll_miaosha3;
    @InjectView(R.id.riv_miaosha3)
    SimpleDraweeView riv_miaosha3;

    @InjectView(R.id.tv_miaosha3_name)
    TextView tv_miaosha3_name;

    @InjectView(R.id.tv_miaosha3_price)
    TextView tv_miaosha3_price;

    @InjectView(R.id.tv_miaosha3_fake_price)
    TextView tv_miaosha3_fake_price;
    //--------------------------------

    @InjectView(R.id.iv_offerinfo_back)
    ImageView iv_offerinfo_back;

    @InjectView(R.id.tv_offerinfo_csxx)
    TextView tv_offerinfo_csxx;
    @InjectView(R.id.ib_offerinfo_cj)
    Button ib_offerinfo_cj;
    @InjectView(R.id.ib_offerinfo_dg)
    Button ib_offerinfo_dg;
    @InjectView(R.id.tv_offerinfo_country)
    TextView tv_offerinfo_country;
    @InjectView(R.id.tv_offerinfo_ch)
    TextView tv_offerinfo_ch;
    @InjectView(R.id.tv_offerinfo_number)
    TextView tv_offerinfo_number;
    @InjectView(R.id.tv_offerinfo_hwdz)
    TextView tv_offerinfo_hwdz;
    @InjectView(R.id.tv_offerinfo_pm)
    TextView tv_offerinfo_pm;
    @InjectView(R.id.tv_offerinfo_gg)
    TextView tv_offerinfo_gg;
    @InjectView(R.id.tv_offerinfo_gg2)
    TextView tv_offerinfo_gg2;
    @InjectView(R.id.tv_offerinfo_gk)
    TextView tv_offerinfo_gk;
    @InjectView(R.id.tv_offerinfo_bzgg)
    TextView tv_offerinfo_bzgg;
    @InjectView(R.id.tv_offerinfo_yfty)
    TextView tv_offerinfo_yfty;
    @InjectView(R.id.tv_offerinfo_cjbj)
    TextView tv_offerinfo_cjbj;
    @InjectView(R.id.tv_offerinfo_kc)
    TextView tv_offerinfo_kc;
    @InjectView(R.id.tv_offerinfo_offertype)
    TextView tv_offerinfo_offertype;
    @InjectView(R.id.tv_offerinfo_yjdg)
    TextView tv_offerinfo_yjdg;
    @InjectView(R.id.tv_offerinfo_zctime)
    TextView tv_offerinfo_zctime;
    @InjectView(R.id.tv_offerinfo_make_time)
    TextView tv_offerinfo_make_time;

    @InjectView(R.id.rl_offerinfo_gk)
    LinearLayout rl_offerinfo_gk;

    @InjectView(R.id.vp_offerinfo)
    ViewPager vp_homepager;
    @InjectView(R.id.tv_xian_qi)
    TextView xianqi;
    @InjectView(R.id.tv_zheng_ling)
    TextView zhengling;
    @InjectView(R.id.tv_pin)
    TextView pin;
    @InjectView(R.id.tv_huan)
    TextView huan;
    @InjectView(R.id.tv_offerinfo_text)
    TextView tvOfferinfoText;
    @InjectView(R.id.tv_offerinfo_bbsj)
    TextView tv_offerinfo_bbsj;



    private Button bt01, bt02, bt_offerinfodingou_ok, bt_offerinfochujia_chujia;
    private EditText edt;
    private int num = 0;//数量
    private PopupWindow mPopWindow;
    private PopupWindow mPopWindowD;
    private ImageView iv_pop, im_offerinfodingou_cancel;
    private WindowManager.LayoutParams lp;
    private View rootview;
    private String region_name;
    private String shop_name;
    private String brand_sn;
    private String goods_name;
    private String spec_1;
    private String spec_1_unit;
    private String port_name;
    private String pack_name;
    private List<OfferInfoBean.BpListBean> bp_list;
    private List<OfferInfoBean.HuckListBean> huck_list;
    private List<HuckListBean> huckListDomelist;
    private String shop_id;
    private OfferInfoRootAdapter offerInfoRootAdapter;
    private TextView tv_offerinfochujia_cjjl;
    private RecyclerView rv_offerinfotoot_chujia;
    private String offer;
    private String goods_id;
    private EditText et_pop;
    private EditText et_pop_message;
    private Integer shopPrice;
    private float shop_price;
    private String sell_tupe;
    private TextView danwei1;
    private OfferInfoBean offerInfoBean;
    private TextView kucun;
    private String offer_type;
    private TextView dinggou_name;
    private TextView dinggou_kucun;
    private String shop_price_am;
    private TextView dinggou_danjia;
    private TextView dinggou_spce2;
    private TextView dinggou_spce2_unit;
    private String spec_2;
    private String getSpec_2_unit;
    private int chujia;
    private TextView tv_offerinfodg_zj;
    private String goods_weight;
    private OfferInfoGridViewAdapter offerInfoGridViewAdapter;
    private String goods_thumb;
    private SimpleDraweeView iv_offerinfodg_icon;
    private String goodsNumber;//库存
    private SoftKeyboardStateHelper softKeyboardStateHelper;
    private int retailCount;//零售最高数量
    private int retailLowCount;//零售最低数量
    private String prepay_name;
    private String prepay_type;
    private String prepay_num;
    private HuckListBean huckListDome;
    private String spec2;

    private Handler mainHandler = new Handler() {
        public void handleMessage(Message msg) {
            vp_homepager.setCurrentItem(vp_homepager.getCurrentItem() + 1);// 设置下一页
            // 延时发送消息
            mainHandler.sendEmptyMessageDelayed(0, 4000);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (intent != null) {
            String param = intent.getStringExtra("param");
        }

        PushAgent.getInstance(OfferInfoActivity.this).onAppStart();
        setContentView(R.layout.activity_offerinfo);
        ButterKnife.inject(this);
        softKeyboardStateHelper = new SoftKeyboardStateHelper(findViewById(R.id.soft_layout));
        softKeyboardStateHelper.addSoftKeyboardStateListener(new SoftKeyboardStateHelper.SoftKeyboardStateListener() {
            @Override
            public void onSoftKeyboardOpened(int keyboardHeightInPx) {

            }

            @Override
            public void onSoftKeyboardClosed() {
                if (num > Integer.parseInt(goodsNumber)) {
                    T.showShort(OfferInfoActivity.this, getString(R.string.dayukucun));
                    if (offerInfoBean.getSell_type().equals("5")) {
                        num = 0;
                        edt.setText(num + "");
                        tv_offerinfodg_zj.setText("￥0.0");
                        bt_offerinfodingou_ok.setBackgroundColor(getResources().getColor(R.color.mask_color));
                    } else {
                        num = retailCount;
                        edt.setText(num + "");
                        tv_offerinfodg_zj.setText("￥" + (num / Float.parseFloat(offerInfoBean.getSpec_2()) * offerInfoBean.getShop_price()));
                        bt_offerinfodingou_ok.setBackgroundColor(getResources().getColor(R.color.bg_bluecolor));
                    }

                } else {
                    if (offerInfoBean.getSell_type().equals("5")) {
                        tv_offerinfodg_zj.setText("￥" + (num * offerInfoBean.getShop_price() * Float.parseFloat(offerInfoBean.getGoods_weight())));
                    } else {
                        if (num < retailLowCount) {
                            num = retailLowCount;
                            T.showShort(OfferInfoActivity.this, getString(R.string.bunengshaoyu) + retailLowCount +getString(R.string.jian));
                            edt.setText(num + "");
                            tv_offerinfodg_zj.setText("￥" + (num / Float.parseFloat(offerInfoBean.getSpec_2()) * offerInfoBean.getShop_price()));
                        } else {
                            edt.setText(num + "");
                            tv_offerinfodg_zj.setText("￥" + (num / Float.parseFloat(offerInfoBean.getSpec_2()) * offerInfoBean.getShop_price()));
                        }
                    }
                    bt_offerinfodingou_ok.setBackgroundColor(getResources().getColor(R.color.bg_bluecolor));
                }
            }
        });
        initListeren();
        initData();

    }

    private OfferInfoHistoryRvAdapter mOfferInfoHistoryRvAdapter;


    public void initListeren() {
        //设置pop依附的rootview
        rootview = LayoutInflater.from(this).inflate(R.layout.activity_offerinfo, null);
        //设置还价contentView
        View contentView = LayoutInflater.from(this).inflate(R.layout.popup_offerinfo_chujia, null);
        mPopWindow = new PopupWindow(contentView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        mPopWindow.setContentView(contentView);

        //设置模式，和Activity的一样，覆盖，调整大小。
        mPopWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        //设置订购contentView
        View contentViewD = LayoutInflater.from(this).inflate(R.layout.popup_offerinfo_dingou, null);
        mPopWindowD = new PopupWindow(contentView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        mPopWindowD.setContentView(contentViewD);
        //设置模式，和Activity的一样，覆盖，调整大小。
        mPopWindowD.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        //出价
        iv_pop = (ImageView) contentView.findViewById(R.id.im_offerinfochujia_cancel);
        et_pop = (EditText) contentView.findViewById(R.id.et_offerinfochujia_chujia);
        et_pop_message = (EditText) contentView.findViewById(R.id.et_offerinfochujia_text);
        bt_offerinfochujia_chujia = (Button) contentView.findViewById(R.id.bt_offerinfochujia_chujia);
        tv_offerinfochujia_cjjl = (TextView) contentView.findViewById(R.id.tv_offerinfochujia_cjjl);
        rv_offerinfotoot_chujia = (RecyclerView) contentView.findViewById(R.id.rv_offerinfotoot_chujia);

        //订购
//        danwei2 = (TextView) contentViewD.findViewById(R.id.tv_offerinfodg_danwei2);
        iv_offerinfodg_icon = (SimpleDraweeView) contentViewD.findViewById(R.id.iv_offerinfodg_icon);
        dinggou_name = (TextView) contentViewD.findViewById(R.id.tv_offerinfodg_name);
        dinggou_kucun = (TextView) contentViewD.findViewById(R.id.tv_offerinfodg_kucun);
        dinggou_danjia = (TextView) contentViewD.findViewById(R.id.tv_offerinfodg_danjia);
        dinggou_spce2 = (TextView) contentViewD.findViewById(R.id.tv_offerinfodg_spce2);
        dinggou_spce2_unit = (TextView) contentViewD.findViewById(R.id.tv_offerinfodg_spce2_unit);
        tv_offerinfodg_zj = (TextView) contentViewD.findViewById(R.id.tv_offerinfodg_zj);

        im_offerinfodingou_cancel = (ImageView) contentViewD.findViewById(R.id.im_offerinfodingou_cancel);
        bt_offerinfodingou_ok = (Button) contentViewD.findViewById(R.id.bt_offerinfodingou_ok);
        bt01 = (Button) contentViewD.findViewById(R.id.subbt);
        bt02 = (Button) contentViewD.findViewById(R.id.addbt);
        edt = (EditText) contentViewD.findViewById(R.id.edt);

        //窗口对象 设置pop弹出时背景色
        lp = getWindow().getAttributes();

        //数量输入框
        edt.setInputType(InputType.TYPE_CLASS_NUMBER);
        edt.setText(String.valueOf(num));
        bt01.setOnClickListener(new OnButtonClickListener());
        bt02.setOnClickListener(new OnButtonClickListener());
        edt.addTextChangedListener(new OnTextChangeListener());

        iv_pop.setOnClickListener(this);
        et_pop.setOnClickListener(this);
        bt_offerinfochujia_chujia.setOnClickListener(this);
        iv_offerinfo_back.setOnClickListener(this);
        im_offerinfodingou_cancel.setOnClickListener(this);
        tv_offerinfo_csxx.setOnClickListener(this);
        ib_offerinfo_cj.setOnClickListener(this);
        ib_offerinfo_dg.setOnClickListener(this);
        findViewById(R.id.ll_shop).setOnClickListener(this);
        bt_offerinfodingou_ok.setOnClickListener(this);
        findViewById(R.id.tv_offer_shuyu_info).setOnClickListener(this);

    }


    public void initData() {
        //出价列表  模板列表
        huckListDomelist = new ArrayList<>();
        goods_id = getIntent().getStringExtra("Goods_id");
        showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
        Map<String, Object> params = new HashMap<>();
        params.put("model", "goods");
        params.put("action", "get_info");
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("goods", "get_info")));
        params.put("goods_id", goods_id);
        HttpUtils.sendPost(params, new HttpRequestListener() {

            @Override
            public void success(String response, int id) {
                offerInfoBean = new Gson().fromJson(response, OfferInfoBean.class);
                if (offerInfoBean == null) {
                    return;
                }
                try {
                    setTuijianGoodsView();
                }
                catch (Exception e){

                }
                tv_offerinfo_bbsj.setText(offerInfoBean.getLast_update());
                //还价
                if (offerInfoBean.getOffer().equals("11")) {
                    xianqi.setVisibility(View.VISIBLE);
                } else {
                    xianqi.setVisibility(View.GONE);
                }
                //拼
                if (offerInfoBean.getIs_pin() == 1) {
                    findViewById(R.id.ll_offerinfo_make_time).setVisibility(View.GONE);
                    pin.setVisibility(View.VISIBLE);
                    findViewById(R.id.ll_guige).setVisibility(View.GONE);
                } else {
                    pin.setVisibility(View.GONE);
                }
                //商品类型  6=期货7=现货
                if (offerInfoBean.getGoods_type().equals("6")) {
                    xianqi.setText(getString(R.string.qi));
                    xianqi.setBackgroundColor(getResources().getColor(R.color.qi));
                    findViewById(R.id.rl_offerinfo_hwszd).setVisibility(View.GONE);
                } else {
                    xianqi.setText(getString(R.string.xian));
                    xianqi.setBackgroundColor(getResources().getColor(R.color.xian));
                    findViewById(R.id.rl_offerinfo_offertype).setVisibility(View.GONE);
                    findViewById(R.id.rl_offerinfo_gk).setVisibility(View.GONE);
                    findViewById(R.id.ll_zhuangchuanriqi).setVisibility(View.GONE);
                    findViewById(R.id.ll_yujidaogang).setVisibility(View.GONE);

                }
                //售卖类型 4零5整
                if (offerInfoBean.getSell_type().equals("4")) {
                    zhengling.setText(getString(R.string.ling));
                    zhengling.setBackgroundColor(getResources().getColor(R.color.ling));
                } else {
                    zhengling.setText(getString(R.string.zheng));
                    zhengling.setBackgroundColor(getResources().getColor(R.color.zheng));
                }

                ((TextView)findViewById(R.id.tv_offerinfo_make_time)).setText(offerInfoBean.getMake_date());

                prepay_type = offerInfoBean.prepay_type;
                prepay_num = offerInfoBean.prepay_num;

                //轮播图

                List<OfferInfoBean.PictureListBean> app_silde = offerInfoBean.getPicture_list();
                OfferInfoBean.PictureListBean first = new OfferInfoBean.PictureListBean();
                first.setId("-1");
                first.setImg_url(offerInfoBean.getGoods_thumb());
                first.setThumb_url(offerInfoBean.getGoods_thumb());
                app_silde.add(0, first);
                OfferDTLBAdapter homeDTLBAdapter = new OfferDTLBAdapter(OfferInfoActivity.this, app_silde);
                vp_homepager.setAdapter(homeDTLBAdapter);

                // 默认设置中间的某个item
                vp_homepager.setCurrentItem(app_silde.size() * 100000);
                // 延时发送消息
                if (mainHandler != null) {
                    mainHandler.removeCallbacksAndMessages(null);
                    mainHandler.sendEmptyMessageDelayed(0, 4000);
                }


//                tv_offerinfo_make_time.setText(offerInfoBean.get);
                goodsNumber = offerInfoBean.getGoods_number();
                goods_thumb = offerInfoBean.getGoods_thumb();
                //订购icon
                iv_offerinfodg_icon.setImageURI(Uri.parse(goods_thumb));
                //编号
                String sku = offerInfoBean.getSku();
                if (sku != null || sku.equals("")) {
                    tv_offerinfo_number.setText(sku);
                } else {
                    tv_offerinfo_number.setText(getString(R.string.zanwu));
                }

                if (offerInfoBean.getIs_pin() == 1) {
                    setQiugouGoodsView();
                } else {
                    findViewById(R.id.ll_pinggui_list).setVisibility(View.GONE);
                }
                //报盘类型
                offer_type = offerInfoBean.getOffer_type();
                goods_weight = offerInfoBean.getGoods_weight();
                if (offer_type == null || offer_type.equals("")) {
                    tv_offerinfo_offertype.setText(getString(R.string.zanwu));
                } else if (offer_type.equals("1")) {
                    tv_offerinfo_offertype.setText("CIF");
                } else if (offer_type.equals("2")) {
                    tv_offerinfo_offertype.setText("FOB");
                } else if (offer_type.equals("3")) {
                    tv_offerinfo_offertype.setText("DDP");
                }else if (offer_type.equals("34")) {
                    tv_offerinfo_offertype.setText("CFR");
                }
//                //商店名
//                shop_name = offerInfoBean.getShop_name();
                ((TextView) findViewById(R.id.tv_shop_name)).setText(offerInfoBean.getShop_name());
                ((TextView) findViewById(R.id.tv_guanzhuliang)).setText(offerInfoBean.getFollow_count());
                ((TextView) findViewById(R.id.tv_chengjiaoliang)).setText(offerInfoBean.getShop_order_num());
                ((TextView) findViewById(R.id.tv_quanbushangpi)).setText(offerInfoBean.getShop_goods_num());
                ((SimpleDraweeView) findViewById(R.id.riv_shop_logo)).setImageURI(offerInfoBean.getShop_logo());
//                tv_offerinfo_shopname.setText(shop_name);
                //国家
                region_name = offerInfoBean.getRegion_name();
                tv_offerinfo_country.setText(region_name);
                //厂号
                brand_sn = offerInfoBean.getBrand_sn();
                tv_offerinfo_ch.setText(brand_sn);
                //品名
                goods_name = offerInfoBean.getGoods_name();
                MobclickAgent.onEvent(OfferInfoActivity.this, offerInfoBean.getGoods_name());
                tv_offerinfo_pm.setText(goods_name);
                dinggou_name.setText(goods_name);
                //规格+规格名
                spec_1 = offerInfoBean.getSpec_1();
                spec_1_unit = offerInfoBean.getSpec_1_unit();
                spec2 = offerInfoBean.getSpec_2();
                getSpec_2_unit = offerInfoBean.getSpec_2_unit();
                if (spec_1.equals("")) {
                    tv_offerinfo_gg.setText(getString(R.string.zanwu));
                } else {
                    tv_offerinfo_gg.setText(spec_1 + spec_1_unit);
                    tv_offerinfo_gg2.setText(spec2 + getSpec_2_unit);
                }

                //包装规格
                pack_name = offerInfoBean.getPack_name();
                tv_offerinfo_bzgg.setText(pack_name);
                //产品单价
                shop_price = offerInfoBean.getShop_price();
                if(offerInfoBean.getCurrency().equals("1")){
                    tv_offerinfo_cjbj.setText(offerInfoBean.getFormated_shop_price() +"/"+ offerInfoBean.getShop_price_unit());
                }
                if(offerInfoBean.getCurrency().equals("2")){
                    tv_offerinfo_cjbj.setText(offerInfoBean.getFormated_shop_price_am() +"/"+ offerInfoBean.getShop_price_unit());
                }
                if(offerInfoBean.getCurrency().equals("3")){
                    tv_offerinfo_cjbj.setText(offerInfoBean.getFormated_shop_price_eur() +"/"+ offerInfoBean.getShop_price_unit());
                }
                String danwei = "";
                if(offerInfoBean.getSell_type().equals("4")){
                    danwei = offerInfoBean.getSpec_2()+getString(R.string.jian)+"/"+getString(R.string.dun);
                }else{
                    danwei = offerInfoBean.getGoods_weight()+getString(R.string.dun)+"/"+getString(R.string.gui);
                }
                dinggou_danjia.setText(offerInfoBean.getFormated_shop_price()+"/"+offerInfoBean.getShop_price_unit()+"    "+danwei);
                tv_offerinfochujia_cjjl.setText(offerInfoBean.getShop_price_formated() +"/"+getString(R.string.dun));


                //港口
                port_name = offerInfoBean.getPort_name();
                if (port_name == null || port_name.equals("")) {
                    tv_offerinfo_gk.setText(getString(R.string.zanwu));
                } else {
                    tv_offerinfo_gk.setText(port_name);
                }

                //预计到港
                String arrive_date = offerInfoBean.getFormated_arrive_date();
                if (arrive_date == null || arrive_date.equals("")) {
                    tv_offerinfo_yjdg.setText(getString(R.string.zanwu));
                } else {
                    tv_offerinfo_yjdg.setText(arrive_date);
                }
                //装船日期
                String lading_date = offerInfoBean.getFormated_lading_date();
                if (lading_date == null || lading_date.equals("")) {
                    tv_offerinfo_zctime.setText(getString(R.string.zanwu));
                } else {
                    tv_offerinfo_zctime.setText(lading_date);
                }
                //预付条约
                prepay_name = offerInfoBean.getPrepay_name();
                if (prepay_name == null || prepay_name.equals("")) {
                    tv_offerinfo_yfty.setText(getString(R.string.zanwu));
                } else {
                    if (prepay_type.equals("1")) {
                        tv_offerinfo_yfty.setText("¥" + prepay_num);
                    } else {
                        tv_offerinfo_yfty.setText(prepay_name + "%");
                    }

                }
                //货物地址
                 tv_offerinfo_hwdz.setText(offerInfoBean.getGoods_local());


                //库存
                tv_offerinfo_kc.setText(offerInfoBean.getGoods_number()+((offerInfoBean.getSell_type().equals("4"))?getString(R.string.jian):getString(R.string.gui)));
                dinggou_kucun.setText(offerInfoBean.getGoods_number()+((offerInfoBean.getSell_type().equals("4"))?getString(R.string.jian):getString(R.string.gui)));
                //店铺id
                shop_id = offerInfoBean.shop_id;
                //报盘历史列表
                bp_list = offerInfoBean.getBp_list();
                //能否出价
                offer = offerInfoBean.getOffer();
                if (!offer.equals("11")) {
                    ib_offerinfo_cj.setBackgroundColor(getResources().getColor(R.color.mask_color));
                }
                //订购 单位
                sell_tupe = offerInfoBean.getSell_type();

                if (("4").equals(sell_tupe)) {
                    num = Integer.parseInt(offerInfoBean.getSpec_2());
                    retailCount = Integer.parseInt(offerInfoBean.getGoods_number());
                    retailLowCount = Integer.parseInt(offerInfoBean.getSpec_2());
                    edt.setText(num + "");
                    tv_offerinfodg_zj.setText("￥" + (num / Integer.parseInt(offerInfoBean.getSpec_2()) * offerInfoBean.getShop_price()));
                    bt_offerinfodingou_ok.setBackgroundColor(getResources().getColor(R.color.bg_bluecolor));
                    dinggou_spce2.setText(offerInfoBean.getSpec_2());
                    dinggou_spce2_unit.setText(offerInfoBean.getSpec_2_unit());
                } else {
                    dinggou_spce2.setText(offerInfoBean.getGoods_weight() + getString(R.string.dun));
                    dinggou_spce2_unit.setText("/"+getString(R.string.gui));
                }

                //出价历史
                huck_list = offerInfoBean.getHuck_list();
                huckListDomelist.clear();
                if (huck_list != null) {
                    for (int i = 0; i < huck_list.size(); i++) {
                        huckListDome = new HuckListBean();
                        huckListDome.setCreated_at(huck_list.get(i).getCreated_at());
                        huckListDome.setFormated_price(huck_list.get(i).getFormated_price());
                        huckListDome.setUnit(huck_list.get(i).unit);
                        huckListDome.setUser_name(huck_list.get(i).getUser_name());
                        huckListDomelist.add(huckListDome);
                    }

                    offerInfoRootAdapter = new OfferInfoRootAdapter(huckListDomelist);
                    rv_offerinfotoot_chujia.setAdapter(offerInfoRootAdapter);
                    offerInfoRootAdapter.notifyDataSetChanged();
                    rv_offerinfotoot_chujia.setLayoutManager(new LinearLayoutManager(OfferInfoActivity.this));
                }


                findViewById(R.id.tv_offerinfo_share).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new ShareAction(OfferInfoActivity.this).setDisplayList(SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE)
                                .withTitle(brand_sn+"  "+goods_name+"  "+tv_offerinfo_cjbj.getText().toString())
                                .withText(brand_sn+"  "+goods_name+"  "+tv_offerinfo_cjbj.getText().toString()+"   "+offerInfoBean.getGoods_txt()  +"   "+(offerInfoBean.getGoods_local().isEmpty()?offerInfoBean.getPort_name():offerInfoBean.getGoods_local()))
                                .withTargetUrl("http://www.taiyanggo.com/mobile/share.php?act=goods&id="+goods_id)
                                .withMedia(new UMImage(OfferInfoActivity.this, offerInfoBean.getGoods_thumb()))
                                .open();
                    }
                });

                //报盘说明
                tvOfferinfoText.setText(offerInfoBean.getGoods_txt());


                bp_list = offerInfoBean.getBp_list();
                if(!bp_list.isEmpty()){
                    findViewById(R.id.ll_history).setVisibility(View.VISIBLE);
                    mOfferInfoHistoryRvAdapter = new OfferInfoHistoryRvAdapter(bp_list);
                    mOfferInfoHistoryRvAdapter.notifyDataSetChanged();
                    rv_offerinfohistory_list.setLayoutManager(new LinearLayoutManager(OfferInfoActivity.this));
                    rv_offerinfohistory_list.setAdapter(mOfferInfoHistoryRvAdapter);
                }else {
                    findViewById(R.id.ll_history).setVisibility(View.GONE);
                }
                dismissProgress(OfferInfoActivity.this);
            }

            @Override
            public void failByOther(String fail, int id) {
                ToastUtil.showToast(fail);
                finish();
            }

            @Override
            public void fail(String fail, int id) {

            }
        });
    }

    private void setQiugouGoodsView() {
        LayoutInflater inflater = OfferInfoActivity.this.getLayoutInflater();
        for (OfferInfoBean.GoodsBaseBean goodsBean : offerInfoBean.getGoods_base()) {
            LinearLayout v = (LinearLayout) inflater
                    .inflate(R.layout.offer_pin_list_item, null);
            ((TextView) v.findViewById(R.id.tv_offerinfo_pm)).setText(goodsBean.getGoods_names());
            ((TextView) v.findViewById(R.id.tv_offerinfo_country)).setText(goodsBean.getRegion_nameX());
            ((TextView) v.findViewById(R.id.tv_offerinfo_ch)).setText(goodsBean.getBrand_snX());
            ((TextView) v.findViewById(R.id.tv_offerinfo_gg)).setText(goodsBean.getSpecs_3() + goodsBean.getSpecs_3_unit());
            ((TextView) v.findViewById(R.id.tv_offerinfo_scrq)).setText(goodsBean.getMake_date());
            ((LinearLayout) findViewById(R.id.ll_pinggui_list)).addView(v);
        }
    }

    private PopupWindow window;

    private void initPopwindow() {

        // 利用layoutInflater获得View
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_offer_shuyu_info_, null);

        // 下面是两种方法得到宽度和高度 getWindow().getDecorView().getWidth()

        window = new PopupWindow(view,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.alpha = 0.5f;

        getWindow().setAttributes(params);
        // 设置popWindow弹出窗体可点击，这句话必须添加，并且是true
        window.setFocusable(true);

        // 必须要给调用这个方法，否则点击popWindow以外部分，popWindow不会消失
        // window.setBackgroundDrawable(new BitmapDrawable());

        window.setBackgroundDrawable(new BitmapDrawable());
        // 在参照的View控件下方显示
        // window.showAsDropDown(MainActivity.this.findViewById(R.id.start));

        // 设置popWindow的显示和消失动画
        window.setAnimationStyle(R.style.mypopwindow_anim_style);
        // 在底部显示
        window.showAtLocation(OfferInfoActivity.this.findViewById(R.id.tv_offer_shuyu_info),
                Gravity.CENTER, 0, 0);
        // 这里检验popWindow里的button是否可以点击
        view.findViewById(R.id.ll_all).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                window.dismiss();
            }
        });


        // popWindow消失监听方法
        window.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams params = getWindow().getAttributes();
                params.alpha = 1f;
                getWindow().setAttributes(params);
            }
        });
    }

    private void setTuijianGoodsView() {

        ll_miaosha1.setVisibility(View.GONE);
        ll_miaosha2.setVisibility(View.GONE);
        ll_miaosha3.setVisibility(View.GONE);

        final OfferInfoBean.GoodsBaseListBean good1 = offerInfoBean.getGoods_base_list().get(0);
        riv_miaosha1.setImageURI(good1.getGoods_thumbX());
        tv_miaosha1_name.setText(good1.getGoods_nameX());
        tv_miaosha1_price.setText(Html.fromHtml("<font color='#e64040'>" + good1.getShop_priceX() + "</font>/" + good1.getShop_price_unit()));
        tv_miaosha1_fake_price.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        ll_miaosha1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(OfferInfoActivity.this, OfferInfoActivity.class);
                i.putExtra("Goods_id", good1.getGoods_idX());
                OfferInfoActivity.this.startActivity(i);
            }
        });

        ll_miaosha1.setVisibility(View.VISIBLE);
        final OfferInfoBean.GoodsBaseListBean good2 = offerInfoBean.getGoods_base_list().get(1);
        riv_miaosha2.setImageURI(good2.getGoods_thumbX());
        tv_miaosha2_name.setText(good2.getGoods_nameX());
        tv_miaosha2_price.setText(Html.fromHtml("<font color='#e64040'>" + good2.getShop_priceX() + "</font>/" + good2.getShop_price_unit()));
        tv_miaosha2_fake_price.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        ll_miaosha2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(OfferInfoActivity.this, OfferInfoActivity.class);
                i.putExtra("Goods_id", good2.getGoods_idX());
                OfferInfoActivity.this.startActivity(i);
            }
        });

        ll_miaosha2.setVisibility(View.VISIBLE);
        final OfferInfoBean.GoodsBaseListBean good3 = offerInfoBean.getGoods_base_list().get(2);
        riv_miaosha3.setImageURI(good3.getGoods_thumbX());
        tv_miaosha3_name.setText(good3.getGoods_nameX());
        tv_miaosha3_price.setText(Html.fromHtml("<font color='#e64040'>" + good3.getShop_priceX() + "</font>/" + good3.getShop_price_unit()));
        tv_miaosha3_fake_price.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        ll_miaosha3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(OfferInfoActivity.this, OfferInfoActivity.class);
                i.putExtra("Goods_id", good3.getGoods_idX());
                OfferInfoActivity.this.startActivity(i);
            }
        });
        ll_miaosha3.setVisibility(View.VISIBLE);


    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tv_offer_shuyu_info:
                initPopwindow();
                break;

            case R.id.iv_offerinfo_back://退出
                finish();
                break;
//
//            case R.id.ib_offerinfo_lsbp://历史报盘
//
//                Intent i = new Intent(this, OfferInfoHistoryActivity.class);
//                i.putExtra("goods_id", goods_id);
//                i.putExtra("shop_id", shop_id);
//                i.putExtra("region_name", region_name);
//                i.putExtra("brand_sn", brand_sn);
//                i.putExtra("goods_name", goods_name);
//                i.putExtra("spec_1", spec_1);
//                i.putExtra("spec_1_unit", spec_1_unit);
//                if (port_name != null)i.putExtra("port_name", port_name);
//                i.putExtra("pack_name", pack_name);
//                startActivity(i);
//
//                break;

            case R.id.tv_offerinfo_csxx://进入店铺

                Intent i2 = new Intent(this, FactoryInfoActivity.class);
                i2.putExtra("shop_id", shop_id);
                startActivity(i2);
                break;

            case R.id.ib_offerinfo_cj://出价Popwinds显示
                if (!MySPEdit.getInstance(this).getIsLogin()) {
                    ToastUtil.showToast(getString(R.string.qingxiandengku));
                    Intent intent = new Intent(this, LoginActivity.class);
                    intent.putExtra("action", Constants.OFFERINFOACTION);
                    startActivity(intent);
                } else {
                    if (offer != null && offer.equals("11")) {

                        //能出价
                        mPopWindow.showAtLocation(rootview, Gravity.BOTTOM, 0, 0);
                        //背景变暗
                        lp.alpha = 0.4f;
                        getWindow().setAttributes(lp);
                    } else {
                        ToastUtil.showToast("不能出价");
                    }
                }


                break;
            case R.id.im_offerinfochujia_cancel://出价 - 取消pop

                mPopWindow.dismiss();
                //背景恢复
                lp.alpha = 1f;
                getWindow().setAttributes(lp);

                break;

            case R.id.ib_offerinfo_dg://订购

                mPopWindowD.showAtLocation(rootview, Gravity.BOTTOM, 0, 0);
                lp.alpha = 0.4f;
                getWindow().setAttributes(lp);

                break;
            case R.id.im_offerinfodingou_cancel://订购 - 取消pop

                mPopWindowD.dismiss();
                //背景恢复
                lp.alpha = 1f;
                getWindow().setAttributes(lp);

                break;
            case R.id.bt_offerinfodingou_ok:

                if (!MySPEdit.getInstance(this).getIsLogin()) {
                    ToastUtil.showToast(getString(R.string.qingxiandengku));
                    Intent intent = new Intent(this, LoginActivity.class);
                    intent.putExtra("action", Constants.OFFERINFOACTION);
                    startActivity(intent);
                } else {
                    if (num == 0) {
                        T.showShort(OfferInfoActivity.this, getString(R.string.qingxuanzegoumaishuliang));
                        return;
                    }
                    mPopWindowD.dismiss();
                    lp.alpha = 1f;
                    getWindow().setAttributes(lp);
                    Intent i3 = new Intent(this, OrderCofirmActivity.class);
                    i3.putExtra("num", num + "");
                    i3.putExtra("goods_thumb", goods_thumb);
                    i3.putExtra("goods_name", goods_name);
                    i3.putExtra("danjia", offerInfoBean.getShop_price());
                    L.e("danjia " + offerInfoBean.getShop_price());
                    i3.putExtra("spec_2", offerInfoBean.getSpec_2() + "");
                    i3.putExtra("getSpec_2_unit", getSpec_2_unit);
                    i3.putExtra("sell_tupe", sell_tupe);
                    i3.putExtra("goods_weight", goods_weight);
                    i3.putExtra("prepay_name", prepay_name);
                    i3.putExtra("prepay_type", prepay_type);
                    i3.putExtra("prepay_num", prepay_num);
                    if (goods_id != null) i3.putExtra("goods_id", goods_id);
                    startActivity(i3);
                }


                break;

            case R.id.ll_shop:
                Intent i4 = new Intent(this, FactoryInfoActivity.class);
                i4.putExtra("shop_id", shop_id);
                startActivity(i4);
                break;

            case R.id.bt_offerinfochujia_chujia: //出价确定

                String stringchujia = et_pop.getText().toString();
                if (stringchujia == null || stringchujia.equals("")) {
                    ToastUtil.showToast("请出价");

                } else {
                    chujia = Integer.parseInt(stringchujia);
                    if (chujia > shop_price) {
                        ToastUtil.showToast("出价不能大于报价");
                    } else if (chujia == shop_price) {
                        ToastUtil.showToast("出价不能与报价相同");
                    } else {
                        notifyCJList();
                    }
                }

                break;

            default:
                break;
        }


    }

    //上传出价数据 重新请求列表
    private void notifyCJList() {

        showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
        Map<String, Object> params = new HashMap<>();
        params.put("model", "goods");
        params.put("action", "save_huck");
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("goods", "save_huck")));
        params.put("user_id", MySPEdit.getInstance(this).getUserId());
        params.put("shop_id", shop_id);
        params.put("price", chujia);
        params.put("last_price", shop_price);
        params.put("goods_id", goods_id);
        params.put("message", et_pop_message.getText());

        HttpUtils.sendPost(params, new HttpRequestListener() {
            @Override
            public void success(String response, int id) {

                L.e("response   " + response);
                List<OfferInfoHuckListBean> huckListBean = new Gson().fromJson(response, new TypeToken<List<OfferInfoHuckListBean>>() {
                }.getType());
                if (huckListBean != null) {

                    huckListDomelist.clear();
                    for (int i = 0; i < huckListBean.size(); i++) {
                        huckListDome = new HuckListBean();
                        huckListDome.setCreated_at(huckListBean.get(i).getCreated_at());
                        huckListDome.setFormated_price(huckListBean.get(i).getFormated_price());
                        huckListDome.setUnit(huckListBean.get(i).getUnit());
                        huckListDome.setUser_name(huckListBean.get(i).getUser_name());
                        huckListDomelist.add(huckListDome);
                    }

                    offerInfoRootAdapter = new OfferInfoRootAdapter(huckListDomelist);
                    rv_offerinfotoot_chujia.setAdapter(offerInfoRootAdapter);
                    offerInfoRootAdapter.notifyDataSetChanged();
                    ToastUtil.showToast("出价成功");
                    et_pop.setText("");
                    et_pop_message.setText("");
                }

                dismissProgress(OfferInfoActivity.this);
            }

            @Override
            public void failByOther(String fail, int id) {
                ToastUtil.showToast(fail);
                dismissProgress(OfferInfoActivity.this);
            }

            @Override
            public void fail(String fail, int id) {
                ToastUtil.showToast(fail);
                dismissProgress(OfferInfoActivity.this);
            }
        });
    }


    /**
     * 加减按钮事件监听器
     */
    class OnButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.addbt:
                    num--;
                    if (sell_tupe.equals("5")) {
                        //柜售
                        if (num == 0) {
                            bt_offerinfodingou_ok.setBackgroundColor(getResources().getColor(R.color.mask_color));
                        }
                        if (num < 0) {
                            num++;
                            T.showShort(OfferInfoActivity.this, "至少购买一件商品");
                        } else {
                            edt.setText(String.valueOf(num));
                            tv_offerinfodg_zj.setText("￥" + (num * offerInfoBean.getShop_price() * Float.parseFloat(offerInfoBean.getGoods_weight())));
                            L.e("1  -5" + (num * offerInfoBean.getShop_price() * Float.parseFloat(offerInfoBean.getGoods_weight())));
                        }

                    } else {
                        //零售
                        if (num < Integer.parseInt(offerInfoBean.getSpec_2())) {
                            num++;
                            T.showShort(OfferInfoActivity.this, "此商品不能少于" + retailLowCount + "件");
                            tv_offerinfodg_zj.setText("￥" + (num / Float.parseFloat(offerInfoBean.getSpec_2()) * offerInfoBean.getShop_price()));
                            L.e("1  -4" + (num / Float.parseFloat(offerInfoBean.getSpec_2()) * offerInfoBean.getShop_price()));
                            edt.setText(num + "");
                        } else {
                            edt.setText(num + "");
                            L.e("价钱:" + (num / Integer.parseInt(offerInfoBean.getSpec_2()) * offerInfoBean.getShop_price()));
                            tv_offerinfodg_zj.setText("￥" + (num / Float.parseFloat(offerInfoBean.getSpec_2()) * offerInfoBean.getShop_price()));
                            L.e("1  -4" + (num / Float.parseFloat(offerInfoBean.getSpec_2()) * offerInfoBean.getShop_price()));
                        }
                    }

                    break;

                case R.id.subbt:
                    num++;
                    if (num > Integer.parseInt(goodsNumber)) {
                        T.showShort(OfferInfoActivity.this, "不能高出库存!");
                        num--;
                        edt.setText(String.valueOf(num));
                    }
                    if (sell_tupe.equals("5")) {
                        //柜售
                        if (num > 0) {
                            bt_offerinfodingou_ok.setBackgroundColor(getResources().getColor(R.color.bg_bluecolor));
                            edt.setText(String.valueOf(num));
                            tv_offerinfodg_zj.setText("￥" + (num * offerInfoBean.getShop_price() * Float.parseFloat(offerInfoBean.getGoods_weight())));
                            bt_offerinfodingou_ok.setBackgroundColor(getResources().getColor(R.color.bg_bluecolor));
                        }
                    } else {
                        //零售

                        edt.setText(num + "");
                        tv_offerinfodg_zj.setText("￥" + (num / Float.parseFloat(offerInfoBean.getSpec_2()) * offerInfoBean.getShop_price()));
                        bt_offerinfodingou_ok.setBackgroundColor(getResources().getColor(R.color.bg_bluecolor));
                    }

                    break;
            }

        }
    }

    /**
     * EditText输入变化事件监听器
     */
    class OnTextChangeListener implements TextWatcher {

        @Override
        public void afterTextChanged(Editable s) {
            String numString = s.toString();
            if (numString == null || numString.equals("")) {
                num = 0;
            } else {
                int numInt = Integer.parseInt(numString);
                if (numInt < 0) {
                    Toast.makeText(MyApplication.getContext(), R.string.feifashuru,
                            Toast.LENGTH_SHORT).show();
                } else {
                    //设置EditText光标位置 为文本末端
                    edt.setSelection(edt.getText().toString().length());
                    num = numInt;
                    L.e("测试文本:" + num);
                }
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /** attention to this below ,must add this**/
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
        Log.d("result", "onActivityResult");
    }
}
