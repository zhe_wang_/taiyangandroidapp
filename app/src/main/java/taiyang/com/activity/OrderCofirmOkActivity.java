package taiyang.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.entity.OrderCofirmOKBean;
import taiyang.com.tydp_b.KProgressActivity;
import taiyang.com.tydp_b.R;

/**
 * 订单提交成功
 */
public class OrderCofirmOkActivity extends KProgressActivity implements View.OnClickListener {

    @InjectView(R.id.iv_orderconfirmok_back)
    ImageView iv_orderconfirmok_back;

    @InjectView(R.id.bt_orderconfirmok_lookorder)
    Button bt_orderconfirmok_lookorder;
    @InjectView(R.id.tv_orderconfirmok_fkje)
    TextView tv_orderconfirmok_fkje;
    @InjectView(R.id.tv_orderconfirmok_num)
    TextView tv_orderconfirmok_num;
    private OrderCofirmOKBean orderCofirmOKBean;
    private String isPinTai;
    private String response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderconfirm_ok);
        ButterKnife.inject(this);

        initView();
        initListeren();


    }

    //初始化 交易界面
    public void initView(){
        showProgress(this,getString(R.string.jiazaizhong_dotdotdot));
        response = getIntent().getStringExtra("response");
        isPinTai = getIntent().getStringExtra("isPinTai");
        if(isPinTai.equals("pintai") && isPinTai!=null){
            initPinTaiData();
        }else {
            initMaiJiaData();
        }
    }


    public void initListeren(){
        iv_orderconfirmok_back.setOnClickListener(this);
        bt_orderconfirmok_lookorder.setOnClickListener(this);
    }

    public void initPinTaiData(){
        if(response==null)return;
        orderCofirmOKBean = new Gson().fromJson(response, OrderCofirmOKBean.class);
        tv_orderconfirmok_fkje.setText(orderCofirmOKBean.getFormated_order_amount());
        tv_orderconfirmok_num.setText(orderCofirmOKBean.getOrder_sn());
        dismissProgress(this);
    }

    private void initMaiJiaData() {

        if(response==null)return;
        orderCofirmOKBean = new Gson().fromJson(response, OrderCofirmOKBean.class);
        tv_orderconfirmok_fkje.setText(orderCofirmOKBean.getFormated_order_amount());
        tv_orderconfirmok_num.setText(orderCofirmOKBean.getOrder_sn());
        dismissProgress(this);
    }

    public void onClick(View view) {
        switch (view.getId()){

            case R.id.iv_orderconfirmok_back://退出
                finish();
                break;
            case R.id.bt_orderconfirmok_lookorder://查看订单
                    Intent intent = new Intent(this,OrderDetailActivity.class);
                    intent.putExtra("order_id",orderCofirmOKBean.getOrder_id());
                    startActivity(intent);
                    finish();

            default:
                break;
        }
    }
}
