package taiyang.com.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import taiyang.com.entity.SellChanpinmingchengBean;
import taiyang.com.tydp_b.R;

/**
 * Created by zhew on 8/22/17.
 * Copyright © 2006-2016 365jia.cn 安徽大尺度网络传媒有限公司版权所有
 **/

class ChanPinMingChengAdapter extends BaseAdapter{
    private LayoutInflater mInflater = null;
    private List<SellChanpinmingchengBean.GoodsNameListBean> goodsNameListBeen = new ArrayList<>();
    public void setFilter(String filter) {
        this.filter = filter;
        if(filter.isEmpty()){
            return;
        }
        goodsNameListBeen.clear();
        for (SellChanpinmingchengBean.GoodsNameListBean goodsNameListBean : list) {
            if(goodsNameListBean.getGoods_name().contains(filter)){
                goodsNameListBeen.add(goodsNameListBean);
            }
        }
    }

    private String filter = "";

    public List<SellChanpinmingchengBean.GoodsNameListBean> getList() {
        if(!filter.isEmpty()){
            return goodsNameListBeen;
        }
        return list;
    }

    private void setList(List<SellChanpinmingchengBean.GoodsNameListBean> list) {
        this.list = list;
    }

    List<SellChanpinmingchengBean.GoodsNameListBean> list = new ArrayList<>();
    public ChanPinMingChengAdapter(Context context, List<SellChanpinmingchengBean.GoodsNameListBean> selecterBeen) {
        this.mInflater = LayoutInflater.from(context);
        setList(selecterBeen);
    }

    @Override
    public int getCount() {

        return getList().size();
    }

    @Override
    public Object getItem(int i) {
        return getList().get(i);
    }

    @Override
    public long getItemId(int i) {
        return Long.parseLong(getList().get(i).getGoods_id());
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View item = mInflater.inflate(R.layout.list_pop_item, null);
        ((TextView)item.findViewById(R.id.tv_pop_item)).setText(getList().get(i).getGoods_name());
        ((TextView)item.findViewById(R.id.tv_pop_item)).setTag(getList().get(i).getGoods_id());
        return item;
    }
}
