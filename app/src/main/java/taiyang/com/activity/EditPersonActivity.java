package taiyang.com.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;

import org.kymjs.kjframe.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;

import butterknife.InjectView;
import taiyang.com.entity.GoosPerson;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.RegularUtils;
import taiyang.com.utils.T;
import taiyang.com.utils.ToastUtil;

/*
    编辑取货人
 */
public class EditPersonActivity extends BaseActivity implements View.OnClickListener {

    @InjectView(R.id.et_editperson_name)
    EditText et_editperson_name;
    @InjectView(R.id.et_editperson_number)
    EditText et_editperson_number;
    @InjectView(R.id.et_editperson_phone)
    EditText et_editperson_phone;
    @InjectView(R.id.bt_editperson_bc)
    Button bt_editperson_bc;
    @InjectView(R.id.bt_editperson_delete)
    Button bt_editperson_delete;
//    @InjectView(R.id.ll_editperson_back)
//    LinearLayout ll_editperson_back;
    private Editable editperson_name;
    private Editable editperson_number;
    private Editable editperson_phone;
    private GoosPerson goodsPerson;

    private String firstName;
    private String firstNumber;
    private String firstPhone;
    private String secondName;
    private String secondNumber;
    private String secondPhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_editperson);
//        ButterKnife.inject(this);
        mTitle.setText(getString(R.string.bianjidizhi));
        initListeren();
        initData();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_editperson;
    }

    private void initListeren() {
        bt_editperson_bc.setOnClickListener(this);
        tvGetCode.setOnClickListener(this);
        bt_editperson_delete.setOnClickListener(this);
    }

    private void initData() {
        goodsPerson= (GoosPerson) getIntent().getSerializableExtra("goodsPerson");
        if (goodsPerson!=null){
            firstName=goodsPerson.getName();
            firstNumber=goodsPerson.getId_number();
            firstPhone=goodsPerson.getMobile();
            et_editperson_name.setText(firstName);
            et_editperson_number.setText(firstNumber);
            et_editperson_phone.setText(firstPhone);
        }
        editperson_name = et_editperson_name.getText();
        editperson_number = et_editperson_number.getText();
        editperson_phone = et_editperson_phone.getText();
    }
    @InjectView(R.id.tv_get_code)
    TextView tvGetCode;
    @InjectView(R.id.et_addperson_code)
    EditText etAddpersonCode;
    @InjectView(R.id. et_phone_country)
    EditText  et_phone_country;
    private TimeCount time;

    private String phone;
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_get_code:
                phone = et_editperson_phone.getText().toString();
                if (RegularUtils.isMobileExact(phone)) {
                    Map<String, Object> params = new HashMap<>();
                    params.put("model", "user");
                    params.put("action", "send_mobile_code");
                    params.put("mobile", phone);
                    params.put("mobile_sign", MD5Utils.encode(MD5Utils.getMobileSign(phone)));
                    params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "send_mobile_code")));
                    L.e(JSON.toJSONString(params));
                    HttpUtils.sendPost(params, this, 102);
                    time = new TimeCount(60000, 1000);
                    time.start();
                } else {
                    T.showShort(this, "请输入正确的电话号码");
                }

                break;
            case R.id.bt_editperson_bc:
                secondName=et_editperson_name.getText().toString();
                secondNumber=et_editperson_number.getText().toString();
                secondPhone=et_editperson_phone.getText().toString();
                if (!RegularUtils.isMobileExact(secondPhone)){
                    T.showShort(this,"请输入正确的电话号码");
                    return;
                }
                if (!RegularUtils.isIDCard18(secondNumber)) {
                    T.showShort(this, "请输入正确的身份证号码");
                    return;
                }
                if (StringUtils.isEmpty(etAddpersonCode.getText().toString())) {
                    T.showShort(this, getString(R.string.qingshuruyanzhengma));
                    return;
                }
                if (secondName.equals(firstName)&&secondNumber.equals(firstNumber)&&secondPhone.equals(firstPhone)){
                    myApplication.setAddressChange(false);
                    finish();
                }else {
                    Map<String,Object> params=new HashMap<>();
                    params.put("model","user");
                    params.put("action","save_address");
                    params.put("sign", MD5Utils.encode(MD5Utils.getSign("user","save_address")));
                    params.put("user_id",mySPEdit.getUserId());
                    params.put("token",mySPEdit.getToken());
                    params.put("name",secondName);
                    params.put("id_number",secondNumber);
                    params.put("mobile_code", Integer.parseInt(etAddpersonCode.getText().toString()));
                    params.put("mobile",secondPhone);
                    params.put("id",goodsPerson.getId());
                    params.put("type",0);
//                    L.e("添加的:"+ JSON.toJSONString(params));
                    HttpUtils.sendPost(params,this,100);
                }
                break;

            case R.id.bt_editperson_delete:
                Map<String,Object> params=new HashMap<>();
                params.put("model","user");
                params.put("action","del_address");
                params.put("sign", MD5Utils.encode(MD5Utils.getSign("user","del_address")));
                params.put("user_id",mySPEdit.getUserId());
                params.put("token",mySPEdit.getToken());
                params.put("id",goodsPerson.getId());
//                    L.e("添加的:"+ JSON.toJSONString(params));
                HttpUtils.sendPost(params,this,101);
                break;

            default:
                break;
        }
    }
    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {// 计时完毕
            tvGetCode.setText(getString(R.string.huoquyanzhengma));
            tvGetCode.setBackground(getDrawable(R.drawable.shape_corner_15));
            tvGetCode.setClickable(true);
        }

        @Override
        public void onTick(long millisUntilFinished) {// 计时过程
            tvGetCode.setClickable(false);//防止重复点击
            tvGetCode.setBackground(getDrawable(R.drawable.shape_corner_15_gray));
            tvGetCode.setText(millisUntilFinished / 1000 + "s");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (time != null) {
            time.cancel();
        }
    }
    @Override
    public void success(String response, int id) {

        switch (id){
            case 100:
                T.showShort(this,"修改成功");
                myApplication.setAddressChange(true);
                finish();
                break;
            case 101:
                T.showShort(this,"删除成功");
                myApplication.setAddressChange(true);
                finish();
                break;
            case 102://获取验证码
                L.e("获取验证码成功");
                break;
        }
    }

    @Override
    public void failByOther(String fail, int id) {

        ToastUtil.showToast(fail);
    }

    @Override
    public void fail(String fail, int id) {

    }


}
