package taiyang.com.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.kymjs.kjframe.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;

import butterknife.InjectView;
import taiyang.com.entity.BankAccountModel;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.RegularUtils;
import taiyang.com.utils.T;

/*
    编辑取货人
 */
public class EditAccountActivity extends BaseActivity implements View.OnClickListener {

    @InjectView(R.id.et_editperson_name)
    EditText et_editperson_name;
    @InjectView(R.id.et_editperson_number)
    EditText et_editperson_number;
    @InjectView(R.id.et_editperson_phone)
    EditText et_editperson_phone;
    @InjectView(R.id.bt_editperson_bc)
    Button bt_editperson_bc;


    @InjectView(R.id.et_editperson_bank)
    EditText et_editperson_bank;
    private BankAccountModel.BankListBean bankListBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle.setText(R.string.bianjizhanghu);
        initListeren();
        initData();

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_edit_bank;
    }

    private void initListeren() {
        bt_editperson_bc.setOnClickListener(this);
    }

    private void initData() {
        bankListBean = (BankAccountModel.BankListBean) getIntent().getSerializableExtra("bankListBean");
        if (bankListBean !=null){
            et_editperson_name.setText(bankListBean.getUsername());
            et_editperson_number.setText(bankListBean.getAccount());
            et_editperson_phone.setText(bankListBean.getMobile());
            et_editperson_bank.setText(bankListBean.getDeposit_bank());
        }
    }


    @Override
    public void onClick(View view) {
        Map<String,Object> params=new HashMap<>();
        switch (view.getId()) {
            case R.id.bt_editperson_bc:
                String addperson_name = et_editperson_name.getText().toString();
                String addperson_number = et_editperson_number.getText().toString();
                String addperson_phone = et_editperson_phone.getText().toString();
                String  addperson_bank = et_editperson_bank.getText().toString();
                if (!RegularUtils.isMobileExact(addperson_phone)) {
                    T.showShort(this, "请输入正确的电话号码!");
                    return;
                }
                if (StringUtils.isEmpty(addperson_bank)||StringUtils.isEmpty(addperson_number)||StringUtils.isEmpty(addperson_name)) {
                    T.showShort(this, getString(R.string.qingjiangxinxitianxiewanzheng));
                    return;
                }
                params.put("model", "seller");
                params.put("action", "saveBank");
                params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller", "saveBank")));
                params.put("user_id", mySPEdit.getUserId());
                params.put("token", mySPEdit.getToken());
                params.put("username", addperson_name);
                params.put("account", addperson_number);
                params.put("mobile", addperson_phone);
                params.put("bank_id ", bankListBean.getId());
                params.put("deposit_bank", addperson_bank);
                HttpUtils.sendPost(params, this,100);
                break;


            default:
                break;
        }
    }

    @Override
    public void success(String response, int id) {

        switch (id){
            case 100:
                T.showShort(this,"修改成功");
                break;
            case 101:
                T.showShort(this,"删除成功");
                break;
            case 102:
                T.showShort(this,"设置成功");
                break;
        }
        myApplication.setAddressChange(true);
        finish();
    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }


}
