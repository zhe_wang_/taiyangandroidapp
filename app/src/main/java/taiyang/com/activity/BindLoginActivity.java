package taiyang.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.RegularUtils;
import taiyang.com.utils.T;

/**
 * 注册
 */
public class BindLoginActivity extends BaseActivity {

    @InjectView(R.id.et_code)
    EditText etCode;
    private String code;
    @InjectView(R.id.et_password)
    EditText etPassword;
    private String password;

    @InjectView(R.id.et_verfify)
    EditText etVerfify;

    @InjectView(R.id.et_phone_bg)
    View phoneBg;
    @InjectView(R.id.et_code_bg)
    View codeBg;
    @InjectView(R.id.et_password_bg)
    View passwordBg;
    @InjectView(R.id.et_verfify_bg)
    View verfifyBg;

    @InjectView(R.id. et_phone_country)
    EditText  et_phone_country;

    @InjectView(R.id.et_phone)
    EditText etPhone;
    private String phone;

    private TimeCount time;
    @InjectView(R.id.bt_code)
    Button codeButton;
    private String verifyPhone = "^((13[0-9])|(14[5,7])|(15[0-3,5-8])|(17[0,3,5-8])|(18[0-9])|(147))\\\\d{8}$";

    @OnClick(R.id.bt_code)
    public void getCode(View v) {
        phone = etPhone.getText().toString();
        if (RegularUtils.isMobileExact(phone)) {
//            if (phone!=null){
            Map<String, Object> params = new HashMap<>();
            params.put("model", "user");
            params.put("action", "send_mobile_code");
            params.put("mobile", phone);
            params.put("nation_code", et_phone_country.getText().toString());
            params.put("mobile_sign", MD5Utils.encode(MD5Utils.getMobileSign(phone)));
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "send_mobile_code")));
            L.e(JSON.toJSONString(params));
            HttpUtils.sendPost(params, this, 100);
            time = new TimeCount(60000, 1000);
            time.start();
            //{"sign":"fe7f9534fe12ae60f5f6c94f01193056","action":"send_mobile_code","model":"user","mobile_sign":"6bcabdd95d3fc4baa2d22f1114f98a5c","mobile":"18500093343"}
        } else {
            T.showShort(this, "请输入正确的电话号码");
//            ToastUtil.showToast("请输入正确的电话号码");
//            {"error":"503","message":"\u53d1\u9001\u9a8c\u8bc1\u5931\u8d25","content":""}
//            {"error":"503","message":"\u53d1\u9001\u9a8c\u8bc1\u5931\u8d25","content":""}
//            {"error":"0","message":"\u9a8c\u8bc1\u7801\u53d1\u9001\u6210\u529f","content":""}
        }

    }

    //注册
    @InjectView(R.id.bt_register)
    Button bt_register;

    @OnClick(R.id.bt_register)
    public void register(View v) {
        password = etPassword.getText().toString();
        code = etCode.getText().toString();
        if (!TextUtils.isEmpty(phone) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(code)) {
            showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
            Map<String, Object> params = new HashMap<>();
            params.put("model", "login");
            params.put("action", "binding");
            params.put("mobile_phone", phone);
            params.put("password", password);
            params.put("openid", getIntent().getStringExtra("openid"));
            params.put("unionid", getIntent().getStringExtra("unionid"));
            params.put("user_face", getIntent().getStringExtra("profile_image_url"));
            params.put("user_name", getIntent().getStringExtra("screen_name"));
            params.put("mobile_code", Integer.parseInt(code));
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("login", "binding")));
            HttpUtils.sendPost(params, this, 101);
        } else {
            L.e("请输入完整信息");
        }


//        L.e(JSON.toJSONString(params));


    }

//    private String url = "http://tydpb2b.bizsoho.com/app/api.php";
//    private MyApplication application = MyApplication.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_register);
//        ButterKnife.inject(this);
        mTitle.setText(getString(R.string.yonghuzhuce));
//        application.addActivity(this);
        initListener();
//        Log.e("ceshi", Thread.currentThread().getId() + "onCreate");
//        Log.e("ceshi",(Looper.myLooper() != Looper.getMainLooper())+"是主线程");

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_bind_login;
    }

    private void initListener() {
        etPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                changeBg(phoneBg, hasFocus);
            }
        });
        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                changeBg(passwordBg, hasFocus);
            }
        });
        etCode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                changeBg(codeBg, hasFocus);
            }
        });
        etVerfify.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                changeBg(verfifyBg, hasFocus);
            }
        });
    }

    private void changeBg(View v, boolean hasFocus) {
        if (hasFocus) {
            v.setBackgroundColor(getResources().getColor(R.color.bg_bluecolor));
        } else {
            v.setBackgroundColor(getResources().getColor(R.color.bg_color));
        }
    }

    @Override
    public void success(String response, int id) {
        L.e("111----" + id);
//            application.finishAll();
        switch (id) {
            case 100://获取验证码
                break;
            case 101://注册
//                ToastUtil.showToast("注册成功");
                dismissProgress(this);
                T.showShort(this, getString(R.string.bangdingchenggong));
                Intent mIntent = new Intent();
                mIntent.putExtra("response", response);
                setResult(1001,mIntent);
                finish();
                break;
        }
    }

    @Override
    public void failByOther(String fail, int id) {
        L.e(fail);
        if (id == 101) {
            dismissProgress(this);
            T.showShort(this, fail);
        }
    }

    @Override
    public void fail(String fail, int id) {
        if (id == 101) {
            dismissProgress(this);
            T.showShort(this, R.string.jianchawangluo);
        }
    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {// 计时完毕
            codeButton.setText(getString(R.string.huoquyanzhengma));
            codeButton.setClickable(true);
        }

        @Override
        public void onTick(long millisUntilFinished) {// 计时过程
            codeButton.setClickable(false);//防止重复点击
            codeButton.setText(millisUntilFinished / 1000 + "s");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (time != null) {
            time.cancel();
        }
    }
}
