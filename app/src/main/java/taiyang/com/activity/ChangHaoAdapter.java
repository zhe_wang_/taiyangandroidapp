package taiyang.com.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import taiyang.com.entity.SellChanghaoBean;
import taiyang.com.tydp_b.R;

/**
 * Created by zhew on 8/22/17.
 * Copyright © 2006-2016 365jia.cn 安徽大尺度网络传媒有限公司版权所有
 **/

class ChangHaoAdapter extends BaseAdapter{
    private LayoutInflater mInflater = null;
    private List<SellChanghaoBean.BrandListBean> brandList = new ArrayList<>();
    public void setFilter(String filter) {
        this.filter = filter;
        if(filter.isEmpty()){
            return;
        }
        brandList.clear();
        for (SellChanghaoBean.BrandListBean brandListBean : list) {
            if(brandListBean.getBrand_name().contains(filter)){
                brandList.add(brandListBean);
            }
        }
    }

    private String filter = "";

    public List<SellChanghaoBean.BrandListBean> getList() {
        if(!filter.isEmpty()){
            return brandList;
        }
        return list;
    }

    private void setList(List<SellChanghaoBean.BrandListBean> list) {
        this.list = list;
    }

     List<SellChanghaoBean.BrandListBean> list = new ArrayList<>();
    public ChangHaoAdapter(Context context, List<SellChanghaoBean.BrandListBean> selecterBeen) {
        this.mInflater = LayoutInflater.from(context);
        setList(selecterBeen);
    }

    @Override
    public int getCount() {

        return getList().size();
    }

    @Override
    public Object getItem(int i) {
        return getList().get(i);
    }

    @Override
    public long getItemId(int i) {
        return Long.parseLong(getList().get(i).getBrand_id());
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View item = mInflater.inflate(R.layout.list_pop_item, null);
        ((TextView)item.findViewById(R.id.tv_pop_item)).setText(getList().get(i).getBrand_name());
        ((TextView)item.findViewById(R.id.tv_pop_item)).setTag(getList().get(i).getBrand_id());

        return item;
    }
}
