package taiyang.com.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.RegularUtils;
import taiyang.com.utils.T;

/**
 * 注册
 */
public class RegisterActivity extends BaseActivity {

    @InjectView(R.id.et_code)
    EditText etCode;
    private String code;
    @InjectView(R.id.et_password)
    EditText etPassword;
    private String password;

    @InjectView(R.id.et_verfify)
    EditText etVerfify;

    @InjectView(R.id.et_phone_bg)
    View phoneBg;
    @InjectView(R.id.et_code_bg)
    View codeBg;
    @InjectView(R.id.et_password_bg)
    View passwordBg;
    @InjectView(R.id.et_verfify_bg)
    View verfifyBg;


    @InjectView(R.id.et_phone)
    EditText etPhone;

    @InjectView(R.id. et_phone_country)
    EditText  et_phone_country;

    private TimeCount time;
    @InjectView(R.id.bt_code)
    Button codeButton;

    @OnClick(R.id.bt_code)
    public void getCode(View v) {
        if (RegularUtils.isMobileExact(etPhone.getText().toString())) {
            Map<String, Object> params = new HashMap<>();
            params.put("model", "user");
            params.put("action", "send_mobile_code");
            params.put("nation_code", et_phone_country.getText().toString());
            params.put("mobile", etPhone.getText().toString());
            params.put("mobile_sign", MD5Utils.encode(MD5Utils.getMobileSign(etPhone.getText().toString())));
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "send_mobile_code")));
            L.e(JSON.toJSONString(params));
            HttpUtils.sendPost(params, this, 100);
            time = new TimeCount(60000, 1000);
            time.start();
        } else {
            T.showShort(this, "请输入正确的电话号码");
        }

    }

    //登录
    @InjectView(R.id.bt_login)
    Button bt_login;

    @OnClick(R.id.bt_login)
    public void login(View v) {
        finish();
    }

    //注册
    @InjectView(R.id.bt_register)
    Button bt_register;

    @OnClick(R.id.bt_register)
    public void register(View v) {
        password = etPassword.getText().toString();
        code = etCode.getText().toString();
        if (!TextUtils.isEmpty(etPhone.getText().toString()) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(code)) {
            showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
            Map<String, Object> params = new HashMap<>();
            params.put("model", "user");
            params.put("action", "register");
            params.put("mobile_phone", etPhone.getText().toString());
            params.put("password", password);
            params.put("code", Integer.parseInt(code));
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "register")));
            HttpUtils.sendPost(params, this, 101);
        } else {
        }


//        L.e(JSON.toJSONString(params));


    }

//    private String url = "http://tydpb2b.bizsoho.com/app/api.php";
//    private MyApplication application = MyApplication.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_register);
//        ButterKnife.inject(this);
        mTitle.setText(getString(R.string.yonghuzhuce));
//        application.addActivity(this);
        initListener();
//        Log.e("ceshi", Thread.currentThread().getId() + "onCreate");
//        Log.e("ceshi",(Looper.myLooper() != Looper.getMainLooper())+"是主线程");

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_register;
    }

    private void initListener() {
        etPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                changeBg(phoneBg, hasFocus);
            }
        });
        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                changeBg(passwordBg, hasFocus);
            }
        });
        etCode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                changeBg(codeBg, hasFocus);
            }
        });
        etVerfify.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                changeBg(verfifyBg, hasFocus);
            }
        });
    }

    private void changeBg(View v, boolean hasFocus) {
        if (hasFocus) {
            v.setBackgroundColor(getResources().getColor(R.color.bg_bluecolor));
        } else {
            v.setBackgroundColor(getResources().getColor(R.color.bg_color));
        }
    }

    @Override
    public void success(String response, int id) {
        switch (id) {
            case 100://获取验证码
                break;
            case 101://注册
                dismissProgress(this);
                T.showShort(this,getString(R.string.caozuochenggong));
                finish();
                break;
        }
    }

    @Override
    public void failByOther(String fail, int id) {
            dismissProgress(this);
            T.showShort(this, fail);

    }

    @Override
    public void fail(String fail, int id) {
        if (id == 101) {
            dismissProgress(this);
            T.showShort(this, R.string.jianchawangluo);
        }

    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {// 计时完毕
            codeButton.setText(getString(R.string.huoquyanzhengma));
            codeButton.setClickable(true);
        }

        @Override
        public void onTick(long millisUntilFinished) {// 计时过程
            codeButton.setClickable(false);//防止重复点击
            codeButton.setText(millisUntilFinished / 1000 + "s");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (time != null) {
            time.cancel();
        }
    }
}
