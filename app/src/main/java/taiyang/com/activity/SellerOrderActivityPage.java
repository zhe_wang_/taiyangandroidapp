package taiyang.com.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import taiyang.com.fragment.SellerOrderFragment;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.T;

/**
 * 全部订单界面
 */
public class SellerOrderActivityPage extends BaseActivity {

    @InjectView(R.id.tabs)
    TabLayout tabLayout;
    @InjectView(R.id.vp_view)
    ViewPager mViewPager;

    private List<String> mTitleList;
    private List<Fragment> mFragmentList;
    private ViewAdapter mAdapter;
    private SellerOrderFragment mOrderFragment;
    private String tab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle.setText(getString(R.string.dingdanhao));
        tab = getIntent().getStringExtra("tab");

        initView();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_order;
    }

    private void initView() {
        //初始化顶部标题
        initTitle();
    }

    private void initTitle() {
        mTitleList = new ArrayList<>();
        mTitleList.add(getString(R.string.quanbu));
        mTitleList.add(getString(R.string.weishoukuan));
        mTitleList.add(getString(R.string.yishoukuan));
        mTitleList.add(getString(R.string.yiyufu));
        mTitleList.add(getString(R.string.yiwancheng));
        mFragmentList = new ArrayList<>();
        for (int i = 0; i < mTitleList.size(); i++) {
            Bundle data = new Bundle();
            data.putString("tabs", mTitleList.get(i));
            mOrderFragment = new SellerOrderFragment();
            mOrderFragment.setArguments(data);
            mFragmentList.add(mOrderFragment);
        }
        mAdapter = new ViewAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(mViewPager);//设置viewPaper和tabs关联
        if (tab != null) {
            if (tab.equals(getString(R.string.weishoukuan))) {
                mViewPager.setCurrentItem(1);
            } else if (tab.equals(getString(R.string.yishoukuan))) {
                mViewPager.setCurrentItem(2);
            }else if (tab.equals(getString(R.string.yiyufu))) {
                mViewPager.setCurrentItem(3);
            }  else if (tab.equals(getString(R.string.yiwancheng))) {
                mViewPager.setCurrentItem(4);
            } else {
                mViewPager.setCurrentItem(0);
            }
        }
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {
        T.showShort(this,R.string.jianchawangluo);
    }

    class ViewAdapter extends FragmentPagerAdapter {

        public ViewAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitleList.get(position);
        }
    }
}
