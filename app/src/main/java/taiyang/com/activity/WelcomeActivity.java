package taiyang.com.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import taiyang.com.tydp_b.MainActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.DensityUtil;
import taiyang.com.utils.MySPEdit;

public class WelcomeActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, View.OnClickListener {

    private Context context;
    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;
    private Button startButton;
    private ArrayList<View> views;
    private int[] images;
    private MySPEdit mySPEdit=MySPEdit.getInstance(this);
    private ImageView[] indicators = null;
    private LinearLayout indicatorLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        context=this;
//        images = new int[]{R.mipmap.welcome_01, R.mipmap.welcome_02,
//                R.mipmap.welcome_03, R.mipmap.welcome_04};
        initView();
    }


    // 初始化视图
    private void initView() {
        // 实例化视图控件
        viewPager = (ViewPager) findViewById(R.id.viewpage);
        startButton = (Button) findViewById(R.id.start_Button);
        startButton.setOnClickListener(this);
        views = new ArrayList<View>();
        indicatorLayout = (LinearLayout) findViewById(R.id.indicator);
        indicators = new ImageView[images.length]; // 定义指示器数组大小
        int paramsValue=DensityUtil.dip2px(this,8);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(paramsValue,paramsValue);

        for (int i = 0; i < images.length; i++) {

            // 循环加入图片
            ImageView imageView = new ImageView(context);
            imageView.setBackgroundResource(images[i]);
            views.add(imageView);
            // 循环加入指示器
            indicators[i] = new ImageView(context);
            indicators[i].setBackgroundResource(R.drawable.indicators_default);
            if (i == 0) {
                indicators[i].setBackgroundResource(R.drawable.indicators_now);
            }else {
                params.leftMargin=paramsValue;
            }
            indicators[i].setLayoutParams(params);
            indicatorLayout.addView(indicators[i]);
        }
        pagerAdapter = new BasePagerAdapter(views);
        viewPager.setAdapter(pagerAdapter); // 设置适配器
        viewPager.setOnPageChangeListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.start_Button) {
            mySPEdit.setIsFirst(false);
            startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
//            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            this.finish();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        // 显示最后一个图片时显示按钮
        if (position == views.size() - 1) {
            startButton.setVisibility(View.VISIBLE);
        } else {
            startButton.setVisibility(View.INVISIBLE);
        }
        // 更改指示器图片
        for (int i = 0; i < indicators.length; i++) {
            indicators[position].setBackgroundResource(R.drawable.indicators_now);
            if (position != i) {
                indicators[i]
                        .setBackgroundResource(R.drawable.indicators_default);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    //引导页使用的pageview适配器
    public class BasePagerAdapter extends PagerAdapter {
        private List<View> views = new ArrayList<View>();

        public BasePagerAdapter(List<View> views) {
            this.views = views;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public int getCount() {
            return views.size();
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView(views.get(position));
        }

        @Override
        public Object instantiateItem(View container, int position) {
            ((ViewPager) container).addView(views.get(position));
            return views.get(position);
        }
    }
}
