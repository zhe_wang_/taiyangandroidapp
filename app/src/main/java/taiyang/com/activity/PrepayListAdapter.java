package taiyang.com.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import taiyang.com.entity.SellIndexBean;
import taiyang.com.tydp_b.R;

/**
 * Created by zhew on 8/22/17.
 * Copyright © 2006-2016 365jia.cn 安徽大尺度网络传媒有限公司版权所有
 **/

class PrepayListAdapter extends BaseAdapter{
    private LayoutInflater mInflater = null;

    public List<SellIndexBean.PrepayBean> getList() {
        return list;
    }

    private void setList(List<SellIndexBean.PrepayBean> list) {
        this.list = list;
    }

     List<SellIndexBean.PrepayBean> list = new ArrayList<>();
    public PrepayListAdapter(Context context, List<SellIndexBean.PrepayBean> selecterBeen) {
        this.mInflater = LayoutInflater.from(context);
        setList(selecterBeen);
    }

    @Override
    public int getCount() {

        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return Long.parseLong(list.get(i).getId());
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View item = mInflater.inflate(R.layout.list_pop_item, null);
        ((TextView)item.findViewById(R.id.tv_pop_item)).setText(list.get(i).getVal());
        ((TextView)item.findViewById(R.id.tv_pop_item)).setTag(list.get(i).getId());
        return item;
    }
}
