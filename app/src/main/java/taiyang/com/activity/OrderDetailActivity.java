package taiyang.com.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumFile;
import com.yanzhenjie.album.AlbumListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.entity.OrderDetailBean;
import taiyang.com.entity.OrderDetailNullSellerBean;
import taiyang.com.tydp_b.KProgressActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.ToastUtil;

/**
 * 订单详情 - 平台
 */
public class OrderDetailActivity extends KProgressActivity implements View.OnClickListener {


    private static String path = "/sdcard/detail/";// sd路径
    @InjectView(R.id.bt_orderdetail_scfkpz)
    Button bt_orderdetail_scfkpz;
    @InjectView(R.id.bt_orderdetail_lipei)
    Button bt_orderdetail_lipei;
    @InjectView(R.id.bt_orderdetail_address)
    Button bt_orderdetail_address;
    @InjectView(R.id.bt_orderdetail_chang_payment)
    Button bt_orderdetail_chang_payment;
    @InjectView(R.id.iv_orderdetail_logo)
    SimpleDraweeView iv_orderdetail_logo;
    @InjectView(R.id.tv_orderdetail_ffstats)
    TextView tv_orderdetail_ffstats;
    @InjectView(R.id.tv_ordername)
    TextView tv_ordername;
    @InjectView(R.id.tv_orderprice)
    TextView tv_orderprice;

    @InjectView(R.id.tv_orderdetail_unit3)
    TextView tv_orderdetail_unit3;
    @InjectView(R.id.tv_orderdetail_num)
    TextView tv_orderdetail_num;
    @InjectView(R.id.tv_orderdetail_status)
    TextView tv_orderdetail_status;
    @InjectView(R.id.tv_orderdetail_ff)
    TextView tv_orderdetail_ff;
    @InjectView(R.id.tv_price)
    TextView tv_price;
    @InjectView(R.id.tv_realprice)
    TextView tv_realprice;
    @InjectView(R.id.rl_mj_name)
    View rl_mj_name;
    @InjectView(R.id.rl_mj_phone)
    View rl_mj_phone;
    @InjectView(R.id.tv_mjorderdetail_name)
    TextView tv_mjorderdetail_name;
    @InjectView(R.id.tv_mjorderdetail_phone)
    TextView tv_mjorderdetail_phone;
    @InjectView(R.id.tv_paid_price)
    TextView tv_paid_price;
    @InjectView(R.id.ll_orderdetail_personinfo)
    LinearLayout ll_orderdetail_personinfo;

    @InjectView(R.id.ll_orderdetail_personinfo_seller)
    LinearLayout ll_orderdetail_personinfo_seller;
    @InjectView(R.id.tv_personinfo_name)
    TextView tv_personinfo_name;
    @InjectView(R.id.tv_personinfo_num)
    TextView tv_personinfo_num;
    @InjectView(R.id.tv_personinfo_phone)
    TextView tv_personinfo_phone;


    @InjectView(R.id.tv_personinfo_name_seller)
    TextView tv_personinfo_name_seller;
    @InjectView(R.id.tv_personinfo_num_seller)
    TextView tv_personinfo_num_seller;
    @InjectView(R.id.tv_personinfo_phone_seller)
    TextView tv_personinfo_phone_seller;
    @InjectView(R.id.tv_orderdetail_stock_status)
    TextView tv_orderdetail_stock_status;
    private String get_address;
    private String fileName;
    private Bitmap head;
    private File temp;
    private String order_id;
    private OrderDetailBean orderDetailBean;
    private OrderDetailNullSellerBean mSellerBean;
    private MySPEdit mySPEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.inject(this);
        mySPEdit = MySPEdit.getInstance(this);
        initListeren();
        initData();

    }

    @OnClick(R.id.ll_orderdetail_back)
    public void close() {
        this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (data==null) return;
                if (!data.hasExtra("address_id")) return;
                if (data.getStringExtra("address_id") == null) return;
                String address_id = data.getStringExtra("address_id");
                Map<String, Object> params = new HashMap<>();
                params.put("model", "order");
                params.put("action", "save_order_address");
                params.put("order_id", order_id);
                params.put("address_id", address_id);
                params.put("user_id", MySPEdit.getInstance(this).getUserId());
                params.put("token", MySPEdit.getInstance(this).getToken());
                params.put("sign", MD5Utils.encode(MD5Utils.getSign("order", "save_order_address")));
                HttpUtils.sendPost(params, new HttpRequestListener() {
                    @Override
                    public void success(String response, int id) {
                        ToastUtil.showToast(response);
                        restartPage();
                    }

                    @Override
                    public void failByOther(String fail, int id) {
                        L.e("failByOther" + fail);
                    }

                    @Override
                    public void fail(String fail, int id) {
                        L.e("fail" + fail);
                    }
                });

                break;
            case 11:
                restartPage();
                break;
            default:
                break;
        }
    }

    private void restartPage() {
        finish();
        Intent intent = new Intent(this, OrderDetailActivity.class);
        intent.putExtra("order_id", order_id);
        startActivity(intent);
    }

    //上传 支付凭证图片
    private void sendBitMapData() {
        showProgress(this,getString(R.string.jiazaizhong_dotdotdot));
        Map<String, Object> params = new HashMap<>();
        params.put("model", "order");
        params.put("action", "upPayImg");
        params.put("order_id", order_id);
        params.put("user_id", mySPEdit.getUserId());
        params.put("token", mySPEdit.getToken());
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("order", "upPayImg")));
        HttpUtils.sendPost(params, "pay_img", fileName, new HttpRequestListener() {
            @Override
            public void success(String response, int id) {
                ToastUtil.showToast("上传付款凭证完成");
                restartPage();
            }

            @Override
            public void failByOther(String fail, int id) {

            }

            @Override
            public void fail(String fail, int id) {

            }
        }, true, 101);

    }

    private void initListeren() {
        bt_orderdetail_scfkpz.setOnClickListener(this);
        bt_orderdetail_lipei.setOnClickListener(this);
        bt_orderdetail_address.setOnClickListener(this);
        bt_orderdetail_chang_payment.setOnClickListener(this);
    }

    private void initData() {
        showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
        order_id = getIntent().getStringExtra("order_id");
        Map<String, Object> params = new HashMap<>();
        params.put("model", "order");
        params.put("action", "order_info");
        params.put("order_id", order_id);
        params.put("user_id", mySPEdit.getUserId());
        params.put("token", mySPEdit.getToken());
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("order", "order_info")));
        HttpUtils.sendPost(params, new HttpRequestListener() {
            @Override
            public void success(String response, int id) {
                mSellerBean = new Gson().fromJson(response, OrderDetailNullSellerBean.class);
                if (mSellerBean == null) return;
                setInfo();
                dismissProgress(OrderDetailActivity.this);
            }

            @Override
            public void failByOther(String fail, int id) {
                L.e("failByOther    " + fail);
                dismissProgress(OrderDetailActivity.this);
            }

            @Override
            public void fail(String fail, int id) {
                L.e("fail    " + fail);
                dismissProgress(OrderDetailActivity.this);
            }
        });

    }

    // 初始化数据  get_address字段无值   未添加取货人信息
    private void setInfo() {

        bt_orderdetail_scfkpz.setVisibility(View.GONE);
        bt_orderdetail_lipei.setVisibility(View.GONE);
        bt_orderdetail_address.setVisibility(View.GONE);
        bt_orderdetail_chang_payment.setVisibility(View.GONE);
        //支付确认中
        if (mSellerBean.getPay_check().equals("1")) {
            tv_orderdetail_ffstats.setVisibility(View.VISIBLE);
        } else {
            tv_orderdetail_ffstats.setVisibility(View.GONE);
        }
        //确认有货，可以付款，并且
        if (mSellerBean.getStock_status().equals("1")&&mSellerBean.getOrder_status().equals("0") ) {
            bt_orderdetail_chang_payment.setVisibility(View.VISIBLE);
        }
        if(mSellerBean.getApply_pay().equals("1")){
            bt_orderdetail_scfkpz.setVisibility(View.VISIBLE);
        }
        //显示银行信息
        if (mSellerBean.getStock_status().equals("1") && mSellerBean.getPay_id().equals("2")) {
            showBankInfo();
        } else {
            hideBankInfo(View.GONE);
        }
        if (mSellerBean.getOrder_status().equals("99")) {
            bt_orderdetail_chang_payment.setVisibility(View.GONE);
            bt_orderdetail_lipei.setVisibility(View.VISIBLE);
        }

        if (mSellerBean.getBuyer_seller_edit().equals("1") && mSellerBean.getStock_status().equals("1")&&mSellerBean.getGet_address().getName().equals("")) {
            bt_orderdetail_address.setVisibility(View.VISIBLE);
        }
        if(!mSellerBean.getGet_address().getName().equals("")){
            ll_orderdetail_personinfo.setVisibility(View.VISIBLE);
            tv_personinfo_name.setText(mSellerBean.getGet_address().getName());
            tv_personinfo_num.setText(mSellerBean.getGet_address().getId_number());
            tv_personinfo_phone.setText(mSellerBean.getGet_address().getMobile());
        }else{
            ll_orderdetail_personinfo.setVisibility(View.GONE);
        }

        if(!mSellerBean.getSend_address().getName().equals("")){
            ll_orderdetail_personinfo_seller.setVisibility(View.VISIBLE);
            tv_personinfo_name_seller.setText(mSellerBean.getSend_address().getName());
            tv_personinfo_num_seller.setText(mSellerBean.getSend_address().getId_number());
            tv_personinfo_phone_seller.setText(mSellerBean.getSend_address().getMobile());
        }else{
            ll_orderdetail_personinfo_seller.setVisibility(View.GONE);
        }
        //订单确认中 底部按钮隐藏
        if (mSellerBean.getPay_check().equals("1") || mSellerBean.getCan_pay() == 0||!mSellerBean.getStock_status().equals("1")) {
            hideAll();
        }
        iv_orderdetail_logo.setImageURI(Uri.parse(mSellerBean.getGoods_info().getGoods_thumb()));
        tv_ordername.setText(mSellerBean.getGoods_info().getGoods_name()+"   "+mSellerBean.getGoods_info().getBrand_sn());
        tv_orderprice.setText(mSellerBean.getGoods_info().getGoods_price());
        tv_orderdetail_num.setText(mSellerBean.getOrder_sn());
        tv_orderdetail_status.setText(mSellerBean.getOrder_status_name());
        tv_orderdetail_ff.setText(mSellerBean.getPay_name());


        tv_orderdetail_unit3.setText("   "+
                (mSellerBean.getGoods_info().getIs_retail().equals("1")?mSellerBean.getGoods_info().getPart_number():mSellerBean.getGoods_info().getPart_weight())+""+
                mSellerBean.getGoods_info().getPart_unit()+"    "+
                mSellerBean.getGoods_info().getGoods_number()+mSellerBean.getGoods_info().getMeasure_unit());


//        if(mSellerBean.getGoods_info().getIs_retail().equals("1")) {
//            tv_orderdetail_unit2.setText("   "+mSellerBean.getGoods_info().getPart_number()+"/"+mSellerBean.getGoods_info().getSpec_1_unit()+"    "+
//                    mSellerBean.getGoods_info().getGoods_number()+mSellerBean.getGoods_info().getMeasure_unit());
//        }else{
//            tv_orderdetail_unit2.setText("   "+mSellerBean.getGoods_info().getPart_weight()+"/"+mSellerBean.getGoods_info().getSpec_1_unit()+"    "+
//                    mSellerBean.getGoods_info().getGoods_number()+mSellerBean.getGoods_info().getMeasure_unit());
//        }

        tv_price.setText(mSellerBean.getGoods_amount());
        tv_realprice.setText(mSellerBean.getOrder_amount());
        if("".equals(mSellerBean.getMoney_paid())){
            findViewById(R.id.rl_moeny_pard).setVisibility(View.GONE);
        }
        tv_paid_price.setText(mSellerBean.getMoney_paid());
        if (mSellerBean.getStock_status().equals("0")) {
            tv_orderdetail_stock_status.setText(R.string.querenzhong);
            showUserInfo(View.GONE);
        }
        if (mSellerBean.getStock_status().equals("1")) {
            tv_orderdetail_stock_status.setText(R.string.querenyouhuo);
            showUserInfo(View.VISIBLE);
        }
        if (mSellerBean.getStock_status().equals("2")) {
            showUserInfo(View.VISIBLE);
            tv_orderdetail_stock_status.setText(R.string.querenwuhuo);
        }

    }

    private void showUserInfo(int gone) {
        rl_mj_name.setVisibility(gone);
        rl_mj_phone.setVisibility(gone);
        tv_mjorderdetail_name.setText(mSellerBean.getUser_info().getName());
        tv_mjorderdetail_phone.setText(mSellerBean.getUser_info().getMobile());
    }

    private void hideAll() {
        bt_orderdetail_chang_payment.setVisibility(View.GONE);
        bt_orderdetail_scfkpz.setVisibility(View.GONE);
        bt_orderdetail_lipei.setVisibility(View.GONE);
        bt_orderdetail_address.setVisibility(View.GONE);
    }

    private void hideBankInfo(int gone) {
        findViewById(R.id.ll_bank_info).setVisibility(gone);
    }

    private void showBankInfo() {
        try {
            hideBankInfo(View.VISIBLE);
            ((TextView) findViewById(R.id.tv_pay_name)).setText(mSellerBean.getBank().getUsername());
            ((TextView) findViewById(R.id.tv_card_number)).setText(mSellerBean.getBank().getAccount());
            ((TextView) findViewById(R.id.tv_bank_name)).setText(mSellerBean.getBank().getDeposit_bank());
        }catch (Exception e){

        }
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.bt_orderdetail_scfkpz:
                //上传凭证
                openPictureSelectDialog();
                break;
            case R.id.bt_orderdetail_chang_payment:
                Intent i = new Intent(this, ChangePaymenyActivity.class);
                i.putExtra("order_id", order_id);
                i.putExtra("alipay", mSellerBean.getPay_online());
                startActivityForResult(i, 11);
                break;
            case R.id.bt_orderdetail_address:
                Intent i2 = new Intent(this, PersonInfoActivity.class);
                startActivityForResult(i2, 10);
                break;
            case R.id.bt_orderdetail_lipei://理赔
                Intent a = new Intent(this, CommitMessageActivity.class);
                a.putExtra("Order_sn", mSellerBean.getOrder_sn());
                startActivity(a);
                break;
            case R.id.ll_orderdetail_back:
                finish();
                break;
            default:
                break;
        }
    }

    /**
     * 打开对话框
     **/

    private void openPictureSelectDialog() {
        Album.image(this) // 选择图片。
                .multipleChoice()
                .requestCode(200)
                .camera(true)
                .columnCount(2)
                .selectCount(1)
//                .checkedList(mAlbumFiles)
                .listener(new AlbumListener<ArrayList<AlbumFile>>() {
                    @Override
                    public void onAlbumResult(int requestCode, @NonNull ArrayList<AlbumFile> result) {
                        fileName = result.get(0).getPath();
                        sendBitMapData();
                    }

                    @Override
                    public void onAlbumCancel(int requestCode) {
                    }
                })
                .start();

    }



}
