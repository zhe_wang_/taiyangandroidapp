package taiyang.com.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.tydp_b.KProgressActivity;
import taiyang.com.tydp_b.R;

/**
 * 专属客服界面
 */
public class ServiceActivity extends KProgressActivity {

    @InjectView(R.id.im_call)
    ImageView call;
    @InjectView(R.id.tv_personnick)
    TextView tv_personnick;
    @InjectView(R.id.im_personlogo)
    SimpleDraweeView im_personlogo;

    @OnClick(R.id.im_call)
    public void call(View v) {
        String phoneNumber = phone.getText().toString();
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intent);
    }
    @InjectView(R.id.back_layout)
    LinearLayout back;
    @OnClick(R.id.back_layout)
    public void back(View v){
        finish();
    }

    @InjectView(R.id.news_layout)
    LinearLayout news;

    @InjectView(R.id.tv_phone_bg)
    TextView phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        ButterKnife.inject(this);

        initData();
    }

    private void initData() {
        String user_name = getIntent().getStringExtra("user_name");
        String user_face = getIntent().getStringExtra("user_face");
        if(user_name!=null)tv_personnick.setText(user_name);
        if(user_face!=null)im_personlogo.setImageURI(Uri.parse(user_face));
    }
}
