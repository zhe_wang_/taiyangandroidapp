package taiyang.com.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.tydp_b.KProgressActivity;
import taiyang.com.tydp_b.R;

/*
  关于我们界面
 */
public class AboutActivity extends KProgressActivity {

    @InjectView(R.id.back_layout)
    LinearLayout backLayout;

    @OnClick(R.id.back_layout)
    public void back(View v) {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.inject(this);
//        RadioGroup radioGroup=new RadioGroup(this);
//        radioGroup.setOrientation(LinearLayout.HORIZONTAL);
//        RadioButton
    }
}
