package taiyang.com.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.adapter.CommentListAdapter;
import taiyang.com.entity.CommentListBean;
import taiyang.com.entity.QiugouInfoBean;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.ToastUtil;

/**
 * Created by zhew on 9/9/17.
 * Copyright © 2006-2016 365jia.cn 安徽大尺度网络传媒有限公司版权所有
 **/

public class QiugouInfoActivity extends BaseActivity {

    @InjectView(R.id.lv_commentlist)
    ListView lvCommentlist;

    private CommentListAdapter mAdapter;
    private QiugouInfoBean qiuGou;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new CommentListAdapter(new ArrayList<CommentListBean.CommentBean>(),this);
        initDate();
    }
    @OnClick(R.id.btn_comment)
    public void comment(){
        if(MySPEdit.getInstance(this).getIsLogin()) {
            Intent i = new Intent(this, CommentActivity.class);
            i.putExtra("p_id", getIntent().getStringExtra("id"));
            startActivityForResult(i, 1001);
        }else{
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }



    @OnClick(R.id.btn_call)
    public void call(){
                if(MySPEdit.getInstance(QiugouInfoActivity.this).getIsLogin()) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + qiuGou.getUser_phone()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }else{
                    ToastUtil.showToast("登录后可联系对方");
                }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 1001 && requestCode ==1001){
            getComments("p_id", "getCommentList", MD5Utils.encode(MD5Utils.getSign("purchase", "getCommentList")), 101);
        }
    }

    private void initDate() {
        getComments("id", "getPurchaseInfo", MD5Utils.encode(MD5Utils.getSign("purchase", "getPurchaseInfo")), 100);
    }

    private void getComments(String p_id, String getCommentList, String encode, int id2) {
        Map<String, Object> params1 = new HashMap<>();
        params1.put("model", "purchase");
        params1.put(p_id, getIntent().getStringExtra("id"));
        params1.put("action", getCommentList);
        params1.put("sign", encode);
        HttpUtils.sendPost(params1, this, id2);
    }

    @Override
    public void success(String response, int id) {
        if (id == 100) {
            View v = LayoutInflater.from(this).inflate(R.layout.view_qiugou_top, null, false);
            qiuGou = new Gson().fromJson(response, QiugouInfoBean.class);
            ((SimpleDraweeView) v.findViewById(R.id.riv_qiugou1)).setImageURI(Uri.parse(qiuGou.getUser_face()));
            ((TextView) v.findViewById(R.id.tv_qiugou1_name)).setText(qiuGou.getUser_name().replaceAll("(\\d{3})\\d{4}(\\d{4})","$1****$2"));
            ((TextView) v.findViewById(R.id.tv_qiugou1_localtion)).setText(qiuGou.getAddress());
            SpannableStringBuilder style2 = new SpannableStringBuilder(qiuGou.getGoods_name() + "    " +
                    qiuGou.getGoods_num() + "吨   " + qiuGou.getPrice_low() + "-" + qiuGou.getPrice_up() + "元");
            ((TextView) v.findViewById(R.id.tv_qiugou1_info)).setText(style2);
            ((TextView) v.findViewById(R.id.tv_qiugou1_text)).setText(qiuGou.getMemo());
            ((TextView) v.findViewById(R.id.tv_qiugou1_time)).setText(qiuGou.getCreated_at());
            ((TextView) v.findViewById(R.id.tv_qiugou1_text)).setVisibility(qiuGou.getMemo().isEmpty() ? View.GONE : View.VISIBLE);
            lvCommentlist.addHeaderView(v);
            getComments("p_id", "getCommentList", MD5Utils.encode(MD5Utils.getSign("purchase", "getCommentList")), 101);

        }
        if(id == 101){
            lvCommentlist.setAdapter(mAdapter);
            mAdapter.getmList().clear();
            CommentListBean commentList = new Gson().fromJson(response, CommentListBean.class);
            mAdapter.setmList(commentList.getComment_list());
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_qiugou;
    }
}
