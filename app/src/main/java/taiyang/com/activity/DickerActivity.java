package taiyang.com.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.alibaba.fastjson.JSON;
import com.cundong.recyclerview.CustRecyclerView;
import com.cundong.recyclerview.HeaderAndFooterRecyclerViewAdapter;
import com.cundong.recyclerview.LoadingFooter;
import com.cundong.recyclerview.RecyclerOnScrollListener;
import com.cundong.recyclerview.RecyclerViewStateUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.InjectView;
import taiyang.com.adapter.DickerAdapter;
import taiyang.com.entity.DickerListModel;
import taiyang.com.entity.DickerModel;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.T;
import taiyang.com.view.ErrorLayout;

/**
 * 我的还价
 */
public class DickerActivity extends BaseActivity {
    @InjectView(R.id.recycler_view)
    CustRecyclerView mRecyclerView;
    @InjectView(R.id.error_layout)
    ErrorLayout mErrorLayout;
    private DickerAdapter mAdapter;
    protected HeaderAndFooterRecyclerViewAdapter mRecyclerViewAdapter;
    protected int mCurrentPage = 1;
    protected int totalPage = 0;
    private DickerListModel mDickerListModel;
    private List<DickerModel> mDatas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_dicker);
//        ButterKnife.inject(this);
        mTitle.setText(R.string.wodehuanjia);
        mDatas = new ArrayList<>();
        mAdapter = new DickerAdapter(this);
        mAdapter.setDataList(mDatas);
        //设置点击事件
        mAdapter.setOnItemClickLitener(new DickerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
//        AnimationAdapter adapter = new ScaleInAnimationAdapter(mAdapter);
//        adapter.setFirstOnly(false);
//        adapter.setDuration(500);
//        adapter.setInterpolator(new OvershootInterpolator(.5f));
        mRecyclerViewAdapter = new HeaderAndFooterRecyclerViewAdapter(this,mAdapter);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
        mRecyclerView.setIsRefreshing(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addOnScrollListener(mOnScrollListener);
        mErrorLayout.setOnLayoutClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mCurrentPage = 1;
                mErrorLayout.setErrorType(ErrorLayout.NETWORK_LOADING);
                requestData();
            }
        });

        requestData();
    }
    protected RecyclerOnScrollListener mOnScrollListener = new RecyclerOnScrollListener() {

        @Override
        public void onScrolled(int dx, int dy) {
            super.onScrolled(dx, dy);

//            if(null != headerView) {
//                if (dy == 0 || dy < headerView.getHeight()) {
//                    toTopBtn.setVisibility(View.GONE);
//                }
//            }


        }

        @Override
        public void onScrollUp() {
            // 滑动时隐藏float button
//            if (toTopBtn.getVisibility() == View.VISIBLE) {
//                toTopBtn.setVisibility(View.GONE);
//                animate(toTopBtn, R.anim.floating_action_button_hide);
//            }
        }

        @Override
        public void onScrollDown() {
//            if (toTopBtn.getVisibility() != View.VISIBLE) {
//                toTopBtn.setVisibility(View.VISIBLE);
//                animate(toTopBtn, R.anim.floating_action_button_show);
//            }
        }

        @Override
        public void onBottom() {
            L.e("onBottom mCurrentPage = " + mCurrentPage);
            LoadingFooter.State state = RecyclerViewStateUtils.getFooterViewState(mRecyclerView);
            if(state == LoadingFooter.State.Loading) {
                return;
            }

            if (mCurrentPage <= totalPage) {
                // loading more
                RecyclerViewStateUtils.setFooterViewState(DickerActivity.this, mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
                requestData();
            } else {
                //the end
                /*if (totalPage > 1){
                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, getPageSize(), LoadingFooter.State.TheEnd, null);
                }else {
                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, mListAdapter.getItemCount(), LoadingFooter.State.TheEnd, null);

                }*/
                RecyclerViewStateUtils.setFooterViewState(DickerActivity.this, mRecyclerView, totalPage, LoadingFooter.State.TheEnd, null);
            }
        }


    };
    private void requestData() {
        Map<String, Object> params = new HashMap<>();
        params.put("model", "user");
        params.put("action", "get_myhuck");
        params.put("user_id", mySPEdit.getUserId());
        params.put("token", mySPEdit.getToken());
        params.put("page", mCurrentPage);
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "get_myhuck")));
        L.e(JSON.toJSONString(params));
        HttpUtils.sendPost(params, this);
    }


    @Override
    protected int getLayout() {
        return R.layout.activity_dicker;
    }

    @Override
    public void success(String response, int id) {
        dismissProgress(this);

        mCurrentPage++;
        mDickerListModel = new Gson().fromJson(response, DickerListModel.class);
        totalPage = mDickerListModel.getTotal().getPage_count();
        mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
        L.e("縂頁數:"+totalPage);
//                mRecyclerView.loadMoreComplete();
        for (int i = 0; i < mDickerListModel.getList().size(); i++) {
            mDatas.add(mDickerListModel.getList().get(i));
        }
//        if (mDatas.size()==0){
////                    mRecyclerView.setVisibility(View.GONE);
//            ll_nodata_layout.setVisibility(View.VISIBLE);
//        }
        if (mCurrentPage == 1) {
            L.e("数据的大小:"+mDatas.size());
            mAdapter.setDataList(mDatas);
        } else {
            L.e("设置数据:");
            RecyclerViewStateUtils.setFooterViewState(mRecyclerView, LoadingFooter.State.Normal);
//            mAdapter.addAll(mDatas);
            mAdapter.setDataList(mDatas);
        }
        mRecyclerView.refreshComplete();
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(this);
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(this);
        T.showShort(this,R.string.jianchawangluo);
        if (mCurrentPage == 1) {
            mErrorLayout.setErrorType(ErrorLayout.NETWORK_ERROR);
        } else {

            //在无网络时，滚动到底部时，mCurrentPage先自加了，然而在失败时却
            //没有减回来，如果刻意在无网络的情况下上拉，可以出现漏页问题
            //find by TopJohn
            mCurrentPage--;

            mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
            RecyclerViewStateUtils.setFooterViewState(DickerActivity.this, mRecyclerView, totalPage, LoadingFooter.State.NetWorkError, mFooterClick);
            mAdapter.notifyDataSetChanged();
        }
    }
    private View.OnClickListener mFooterClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RecyclerViewStateUtils.setFooterViewState(DickerActivity.this, mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
            requestData();
        }
    };
}
