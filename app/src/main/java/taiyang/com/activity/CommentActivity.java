package taiyang.com.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.ToastUtil;

/**
 * Created by zhew on 9/9/17.
 * Copyright © 2006-2016 365jia.cn 安徽大尺度网络传媒有限公司版权所有
 **/

public class CommentActivity extends BaseActivity {
    @InjectView(R.id.title_layout)
    RelativeLayout titleLayout;
    @InjectView(R.id.btn_comment)
    Button btnComment;
    @InjectView(R.id.et_comment)
    EditText etComment;

    @Override
    public void success(String response, int id) {
        ToastUtil.showToast("发布成功");
        setResult(1001);
        finish();
    }

    @Override
    public void failByOther(String fail, int id) {
        ToastUtil.showToast(fail);
    }

    @Override
    public void fail(String fail, int id) {
        ToastUtil.showToast("发布失败");
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_comment;
    }

    @OnClick(R.id.btn_comment)
    public void comment() {
        if (!etComment.getText().toString().trim().isEmpty()){
            Map<String, Object> params1 = new HashMap<>();
            params1.put("model", "purchase");
            params1.put("p_id", getIntent().getStringExtra("p_id"));
            params1.put("action", "addComment");
            params1.put("content", etComment.getText());
            params1.put("user_id", MySPEdit.getInstance(this).getUserId());
            params1.put("sign", MD5Utils.encode(MD5Utils.getSign("purchase", "addComment")));
            HttpUtils.sendPost(params1, this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.inject(this);


    }
}
