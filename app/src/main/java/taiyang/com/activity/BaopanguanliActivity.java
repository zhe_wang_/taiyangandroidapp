package taiyang.com.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;

import com.cundong.recyclerview.CustRecyclerView;
import com.cundong.recyclerview.HeaderAndFooterRecyclerViewAdapter;
import com.cundong.recyclerview.LoadingFooter;
import com.cundong.recyclerview.RecyclerOnScrollListener;
import com.cundong.recyclerview.RecyclerViewStateUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.adapter.BaopanAdapter;
import taiyang.com.entity.BaopanListModel;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.T;
import taiyang.com.view.ErrorLayout;

/**
 * 报盘管理
 */
public class BaopanguanliActivity extends BaseActivity {
    @InjectView(R.id.recycler_view)
    CustRecyclerView mRecyclerView;
    @InjectView(R.id.error_layout)
    ErrorLayout mErrorLayout;

    @InjectView(R.id.tv_sell_shangjia)
    TextView tv_sell_shangjia;
    @InjectView(R.id.tv_sell_type)
    TextView tv_sell_type;
    @InjectView(R.id.tv_sell_time)
    TextView tv_sell_time;



    @InjectView(R.id.ll_sell_shangjia)
    View ll_sell_shangjia;
    @InjectView(R.id.ll_sell_type)
    View ll_sell_type;
    @InjectView(R.id.ll_sell_time)
    View ll_sell_time;

    @OnClick(R.id.ll_sell_type)
    public void ChangeType(){
            final String[] items = { getString(R.string.dunshou),getString(R.string.guishou)};
            AlertDialog.Builder listDialog =
                    new AlertDialog.Builder(BaopanguanliActivity.this);
            listDialog.setTitle(getString(R.string.qingxuanze));
            listDialog.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(which==0){
                        sell_type ="4";
                    }
                    if(which==1){
                        sell_type ="5";
                    }
                    tv_sell_type.setText(items[which]);
                    initData();
                }
            });
            listDialog.show();
    }

    @OnClick(R.id.tv_wts)
    public void add(){
        Intent i = new Intent(BaopanguanliActivity.this, SellActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.ll_sell_time)
    public void ChangeTime(){
        final String[] items = { getString(R.string.qihuo),getString(R.string.xianhuo)};
        AlertDialog.Builder listDialog =
                new AlertDialog.Builder(BaopanguanliActivity.this);
        listDialog.setTitle(getString(R.string.qingxuanze));
        listDialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which==0){
                    goods_type ="6";
                }
                if(which==1){
                    goods_type ="7";
                }
                tv_sell_time.setText(items[which]);
                initData();
            }
        });
        listDialog.show();
    }



    @OnClick(R.id.ll_sell_shangjia)
    public void ChangeShangjia(){
        final String[] items = { getString(R.string.shangjia),getString(R.string.xiajia) };
        AlertDialog.Builder listDialog =
                new AlertDialog.Builder(BaopanguanliActivity.this);
        listDialog.setTitle(getString(R.string.qingxuanze));
        listDialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which==0){
                    is_on_sale ="1";
                }
                if(which==1){
                    is_on_sale ="0";
                }

                tv_sell_shangjia.setText(items[which]);
                initData();
            }
        });
        listDialog.show();
    }

    private BaopanAdapter mAdapter;
    protected HeaderAndFooterRecyclerViewAdapter mRecyclerViewAdapter;
    protected int mCurrentPage = 1;
    protected int totalPage = 0;
    private BaopanListModel mDickerListModel;
    public List<BaopanListModel.ListBean> mDatas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_dicker);
//        ButterKnife.inject(this);
        mTitle.setText(R.string.baopanguanli);
        initData();
    }

    public void initData() {
        mCurrentPage = 1;
        mDatas = new ArrayList<>();
        mAdapter = new BaopanAdapter(this);
        mAdapter.setDataList(mDatas);
        mRecyclerViewAdapter = new HeaderAndFooterRecyclerViewAdapter(this,mAdapter);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
        mRecyclerView.setIsRefreshing(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addOnScrollListener(mOnScrollListener);
        mErrorLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentPage = 1;
                mErrorLayout.setErrorType(ErrorLayout.NETWORK_LOADING);
                requestData();
            }
        });
        requestData();
    }

    protected RecyclerOnScrollListener mOnScrollListener = new RecyclerOnScrollListener() {

        @Override
        public void onScrolled(int dx, int dy) {
            super.onScrolled(dx, dy);
        }

        @Override
        public void onScrollUp() {

        }

        @Override
        public void onScrollDown() {
        }

        @Override
        public void onBottom() {
            L.e("onBottom mCurrentPage = " + mCurrentPage);
            LoadingFooter.State state = RecyclerViewStateUtils.getFooterViewState(mRecyclerView);
            if(state == LoadingFooter.State.Loading) {
                return;
            }

            if (mCurrentPage <= totalPage) {
                // loading more
                RecyclerViewStateUtils.setFooterViewState(BaopanguanliActivity.this, mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
                requestData();
            } else {
                RecyclerViewStateUtils.setFooterViewState(BaopanguanliActivity.this, mRecyclerView, totalPage, LoadingFooter.State.TheEnd, null);
            }
        }


    };

    private String is_on_sale ="1";
    private String sell_type ="4";
    private String goods_type ="6";
    public void requestData() {
        Map<String, Object> params = new HashMap<>();
        params.put("model", "seller_offer");
        params.put("action", "offer_list");
        params.put("user_id", mySPEdit.getUserId());
        params.put("token", mySPEdit.getToken());
        params.put("is_on_sale",is_on_sale);
        params.put("goods_type",goods_type);
        params.put("page", mCurrentPage);
        params.put("sell_type", sell_type);
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller_offer", "offer_list")));
        HttpUtils.sendPost(params, this);
    }



    @Override
    protected int getLayout() {
        return R.layout.activity_seller_baopanguanli;
    }

    @Override
    public void success(String response, int id) {
        dismissProgress(this);

        mCurrentPage++;
        mDickerListModel = new Gson().fromJson(response, BaopanListModel.class);
        totalPage = mDickerListModel.getTotal().getPage_count();
        mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
        L.e("縂頁數:"+totalPage);
//                mRecyclerView.loadMoreComplete();
        for (int i = 0; i < mDickerListModel.getList().size(); i++) {
            mDatas.add(mDickerListModel.getList().get(i));
        }
//
        if (mCurrentPage == 1) {
            L.e("数据的大小:"+mDatas.size());
            mAdapter.setDataList(mDatas);
        } else {
            L.e("设置数据:");
            RecyclerViewStateUtils.setFooterViewState(mRecyclerView, LoadingFooter.State.Normal);
//            mAdapter.addAll(mDatas);
            mAdapter.setDataList(mDatas);
        }
        mRecyclerView.refreshComplete();
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(this);
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(this);
        T.showShort(this,R.string.jianchawangluo);
        if (mCurrentPage == 1) {
            mErrorLayout.setErrorType(ErrorLayout.NETWORK_ERROR);
        } else {
            mCurrentPage--;
            mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
            RecyclerViewStateUtils.setFooterViewState(BaopanguanliActivity.this, mRecyclerView, totalPage, LoadingFooter.State.NetWorkError, mFooterClick);
            mAdapter.notifyDataSetChanged();
        }
    }
    private View.OnClickListener mFooterClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RecyclerViewStateUtils.setFooterViewState(BaopanguanliActivity.this, mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
            requestData();
        }
    };
}
