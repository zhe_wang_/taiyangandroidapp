package taiyang.com.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.EnvUtils;
import com.alipay.sdk.app.PayTask;

import java.util.HashMap;
import java.util.Map;

import butterknife.InjectView;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.PayResult;
import taiyang.com.utils.ToastUtil;

/**
 * Created by zhew on 10/3/17.
 * Copyright © 2006-2016 365jia.cn 安徽大尺度网络传媒有限公司版权所有
 **/

public class ChangePaymenyActivity extends BaseActivity {
    @InjectView(R.id.tv_title)
    TextView tvTitle;
    @InjectView(R.id.rb_alipay)
    RadioButton rbAlipay;
    @InjectView(R.id.rb_xianxia)
    RadioButton rbXianxia;
    @InjectView(R.id.rb_yinhangka)
    RadioButton rbYinhangka;
    @InjectView(R.id.tv_submit)
    TextView tvSubmit;
    private String pagType;

    @Override
    public void success(String response, int id) {
        if(!"3".equals(pagType)) {
            finish();
        }
    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(ChangePaymenyActivity.this);
        ToastUtil.showToast(fail);
        finish();
    }

    @Override
    public void fail(String fail, int id) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final String orderInfo = getIntent().getStringExtra("alipay");
        tvTitle.setText(R.string.xuanzezhifufangshi);

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!rbAlipay.isChecked()&&!rbXianxia.isChecked()&&!rbYinhangka.isChecked()){
                    ToastUtil.showToast(getString(R.string.xuanzezhifufangshi));
                }else{
                    showProgress(ChangePaymenyActivity.this, getString(R.string.jiazaizhong_dotdotdot));
                    Map<String, Object> params = new HashMap<>();
                    params.put("model", "order");
                    params.put("action", "order_edit_payment");
                    params.put("user_id", MySPEdit.getInstance(ChangePaymenyActivity.this).getUserId());
                    params.put("order_id", getIntent().getStringExtra("order_id"));
                    pagType = "";
                    if(rbAlipay.isChecked()){
                        Runnable payRunnable = new Runnable() {

                            @Override
                            public void run() {
                                PayTask alipay = new PayTask(ChangePaymenyActivity.this);
                                Map<String, String> result = alipay.payV2(orderInfo,true);
                                Message msg = new Message();
                                msg.obj = result;
                                mHandler.sendMessage(msg);
                            }
                        };
                        // 必须异步调用
                        Thread payThread = new Thread(payRunnable);
                        payThread.start();
                        pagType = "3";
                    }
                    else if(rbXianxia.isChecked()){
                        pagType = "-1";
                    }
                    else if(rbYinhangka.isChecked()){
                        pagType = "2";
                    }
                    params.put("pay_id", pagType);
                    params.put("sign", MD5Utils.encode(MD5Utils.getSign("order", "order_edit_payment")));
                    HttpUtils.sendPost(params,ChangePaymenyActivity.this);
                }
            }
        });
    }
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            PayResult payResult = new PayResult((Map<String, String>) msg.obj);
            /**
             对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
             */
            showProgress(ChangePaymenyActivity.this, getString(R.string.jiazaizhong_dotdotdot));
            Map<String, Object> params = new HashMap<>();
            params.put("model", "order");
            params.put("action", "savePayStatus");
            params.put("trade_response",  payResult.getResult());
            params.put("user_id", MySPEdit.getInstance(ChangePaymenyActivity.this).getUserId());
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("order", "savePayStatus")));
            HttpUtils.sendPost(params, new HttpRequestListener() {
                @Override
                public void success(String response, int id) {
                        dismissProgress(ChangePaymenyActivity.this);
                        Toast.makeText(ChangePaymenyActivity.this, "支付成功", Toast.LENGTH_SHORT).show();
                        setResult(11);
                        finish();
                }

                @Override
                public void failByOther(String fail, int id) {

                    dismissProgress(ChangePaymenyActivity.this);
                Toast.makeText(ChangePaymenyActivity.this, "支付失败", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void fail(String fail, int id) {

                }
            });

        }
    };

    @Override
    protected int getLayout() {
        return R.layout.activity_change_payment;
    }
}
