package taiyang.com.activity;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import taiyang.com.entity.OfferInfoBean;
import taiyang.com.tydp_b.R;

/**
 * Created by Administrator on 2016/8/29.
 * 报盘信息图片集合
 */
public class OfferInfoGridViewAdapter extends BaseAdapter {

    private List<OfferInfoBean.PictureListBean> pictureList;

    public OfferInfoGridViewAdapter(List<OfferInfoBean.PictureListBean> pictureList) {
        this.pictureList = pictureList;
    }

    @Override
    public int getCount() {
        return pictureList.size();
    }

    @Override
    public Object getItem(int position) {
        return pictureList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_offerinfo_gridview, null);
        }

        ViewHodler holder = ViewHodler.getHolder(convertView);
        holder.iv_offerinfo_child.setImageURI(Uri.parse(pictureList.get(position).getThumb_url()));

        return convertView;

    }

    //gv 子布局ViewHodler
    static class ViewHodler {
        private SimpleDraweeView iv_offerinfo_child;

        ViewHodler(View convertView) {
            iv_offerinfo_child = (SimpleDraweeView) convertView.findViewById(R.id.iv_offerinfo_child);
        }

        public static ViewHodler getHolder(View convertView) {
            ViewHodler holder = (ViewHodler) convertView.getTag();
            if (holder == null) {
                holder = new ViewHodler(convertView);
                convertView.setTag(holder);
            }

            return holder;
        }

    }

}
