package taiyang.com.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.adapter.FactoryListAdapter;
import taiyang.com.entity.FactoryListBean;
import taiyang.com.tydp_b.KProgressActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;

//店铺列表
public class FactoryListActivity extends KProgressActivity {

    @InjectView(R.id.rc_factorylist)
    RecyclerView rc_factorylist;
    private FactoryListAdapter mAdapter;

    @OnClick(R.id.ll_factorylist_back)
    public void back(View v) {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factory);
        ButterKnife.inject(this);
        initData();
    }

    private void initData() {
        String keywords = getIntent().getStringExtra("relut");
        Map<String, Object> params = new HashMap<>();
        params.put("model", "user");
        params.put("action", "get_shop_list");
        if(keywords!=null)params.put("keywords", keywords);
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "get_shop_list")));
        HttpUtils.sendPost(params, new HttpRequestListener() {
                    @Override
                    public void success(String response, int id) {
                        List<FactoryListBean> factoryListBean = new Gson().fromJson(response, new TypeToken<List<FactoryListBean>>() {}.getType());
                        if(factoryListBean ==null )return;
                        mAdapter = new FactoryListAdapter(factoryListBean,FactoryListActivity.this);
                        rc_factorylist.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                        rc_factorylist.setLayoutManager(new LinearLayoutManager(FactoryListActivity.this));
                        mAdapter.setOnItemClickLitener(new FactoryListAdapter.OnItemClickListener(){
                            @Override
                            public void onItemClick(View view, int position) {

                            }
                        });
                    }

                    @Override
                    public void failByOther(String fail, int id) {

                    }

                    @Override
                    public void fail(String fail, int id) {

                    }
                });


    }

}
