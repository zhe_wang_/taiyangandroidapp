package taiyang.com.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.kymjs.kjframe.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;

import butterknife.InjectView;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.RegularUtils;
import taiyang.com.utils.T;
import taiyang.com.utils.ToastUtil;

/*
    新增取货人
 */
public class AddAccountActivity extends BaseActivity implements View.OnClickListener {

    @InjectView(R.id.bt_addperson_bc)
    Button bt_addperson_bc;
    @InjectView(R.id.et_addperson_name)
    EditText et_addperson_name;
    @InjectView(R.id.et_addperson_number)
    EditText et_addperson_number;
    @InjectView(R.id.et_addperson_phone)
    EditText et_addperson_phone;
    @InjectView(R.id.et_addperson_bank)
    EditText et_addperson_bank;
    private String addperson_name;
    private String addperson_number;
    private String addperson_phone;
    private String addperson_bank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle.setText(getString(R.string.xinzengzhanghu));
        initListeren();
        initData();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_add_bank;
    }


    private void initListeren() {
        bt_addperson_bc.setOnClickListener(this);
    }

    private void initData() {
        addperson_name = et_addperson_name.getText().toString();
        addperson_number = et_addperson_number.getText().toString();
        addperson_phone = et_addperson_phone.getText().toString();
        addperson_bank = et_addperson_bank.getText().toString();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_addperson_bc:
                addperson_name = et_addperson_name.getText().toString();
                addperson_number = et_addperson_number.getText().toString();
                addperson_phone = et_addperson_phone.getText().toString();
                addperson_bank = et_addperson_bank.getText().toString();

                if (StringUtils.isEmpty(addperson_bank)||StringUtils.isEmpty(addperson_number)||StringUtils.isEmpty(addperson_name)) {
                    T.showShort(this, getString(R.string.qingjiangxinxitianxiewanzheng));
                    return;
                }
                    Map<String, Object> params = new HashMap<>();
                    params.put("model", "seller");
                    params.put("action", "saveBank");
                    params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller", "saveBank")));
                    params.put("user_id", mySPEdit.getUserId());
                    params.put("token", mySPEdit.getToken());
                    params.put("username", addperson_name);
                    params.put("account", addperson_number);
                    params.put("mobile", addperson_phone);
                    params.put("deposit_bank", addperson_bank);
                    HttpUtils.sendPost(params, this);
                break;

            default:
                break;
        }
    }

    @Override
    public void success(String response, int id) {
        T.showShort(this, getString(R.string.tianjiachenggong));
        finish();
    }

    @Override
    public void failByOther(String fail, int id) {
        ToastUtil.showToast(fail);
    }

    @Override
    public void fail(String fail, int id) {
        T.showShort(this, R.string.jianchawangluo);
    }
}
