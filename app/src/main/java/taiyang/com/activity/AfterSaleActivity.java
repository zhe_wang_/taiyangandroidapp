package taiyang.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.fragment.MessageFragment;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;

/**
 * 售后服务界面
 */
public class AfterSaleActivity extends BaseActivity {
//    @InjectView(R.id.back_layout)
//    LinearLayout back;
//
//    @OnClick(R.id.back_layout)
//    public void back(View v) {
//        finish();
//    }
//
//    @InjectView(R.id.tv_title)
//    TextView mTitle;
    @InjectView(R.id.tabs)
    TabLayout tabLayout;
    @InjectView(R.id.vp_view)
    ViewPager mViewPager;
    @InjectView(R.id.bt_commit)
    Button commitButton;

    @OnClick(R.id.bt_commit)
    public void commit(View v) {
        Intent intent = new Intent(this, CommitMessageActivity.class);
        startActivity(intent);
    }

    private List<Fragment> mFragmentList;
    private ViewAdapter mAdapter;
    private List<String> mTitleList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_after_sale);
//        ButterKnife.inject(this);
        mTitle.setText(R.string.liuyan);
//        initView();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_after_sale;
    }

    @Override
    protected void onResume() {
        super.onResume();
        initTitle();
    }

    private void initView() {
        //初始化顶部标题
        initTitle();
    }

    private void initTitle() {
        mTitleList = new ArrayList<>();
        mTitleList.add(getString(R.string.quanbu));
        mTitleList.add(getString(R.string.liuyan));
        mTitleList.add(getString(R.string.tousu));
        mTitleList.add(getString(R.string.shouhou));
        mFragmentList = new ArrayList<>();
        for (int i=0;i<mTitleList.size();i++){
            MessageFragment messageFragment= new MessageFragment();
            Bundle data=new Bundle();
            data.putString("title",mTitleList.get(i));
            messageFragment.setArguments(data);
            mFragmentList.add(messageFragment);
        }
        mAdapter = new ViewAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(mViewPager);//设置viewPaper和tabs关联
    }

    @Override
    public void success(String response,int id) {

    }

    @Override
    public void failByOther(String fail,int id) {

    }

    @Override
    public void fail(String fail,int id) {

    }

    class ViewAdapter extends FragmentPagerAdapter {

        public ViewAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitleList.get(position);
        }
    }
}
