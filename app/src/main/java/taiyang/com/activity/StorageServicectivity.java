package taiyang.com.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.entity.UserModel;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.T;
import taiyang.com.utils.ToastUtil;
import taiyang.com.view.MyDialog;

/**
 * 仓储，代理报关，贷押货款 界面
 */
public class StorageServicectivity extends BaseActivity{

    private MyDialog dialog;
    private TextView tv_know;

    @InjectView(R.id.et_shopName)
    EditText et_shopName;
    private String shopName;

    @InjectView(R.id.et_shopNumber)
    EditText et_shopNumber;
    private String shopNumber;

    @InjectView(R.id.et_shopStandard)
    EditText et_shopStandard;
    private String shopStandard;

    @InjectView(R.id.et_factory)
    EditText et_factory;
    private String factory;

    @InjectView(R.id.et_factoryAddress)
    EditText et_factoryAddress;
    private String factoryAddress;

    @InjectView(R.id.et_remark)
    EditText et_remark;
    private String remark;

    @InjectView(R.id.tv_personName)
    TextView tv_personName;
    private String personName;

    @InjectView(R.id.tv_personPhone)
    TextView tv_personPhone;
    private String personPhone;
    @InjectView(R.id.bt_commit)
    Button commitButton;

    @OnClick(R.id.bt_commit)
    public void commit(View v) {
        shopName = et_shopName.getText().toString();
        shopNumber = et_shopNumber.getText().toString();
        shopStandard = et_shopStandard.getText().toString();
        factory = et_factory.getText().toString();
        factoryAddress = et_factoryAddress.getText().toString();
        remark = et_remark.getText().toString();
        personName = tv_personName.getText().toString();
        personPhone = tv_personPhone.getText().toString();
        if (!TextUtils.isEmpty(shopName) && !TextUtils.isEmpty(shopNumber) && !TextUtils.isEmpty(shopStandard) && !TextUtils.isEmpty(factory)
                && !TextUtils.isEmpty(factoryAddress) && !TextUtils.isEmpty(remark) && !TextUtils.isEmpty(personName) && !TextUtils.isEmpty(personPhone)) {
            showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
            Map<String, Object> params = new HashMap<>();
            params.put("model", "other");
            if (getString(R.string.cangcufuwu).equals(title)) {
                params.put("action", "save_storage");
                params.put("sign", MD5Utils.encode(MD5Utils.getSign("other", "save_storage")));
            } else if (getString(R.string.dailibaoguan).equals(title)) {
                params.put("action", "save_abroad");
                params.put("sign", MD5Utils.encode(MD5Utils.getSign("other", "save_abroad")));
            } else {
                params.put("action", "save_loan");
                params.put("sign", MD5Utils.encode(MD5Utils.getSign("other", "save_loan")));
            }

            params.put("user_id", mySPEdit.getUserId());
            params.put("token", mySPEdit.getToken());
            params.put("goods_name", shopName);
            params.put("num", shopNumber);
            params.put("spec", shopStandard);
            params.put("brand_sn", factory);
            params.put("country", factoryAddress);
            params.put("user_name", personName);
            params.put("user_phone", personPhone);
            params.put("memo", remark);
            HttpUtils.sendPost(params, this);
        } else {
            T.showShort(this, getString(R.string.qingjiangxinxibuchongwanzheng));
            return;
        }
    }

    private String title;

    private UserModel mUserModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title = getIntent().getStringExtra("title");
        mTitle.setText(title);

        mUserModel=new Gson().fromJson(mySPEdit.getUserInfo(),UserModel.class);
        initView();
    }

    private void initView() {
        tv_personName.setText(mUserModel.getAlias());
        tv_personPhone.setText(mUserModel.getMobile_phone());
    }


    @Override
    protected int getLayout() {
        return R.layout.activity_storage_servicectivity;
    }

    @Override
    public void success(String response, int id) {
        dismissProgress(this);
        ToastUtil.showToast(response);
        finish();
    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }
}
