package taiyang.com.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.cundong.recyclerview.CustRecyclerView;
import com.cundong.recyclerview.HeaderAndFooterRecyclerViewAdapter;
import com.cundong.recyclerview.LoadingFooter;
import com.cundong.recyclerview.RecyclerOnScrollListener;
import com.cundong.recyclerview.RecyclerViewStateUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.InjectView;
import taiyang.com.adapter.BuyAdapter;
import taiyang.com.entity.InquiryListModel;
import taiyang.com.entity.InquiryModel;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.view.ErrorLayout;

/**
 * 我的求购界面
 */
public class MyBuyActivity extends BaseActivity {
    private boolean firstFlag;//用于判断是否重新加载
    private BuyAdapter mAdapter;

    private InquiryListModel mInquiryListModel;
    private List<InquiryModel> mDatas;


    @InjectView(R.id.recycler_view)
    CustRecyclerView mRecyclerView;
    @InjectView(R.id.error_layout)
    ErrorLayout mErrorLayout;

    protected HeaderAndFooterRecyclerViewAdapter mRecyclerViewAdapter;

    protected int mCurrentPage = 1;
    protected int totalPage = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle.setText(getString(R.string.xunpanliebiao));
        mDatas = new ArrayList<>();
        mAdapter = new BuyAdapter(this);

        mAdapter.setDataList(mDatas);
        mRecyclerViewAdapter = new HeaderAndFooterRecyclerViewAdapter(this,mAdapter);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
//        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.addOnScrollListener(mOnScrollListener);
        mRecyclerView.setIsRefreshing(false);
        mErrorLayout.setOnLayoutClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mCurrentPage = 1;
                mErrorLayout.setErrorType(ErrorLayout.NETWORK_LOADING);
                requestData();
            }
        });
        mAdapter.setOnItemClickLitener(new BuyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        requestData();
       /* // 设置item动画
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (firstFlag) {
            requestData();
        }
    }

    private void requestData() {
        showProgress(this,getString(R.string.jiazaizhong_dotdotdot));
        Map<String, Object> params = new HashMap<>();
        params.put("model", "user");
        params.put("action", "get_purchase");
        params.put("user_id", mySPEdit.getUserId());
        params.put("page", mCurrentPage);
        params.put("token", mySPEdit.getToken());
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "get_purchase")));
        HttpUtils.sendPost(params, this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_my_buy;
    }

    @Override
    public void success(String response, int id) {
        dismissProgress(this);

        mInquiryListModel = new Gson().fromJson(response, InquiryListModel.class);
//        totalPage = messageListModel.getTotal().getPage_count();
        mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
        L.e("縂頁數:"+totalPage);
//                mRecyclerView.loadMoreComplete();
        if (firstFlag){
            mDatas.clear();
        }
        for (int i = 0; i < mInquiryListModel.getList().size(); i++) {
            mDatas.add(mInquiryListModel.getList().get(i));
        }
        if (mCurrentPage == 1) {
            L.e("数据的大小:"+mDatas.size());
            mAdapter.setDataList(mDatas);
        } else {
            L.e("设置数据:");
            RecyclerViewStateUtils.setFooterViewState(mRecyclerView, LoadingFooter.State.Normal);
//            mAdapter.addAll(mDatas);
            mAdapter.setDataList(mDatas);
        }
        mRecyclerView.refreshComplete();
        mAdapter.notifyDataSetChanged();


        firstFlag = true;
    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(this);
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(this);
    }
    private View.OnClickListener mFooterClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RecyclerViewStateUtils.setFooterViewState(MyBuyActivity.this, mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
            requestData();
        }
    };
    protected RecyclerOnScrollListener mOnScrollListener = new RecyclerOnScrollListener() {

        @Override
        public void onScrolled(int dx, int dy) {
            super.onScrolled(dx, dy);

//            if(null != headerView) {
//                if (dy == 0 || dy < headerView.getHeight()) {
//                    toTopBtn.setVisibility(View.GONE);
//                }
//            }


        }

        @Override
        public void onScrollUp() {
            // 滑动时隐藏float button
//            if (toTopBtn.getVisibility() == View.VISIBLE) {
//                toTopBtn.setVisibility(View.GONE);
//                animate(toTopBtn, R.anim.floating_action_button_hide);
//            }
        }

        @Override
        public void onScrollDown() {
//            if (toTopBtn.getVisibility() != View.VISIBLE) {
//                toTopBtn.setVisibility(View.VISIBLE);
//                animate(toTopBtn, R.anim.floating_action_button_show);
//            }
        }

        @Override
        public void onBottom() {
            LoadingFooter.State state = RecyclerViewStateUtils.getFooterViewState(mRecyclerView);
            if(state == LoadingFooter.State.Loading) {
                return;
            }
            L.e("當前頁:"+mCurrentPage);
            L.e("縂頁數:"+totalPage);
            if (mCurrentPage <=totalPage) {
                // loading more
                RecyclerViewStateUtils.setFooterViewState(MyBuyActivity.this, mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
                requestData();
            } else {
                //the end
                /*if (totalPage > 1){
                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, getPageSize(), LoadingFooter.State.TheEnd, null);
                }else {
                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, mListAdapter.getItemCount(), LoadingFooter.State.TheEnd, null);

                }*/
                RecyclerViewStateUtils.setFooterViewState(MyBuyActivity.this, mRecyclerView, totalPage, LoadingFooter.State.TheEnd, null);
            }
        }


    };
}
