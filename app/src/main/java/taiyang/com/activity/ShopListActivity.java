package taiyang.com.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.adapter.ShopListAdapter;
import taiyang.com.tydp_b.KProgressActivity;
import taiyang.com.tydp_b.R;

/**
 * 店铺列表 8.1
 */
public class ShopListActivity extends KProgressActivity {

    @InjectView(R.id.shoplist_recycleView)
    RecyclerView shoplist_recycleView;
    @OnClick(R.id.ll_shoplist_back)
    public void back(View v){
        finish();
    }

    private ShopListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoplist);
        ButterKnife.inject(this);

        initView();
        initData();

    }

    private void initView() {
        List mDatas=new ArrayList();
        mAdapter=new ShopListAdapter(mDatas);
        shoplist_recycleView.setAdapter(mAdapter);
        shoplist_recycleView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter.setOnItemClickLitener(new ShopListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
    }

    private void initData() {

    }


}
