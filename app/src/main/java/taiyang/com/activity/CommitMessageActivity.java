package taiyang.com.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.T;
import taiyang.com.view.MyDialog;

/**
 * 提交留言界面
 */
public class CommitMessageActivity extends BaseActivity implements View.OnClickListener {
    @InjectView(R.id.back_layout)
    LinearLayout backLayout;
    private String order_sn;

    @OnClick(R.id.back_layout)
    public void back(View v) {
        finish();
    }

    @InjectView(R.id.tv_title)
    TextView mTitle;

    @InjectView(R.id.et_title)
    EditText et_title;
    @InjectView(R.id.et_content)
    EditText et_content;

    @InjectView(R.id.type_layout)
    RelativeLayout typeLayout;

    @OnClick(R.id.type_layout)
    public void typeLayout(View v) {
        View view = LayoutInflater.from(this).inflate(R.layout.message_dialog, null);
        mMessage = (TextView) view.findViewById(R.id.tv_message);
        mComplain = (TextView) view.findViewById(R.id.tv_complain);
        maftersale = (TextView) view.findViewById(R.id.tv_aftersale);
        iv_cancel = (ImageView) view.findViewById(R.id.iv_cancel);
        mMessage.setOnClickListener(this);
        mComplain.setOnClickListener(this);
        maftersale.setOnClickListener(this);
        iv_cancel.setOnClickListener(this);
        myDialog = new MyDialog(this, view, R.style.dialog);
        myDialog.show();
    }

    private TextView mMessage, mComplain, maftersale;
    private ImageView iv_cancel;
    private MyDialog myDialog;
    @InjectView(R.id.tv_messagetype)
    TextView tv_messagetype;

    @InjectView(R.id.bt_commit)
    Button bt_commit;
    private int type;
    @OnClick(R.id.bt_commit)
    public void commit(View v) {
        String msg_type = tv_messagetype.getText().toString();
        String msg_title = et_title.getText().toString();
        String msg_content = et_content.getText().toString();
        if (!TextUtils.isEmpty(msg_type) && !TextUtils.isEmpty(msg_title) && !TextUtils.isEmpty(msg_content)) {
            showProgress(this,getString(R.string.jiazaizhong_dotdotdot));
            if (msg_type.equals("投诉")){
                type=1;
            }else if (msg_type.equals("售后")){
                type=2;
            }
            Map<String,Object> params=new HashMap<>();
            params.put("model","user");
            params.put("action","message_add");
            params.put("user_id",mySPEdit.getUserId());
            params.put("token",mySPEdit.getToken());
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user","message_add")));
            params.put("msg_type",type);
            L.e("售后类型"+type);
            params.put("msg_title",msg_title);
            params.put("msg_content",msg_content);
            HttpUtils.sendPost(params,this);
        } else {
            T.showShort(this, "请将信息补充完整");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle.setText(getString(R.string.liuyan));
        order_sn = getIntent().getStringExtra("Order_sn");
        if(order_sn!=null){
            et_title.setText(getString(R.string.dingdanhao)+":"+order_sn);
        }

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_commit_message;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_message:
                tv_messagetype.setText(getString(R.string.liuyan));
                myDialog.dismiss();
                break;
            case R.id.tv_complain:
                tv_messagetype.setText(getString(R.string.tousu));
                myDialog.dismiss();
                break;
            case R.id.tv_aftersale:
                tv_messagetype.setText(getString(R.string.shouhou));
                myDialog.dismiss();
                break;
            case R.id.iv_cancel:
                myDialog.dismiss();
                break;
        }
    }

    @Override
    public void success(String response, int id) {
        dismissProgress(this);
        T.showShort(this,response);
        finish();
    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(this);
        T.showShort(this,fail);
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(this);
        T.showShort(this,fail);
    }
}
