package taiyang.com.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.tydp_b.KProgressActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.ToastUtil;

/**
 * 确认订单
 */
public class OrderCofirmActivity extends KProgressActivity implements View.OnClickListener {

    @InjectView(R.id.iv_orderconfirm_icon)
    SimpleDraweeView iv_orderconfirm_icon;
    @InjectView(R.id.iv_orderconfirm_back)
    ImageView iv_orderconfirm_back;
    @InjectView(R.id.bt_orderconfirm_confirm)
    Button bt_orderconfirm_confirm;
    @InjectView(R.id.tv_orderconfirm_name)
    TextView tv_orderconfirm_name;
    @InjectView(R.id.tv_orderconfirm_jiage)
    TextView tv_orderconfirm_jiage;
    @InjectView(R.id.tv_orderconfirm_spce2)
    TextView tv_orderconfirm_spce2;
    @InjectView(R.id.tv_orderconfirm_spce2_unit)
    TextView tv_orderconfirm_spce2_unit;
    @InjectView(R.id.tv_orderconfirm_dgsl)
    TextView tv_orderconfirm_dgsl;
    @InjectView(R.id.tv_orderconfirm_cpzj)
    TextView tv_orderconfirm_cpzj;
    @InjectView(R.id.tv_orderconfirm_yf)
    TextView tv_orderconfirm_yf;
    @InjectView(R.id.tv_orderconfirm_zj)
    TextView tv_orderconfirm_zj;

    @InjectView(R.id.tv_orderconfirm_phone)
    EditText  tv_orderconfirm_phone;
    @InjectView(R.id.et_orderconfirm_real_name)
    EditText et_orderconfirm_real_name;


    @InjectView(R.id.et_orderconfirm_bz)
    EditText et_orderconfirm_bz;

    private String goods_thumb;
    private String goods_name;
    private float shop_price;
    private String spec_2;
    private String getSpec_2_unit;
    private String num;
    private String sell_tupe;
    private String goods_weight;
    private String prepay_name;
    private String goods_id;
    private String userId;
    private String token;
    private String prepay_type;
    private String prepay_num;
    private int intspec_2;
    private String isPinTai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderconfirm);
        ButterKnife.inject(this);

        initListeren();
        initData();

    }

    public void initListeren(){
        iv_orderconfirm_back.setOnClickListener(this);
        bt_orderconfirm_confirm.setOnClickListener(this);
    }

    public void initData(){

        userId = MySPEdit.getInstance(this).getUserId();
        token = MySPEdit.getInstance(this).getToken();
        goods_thumb = getIntent().getStringExtra("goods_thumb");
        goods_name = getIntent().getStringExtra("goods_name");
        shop_price = getIntent().getFloatExtra("danjia",0);
        spec_2 = getIntent().getStringExtra("spec_2");
        getSpec_2_unit = getIntent().getStringExtra("getSpec_2_unit");
        num = getIntent().getStringExtra("num");
        sell_tupe = getIntent().getStringExtra("sell_tupe");
        goods_weight = getIntent().getStringExtra("goods_weight");
        prepay_name = getIntent().getStringExtra("prepay_name");
        goods_id = getIntent().getStringExtra("goods_id");
        prepay_type = getIntent().getStringExtra("prepay_type");
        prepay_num = getIntent().getStringExtra("prepay_num");
        if(MySPEdit.getInstance(this).getAlias().equals("")){
            et_orderconfirm_real_name.setEnabled(true);
        }else{
            et_orderconfirm_real_name.setEnabled(false);
        }
        et_orderconfirm_real_name.setText(MySPEdit.getInstance(this).getAlias());


        if(MySPEdit.getInstance(this).getMobilePhone().equals("")){
            tv_orderconfirm_phone.setEnabled(true);
        }else{
            tv_orderconfirm_phone.setEnabled(false);
        }
        tv_orderconfirm_phone.setText(MySPEdit.getInstance(this).getMobilePhone());

        if(goods_name!=null)tv_orderconfirm_name.setText(goods_name);
        if(goods_thumb!=null)iv_orderconfirm_icon.setImageURI(Uri.parse(goods_thumb));
        if(shop_price!=0)tv_orderconfirm_jiage.setText("¥"+shop_price);


        int intnum = Integer.parseInt(num);
        if(!spec_2.equals("")){
            intspec_2 = Integer.parseInt(spec_2);
        }
       // int intshop_price = Integer.parseInt(shop_price);
        int intprepay_name = Integer.parseInt(prepay_name);
        Float fgoods_weight = Float.parseFloat(goods_weight);

        if(sell_tupe.equals("4")){
            if(num!=null)tv_orderconfirm_dgsl.setText(num+getString(R.string.jian));
            tv_orderconfirm_spce2.setText(spec_2);
            tv_orderconfirm_spce2_unit.setText(getSpec_2_unit);
            tv_orderconfirm_cpzj.setText("￥" + (intnum / (float)intspec_2 * shop_price));

            if(prepay_type.equals("1")){
                if(prepay_name.equals("0")){
                    tv_orderconfirm_yf.setText(getString(R.string.yufu));
                }else {
                    tv_orderconfirm_yf.setText(getString(R.string.yufu)+" （"+prepay_name+" % )");
                }
                tv_orderconfirm_zj.setText("￥"+prepay_num);
            }else {
                tv_orderconfirm_yf.setText(getString(R.string.yufu));
                tv_orderconfirm_zj.setText("￥"+(intnum / (float)intspec_2 * shop_price)* intprepay_name/100);
            }
        }else {
            if(num!=null)tv_orderconfirm_dgsl.setText(num+getString(R.string.gui));
            tv_orderconfirm_spce2.setText(goods_weight);
            tv_orderconfirm_spce2_unit.setText(getString(R.string.dun_slash_gui));
            tv_orderconfirm_cpzj.setText("￥" + (intnum * shop_price * fgoods_weight));

            if(prepay_type.equals("1")){
                tv_orderconfirm_yf.setText(getString(R.string.yufu)+" （"+prepay_name+" % )");
                tv_orderconfirm_zj.setText("￥"+prepay_num);
            }else {
                tv_orderconfirm_yf.setText(getString(R.string.yufu));
                tv_orderconfirm_zj.setText("￥"+(intnum * shop_price * fgoods_weight)* intprepay_name/100);
            }
        }

    }

    public void onClick(View view) {
        switch (view.getId()){

            case R.id.iv_orderconfirm_back://退出
                finish();
                break;

            case R.id.bt_orderconfirm_confirm://订单提交

                int pay_type_id;
                    pay_type_id = 0;
                    isPinTai = "pintai" ;

                if(et_orderconfirm_real_name.getText().toString().equals("")){
                    ToastUtil.showToast("请输入真实姓名");
                    return;
                }
                sendOrder(pay_type_id);
                break;
            default:
                break;
        }
    }

    private void sendOrder(int pay_type_id) {
        showProgress(this,getString(R.string.jiazaizhong_dotdotdot));
        Map<String, Object> params = new HashMap<>();
        params.put("model", "order");
        params.put("action", "save_order");
        if(userId!=null)params.put("user_id", userId);
        if(token!=null)params.put("token", token);
        params.put("goods_id", goods_id);
        params.put("buy_number", num);
        params.put("postscript", et_orderconfirm_bz.getText());
        params.put("pay_type_id", pay_type_id);
        params.put("pay_type_id", pay_type_id);
        params.put("payment", 2);
        params.put("alias", et_orderconfirm_real_name.getText().toString());
        MySPEdit.getInstance(this).setAlias(et_orderconfirm_real_name.getText().toString());
        params.put("mobile_phone", tv_orderconfirm_phone.getText().toString());
        MySPEdit.getInstance(this).setAlias(et_orderconfirm_real_name.getText().toString());

        params.put("sign", MD5Utils.encode(MD5Utils.getSign("order", "save_order")));
        HttpUtils.sendPost(params, new HttpRequestListener() {
            @Override
            public void success(String response, int id) {
                Intent intent = new Intent(OrderCofirmActivity.this,OrderCofirmOkActivity.class);
                intent.putExtra("response",response);
                intent.putExtra("isPinTai",isPinTai);
                startActivity(intent);
                finish();
                dismissProgress(OrderCofirmActivity.this);
            }

            @Override
            public void failByOther(String fail, int id) {
                dismissProgress(OrderCofirmActivity.this);
                ToastUtil.showToast(fail+"");
            }

            @Override
            public void fail(String fail, int id) {
                ToastUtil.showToast(fail+"");
            }
        });
    }
}
