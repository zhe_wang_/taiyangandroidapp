package taiyang.com.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.cundong.recyclerview.CustRecyclerView;
import com.cundong.recyclerview.HeaderAndFooterRecyclerViewAdapter;
import com.cundong.recyclerview.LoadingFooter;
import com.cundong.recyclerview.RecyclerOnScrollListener;
import com.cundong.recyclerview.RecyclerViewStateUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.adapter.InquiryListAdapter;
import taiyang.com.entity.InquiryListContentBean;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.Constants;
import taiyang.com.utils.DensityUtil;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.T;
import taiyang.com.view.ErrorLayout;
import taiyang.com.view.MyPopupWindow;

/**
 * 筛选列表activity
 */
public class InquiryListActivity extends BaseActivity implements View.OnClickListener {

    protected HeaderAndFooterRecyclerViewAdapter mRecyclerViewAdapter;
    protected int mCurrentPage = 1;
    protected int totalPage = 0;
    @InjectView(R.id.recycler_view)
    CustRecyclerView mRecyclerView;
    @InjectView(R.id.error_layout)
    ErrorLayout mErrorLayout;
    Drawable im_bottom;
    String qhInts;
    String zxhInts;
    String xhInts;
    String zgInts;
    //    private String
    @InjectView(R.id.iv_ila_back)
    ImageView iv_ila_back;
    @InjectView(R.id.et_content)
    EditText et_content;
    @InjectView(R.id.rb_ila_zxh)
    RadioButton rb_ila_zxh;
    @InjectView(R.id.rb_ila_xh)
    RadioButton rb_ila_xh;
    @InjectView(R.id.rb_ila_qh)
    RadioButton rb_ila_qh;
    @InjectView(R.id.rb_ila_zg)
    RadioButton rb_ila_zg;
    @InjectView(R.id.rb_ila_ls)
    RadioButton rb_ila_ls;
    @InjectView(R.id.ll_hot)
    LinearLayout ll_hot;
    @InjectView(R.id.iv_search)
    ImageView iv_search;
    @InjectView(R.id.iv_select_back)
    ImageView iv_select_back;
    @InjectView(R.id.ll_time)
    LinearLayout ll_time;
    @InjectView(R.id.ll_price)
    LinearLayout ll_price;
    @InjectView(R.id.ll_address)
    LinearLayout ll_address;
    @InjectView(R.id.tv_hot)
    TextView tv_hot;
    @InjectView(R.id.tv_time)
    TextView tv_time;
    @InjectView(R.id.tv_price)
    TextView tv_price;
    @InjectView(R.id.tv_address)
    TextView tv_address;
    private int parentIndex;//顶部导航
    private InquiryListAdapter mAdapter;
    private String retailInt;//零售
    private boolean isCash;//是否现货
    private String goods_local;//现货参数
    private String region_id;//非现货参数
    private boolean isClickLocal;//点击产地后筛选结果
    /*@InjectView(R.id.ila_recycleView)
    XRecyclerView mRecyclerView;*/
    private int hotIndex = -1;
    private String response;

    private String result;
    private int timeIndex = -1;
    private int priceIndex = -1;
    private int addressIndex = -1;
    private Drawable drawableHot;
    private Drawable drawableTime;
    private Drawable drawablePrice;
    private Drawable drawableSelect;
    private List<InquiryListContentBean.ListBean> mDatas;//推荐列表
    private Map<String, Object> params;
    private List<InquiryListContentBean.Local> localList;
    private boolean clearFlag = true;//是否清除adapter中的内容
    private String baopan;
    private String cat_id;
    protected RecyclerOnScrollListener mOnScrollListener = new RecyclerOnScrollListener() {

        @Override
        public void onScrolled(int dx, int dy) {
            super.onScrolled(dx, dy);



        }

        @Override
        public void onScrollUp() {
        }

        @Override
        public void onScrollDown() {

        }

        @Override
        public void onBottom() {
            LoadingFooter.State state = RecyclerViewStateUtils.getFooterViewState(mRecyclerView);
            if (state == LoadingFooter.State.Loading) {
                return;
            }
            L.e("當前頁:" + mCurrentPage);
            L.e("縂頁數:" + totalPage);
            clearFlag = false;
            if (mCurrentPage <= totalPage) {
                // loading more
                RecyclerViewStateUtils.setFooterViewState(InquiryListActivity.this, mRecyclerView, totalPage, LoadingFooter.State.Loading, null);

                requestData();
            } else {
                //the end
                /*if (totalPage > 1){
                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, getPageSize(), LoadingFooter.State.TheEnd, null);
                }else {
                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, mListAdapter.getItemCount(), LoadingFooter.State.TheEnd, null);

                }*/
                RecyclerViewStateUtils.setFooterViewState(InquiryListActivity.this, mRecyclerView, totalPage, LoadingFooter.State.TheEnd, null);
            }
        }


    };
    private LinearLayout layout;
    private GridView gridView;
    private MyPopupWindow popupWindow;
//    private String title[] = {"1", "2", "3", "4", "5"};
    private View.OnClickListener mFooterClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RecyclerViewStateUtils.setFooterViewState(InquiryListActivity.this, mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
            requestData();
        }
    };

    @OnClick(R.id.iv_search)
    public void search(View v) {
        result = et_content.getText().toString().trim();
        if (!TextUtils.isEmpty(result)) {
            parentIndex = 5;
            initDefaultParams();
            initDefaultIndex();
            requestData();
        } else {
            T.showShort(this, "请输入查询内容!");
        }
    }

    @OnClick(R.id.iv_select_back)
    public void selectBack(View v) {

        //数据是使用Intent返回
        Intent intent = new Intent();
        //设置返回数据
        InquiryListActivity.this.setResult(Constants.BACKSELECT, intent);
        //关闭Activity
        InquiryListActivity.this.finish();
    }

    //初始化默认参数
    private void initDefaultParams() {
        clearFlag = true;//是否清除adapter中的内容
        isCash = false;//是否现货
        isClickLocal = false;//点击产地后筛选结果
    }

    private void initDefaultIndex() {
        hotIndex = 0;
        timeIndex = -1;
        priceIndex = -1;
        addressIndex = -1;
        changeUi();
        mCurrentPage = 1;
    }

    @OnClick(R.id.ll_hot)
    public void hot(View v) {
        showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
        if (hotIndex == -1) {
            hotIndex = 1;
        }

        if (hotIndex == 0) {
            hotIndex = 1;
            timeIndex = -1;
            priceIndex = -1;
            addressIndex = -1;
            changeUi();
            //热门加载
            requestData();

        } else if (hotIndex == 1) {
            hotIndex = 0;
            timeIndex = -1;
            priceIndex = -1;
            addressIndex = -1;
            changeUi();
            requestData();
        }
    }

    //设置参数
    private void setParams() {
        params = new HashMap<>();
        params.put("model", "goods");
        params.put("action", "get_list");
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("goods", "get_list")));
        if (parentIndex == 0) {
            params.put("goods_type", "8");
        }
        else if (parentIndex == 1) {
            params.put("goods_type", "7");
        } else if (parentIndex == 2) {
            params.put("goods_type", "6");
        } else if (parentIndex == 4) {
            params.put("sell_type", "4");
        } else if (parentIndex == 3) {
            params.put("sell_type", "5");
        } else if (parentIndex == 5) {
            params.put("keywords", result);
        } else if (parentIndex == 7) {
            params.put("cat_id", cat_id);
        }
        if (isClickLocal) {
            if (isCash) {
                params.put("goods_local", goods_local);
            } else {
                params.put("region_id", region_id);
            }
        }
    }

    @OnClick(R.id.ll_time)
    public void time(View v) {
        showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
        if (timeIndex == -1) {
            timeIndex = 1;
        }
        if (timeIndex == 0) {
            timeIndex = 1;
            hotIndex = -1;
            priceIndex = -1;
            addressIndex = -1;
            changeUi();
            requestData();

        } else {
            timeIndex = 0;
            hotIndex = -1;
            priceIndex = -1;
            addressIndex = -1;
            changeUi();
            requestData();
        }
    }

    @OnClick(R.id.ll_price)
    public void price(View v) {
//        T.showShort(this,""+priceIndex);
//        L.e(priceIndex+"");
        showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
        if (priceIndex == -1) {
            priceIndex = 1;
        }

        if (priceIndex == 0) {
            priceIndex = 1;
            hotIndex = -1;
            timeIndex = -1;
            addressIndex = -1;
            changeUi();
            requestData();

        } else {
            priceIndex = 0;
            hotIndex = -1;
            timeIndex = -1;
            addressIndex = -1;
            changeUi();
            requestData();
        }
    }

    @OnClick(R.id.ll_address)
    public void address(View v) {
        if (addressIndex == -1) {
            addressIndex = 1;
        }
        addressIndex = 1;
        hotIndex = -1;
        timeIndex = -1;
        priceIndex = -1;
        changeUi();

        showPopupWindow(ll_address);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setContentView(R.layout.activity_inquirylist);
//        ButterKnife.inject(this);

        initViews();
        initListener();
        localList = new ArrayList<>();
        if (qhInts != null && qhInts.equals("qhInt")) {//homeFragment 期货跳转
            checkAdapter(3);
        } else if (xhInts != null && xhInts.equals("xhInt")) {//homeFragment 现货跳转
            isCash = true;
            checkAdapter(2);
        }
        else if (zxhInts != null && zxhInts.equals("zxhInt")) {//homeFragment 准现货跳转
            checkAdapter(1);
            parentIndex = 0;
        } else if (zgInts != null && zgInts.equals("zgInt")) {
            checkAdapter(4);
            parentIndex = 3;
        } else if (params != null && baopan.equals("baopan")) {//offerFragment 筛选跳转
            checkAdapter(6);
            parentIndex = 6;
        } else if (result != null) {//homeFragment offerFragment 搜索跳转
            checkAdapter(7);
            parentIndex = 5;
        } else if (cat_id != null) {
            checkAdapter(6);
            parentIndex = 7;
        } else if (retailInt != null && retailInt.equals("retailInt")) {
            checkAdapter(5);
        }
        initData();

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_inquirylist;
    }

    private void initData() {
        showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
        if (parentIndex != 6) {
            setParams();
        }
        params.put("page", mCurrentPage);
        params.put("order", "DESC");
        L.e("傳遞的參數:" + JSON.toJSONString(params));
        HttpUtils.sendPost(params, this);
    }

    private void initViews() {
        //获取offerFragment传递的值, 确定选择哪个RB的adapter
        //params = getIntent().getStringExtra("offerFragment");
        params = (Map<String, Object>) getIntent().getSerializableExtra("offerFragment");
        baopan = getIntent().getStringExtra("baopan");
        L.e("测试map......:" + JSON.toJSONString(params));
        //获取homeFragment搜索的值, 确定选择哪个RB的adapter
        result = getIntent().getStringExtra("relut");
        //获取homeFragment传递的值, 确定选择哪个RB的adapter
        qhInts = getIntent().getStringExtra("qhInt");//期货
        xhInts = getIntent().getStringExtra("xhInt");//期货
        zxhInts = getIntent().getStringExtra("zxhInt");//期货
        zgInts = getIntent().getStringExtra("zgInt");
        retailInt = getIntent().getStringExtra("retailInt");
        cat_id = getIntent().getStringExtra("cat_id");
        //RB底部红线
        Resources res = getResources();
        im_bottom = res.getDrawable(R.mipmap.list_line_orange);
        im_bottom.setBounds(0, 0, im_bottom.getMinimumWidth(), im_bottom.getMinimumHeight());
        hotIndex = 0;
        changeUi();
        mDatas = new ArrayList<>();
//        mAdapter = new InquiryListAdapter(inquirylist, InquiryListActivity.this);

        mAdapter = new InquiryListAdapter(this);
        mAdapter.setDataList(mDatas);
        mRecyclerViewAdapter = new HeaderAndFooterRecyclerViewAdapter(this, mAdapter);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
//        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.addOnScrollListener(mOnScrollListener);
        mRecyclerView.setIsRefreshing(false);
        mErrorLayout.setOnLayoutClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mCurrentPage = 1;
                mErrorLayout.setErrorType(ErrorLayout.NETWORK_LOADING);
                requestData();
            }
        });

       /* mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setPullRefreshEnabled(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(InquiryListActivity.this));
        mRecyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
            }

            @Override
            public void onLoadMore() {
                if (page < pageCount) {
                    page++;
                    clearFlag = false;
                    dataLoad();
                }
            }
        });*/
        mAdapter.setOnItemClickLitener(new InquiryListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
    }

    private void requestData() {
        if (hotIndex == 1) {
            if (parentIndex != 6) {
                setParams();
            }
            params.put("page", mCurrentPage);
            params.put("order", "ASC");
//            HttpUtils.sendPost(params, this);
        } else if (hotIndex == 0) {
            if (parentIndex != 6) {
                setParams();
            }
            params.put("page", mCurrentPage);
            params.put("order", "DESC");
            L.e("测试params:" + JSON.toJSONString(params));
        } else if (timeIndex == 1) {
            if (parentIndex != 6) {
                setParams();
            }

            params.put("page", mCurrentPage);
            params.put("sort", "last_update");
            params.put("order", "ASC");
//            HttpUtils.sendPost(params, this);
        } else if (timeIndex == 0) {
            if (parentIndex != 6) {
                setParams();
            }

            params.put("page", mCurrentPage);
            params.put("sort", "last_update");
            params.put("order", "DESC");
//            HttpUtils.sendPost(params, this);
        } else if (priceIndex == 1) {
            if (parentIndex != 6) {
                setParams();
            }

            params.put("page", mCurrentPage);
            params.put("sort", "shop_price");
            params.put("order", "ASC");
//            HttpUtils.sendPost(params, this);
        } else if (priceIndex == 0) {
            if (parentIndex != 6) {
                setParams();
            }
            params.put("page", mCurrentPage);
            params.put("sort", "shop_price");
            params.put("order", "DESC");

        }
        HttpUtils.sendPost(params, this);
    }

    private void changeUi() {
        clearFlag = true;
        mCurrentPage = 1;
//        if (addressIndex==1){
//            mRecyclerView.setNoMore(true);
//        }else{

//        }

        if (hotIndex == 0) {
            drawableHot = getResources().getDrawable(
                    R.mipmap.productlist_down);
            drawableTime = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawablePrice = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableSelect = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableHot.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawableTime.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawablePrice.setBounds(0, 0, drawablePrice.getMinimumWidth(),
                    drawablePrice.getMinimumHeight());
            drawableSelect.setBounds(0, 0, drawableSelect.getMinimumWidth(),
                    drawableSelect.getMinimumHeight());
            tv_hot.setCompoundDrawables(null, null, drawableHot, null);
            tv_time.setCompoundDrawables(null, null, drawableTime, null);
            tv_price.setCompoundDrawables(null, null, drawablePrice, null);
            tv_address.setCompoundDrawables(null, null, drawableSelect, null);
            tv_hot.setTextColor(getResources().getColor(R.color.message_bg));
            tv_time.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
            tv_price.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
            tv_address.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
        } else if (hotIndex == 1) {
            drawableHot = getResources().getDrawable(
                    R.mipmap.productlist_up);
            drawableTime = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawablePrice = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableSelect = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableHot.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawableTime.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawablePrice.setBounds(0, 0, drawablePrice.getMinimumWidth(),
                    drawablePrice.getMinimumHeight());
            drawableSelect.setBounds(0, 0, drawableSelect.getMinimumWidth(),
                    drawableSelect.getMinimumHeight());
            tv_hot.setCompoundDrawables(null, null, drawableHot, null);
            tv_time.setCompoundDrawables(null, null, drawableTime, null);
            tv_price.setCompoundDrawables(null, null, drawablePrice, null);
            tv_address.setCompoundDrawables(null, null, drawableSelect, null);
            tv_hot.setTextColor(getResources().getColor(R.color.message_bg));
            tv_time.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
            tv_price.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
            tv_address.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
        } else if (timeIndex == 0) {
            drawableHot = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableTime = getResources().getDrawable(
                    R.mipmap.productlist_down);
            drawablePrice = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableSelect = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableHot.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawableTime.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawablePrice.setBounds(0, 0, drawablePrice.getMinimumWidth(),
                    drawablePrice.getMinimumHeight());
            drawableSelect.setBounds(0, 0, drawableSelect.getMinimumWidth(),
                    drawableSelect.getMinimumHeight());
            tv_hot.setCompoundDrawables(null, null, drawableHot, null);
            tv_time.setCompoundDrawables(null, null, drawableTime, null);
            tv_price.setCompoundDrawables(null, null, drawablePrice, null);
            tv_address.setCompoundDrawables(null, null, drawableSelect, null);
            tv_hot.setTextColor(getResources().getColor(R.color.color_text_normal));
            tv_time.setTextColor(getResources().getColor(
                    R.color.message_bg));
            tv_price.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
            tv_address.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
        } else if (timeIndex == 1) {
            drawableHot = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableTime = getResources().getDrawable(
                    R.mipmap.productlist_up);
            drawablePrice = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableSelect = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableHot.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawableTime.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawablePrice.setBounds(0, 0, drawablePrice.getMinimumWidth(),
                    drawablePrice.getMinimumHeight());
            drawableSelect.setBounds(0, 0, drawableSelect.getMinimumWidth(),
                    drawableSelect.getMinimumHeight());
            tv_hot.setCompoundDrawables(null, null, drawableHot, null);
            tv_time.setCompoundDrawables(null, null, drawableTime, null);
            tv_price.setCompoundDrawables(null, null, drawablePrice, null);
            tv_address.setCompoundDrawables(null, null, drawableSelect, null);
            tv_hot.setTextColor(getResources().getColor(R.color.color_text_normal));
            tv_time.setTextColor(getResources().getColor(
                    R.color.message_bg));
            tv_price.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
            tv_address.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
        } else if (priceIndex == 0) {
            drawableHot = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableTime = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawablePrice = getResources().getDrawable(
                    R.mipmap.productlist_down);
            drawableSelect = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableHot.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawableTime.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawablePrice.setBounds(0, 0, drawablePrice.getMinimumWidth(),
                    drawablePrice.getMinimumHeight());
            drawableSelect.setBounds(0, 0, drawableSelect.getMinimumWidth(),
                    drawableSelect.getMinimumHeight());
            tv_hot.setCompoundDrawables(null, null, drawableHot, null);
            tv_time.setCompoundDrawables(null, null, drawableTime, null);
            tv_price.setCompoundDrawables(null, null, drawablePrice, null);
            tv_address.setCompoundDrawables(null, null, drawableSelect, null);
            tv_hot.setTextColor(getResources().getColor(R.color.color_text_normal));
            tv_time.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
            tv_price.setTextColor(getResources().getColor(
                    R.color.message_bg));
            tv_address.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
        } else if (priceIndex == 1) {
            drawableHot = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableTime = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawablePrice = getResources().getDrawable(
                    R.mipmap.productlist_up);
            drawableSelect = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableHot.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawableTime.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawablePrice.setBounds(0, 0, drawablePrice.getMinimumWidth(),
                    drawablePrice.getMinimumHeight());
            drawableSelect.setBounds(0, 0, drawableSelect.getMinimumWidth(),
                    drawableSelect.getMinimumHeight());
            tv_hot.setCompoundDrawables(null, null, drawableHot, null);
            tv_time.setCompoundDrawables(null, null, drawableTime, null);
            tv_price.setCompoundDrawables(null, null, drawablePrice, null);
            tv_address.setCompoundDrawables(null, null, drawableSelect, null);
            tv_hot.setTextColor(getResources().getColor(R.color.color_text_normal));
            tv_time.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
            tv_price.setTextColor(getResources().getColor(
                    R.color.message_bg));
            tv_address.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
        } else if (addressIndex == 0) {
            drawableHot = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableTime = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawablePrice = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableSelect = getResources().getDrawable(
                    R.mipmap.productlist_down);
            drawableHot.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawableTime.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawablePrice.setBounds(0, 0, drawablePrice.getMinimumWidth(),
                    drawablePrice.getMinimumHeight());
            drawableSelect.setBounds(0, 0, drawableSelect.getMinimumWidth(),
                    drawableSelect.getMinimumHeight());
            tv_hot.setCompoundDrawables(null, null, drawableHot, null);
            tv_time.setCompoundDrawables(null, null, drawableTime, null);
            tv_price.setCompoundDrawables(null, null, drawablePrice, null);
            tv_address.setCompoundDrawables(null, null, drawableSelect, null);
            tv_hot.setTextColor(getResources().getColor(R.color.color_text_normal));
            tv_time.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
            tv_price.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
            tv_address.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
        } else if (addressIndex == 1) {
            drawableHot = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableTime = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawablePrice = getResources().getDrawable(
                    R.mipmap.productlist_down_n);
            drawableSelect = getResources().getDrawable(
                    R.mipmap.productlist_up);
            drawableHot.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawableTime.setBounds(0, 0, drawableHot.getMinimumWidth(),
                    drawableHot.getMinimumHeight());
            drawablePrice.setBounds(0, 0, drawablePrice.getMinimumWidth(),
                    drawablePrice.getMinimumHeight());
            drawableSelect.setBounds(0, 0, drawableSelect.getMinimumWidth(),
                    drawableSelect.getMinimumHeight());
            tv_hot.setCompoundDrawables(null, null, drawableHot, null);
            tv_time.setCompoundDrawables(null, null, drawableTime, null);
            tv_price.setCompoundDrawables(null, null, drawablePrice, null);
            tv_address.setCompoundDrawables(null, null, drawableSelect, null);
            tv_hot.setTextColor(getResources().getColor(R.color.color_text_normal));
            tv_time.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
            tv_price.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
            tv_address.setTextColor(getResources().getColor(
                    R.color.color_text_normal));
        }

    }

    private void initListener() {
        iv_ila_back.setOnClickListener(this);
//        rl_ils_quiry.setOnClickListener(this);
        rb_ila_zxh.setOnClickListener(this);
        rb_ila_xh.setOnClickListener(this);
        rb_ila_qh.setOnClickListener(this);
        rb_ila_zg.setOnClickListener(this);
        rb_ila_ls.setOnClickListener(this);
//        rb_ila_price.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_ila_back://退出
                finish();
                break;

            case R.id.rb_ila_zxh://准现货adapter
                isCash = false;
                isClickLocal = false;
                tv_address.setText(getString(R.string.chandi));
                hotIndex = 0;
                timeIndex = -1;
                priceIndex = -1;
                addressIndex = -1;
                changeUi();
                checkAdapter(1);
                initData();
                break;

            case R.id.rb_ila_xh://现货adapter
                isCash = true;
                isClickLocal = false;
                tv_address.setText(getString(R.string.suozaidi));
                hotIndex = 0;
                timeIndex = -1;
                priceIndex = -1;
                addressIndex = -1;
                changeUi();
                checkAdapter(2);
                initData();
                break;

            case R.id.rb_ila_qh://期货adapter
                isCash = false;
                isClickLocal = false;
                tv_address.setText(getString(R.string.chandi));
                hotIndex = 0;
                timeIndex = -1;
                priceIndex = -1;
                addressIndex = -1;
                changeUi();
                checkAdapter(3);
                initData();
                break;

            case R.id.rb_ila_zg://整柜adapter
                isCash = false;
                isClickLocal = false;
                tv_address.setText(getString(R.string.suozaidi));
                hotIndex = 0;
                timeIndex = -1;
                priceIndex = -1;
                addressIndex = -1;
                changeUi();
                checkAdapter(4);
                initData();
                break;

            case R.id.rb_ila_ls://零售adapter
                isCash = false;
                isClickLocal = false;
                tv_address.setText(getString(R.string.suozaidi));
                hotIndex = 0;
                timeIndex = -1;
                priceIndex = -1;
                addressIndex = -1;
                changeUi();
                checkAdapter(5);
                initData();
                break;

            default:
                break;

        }
    }

    //切换不同Adapter
    public void checkAdapter(int index) {
        rb_ila_zxh.setChecked(false);
        rb_ila_zxh.setCompoundDrawables(null, null, null, null);
        rb_ila_xh.setChecked(false);
        rb_ila_xh.setCompoundDrawables(null, null, null, null);
        rb_ila_qh.setChecked(false);
        rb_ila_qh.setCompoundDrawables(null, null, null, null);
        rb_ila_zg.setChecked(false);
        rb_ila_zg.setCompoundDrawables(null, null, null, null);
        rb_ila_ls.setChecked(false);
        rb_ila_ls.setCompoundDrawables(null, null, null, null);
        switch (index) {
            case 1:
                parentIndex = 0;
                //设置点击状态, 红线显示
                rb_ila_zxh.setChecked(true);
                rb_ila_zxh.setCompoundDrawables(null, null, null, im_bottom);
                break;

            case 2:
                parentIndex = 1;
//                initXHData();
                //设置点击状态, 红线显示
                rb_ila_xh.setChecked(true);
                rb_ila_xh.setCompoundDrawables(null, null, null, im_bottom);
                break;

            case 3:
                parentIndex = 2;
                rb_ila_qh.setChecked(true);
                rb_ila_qh.setCompoundDrawables(null, null, null, im_bottom);
                break;

            case 4:
                parentIndex = 3;
//                initZGData();
                //设置点击状态, 红线显示
                rb_ila_zg.setChecked(true);
                rb_ila_zg.setCompoundDrawables(null, null, null, im_bottom);
                break;

            case 5:
                parentIndex = 4;
//                initLSData();
                //设置点击状态, 红线显示
                rb_ila_ls.setChecked(true);
                rb_ila_ls.setCompoundDrawables(null, null, null, im_bottom);
                break;
            default:
                break;
        }
    }

    public void showPopupWindow(View parent) {
        //加载布局
        layout = (LinearLayout) LayoutInflater.from(InquiryListActivity.this).inflate(
                R.layout.dialog, null);
        //找到布局的控件
        gridView = (GridView) layout.findViewById(R.id.lv_dialog);
        //设置适配器
        gridView.setAdapter(new LocaAdapter());
        // 实例化popupWindow
        popupWindow = new MyPopupWindow(layout, DensityUtil.dip2px(this, 300), DensityUtil.dip2px(this, 200));
        //控制键盘是否可以获得焦点
        popupWindow.setFocusable(true);
        //设置popupWindow弹出窗体的背景
//        popupWindow.setBackgroundDrawable(new BitmapDrawable(null, ""));
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        WindowManager manager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        @SuppressWarnings("deprecation")
        //获取xoff
                int xpos = manager.getDefaultDisplay().getWidth() / 2 - popupWindow.getWidth() / 2;
        //xoff,yoff基于anchor的左下角进行偏移。
        popupWindow.showAsDropDown(parent, xpos, 0);
        popupWindow.setSendMessage(new MyPopupWindow.SendMessage() {
            @Override
            public void send() {
                InquiryListActivity.this.addressIndex = 0;
                changeUi();
            }
        });
        //监听
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int positon,
                                    long arg3) {
                //关闭popupWindow
                popupWindow.dismiss();
                popupWindow = null;
                goods_local = localList.get(positon).getName();
                L.e("goods_local:::" + goods_local);
                region_id = localList.get(positon).getId();
                L.e("region_id:::" + region_id);
                isClickLocal = true;
//                hotIndex=1;
                initDefaultIndex();
                showProgress(InquiryListActivity.this, getString(R.string.jiazaizhong_dotdotdot));
                requestData();
//                params = new HashMap<String, Object>();
//                params.put("model", "goods");
//                params.put("action", "get_list");
//                params.put("sign", MD5Utils.encode(MD5Utils.getSign("goods", "get_list")));
//                if (isCash) {
//                    params.put("goods_local", localList.get(positon).getName());
//                } else {
//                    params.put("region_id", localList.get(positon).getId());
//                }
//                HttpUtils.sendPost(params, InquiryListActivity.this);

            }
        });
    }

    @Override
    public void success(String response, int id) {
        L.e("page:" + mCurrentPage);
        dismissProgress(this);

        if (clearFlag) {
            mDatas.clear();
            mRecyclerView.scrollToPosition(0);
        }

        mCurrentPage++;
        InquiryListContentBean inquiryListBean = new Gson().fromJson(response, InquiryListContentBean.class);
        totalPage = inquiryListBean.getTotal().getPage_count();
        mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
        L.e("縂頁數:" + totalPage);
//                mRecyclerView.loadMoreComplete();
        for (int i = 0; i < inquiryListBean.getList().size(); i++) {
            mDatas.add(inquiryListBean.getList().get(i));
        }
        localList.clear();
        for (int i = 0; i < inquiryListBean.getLocaList().size(); i++) {
            localList.add(inquiryListBean.getLocaList().get(i));
        }
        if (mCurrentPage == 1) {
            L.e("数据的大小:" + mDatas.size());
            mAdapter.setDataList(mDatas);
        } else {
            L.e("设置数据:");
            RecyclerViewStateUtils.setFooterViewState(mRecyclerView, LoadingFooter.State.Normal);
//            mAdapter.addAll(mDatas);
            mAdapter.setDataList(mDatas);
        }
        L.e("mDatas的大小" + mDatas.size());
        mRecyclerView.refreshComplete();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(this);
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(this);
        T.showShort(this, R.string.jianchawangluo);
        if (mCurrentPage == 1) {
            mErrorLayout.setErrorType(ErrorLayout.NETWORK_ERROR);
        } else {

            //在无网络时，滚动到底部时，mCurrentPage先自加了，然而在失败时却
            //没有减回来，如果刻意在无网络的情况下上拉，可以出现漏页问题
            //find by TopJohn
//            mCurrentPage--;

            mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
            RecyclerViewStateUtils.setFooterViewState(InquiryListActivity.this, mRecyclerView, totalPage, LoadingFooter.State.NetWorkError, mFooterClick);
            mAdapter.notifyDataSetChanged();
        }
    }

    class LocaAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return localList.size();
        }

        @Override
        public Object getItem(int position) {
            return localList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = LayoutInflater.from(InquiryListActivity.this).inflate(R.layout.text, null, false);
                holder = new ViewHolder();
                holder.tv_location = (TextView) convertView.findViewById(R.id.tv_location);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.tv_location.setText(localList.get(position).getName());
            return convertView;
        }
    }

    class ViewHolder {
        TextView tv_location;

    }
}
