package taiyang.com.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.entity.InquiryModel;
import taiyang.com.entity.UserModel;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MyApplication;
import taiyang.com.utils.T;
import taiyang.com.utils.ToastUtil;
import taiyang.com.view.MyDialog;

/**
 * 发布求购界面
 */
public class PublishActivity extends BaseActivity implements View.OnClickListener {

    @InjectView(R.id.et_title)
    EditText et_title;
    private String title;

    @InjectView(R.id.et_shop)
    EditText et_shop;
    private String shopName;


    @InjectView(R.id.et_number)
    EditText et_number;
    private String number;

    @InjectView(R.id.et_minprice)
    EditText et_minprice;
    private String minPrice;

    @InjectView(R.id.et_maxprice)
    EditText et_maxprice;
    private String maxPrice;

    @InjectView(R.id.et_remark)
    EditText et_remark;
    private String remark;

    @InjectView(R.id.et_address)
    EditText et_address;
    private String address;

    @InjectView(R.id.tv_personname)
    TextView tv_personname;

    @InjectView(R.id.tv_personphone)
    TextView tv_personphone;
    @InjectView(R.id.bt_commit)
    Button commitButton;
    private int intMin;
    private int intMin1;
    private int intMax;

    @OnClick(R.id.bt_commit)
    public void commit(View v) {
        title = et_title.getText().toString();

        type = tv_type_select.getTag().toString().trim();



        shopName = et_shop.getText().toString();
        number = et_number.getText().toString();
        minPrice = et_minprice.getText().toString();
        maxPrice = et_maxprice.getText().toString();

        remark = et_remark.getText().toString();
        address = et_address.getText().toString();

        if(!minPrice.equals("") && !maxPrice.equals("")){
            intMin = Integer.parseInt(minPrice);
            intMax = Integer.parseInt(maxPrice);
            L.e("intMin" + intMin);
            L.e("intMax" + intMax);

            if(intMin>intMax){
                ToastUtil.showToast("最小报价不能大于最大报价");
                return;
            }
        }

        if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(type) && !TextUtils.isEmpty(shopName) && !TextUtils.isEmpty(number) && !TextUtils.isEmpty(minPrice) && !TextUtils.isEmpty(maxPrice)
                && !TextUtils.isEmpty(remark) && !TextUtils.isEmpty(address) && !type.equals(getString(R.string.qingxuanze))) {

            Map<String, Object> params = new HashMap<>();
            params.put("model", "other");
            params.put("action", "save_purchase");
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("other", "save_purchase")));
            params.put("user_id", mySPEdit.getUserId());
            params.put("token", mySPEdit.getToken());
            params.put("title", title);
            params.put("goods_name", shopName);
            params.put("goods_num", number);
            params.put("price_low", minPrice);
            params.put("price_up", maxPrice);
            params.put("address", title);
            params.put("user_name", mUserModel.getUser_name());
            params.put("user_phone", mUserModel.getMobile_phone());
            params.put("memo", remark);
            params.put("type", type);
            if (mInquiryModel!=null){
                params.put("id", mInquiryModel.getId());
            }
            HttpUtils.sendPost(params, this);
        } else {
            T.showShort(this, "请将信息补充完整");
            return;
        }
        if (mInquiryModel!=null){
            showProgress(PublishActivity.this, getString(R.string.jiazaizhong_dotdotdot));
        }else {
            showProgress(PublishActivity.this, getString(R.string.jiazaizhong_dotdotdot));
        }

//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                Message msg=Message.obtain();
//                msg.what=1;
//                mHandler.sendMessage(msg);
//            }
//        }, 5000);
//        if (!TextUtils.isEmpty())
    }

    private MyDialog myDialog;
    private RadioButton rb_default, rb_zhu, rb_niu, rb_yang, rb_qin, rb_hai, rb_other;
    private String type;
    @InjectView(R.id.tv_type_select)
    TextView tv_type_select;

    @OnClick(R.id.tv_type_select)
    public void select(View v) {
        type = tv_type_select.getTag().toString().trim();
        View view = LayoutInflater.from(this).inflate(R.layout.type_dialog, null);
        rb_default = (RadioButton) view.findViewById(R.id.rb_default);
        rb_zhu = (RadioButton) view.findViewById(R.id.rb_zhu);
        rb_niu = (RadioButton) view.findViewById(R.id.rb_niu);
        rb_yang = (RadioButton) view.findViewById(R.id.rb_yang);
        rb_qin = (RadioButton) view.findViewById(R.id.rb_qin);
        rb_hai = (RadioButton) view.findViewById(R.id.rb_hai);
        rb_other = (RadioButton) view.findViewById(R.id.rb_other);
        switch (type) {
            case "请选择":
                rb_default.setChecked(true);
                break;
            case "猪":
                rb_zhu.setChecked(true);
                break;
            case "牛":
                rb_niu.setChecked(true);
                break;
            case "羊":
                rb_yang.setChecked(true);
                break;
            case "禽类":
                rb_qin.setChecked(true);
                break;
            case "水产":
                rb_hai.setChecked(true);
                break;
            case "其它":
                rb_other.setChecked(true);
                break;
        }
        rb_default.setOnClickListener(this);
        rb_zhu.setOnClickListener(this);
        rb_niu.setOnClickListener(this);
        rb_yang.setOnClickListener(this);
        rb_qin.setOnClickListener(this);
        rb_hai.setOnClickListener(this);
        rb_other.setOnClickListener(this);
        rb_default.setTag("请选择");
        rb_zhu.setTag("猪");
        rb_niu.setTag("牛");
        rb_yang.setTag("羊");
        rb_qin.setTag("禽类");
        rb_hai.setTag("水产");
        rb_other.setTag("其他");
        myDialog = new MyDialog(this, view, R.style.dialog);
        myDialog.show();
    }

    private MyApplication myApplication;
    private UserModel mUserModel;
    private InquiryModel mInquiryModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_publish);
//        ButterKnife.inject(this);
        mTitle.setText(getString(R.string.fabuxunpan));
        myApplication = MyApplication.getInstance();
        mUserModel = new Gson().fromJson(mySPEdit.getUserInfo(), UserModel.class);
        if (getIntent().getExtras()!=null){
            mInquiryModel= (InquiryModel) getIntent().getExtras().get("data");
        }

        initView();
    }

    private void initView() {
        tv_personname.setText(mUserModel.getAlias());
        tv_personphone.setText(mUserModel.getMobile_phone());

        tv_type_select.setTag(getString(R.string.qingxuanze));
        if (mInquiryModel!=null){
            et_title.setText(mInquiryModel.getTitle());
            tv_type_select.setText(mInquiryModel.getType());
            tv_type_select.setTag(mInquiryModel.getType());
            et_shop.setText(mInquiryModel.getGoods_name());
            et_number.setText(mInquiryModel.getGoods_num());
            et_minprice.setText(mInquiryModel.getPrice_low());
            et_maxprice.setText(mInquiryModel.getPrice_up());
            et_remark.setText(mInquiryModel.getMemo());
            et_address.setText(mInquiryModel.getAddress());
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_publish;
    }

    @Override
    public void success(String response, int id) {
        dismissProgress(this);
        if (mInquiryModel!=null){
            T.showShort(this,"修改成功");
        }else {
            T.showShort(this,"发布成功");
        }
        finish();
    }

    @Override
    public void failByOther(String fail, int id) {
        L.e("222");
    }

    @Override
    public void fail(String fail, int id) {
        T.showShort(this,R.string.jianchawangluo);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rb_default:
                tv_type_select.setText(getString(R.string.qingxuanze));
                tv_type_select.setTag(getString(R.string.qingxuanze));
                break;
            case R.id.rb_zhu:
                tv_type_select.setText(getString(R.string.zhu));
                tv_type_select.setTag("猪");
                break;
            case R.id.rb_niu:
                tv_type_select.setText(getString(R.string.niu));
                tv_type_select.setTag("牛");
                break;
            case R.id.rb_yang:
                tv_type_select.setText(getString(R.string.yang));
                tv_type_select.setTag("羊");
                break;
            case R.id.rb_qin:
                tv_type_select.setText(getString(R.string.qinlei));
                tv_type_select.setTag("禽类");
                break;
            case R.id.rb_hai:
                tv_type_select.setText(getString(R.string.shuichan));
                tv_type_select.setTag("水产");
                break;
            case R.id.rb_other:
                tv_type_select.setText(getString(R.string.qita));
                tv_type_select.setTag("其他");
                break;
        }

        myDialog.dismiss();
    }


}
