package taiyang.com.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;

import org.kymjs.kjframe.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;

import butterknife.InjectView;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.RegularUtils;
import taiyang.com.utils.T;
import taiyang.com.utils.ToastUtil;

/*
    新增取货人
 */
public class AddPersonActivity extends BaseActivity implements View.OnClickListener {

    @InjectView(R.id.bt_addperson_bc)
    Button bt_addperson_bc;
    @InjectView(R.id.et_addperson_name)
    EditText et_addperson_name;
    @InjectView(R.id.et_addperson_number)
    EditText et_addperson_number;
    @InjectView(R.id.et_addperson_phone)
    EditText et_addperson_phone;
    @InjectView(R.id.tv_get_code)
    TextView tvGetCode;
    @InjectView(R.id.et_addperson_code)
    EditText etAddpersonCode;
    int type;
    private String addperson_name;
    private String addperson_number;
    private String addperson_phone;
    private TimeCount time;
    private String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle.setText(getString(R.string.xinzengdizhi));
        initListeren();
        initData();

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_addperson;
    }

    private void initListeren() {
        bt_addperson_bc.setOnClickListener(this);
        tvGetCode.setOnClickListener(this);
    }

    private void initData() {
        type = getIntent().getIntExtra("seller", 0);
        addperson_name = et_addperson_name.getText().toString();
        addperson_number = et_addperson_number.getText().toString();
        addperson_phone = et_addperson_phone.getText().toString();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_get_code:
                phone = et_addperson_phone.getText().toString();
                if (RegularUtils.isMobileExact(phone)) {
                    Map<String, Object> params = new HashMap<>();
                    params.put("model", "user");
                    params.put("action", "send_mobile_code");
                    params.put("mobile", phone);
                    params.put("mobile_sign", MD5Utils.encode(MD5Utils.getMobileSign(phone)));
                    params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "send_mobile_code")));
                    L.e(JSON.toJSONString(params));
                    HttpUtils.sendPost(params, this, 100);
                    time = new TimeCount(60000, 1000);
                    time.start();
                } else {
                    T.showShort(this, "请输入正确的电话号码");
                }

                break;
            case R.id.bt_addperson_bc:
                addperson_name = et_addperson_name.getText().toString();
                addperson_number = et_addperson_number.getText().toString();
                addperson_phone = et_addperson_phone.getText().toString();
                if (!RegularUtils.isMobileExact(addperson_phone)) {
                    T.showShort(this, "请输入正确的电话号码!");
                    return;
                }
                if (!RegularUtils.isIDCard18(addperson_number)) {
                    T.showShort(this, "请输入正确的身份证号码!");
                    return;
                }
                if (StringUtils.isEmpty(etAddpersonCode.getText().toString())) {
                    T.showShort(this, R.string.qingshuruyanzhengma);
                    return;
                }
                if (!TextUtils.isEmpty(addperson_name) && !TextUtils.isEmpty(addperson_number) && !TextUtils.isEmpty(addperson_phone)) {
                    Map<String, Object> params = new HashMap<>();
                    params.put("model", "user");
                    params.put("action", "save_address");
                    params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "save_address")));
                    params.put("user_id", mySPEdit.getUserId());
                    params.put("token", mySPEdit.getToken());
                    params.put("name", addperson_name);
                    params.put("id_number", addperson_number);
                    params.put("mobile_code", Integer.parseInt(etAddpersonCode.getText().toString()));
                    params.put("mobile", addperson_phone);
                    params.put("type", type);
                    L.e("添加的:" + JSON.toJSONString(params));
                    HttpUtils.sendPost(params, this, 101);
                } else {
                    T.showShort(this, getString(R.string.qingjiangxinxibuchongwanzheng));
                }
                break;

            default:
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (time != null) {
            time.cancel();
        }
    }

    @Override
    public void success(String response, int id) {

        switch (id) {
            case 100://获取验证码
                L.e("获取验证码成功");
                break;
            case 101://注册
                T.showShort(this, getString(R.string.tianjiachenggong));
                finish();
                break;
        }
    }

    @Override
    public void failByOther(String fail, int id) {
        ToastUtil.showToast(fail);
    }

    @Override
    public void fail(String fail, int id) {
        T.showShort(this, getString(R.string.jianchawangluo));
    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {// 计时完毕
            tvGetCode.setText("获取验证码");
            tvGetCode.setBackground(getDrawable(R.drawable.shape_corner_15));
            tvGetCode.setClickable(true);
        }

        @Override
        public void onTick(long millisUntilFinished) {// 计时过程
            tvGetCode.setClickable(false);//防止重复点击
            tvGetCode.setBackground(getDrawable(R.drawable.shape_corner_15_gray));
            tvGetCode.setText(millisUntilFinished / 1000 + "s");
        }
    }


}
