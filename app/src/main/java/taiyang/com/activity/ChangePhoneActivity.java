package taiyang.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.Constants;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.RegularUtils;
import taiyang.com.utils.T;
import taiyang.com.utils.ToastUtil;
import taiyang.com.view.ClearEditText;

/**
 * 修改手机号界面
 */
public class ChangePhoneActivity extends BaseActivity {

    @InjectView(R.id.et_phone)
    ClearEditText etPhone;
    @InjectView(R.id.et_verfify)
    ClearEditText etVerfify;
    @InjectView(R.id.et_phone_country)
    ClearEditText et_phone_country;

    @InjectView(R.id.bt_commit)
    Button commitButton;

    @OnClick(R.id.bt_commit)
    public void commit(View v){
        showProgress(this,getString(R.string.jiazaizhong_dotdotdot));
        String code=etVerfify.getText().toString();
        Map<String,Object> params=new HashMap<>();
        params.put("model","user");
        params.put("action","change_mobile");
        params.put("sign",MD5Utils.encode(MD5Utils.getSign("user","change_mobile")));
        params.put("mobile",phone);
        params.put("user_id",mySPEdit.getUserId());
        params.put("token",mySPEdit.getToken());
        params.put("code",code);
        HttpUtils.sendPost(params,this,101);
    }

    @InjectView(R.id.tv_verify)
    TextView tvVerify;
    private String phone;

    @OnClick(R.id.tv_verify)
    public void sendVerify(View v){
        phone = etPhone.getText().toString();
        if (RegularUtils.isMobileExact(phone)) {
//            if (phone!=null){
            time = new TimeCount(60000, 1000);
            time.start();
            Map<String, Object> params = new HashMap<>();
            params.put("model", "user");
            params.put("action", "send_mobile_code");
            params.put("mobile", phone);
            params.put("nation_code", et_phone_country.getText().toString());
            params.put("mobile_sign", MD5Utils.encode(MD5Utils.getMobileSign(phone)));
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "send_mobile_code")));
            L.e(JSON.toJSONString(params));
            HttpUtils.sendPost(params, this, 100);
        } else {
            T.showShort(this, "请输入正确的电话号码");
        }
    }
    @InjectView(R.id.tv_phonenumber)
    TextView tv_phonenumber;

    private boolean flag;
    private TimeCount time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle.setText(R.string.genggaishoujihaoma);
        String phone=getIntent().getStringExtra("phone");
        tv_phonenumber.setText(phone);
        initListener();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_change_phone;
    }

    private void initListener() {
        etPhone.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)}); //最大输入长度
        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etPhone.getText().toString().length() != 0) {
                    tvVerify.setBackgroundColor(getResources().getColor(R.color.change_bg));
                    tvVerify.setTextColor(getResources().getColor(R.color.white));
                    tvVerify.setEnabled(true);
                    commitButton.setEnabled(false);
                    flag = true;
                } else {
                    tvVerify.setBackgroundResource(R.drawable.verfiy_bg);
                    tvVerify.setTextColor(getResources().getColor(R.color.textColorVerify));
                    commitButton.setBackgroundColor(getResources().getColor(R.color.message_bg2));
                    commitButton.setEnabled(false);
                    tvVerify.setEnabled(false);
                    flag = false;
                }
            }
        });
        etVerfify.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etVerfify.getText().toString().length() > 4) {
                    if (flag) {
                        commitButton.setBackgroundColor(getResources().getColor(R.color.change_bg));
                        commitButton.setEnabled(true);
                    }
                } else {
                    commitButton.setBackgroundColor(getResources().getColor(R.color.message_bg2));
                    commitButton.setEnabled(false);
                }
            }
        });
    }

    @Override
    public void success(String response, int id) {
        switch (id){
            case 101:
                dismissProgress(this);
                T.showShort(this,"修改成功");
                //数据是使用Intent返回
                Intent intent = new Intent();
                intent.putExtra("newphone",phone);
                //设置返回数据
                ChangePhoneActivity.this.setResult(Constants.CHANGEPHONE, intent);
                //关闭Activity
                ChangePhoneActivity.this.finish();
                break;
        }
    }

    @Override
    public void failByOther(String fail, int id) {
        ToastUtil.showToast(fail);
        dismissProgress(this);
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(this);
        T.showShort(this,R.string.jianchawangluo);
    }


    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {// 计时完毕
            tvVerify.setText(R.string.huoquyanzhengma);
            tvVerify.setClickable(true);
            tvVerify.setBackgroundColor(getResources().getColor(R.color.change_bg));
            tvVerify.setTextColor(getResources().getColor(R.color.white));
        }

        @Override
        public void onTick(long millisUntilFinished) {// 计时过程
            tvVerify.setClickable(false);//防止重复点击
            tvVerify.setText(millisUntilFinished / 1000 + "s");
            tvVerify.setBackgroundResource(R.drawable.verfiy_bg);
            tvVerify.setTextColor(getResources().getColor(R.color.textColorVerify));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (time != null) {
            time.cancel();
        }
    }

}
