package taiyang.com.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.cundong.recyclerview.CustRecyclerView;
import com.cundong.recyclerview.HeaderAndFooterRecyclerViewAdapter;
import com.cundong.recyclerview.RecyclerOnScrollListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.InjectView;
import taiyang.com.adapter.InquiryListMoreAdapter;
import taiyang.com.entity.InquiryListMoreContentBean;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.T;
import taiyang.com.view.ErrorLayout;

/**
 * 筛选列表activity
 */
public class PureInquiryListActivity extends BaseActivity implements View.OnClickListener {

    protected HeaderAndFooterRecyclerViewAdapter mRecyclerViewAdapter;
    protected RecyclerOnScrollListener mOnScrollListener = new RecyclerOnScrollListener() {

        @Override
        public void onScrolled(int dx, int dy) {
            super.onScrolled(dx, dy);

//            if(null != headerView) {
//                if (dy == 0 || dy < headerView.getHeight()) {
//                    toTopBtn.setVisibility(View.GONE);
//                }
//            }


        }

        @Override
        public void onScrollUp() {
            // 滑动时隐藏float button
//            if (toTopBtn.getVisibility() == View.VISIBLE) {
//                toTopBtn.setVisibility(View.GONE);
//                animate(toTopBtn, R.anim.floating_action_button_hide);
//            }
        }

        @Override
        public void onScrollDown() {
//            if (toTopBtn.getVisibility() != View.VISIBLE) {
//                toTopBtn.setVisibility(View.VISIBLE);
//                animate(toTopBtn, R.anim.floating_action_button_show);
//            }
        }


    };

    @InjectView(R.id.tv_title)
    TextView tv_title;
    @InjectView(R.id.recycler_view)
    CustRecyclerView mRecyclerView;
    @InjectView(R.id.error_layout)
    ErrorLayout mErrorLayout;
    private InquiryListMoreAdapter mAdapter;
    private String type;
    private List<InquiryListMoreContentBean.ContentBean.GoodsListBean> mDatas;
    private Map<String, Object> params;
    private boolean clearFlag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initViews();
        initData();

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_inquirylist_pure;
    }

    private void initData() {
        params = new HashMap<>();

        params.put("model", "other");
        params.put("action", "productList");
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("other", "productList")));
        params.put("type", type);
        L.e("傳遞的參數:" + JSON.toJSONString(params));
        HttpUtils.sendPost(params, this);
    }

    private void initViews() {
        L.e("测试map......:" + JSON.toJSONString(params));
        tv_title.setText(getIntent().getStringExtra("title"));
        type = getIntent().getStringExtra("type");
        mDatas = new ArrayList<>();
        mAdapter = new InquiryListMoreAdapter(this);
        mAdapter.setDataList(mDatas);
        mRecyclerViewAdapter = new HeaderAndFooterRecyclerViewAdapter(this, mAdapter);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addOnScrollListener(mOnScrollListener);
        mRecyclerView.setIsRefreshing(false);
        mErrorLayout.setOnLayoutClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mErrorLayout.setErrorType(ErrorLayout.NETWORK_LOADING);
                requestData();
            }
        });

        mAdapter.setOnItemClickLitener(new InquiryListMoreAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_ila_back://退出
                finish();
                break;

            default:
                break;

        }
    }

    @Override
    public void success(String response, int id) {
        dismissProgress(this);
            mRecyclerView.scrollToPosition(0);
        InquiryListMoreContentBean.ContentBean inquiryListBean = new Gson().fromJson(response, InquiryListMoreContentBean.ContentBean.class);
        mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
//                mRecyclerView.loadMoreComplete();
        for (InquiryListMoreContentBean.ContentBean.GoodsListBean goodsListBean : inquiryListBean.getGoods_list()) {
            mDatas.add(goodsListBean);
        }
        mAdapter.setDataList(mDatas);
        mRecyclerView.refreshComplete();
        mAdapter.notifyDataSetChanged();
    }

    private void requestData() {
        params.put("type", type);
        HttpUtils.sendPost(params, this);
    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(this);
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(this);
        T.showShort(this, R.string.jianchawangluo);
    }
}
