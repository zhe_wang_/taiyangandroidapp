package taiyang.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;

import java.util.HashMap;
import java.util.Map;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.entity.UserModel;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MyApplication;
import taiyang.com.utils.T;

/**
 * 登录Activity
 */

public class LoginActivity extends BaseActivity {
    @InjectView(R.id.tv_register)
    TextView registerButton;
    @InjectView(R.id.et_phone)
    EditText etPhone;
    @InjectView(R.id.et_password)
    EditText etPassword;
    @InjectView(R.id.et_phone_bg)
    View phoneBg;
    @InjectView(R.id.et_password_bg)
    View passwordBg;
    //微信登录
    @InjectView(R.id.tv_wechat)
    TextView tv_wechat;
    @InjectView(R.id.tv_fpassword)
    TextView tv_fpassword;
    @InjectView(R.id.bt_login)
    Button loginButton;
    private UMShareAPI mShareAPI = null;//友盟登录的api
    private String action;//用于登录成功后进入对应的界面
    private String title;
    private UserModel mUserModel;
    private MyApplication application;

    //    @InjectView(R.id.back_layout)
//    LinearLayout backLayout;
    private String my;
    private String userName;
    private String password;
    /**
     * auth callback interface
     **/
    private UMAuthListener umAuthListener = new UMAuthListener() {
        @Override
        public void onComplete(SHARE_MEDIA platform, int action, final Map<String, String> data) {
//            Toast.makeText(getApplicationContext(), "Authorize succeed", Toast.LENGTH_SHORT).show();
//            L.e("onComplete  action:---"+action);
//            L.e("user info:"+data.toString());
            /*action
            0  授权登录
            2  获取用户信息
            */
            switch (action) {
                case 0:
                    mShareAPI.getPlatformInfo(LoginActivity.this, platform, umAuthListener);
                    break;
                case 2:
                    L.e("用户信息:" + JSON.toJSONString(data));
                    Map<String, Object> params = new HashMap<>();
                    params.put("openid", data.get("openid"));
                    params.put("unionid", data.get("unionid"));
                    params.put("model", "login");
                    params.put("action", "wxLogin");
                    params.put("device_type", 2);
                    params.put("device_token", mySPEdit.getDeviceToken());
                    params.put("sign", MD5Utils.encode(MD5Utils.getSign("login", "wxLogin")));
                    HttpUtils.sendPost(params, new HttpRequestListener() {
                        @Override
                        public void success(String response, int id) {
                            LoginActivity.this.success(response, id);
                        }

                        @Override
                        public void failByOther(String fail, int id) {
                            Intent intent = new Intent(LoginActivity.this, BindLoginActivity.class);
                            intent.putExtra("my", my);
                            intent.putExtra("openid", data.get("openid"));
                            intent.putExtra("unionid", data.get("unionid"));
                            intent.putExtra("profile_image_url", data.get("profile_image_url"));
                            intent.putExtra("screen_name", data.get("screen_name"));
                            startActivityForResult(intent, 1001);
                        }

                        @Override
                        public void fail(String fail, int id) {
                        }
                    });
                    break;
            }
        }

        @Override
        public void onError(SHARE_MEDIA platform, int action, Throwable t) {
            Toast.makeText(getApplicationContext(), "Authorize fail", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCancel(SHARE_MEDIA platform, int action) {
            L.e("onCancel  action:---" + action);
            Toast.makeText(getApplicationContext(), "Authorize cancel", Toast.LENGTH_SHORT).show();
        }
    };

    @OnClick(R.id.tv_wechat)
    public void loginByWeiChat(View v) {
        SHARE_MEDIA platform = SHARE_MEDIA.WEIXIN;
        mShareAPI.doOauthVerify(LoginActivity.this, platform, umAuthListener);
    }

    //忘记密码
    @OnClick(R.id.tv_fpassword)
    public void reset(View v) {
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        startActivity(intent);
    }

    //注册
    @OnClick(R.id.tv_register)
    public void register(View v) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.back_layout)
    public void back(View v) {
//        finish();
        if (my != null) {
            endActivity(100);
        } else {

            finish();
        }
    }

    private void endActivity(int code) {
        //数据是使用Intent返回
        Intent intent = new Intent();
        //设置返回数据
        LoginActivity.this.setResult(code, intent);
        //关闭Activity
        LoginActivity.this.finish();
    }

    @OnClick(R.id.bt_login)
    public void login(View v) {
        userName = etPhone.getText().toString();
        password = etPassword.getText().toString();
        if (TextUtils.isEmpty(userName)) {
            Toast.makeText(this, "请输入用户名", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "请输入密码", Toast.LENGTH_LONG).show();
            return;
        }
        showProgress(LoginActivity.this, getString(R.string.jiazaizhong_dotdotdot));
        Map<String, Object> params = new HashMap<>();
        params.put("model", "user");
        params.put("action", "login");
        params.put("device_token", mySPEdit.getDeviceToken());
        params.put("device_type", 2);
        params.put("user_name", userName);
        params.put("password", password);
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "login")));
        HttpUtils.sendPost(params, this, 100);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle.setText(getString(R.string.yonghudenglu));
        mShareAPI = UMShareAPI.get(this);
        initData();
        initListener();
    }

    private void initData() {
        application = MyApplication.getInstance();
        application.addActivity(this);
        my = getIntent().getStringExtra("my");
        action = getIntent().getStringExtra("action");
        title = getIntent().getStringExtra("title");
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_login;
    }

    private void initListener() {
        etPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    phoneBg.setBackgroundColor(getResources().getColor(R.color.bg_bluecolor));
                } else {
                    phoneBg.setBackgroundColor(getResources().getColor(R.color.bg_color));
                }
            }
        });
        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    passwordBg.setBackgroundColor(getResources().getColor(R.color.bg_bluecolor));
                } else {
                    passwordBg.setBackgroundColor(getResources().getColor(R.color.bg_color));
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK) {
            if (my != null) {
                endActivity(100);
            }
        }
        return super.onKeyDown(keyCode, event);
    }



    @Override
    public void success(String response, int id) {
//        dismissProgress(LoginActivity.this);
        dismissProgress(LoginActivity.this);
        mUserModel = new Gson().fromJson(response, UserModel.class);
        mySPEdit.setIsLogin(true);
        mySPEdit.setToken(mUserModel.getToken());
        mySPEdit.setUserId(mUserModel.getUser_id());
        mySPEdit.setAlias(mUserModel.getAlias());
        mySPEdit.setMobilePhone(mUserModel.getMobile_phone());
        mySPEdit.setUserName(userName);
        application.setCurrentUser(mUserModel);
        mySPEdit.setFirstLogin(mUserModel.getLast_login());
        mySPEdit.setUserInfo(response);
//        Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_LONG).show();
        T.showShort(this, getString(R.string.dengluchenggong));
        finish();
//        if (my != null) {
//            if (my.equals("我的")) {
//                endActivity(0);
//            } else if (my.equals("服务")) {
//                endActivity(1);
//            }
//        }
//        if (action != null) {
//            if (title != null) {
//                Intent intent = new Intent();
//                intent.setAction(action);
//                intent.putExtra("title", title);
//                startActivity(intent);
//                finish();
//            } else {
//                //报盘信息未登录，登录成功跳转 确认订单
//                Intent intent = new Intent();
//                intent.setAction(action);
//                startActivity(intent);
//                finish();
//            }
//
//        }
    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(LoginActivity.this);
//        Toast.makeText(this, fail, Toast.LENGTH_LONG).show();
        T.showShort(this, fail);
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(LoginActivity.this);
        T.showShort(this, R.string.jianchawangluo);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mShareAPI.onActivityResult(requestCode, resultCode, data);
        if(resultCode==1001&&requestCode==1001){
            LoginActivity.this.success(data.getStringExtra("response"),0);
        }
    }
}
