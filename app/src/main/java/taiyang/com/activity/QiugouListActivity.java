package taiyang.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.adapter.QiugouListAdapter;
import taiyang.com.entity.QiugouListBean;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;

//求购列表
public class QiugouListActivity extends BaseActivity implements View.OnClickListener{

    @InjectView(R.id.lv_qiugoulist)
    ListView lv_qiugoulist;
    @InjectView(R.id.rb_zhu)
    RadioButton rbZhu;
    @InjectView(R.id.rb_niu)
    RadioButton rbNiu;
    @InjectView(R.id.rb_yang)
    RadioButton rbYang;
    @InjectView(R.id.rb_qinlei)
    RadioButton rbQinlei;
    @InjectView(R.id.rb_shuichan)
    RadioButton rbShuichan;
    @InjectView(R.id.rb_qita)
    RadioButton rbQita;
    private QiugouListAdapter mAdapter;
    private int page = 1;
    private int pageCount = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.inject(this);
        mAdapter = new QiugouListAdapter(new ArrayList<QiugouListBean.ListBean>(), this);
        lv_qiugoulist.setAdapter(mAdapter);
        lv_qiugoulist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(QiugouListActivity.this,QiugouInfoActivity.class);
                intent.putExtra("id",mAdapter.getItemId(i)+"");
                startActivity(intent);
            }
        });
        rbZhu.setOnClickListener(this);
        rbNiu.setOnClickListener(this);
        rbYang.setOnClickListener(this);
        rbQinlei.setOnClickListener(this);
        rbShuichan.setOnClickListener(this);
        rbQita.setOnClickListener(this);

        rbZhu.setTag("猪");
                rbNiu.setTag("牛");
        rbYang.setTag("羊");
                rbQinlei.setTag("禽类");
        rbShuichan.setTag("水产");
                rbQita.setTag("其他");
        rbZhu.performClick();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_qiuguo_list;
    }

    private void initData() {
        Map<String, Object> params = new HashMap<>();
        params.put("model", "purchase");
        params.put("action", "purchaseList");
        params.put("type", type);
        params.put("page", page);
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("purchase", "purchaseList")));
        HttpUtils.sendPost(params,this);
    }

    String type = "猪";


    @Override
    public void success(String response, int id) {
        dismissProgress(QiugouListActivity.this);
        QiugouListBean qiugouListBean = new Gson().fromJson(response, QiugouListBean.class);
        if (qiugouListBean == null) return;
        pageCount = qiugouListBean.getTotal().getPage_count();
        page = qiugouListBean.getTotal().getPage();
        mAdapter.setmList(qiugouListBean.getList());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

    @Override
    public void onClick(View view) {
        showProgress(this,getString(R.string.jiazaizhong_dotdotdot));
        mAdapter.getmList().clear();
        mAdapter.notifyDataSetChanged();
        page  =1;
        type  = view.getTag().toString();
        initData();
    }
}
