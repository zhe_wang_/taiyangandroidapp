package taiyang.com.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.entity.SellerOrderModel;
import taiyang.com.tydp_b.KProgressActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MyApplication;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.ToastUtil;

/**
 * 订单详情 - 
 */
public class OrderDetailSellerActivity extends KProgressActivity implements View.OnClickListener {

    @InjectView(R.id.iv_mjorderdetail_logo)
    SimpleDraweeView iv_mjorderdetail_logo;
    @InjectView(R.id.tv_mjordername)
    TextView tv_mjordername;
    @InjectView(R.id.tv_mjorderdetail_unit3)
    TextView tv_mjorderdetail_unit3;
    @InjectView(R.id.tv_mjorderdetail_num)
    TextView tv_mjorderdetail_num;
    @InjectView(R.id.tv_mjorderdetail_status)
    TextView tv_mjorderdetail_status;
    @InjectView(R.id.tv_mjorderdetail_ff)
    TextView tv_mjorderdetail_ff;
    @InjectView(R.id.tv_mjorderprice)
    TextView tv_mjorderprice;
    @InjectView(R.id.tv_mjordercount)
    TextView tv_mjordercount;

    @InjectView(R.id.tv_price)
    TextView tv_price;
    @InjectView(R.id.tv_realprice)
    TextView tv_realprice;
    @InjectView(R.id.tv_mjorderdetail_name)
    TextView tv_mjorderdetail_name;
    @InjectView(R.id.tv_mjorderdetail_qhr)
    TextView tv_mjorderdetail_qhr;
    @InjectView(R.id.tv_mjorderdetail_sfznum)
    TextView tv_mjorderdetail_sfznum;
    @InjectView(R.id.tv_mjorderdetail_lxphone)
    TextView tv_mjorderdetail_lxphone;
    @InjectView(R.id.tv_mjorderdetail_phone)
    TextView tv_mjorderdetail_phone;
    @InjectView(R.id.ll_mjorderdetail_back)
    LinearLayout ll_mjorderdetail_back;
    @InjectView(R.id.bt_wuhuo)
    Button bt_wuhuo;
    @InjectView(R.id.bt_youhuo)
    Button bt_youhuo;
    @InjectView(R.id.rl_mj_name)
    View rl_mj_name;
    @InjectView(R.id.rl_mj_phone)
    View rl_mj_phone;
    @InjectView(R.id.ll_mjorderdetail_qhrinfo)
    LinearLayout ll_mjorderdetail_qhrinfo;

    @InjectView(R.id.et_queren)
    EditText et_queren;

    @InjectView(R.id.tv_paid_price)
    TextView tv_paid_price;
    @InjectView(R.id.ll_orderdetail_personinfo)
    LinearLayout ll_orderdetail_personinfo;

    @InjectView(R.id.ll_orderdetail_personinfo_seller)
    LinearLayout ll_orderdetail_personinfo_seller;
    @InjectView(R.id.tv_personinfo_name)
    TextView tv_personinfo_name;
    @InjectView(R.id.tv_personinfo_num)
    TextView tv_personinfo_num;
    @InjectView(R.id.tv_personinfo_phone)
    TextView tv_personinfo_phone;

    @InjectView(R.id.tv_personinfo_name_seller)
    TextView tv_personinfo_name_seller;
    @InjectView(R.id.tv_personinfo_num_seller)
    TextView tv_personinfo_num_seller;
    @InjectView(R.id.tv_personinfo_phone_seller)
    TextView tv_personinfo_phone_seller;
    SellerOrderModel mSellerOrderModel;
    private String order_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_seller_detail);
        ButterKnife.inject(this);
        initListeren();
        initData();

    }

    private void initListeren(){
        bt_wuhuo.setOnClickListener(this);
        ll_mjorderdetail_back.setOnClickListener(this);
        bt_youhuo.setOnClickListener(this);
        bt_orderdetail_address.setOnClickListener(this);
        bt_orderdetail_address.setVisibility(View.GONE);
    }



    private  void initData(){
        showProgress(this,getString(R.string.jiazaizhong_dotdotdot));
        order_id = getIntent().getStringExtra("order_id");
        Map<String, Object> params = new HashMap<>();
        params.put("model", "seller");
        params.put("action", "order_info");
        params.put("order_id", order_id);
        params.put("user_id", MySPEdit.getInstance(this).getUserId());
        params.put("token", MySPEdit.getInstance(this).getToken());
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller", "order_info")));
        HttpUtils.sendPost(params, new HttpRequestListener() {
            @Override
            public void success(String response, int id) {

                mSellerOrderModel = new Gson().fromJson(response, SellerOrderModel.class);
                    iv_mjorderdetail_logo.setImageURI(Uri.parse(mSellerOrderModel.getGoods_info().getGoods_thumb()));
                    tv_mjordername.setText(mSellerOrderModel.getGoods_info().getGoods_name()+mSellerOrderModel.getGoods_info().getBrand_sn());
                    tv_mjorderprice.setText(mSellerOrderModel.getGoods_info().getFormated_goods_price());
                    tv_price.setText("¥"+mSellerOrderModel.getGoods_amount());
                    tv_realprice.setText("¥"+mSellerOrderModel.getOrder_amount());
                    tv_mjorderdetail_num.setText(mSellerOrderModel.getOrder_sn());
                    tv_mjorderdetail_status.setText(mSellerOrderModel.getOrder_status_name());

                    tv_mjorderdetail_unit3.setText("   "+(mSellerOrderModel.getGoods_info().getIs_retail().equals("1")?mSellerOrderModel.getGoods_info().getPart_number():mSellerOrderModel.getGoods_info().getPart_weight())
                            +mSellerOrderModel.getGoods_info().getPart_unit()+"    "+
                    mSellerOrderModel.getGoods_info().getGoods_number()+mSellerOrderModel.getGoods_info().getMeasure_unit());


                    tv_mjorderdetail_ff.setText(mSellerOrderModel.getPay_name());
                        rl_mj_name.setVisibility(View.VISIBLE);
                        rl_mj_phone.setVisibility(View.VISIBLE);
                        tv_mjorderdetail_name.setText(mSellerOrderModel.getUser_info().getName());
                        tv_mjorderdetail_phone.setText(mSellerOrderModel.getUser_info().getMobile());
                if(mSellerOrderModel.getStock_status().equals("0")){
                        findViewById(R.id.ll_queren).setVisibility(View.VISIBLE);
                }else{
                    findViewById(R.id.ll_queren).setVisibility(View.GONE);
                }
                if (mSellerOrderModel.getBuyer_seller_edit().equals("1") && mSellerOrderModel.getStock_status().equals("1")&&mSellerOrderModel.getSend_address().getName().equals("")) {
                    bt_orderdetail_address.setVisibility(View.VISIBLE);
                }
                if(!mSellerOrderModel.getGet_address().getName().equals("")){
                    ll_orderdetail_personinfo.setVisibility(View.VISIBLE);
                    tv_personinfo_name.setText(mSellerOrderModel.getGet_address().getName());
                    tv_personinfo_num.setText(mSellerOrderModel.getGet_address().getId_number());
                    tv_personinfo_phone.setText(mSellerOrderModel.getGet_address().getMobile());
                }else{
                    ll_orderdetail_personinfo.setVisibility(View.GONE);
                }
                if(!mSellerOrderModel.getSend_address().getName().equals("")){
                    ll_orderdetail_personinfo_seller.setVisibility(View.VISIBLE);
                    tv_personinfo_name_seller.setText(mSellerOrderModel.getSend_address().getName());
                    tv_personinfo_num_seller.setText(mSellerOrderModel.getSend_address().getId_number());
                    tv_personinfo_phone_seller.setText(mSellerOrderModel.getSend_address().getMobile());
                }else{
                    ll_orderdetail_personinfo_seller.setVisibility(View.GONE);
                }
                if("".equals(mSellerOrderModel.getMoney_paid())){
                    findViewById(R.id.rl_moeny_pard).setVisibility(View.GONE);
                }
                tv_paid_price.setText(mSellerOrderModel.getMoney_paid());
                dismissProgress(OrderDetailSellerActivity.this);

            }

            @Override
            public void failByOther(String fail, int id) {
                L.e("failByOther   "+fail);
            }

            @Override
            public void fail(String fail, int id) {
                L.e("fail   "+fail);
            }
        });

    }

    private void actionQueren(int stock_status){
        Map<String, Object> params = new HashMap<>();
        params.put("model", "seller");
        params.put("action", "stock_status");
        params.put("order_id", order_id);
        params.put("stock_status", stock_status);
        params.put("stock_remark", et_queren.getText().toString());
        params.put("user_id", MySPEdit.getInstance(this).getUserId());
        params.put("token", MySPEdit.getInstance(this).getToken());
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller", "stock_status")));
        HttpUtils.sendPost(params, new HttpRequestListener() {
            @Override
            public void success(String response, int id) {
                ToastUtil.showToast(getString(R.string.caozuochenggong));
                finish();
                Intent intent2 = new Intent(MyApplication.getContext(),OrderDetailSellerActivity.class);
                intent2.putExtra("order_id",order_id);
                startActivity(intent2);
            }

            @Override
            public void failByOther(String fail, int id) {
                ToastUtil.showToast(fail);
            }

            @Override
            public void fail(String fail, int id) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_wuhuo:
                actionQueren(2);
                break;
            case R.id.bt_youhuo:
                actionQueren(1);
                break;
            case R.id.ll_mjorderdetail_back:
                finish();
                break;
            case R.id.bt_orderdetail_address:
                Intent i2 = new Intent(this, PersonInfoActivity.class);
                i2.putExtra("seller",1);
                startActivityForResult(i2, 10);
                break;
            default:
                break;
        }
    }

    @InjectView(R.id.bt_orderdetail_address)
    Button bt_orderdetail_address;

    private void restartPage() {
        finish();
        Intent intent = new Intent(this, OrderDetailSellerActivity.class);
        intent.putExtra("order_id", order_id);
        startActivity(intent);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case 10:
                if (data==null) return;
                if (!data.hasExtra("address_id")) return;
                if (data.getStringExtra("address_id") == null) return;
                String address_id = data.getStringExtra("address_id");
                Map<String, Object> params = new HashMap<>();
                params.put("model", "order");
                params.put("action", "save_order_address");
                params.put("order_id", order_id);
                params.put("address_id", address_id);
                //卖家添加地址
                params.put("type","1");
                params.put("user_id", MySPEdit.getInstance(this).getUserId());
                params.put("token", MySPEdit.getInstance(this).getToken());
                params.put("sign", MD5Utils.encode(MD5Utils.getSign("order", "save_order_address")));
                HttpUtils.sendPost(params, new HttpRequestListener() {
                    @Override
                    public void success(String response, int id) {
                        ToastUtil.showToast(response);
                        restartPage();
                    }

                    @Override
                    public void failByOther(String fail, int id) {
                        L.e("failByOther" + fail);
                    }

                    @Override
                    public void fail(String fail, int id) {
                        L.e("fail" + fail);
                    }
                });

                break;

            default:
                break;
        }
    }
}
