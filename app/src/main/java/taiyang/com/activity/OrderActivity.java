package taiyang.com.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import taiyang.com.fragment.OrderFragment;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.T;

/**
 * 全部订单界面
 */
public class OrderActivity extends BaseActivity {

    @InjectView(R.id.tabs)
    TabLayout tabLayout;
    @InjectView(R.id.vp_view)
    ViewPager mViewPager;

    private List<String> mTitleList;
    private List<Fragment> mFragmentList;
    private ViewAdapter mAdapter;
    private OrderFragment mOrderFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle.setText(R.string.quanbudingdan);
        initTitle();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_order;
    }

    private void initTitle() {
        mTitleList = new ArrayList<>();
        mTitleList.add(getString(R.string.quanbu));
        mTitleList.add(getString(R.string.daifukuan));
        mTitleList.add(getString(R.string.yifukuan));
        mTitleList.add(getString(R.string.yiyufu));
        mTitleList.add(getString(R.string.yiwancheng));
        mFragmentList = new ArrayList<>();
        for (String s : mTitleList) {
            Bundle data = new Bundle();
            data.putString("tabs", s);
            mOrderFragment = new OrderFragment();
            mOrderFragment.setArguments(data);
            mFragmentList.add(mOrderFragment);
        }
        mAdapter = new ViewAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(mViewPager);//设置viewPaper和tabs关联
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {
        T.showShort(this,R.string.jianchawangluo);
    }

    class ViewAdapter extends FragmentPagerAdapter {

        public ViewAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitleList.get(position);
        }
    }
}
