package taiyang.com.activity;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumFile;
import com.yanzhenjie.album.AlbumListener;

import org.feezu.liuli.timeselector.TimeSelector;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.entity.ChangeOfferInfoBean;
import taiyang.com.entity.SellChanghaoBean;
import taiyang.com.entity.SellChanpinmingchengBean;
import taiyang.com.entity.SellIndexBean;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.T;
import taiyang.com.utils.ToastUtil;

/**
 * 发布报盘
 */
public class SellActivity extends BaseActivity {

    @InjectView(R.id.tv_title)
    TextView tvTitle;
    @InjectView(R.id.tv_kucun)
    TextView tvKucun;

    @InjectView(R.id.title_layout)
    RelativeLayout titleLayout;
    @InjectView(R.id.et_sell_type_title)
    TextView etSellTypeTitle;
    @InjectView(R.id.rb_sell_time_xianhuo)
    RadioButton rbSellTimeXianhuo;
    @InjectView(R.id.rb_sell_time_qihuo)
    RadioButton rbSellTimeQihuo;
    @InjectView(R.id.rg_sell_time)
    RadioGroup rgSellTime;
    @InjectView(R.id.rb_sell_type_lingshou)
    RadioButton rbSellTypeLingshou;
    @InjectView(R.id.rb_sell_type_zhenggui)
    RadioButton rbSellTypeZhenggui;
    @InjectView(R.id.rg_sell_type)
    RadioGroup rgSellType;
    @InjectView(R.id.rb_sell_price_huanjia)
    RadioButton rbSellPriceHuanjia;
    @InjectView(R.id.rb_sell_price_zhengjia)
    RadioButton rbSellPriceZhengjia;
    @InjectView(R.id.rg_sell_price)
    RadioGroup rgSellPrice;
    @InjectView(R.id.sell_type_layout)
    LinearLayout sellTypeLayout;
    @InjectView(R.id.tv_baojialeixing)
    TextView tvBaojialeixing;
    @InjectView(R.id.et_changshangdingdanhao)
    EditText etChangshangdingdanhao;
    @InjectView(R.id.tv_yujidaogangqi)
    TextView tvYujidaogangqi;
    @InjectView(R.id.tv_zhuangchuanqi)
    TextView tvZhuangchuanqi;
    @InjectView(R.id.tv_daodagangkou)
    TextView tvDaodagangkou;
    @InjectView(R.id.view_sell_info)
    LinearLayout viewSellInfo;
    @InjectView(R.id.et_kuncun)
    EditText etKuncun;
    @InjectView(R.id.rb_renminbi)
    RadioButton rbRenminbi;
    @InjectView(R.id.rb_meiyuan)
    RadioButton rbMeiyuan;
    @InjectView(R.id.rb_ouyuan)
    RadioButton rbOuyuan;
    @InjectView(R.id.rg_jin_e)
    RadioGroup rgJinE;
    @InjectView(R.id.et_jin_e)
    EditText etJinE;
    @InjectView(R.id.tv_yufutiaokuan)
    TextView tvYufutiaokuan;
    @InjectView(R.id.iv_add_image)
    SimpleDraweeView ivAddImage;
    @InjectView(R.id.et_beizhu)
    EditText etBeizhu;
    @InjectView(R.id.rb_shangjia)
    RadioButton rbShangjia;
    @InjectView(R.id.rb_no_shangjia)
    RadioButton rbNoShangjia;
    @InjectView(R.id.rg_shangjia)
    RadioGroup rgShangjia;
    @InjectView(R.id.scrollview)
    ScrollView scrollview;
    @InjectView(R.id.bt_commit)
    Button btCommit;
    @InjectView(R.id.et_lianxirenxingimng)
    EditText etLianxirenxingimng;
    @InjectView(R.id.et_lianxirendianhua)
    EditText etLianxirendianhua;
    @InjectView(R.id.et_huowusuozaidi)
    EditText etHuowusuozaidi;
    @InjectView(R.id.ll_huowusuozaidi)
    LinearLayout llHuowusuozaidi;
    @InjectView(R.id.gv_images)
    GridView gvImages;
    private ArrayList<View> productViews = new ArrayList<>();
    private SellIndexBean sellIndexBean;
    private SellChanghaoBean sellChanghaoBean;
    private SellChanpinmingchengBean sellChanpinmingchengBean;
    private PopupWindow window;
    private String coverImg = "";
    private ArrayList<String> coverImgs = new ArrayList<String>();
    private Map<String, Object> params;
    private imgAdapter adapter;
    private View productView1;
    private View productView2;
    private View productView3;

    public static void setListViewHeightBasedOnChildren(GridView listView) {
        // 获取listview的adapter
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        // 固定列宽，有多少列
        int col = 3;// listView.getNumColumns();
        int totalHeight = 0;
        // i每次加4，相当于listAdapter.getCount()小于等于4时 循环一次，计算一次item的高度，
        // listAdapter.getCount()小于等于8时计算两次高度相加
        for (int i = 0; i < listAdapter.getCount(); i += col) {
            // 获取listview的每一个item
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            // 获取item的高度和
            totalHeight += listItem.getMeasuredHeight();
        }

        // 获取listview的布局参数
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        // 设置高度
        params.height = totalHeight;
        // 设置margin
        ((ViewGroup.MarginLayoutParams) params).setMargins(10, 10, 10, 10);
        // 设置参数
        listView.setLayoutParams(params);
    }

    private ChangeOfferInfoBean changeOfferInfoBean;


    public void initData(String goods_id) {
        showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
        Map<String, Object> params = new HashMap<>();
        params.put("model", "seller_offer");
        params.put("action", "get_offer_info");
        params.put("user_id", MySPEdit.getInstance(SellActivity.this).getUserId());
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller_offer", "get_offer_info")));
        params.put("goods_id", goods_id);
        HttpUtils.sendPost(params, new HttpRequestListener() {

            @Override
            public void success(String response, int id) {
                dismissProgress(SellActivity.this);
                changeOfferInfoBean = new Gson().fromJson(response, ChangeOfferInfoBean.class);
                setChangeView();
            }

            @Override
            public void failByOther(String fail, int id) {
                ToastUtil.showToast(fail);
                finish();
            }

            @Override
            public void fail(String fail, int id) {

            }
        });
    }

    private void setChangeView() {

        rbSellTimeXianhuo.setEnabled(false);
        rbSellTimeQihuo.setEnabled(false);
        rbSellTypeLingshou.setEnabled(false);
        rbSellTypeZhenggui.setEnabled(false);
        rbSellPriceHuanjia.setEnabled(false);
        rbSellPriceZhengjia.setEnabled(false);
        rbSellTimeXianhuo.setChecked(changeOfferInfoBean.getGoods_type().equals("7"));
        rbSellTimeQihuo.setChecked(changeOfferInfoBean.getGoods_type().equals("6"));
        rbSellTypeLingshou.setChecked(changeOfferInfoBean.getSell_type().equals("4"));
        rbSellTypeZhenggui.setChecked(changeOfferInfoBean.getSell_type().equals("5"));
        rbSellPriceHuanjia.setChecked(changeOfferInfoBean.getOffer().equals("11"));
        rbSellPriceZhengjia.setChecked(changeOfferInfoBean.getOffer().equals("10"));


        rbShangjia.setChecked(changeOfferInfoBean.getIs_on_sale().equals("1"));
        for (SellIndexBean.PrepayBean prepayBean : sellIndexBean.getPrepay()) {
            if(changeOfferInfoBean.getPrepay().equals(prepayBean.getId())){
                ((TextView)findViewById(R.id.view_sell).findViewById(R.id.tv_yufutiaokuan)).setTag(prepayBean.getId());
                ((TextView)findViewById(R.id.view_sell).findViewById(R.id.tv_yufutiaokuan)).setText(prepayBean.getVal());
            }
        }
        //期货属性
        if(rbSellTimeQihuo.isChecked()) {

            for (SellIndexBean.PrepayBean prepayBean : sellIndexBean.getPrepay()) {
                if (changeOfferInfoBean.getPort().equals(prepayBean .getId())) {
                    ((TextView) findViewById(R.id.view_sell).findViewById(R.id.tv_yufutiaokuan)).setTag(prepayBean .getId());
                    ((TextView) findViewById(R.id.view_sell).findViewById(R.id.tv_yufutiaokuan)).setText(prepayBean .getVal());
                }
            }

            for (SellIndexBean.OfferTypeBean offerTypeBean : sellIndexBean.getOffer_type()) {
                if (changeOfferInfoBean.getOffer_type().equals(offerTypeBean.getId())) {
                    ((TextView) findViewById(R.id.view_sell_qihuo).findViewById(R.id.tv_baojialeixing)).setTag(offerTypeBean.getId());
                    ((TextView) findViewById(R.id.view_sell_qihuo).findViewById(R.id.tv_baojialeixing)).setText(offerTypeBean.getVal());
                }
            }

            //期货
            etChangshangdingdanhao.setText(changeOfferInfoBean.getGoods_sn());
            tvYujidaogangqi.setText(changeOfferInfoBean.getArrive_date());
            tvYujidaogangqi.setTag(changeOfferInfoBean.getArrive_date());
            tvZhuangchuanqi.setText(changeOfferInfoBean.getLading_date());
            tvZhuangchuanqi.setTag(changeOfferInfoBean.getLading_date());

            for (SellIndexBean.PortBean portBean : sellIndexBean.getPort()) {
                if (changeOfferInfoBean.getPort().equals(portBean.getId())) {
                    tvDaodagangkou.setTag(portBean.getId());
                    tvDaodagangkou.setText(portBean.getVal());
                }
            }
        }


        if(changeOfferInfoBean.getGoods_base()==null||changeOfferInfoBean.getGoods_base().isEmpty()){
            //不是拼盘
            productView1.setVisibility(View.VISIBLE);
            productView2.setVisibility(View.GONE);
            productView3.setVisibility(View.GONE);

            ((TextView)productView1.findViewById(R.id.tv_guojia)).setText(changeOfferInfoBean.getRegion_name());
            ((TextView)productView1.findViewById(R.id.tv_guojia)).setTag(changeOfferInfoBean.getRegion_id());

            ((TextView)productView1.findViewById(R.id.tv_changhao)).setText(changeOfferInfoBean.getBrand_sn());
            ((TextView)productView1.findViewById(R.id.tv_changhao)).setTag(changeOfferInfoBean.getBrand_id());

            ((TextView)productView1.findViewById(R.id.tv_chanpinmingcheng)).setText(changeOfferInfoBean.getGoods_name());
            ((TextView)productView1.findViewById(R.id.tv_chanpinmingcheng)).setTag(changeOfferInfoBean.getBase_id());

            ((TextView)productView1.findViewById(R.id.tv_guojia)).setText(changeOfferInfoBean.getRegion_name());
            ((TextView)productView1.findViewById(R.id.tv_guojia)).setTag(changeOfferInfoBean.getRegion_id());

            for (SellIndexBean.CatListBean catListBean : sellIndexBean.getCat_list()) {
                if((changeOfferInfoBean.getCat_id_first().equals(catListBean.getId()))){
                    ((TextView)productView1.findViewById(R.id.tv_chanpinfenlei)).setTag(catListBean.getId());
                    ((TextView)productView1.findViewById(R.id.tv_chanpinfenlei)).setText(catListBean.getVal());
                }
            }
            ((TextView)productView1.findViewById(R.id.tv_shengchanriqi)).setText(changeOfferInfoBean.getMake_date());
            ((TextView)productView1.findViewById(R.id.tv_shengchanriqi)).setTag(changeOfferInfoBean.getMake_date());
            ((TextView) productView1.findViewById(R.id.et_baozhuangguige)).setText(changeOfferInfoBean.getSpec_1() + "");
            ((TextView) productView1.findViewById(R.id.et_baozhuangguige2)).setText(changeOfferInfoBean.getSpecs_3() + "");

            ((RadioButton) productView1.findViewById(R.id.rb_dingzhuang)).setChecked(changeOfferInfoBean.getPack().equals("8"));
            ((RadioButton) productView1.findViewById(R.id.rb_chaoma)).setChecked(changeOfferInfoBean.getPack().equals("9"));


        }else{
            //拼盘
            productView1.setVisibility(View.GONE);
            productView2.setVisibility(View.GONE);
            productView3.setVisibility(View.GONE);

            for (int i = 0; i < changeOfferInfoBean.getGoods_base().size(); i++) {
                 View v = productViews.get(i);
               ChangeOfferInfoBean.GoodsBaseBean baseBean =  changeOfferInfoBean.getGoods_base().get(i);
                v.setVisibility(View.VISIBLE);
                ((TextView)v.findViewById(R.id.tv_guojia)).setText(baseBean.getRegion_name());
                ((TextView)v.findViewById(R.id.tv_guojia)).setTag(baseBean.getRegion());

                ((TextView)v.findViewById(R.id.tv_changhao)).setText(baseBean.getBrand_name());
                ((TextView)v.findViewById(R.id.tv_changhao)).setTag(baseBean.getBrand_ids());

                ((TextView)v.findViewById(R.id.tv_chanpinmingcheng)).setText(baseBean.getGoods_names());
                ((TextView)v.findViewById(R.id.tv_chanpinmingcheng)).setTag(baseBean.getBase_ids());

                ((TextView)v.findViewById(R.id.tv_guojia)).setText(baseBean.getRegion_name());
                ((TextView)v.findViewById(R.id.tv_guojia)).setTag(baseBean.getRegion());

                for (SellIndexBean.CatListBean catListBean : sellIndexBean.getCat_list()) {
                    if(baseBean.getCat_id_first().equals(catListBean.getId())){
                        ((TextView)v.findViewById(R.id.tv_chanpinfenlei)).setTag(catListBean.getId());
                        ((TextView)v.findViewById(R.id.tv_chanpinfenlei)).setText(catListBean.getVal());
                    }

                }
                ((TextView)v.findViewById(R.id.tv_shengchanriqi)).setText(baseBean.getMake_date());
                ((TextView) v.findViewById(R.id.et_baozhuangguige)).setText(baseBean.getSpecs_1() + "");
                ((TextView) v.findViewById(R.id.et_baozhuangguige2)).setText(baseBean.getSpecs_3() + "");

                ((RadioButton) v.findViewById(R.id.rb_dingzhuang)).setChecked(baseBean.getPacks().equals("8"));
                ((RadioButton) v.findViewById(R.id.rb_chaoma)).setChecked(baseBean.getPacks().equals("9"));
            }



        }


            rbRenminbi.setChecked(changeOfferInfoBean.getCurrency().equals("1"));
        rbMeiyuan.setChecked(changeOfferInfoBean.getCurrency().equals("2"));
        rbOuyuan.setChecked(changeOfferInfoBean.getCurrency().equals("3"));
        etHuowusuozaidi.setText(changeOfferInfoBean.getGoods_local());
        etKuncun.setText(changeOfferInfoBean.getGoods_number());
        etJinE.setText(changeOfferInfoBean.getCurrency_money_input());
        etBeizhu.setText(changeOfferInfoBean.getGoods_txt());
        ((RadioButton) productView1.findViewById(R.id.rb_jian)).setChecked(changeOfferInfoBean.getShop_price_unit().equals("1"));
        ((RadioButton) productView1.findViewById(R.id.rb_dun)).setChecked(changeOfferInfoBean.getShop_price_unit().equals("0"));

        //封面图片
        ivAddImage.setImageURI(changeOfferInfoBean.getGood_face());
        coverImg = changeOfferInfoBean.getGood_face();
        //产品图片
        for (ChangeOfferInfoBean.PictureListBean pictureListBean : changeOfferInfoBean.getPicture_list()) {
            coverImgs.add(pictureListBean.getImg_url());
        }
        adapter.notifyDataSetChanged();
        setListViewHeightBasedOnChildren(gvImages);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mTitle.setText(R.string.fabubaopan);
        ButterKnife.inject(this);
        productView1 = View.inflate(SellActivity.this, R.layout.view_sell_info, null);
        productView2 = View.inflate(SellActivity.this, R.layout.view_sell_info, null);
        productView3 = View.inflate(SellActivity.this, R.layout.view_sell_info, null);

        productView1.findViewById(R.id.btn_delete_pingui).setVisibility(View.GONE);
        productView3.findViewById(R.id.btn_add_pingui).setVisibility(View.GONE);

        productView1.findViewById(R.id.btn_add_pingui).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productView2.setVisibility(View.VISIBLE);
            }
        });
        productView2.findViewById(R.id.btn_delete_pingui).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productView2.setVisibility(View.GONE);
            }
        });
        productView2.findViewById(R.id.btn_add_pingui).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productView3.setVisibility(View.VISIBLE);
            }
        });
        productView3.findViewById(R.id.btn_delete_pingui).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productView3.setVisibility(View.GONE);
            }
        });

        productViews.add(productView1);
        productViews.add(productView2);
        productViews.add(productView3);
        viewSellInfo.addView(productView1);
        viewSellInfo.addView(productView2);
        viewSellInfo.addView(productView3);

        //设置默认选择现货
        rgSellTime.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                for (View productView : productViews) {
                    llHuowusuozaidi.setVisibility(rbSellTimeXianhuo.isChecked() ? View.VISIBLE : View.GONE);
                    productView.findViewById(R.id.ll_dungui).setVisibility(rbSellTimeXianhuo.isChecked() ? View.VISIBLE : View.GONE);
                }
                findViewById(R.id.view_sell_qihuo).setVisibility(rbSellTimeXianhuo.isChecked() ? View.GONE : View.VISIBLE);
            }
        });
        rbSellTimeXianhuo.setChecked(true);

        rgSellType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                if (rbSellTypeLingshou.isChecked()) {
                    for (View productView : productViews) {
                        productView.setVisibility(View.GONE);
                    }
                    productViews.get(0).setVisibility(View.VISIBLE);
                    tvKucun.setText(getString(R.string.jian));
                } else {
                    tvKucun.setText(R.string.gui);
                }
                for (final View productView : productViews) {
                    productView.findViewById(R.id.ll_dungui).setVisibility(rbSellTypeLingshou.isChecked() ? View.GONE : View.VISIBLE);
                    productView.findViewById(R.id.ll_dun_jian).setVisibility(rbSellTypeLingshou.isChecked() ? View.VISIBLE : View.GONE);
                    productView.findViewById(R.id.ll_add_pingui).setVisibility(rbSellTypeLingshou.isChecked() ? View.GONE : View.VISIBLE);
                    productView.findViewById(R.id.textView2).setVisibility(rbSellTypeLingshou.isChecked() ? View.INVISIBLE : View.VISIBLE);
                    ((TextView) findViewById(R.id.tv_jian_dun_)).setText(rbSellTypeLingshou.isChecked() ? getString(R.string.jian): getString(R.string.dun));
                    ((TextView) findViewById(R.id.tv_yuan_)).setText(getString(R.string.yuan)+"/");
                    ((RadioButton) productView.findViewById(R.id.rb_jian)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            ((TextView) findViewById(R.id.tv_jian_dun_)).setText(rbSellTypeLingshou.isChecked() && b? getString(R.string.jian): getString(R.string.dun));
                        }
                    });
                    rgJinE.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup radioGroup, int i) {
                                ((TextView) findViewById(R.id.tv_yuan_)).setText(getYuanSlashDun());
                        }
                    });

                }

            }
        });

        rbSellTypeLingshou.setChecked(true);
        rbSellPriceHuanjia.setChecked(true);

        getIndexData();

    }

    private void addTimeSelecter(final TextView click) {
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                TimeSelector timeSelector = new TimeSelector(SellActivity.this, new TimeSelector.ResultHandler() {
                    @Override
                    public void handle(String time) {
                        click.setText(time.substring(0, time.indexOf(':') - 2));
                        click.setTag(time.substring(0, time.indexOf(':') - 2));
                    }
                }, "2017-1-1 17:34", "2027-12-31 17:34");
                timeSelector.setMode(TimeSelector.MODE.YMD);
                timeSelector.show();
            }
        });
    }

    @OnClick(R.id.iv_add_image)
    public void addImage(View v) {
        Album.image(this) // 选择图片。
                .multipleChoice()
                .requestCode(200)
                .camera(true)
                .columnCount(2)
                .selectCount(1)
//                .checkedList(mAlbumFiles)
                .listener(new AlbumListener<ArrayList<AlbumFile>>() {
                    @Override
                    public void onAlbumResult(int requestCode, @NonNull ArrayList<AlbumFile> result) {
                        coverImg = result.get(0).getPath();
                        ivAddImage.setImageURI(Uri.parse("file://" + result.get(0).getPath()));
                    }

                    @Override
                    public void onAlbumCancel(int requestCode) {
                    }
                })
                .start();

    }

    void initWindow(View view) {

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.alpha = 0.5f;
        getWindow().setAttributes(params);
        // 设置popWindow弹出窗体可点击，这句话必须添加，并且是true

        window = new PopupWindow(view,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        window.setFocusable(true);
        // 设置popWindow的显示和消失动画
        window.setAnimationStyle(R.style.mypopwindow_anim_style);
        window.showAtLocation(view.findViewById(R.id.close_btn),
                Gravity.CENTER, 0, 0);

        // popWindow消失监听方法
        window.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                WindowManager.LayoutParams params = getWindow().getAttributes();
                params.alpha = 1f;
                getWindow().setAttributes(params);
            }
        });
    }

    void ShowSelect(final TextView textView, final BaseAdapter baseAdapter, final GetChildSelecter getChildSelecter) {
        ShowSelect(textView, baseAdapter, getChildSelecter, null, null);

    }

    void ShowSelect(final TextView textView, final BaseAdapter baseAdapter, final GetChildSelecter getChildSelecter, final String type, final TextView tv) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_list, null);
        initWindow(view);

        final EditText editText = (EditText) view.findViewById(R.id.et_content);
        view.findViewById(R.id.rl_pop_search_content).setVisibility(type != null ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.iv_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("changhao".equals(type)) {
                    Map<String, Object> params = new HashMap<>();
                    params.put("model", "seller_offer");
                    params.put("action", "add_brand");
                    params.put("sid", tv.getTag().toString());
                    params.put("brand_sn", editText.getText());
                    params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller_offer", "add_brand")));
                    HttpUtils.sendPost(params, new HttpRequestListener() {
                        @Override
                        public void success(String response, int id) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                textView.setTag(jsonObject.getString("brand_id"));
                                textView.setText(jsonObject.getString("brand_sn"));
                                window.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void failByOther(String fail, int id) {

                        }

                        @Override
                        public void fail(String fail, int id) {

                        }
                    }, 106);
                } else {
                    Map<String, Object> params = new HashMap<>();
                    params.put("model", "seller_offer");
                    params.put("action", "add_base");
                    params.put("cat_id", tv.getTag().toString());
                    params.put("base_name", editText.getText());
                    params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller_offer", "add_base")));
                    HttpUtils.sendPost(params, new HttpRequestListener() {
                        @Override
                        public void success(String response, int id) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                textView.setTag(jsonObject.getString("goods_id"));
                                textView.setText(jsonObject.getString("goods_name"));
                                window.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void failByOther(String fail, int id) {

                        }

                        @Override
                        public void fail(String fail, int id) {

                        }
                    }, 105);
                }
            }
        });
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if ("changhao".equals(type)) {
                    ((ChangHaoAdapter) baseAdapter).setFilter(editable.toString());
                } else {
                    ((ChanPinMingChengAdapter) baseAdapter).setFilter(editable.toString());
                }
                baseAdapter.notifyDataSetChanged();
            }
        });

        ListView listView = (ListView) view.findViewById(R.id.lv_pop);
        listView.setAdapter(baseAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                textView.setTag(view.findViewById(R.id.tv_pop_item).getTag());
                textView.setText(((TextView) view.findViewById(R.id.tv_pop_item)).getText());
                if (getChildSelecter != null) {
                    getChildSelecter.getChildIndex((String) view.findViewById(R.id.tv_pop_item).getTag());
                }
                window.dismiss();
            }
        });
        view.findViewById(R.id.close_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                window.dismiss();
            }
        });

    }

    private void getIndexData() {
        showProgress(SellActivity.this, getString(R.string.jiazaizhong_dotdotdot));
        Map<String, Object> params = new HashMap<>();
        params.put("model", "seller_offer");
        params.put("action", "get_offer_index");
        params.put("user_id", MySPEdit.getInstance(this).getUserId());
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller_offer", "get_offer_index")));
        HttpUtils.sendPost(params, this, 1);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_sell;
    }

    private void enableSelecter() {
        for (final View productView : productViews) {
            addTimeSelecter(((TextView) productView.findViewById(R.id.tv_shengchanriqi)));
            ((EditText) productView.findViewById(R.id.et_baozhuangguige)).addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    try {
                        int i = 1000 / Integer.parseInt(editable.toString());
                        ((TextView) productView.findViewById(R.id.tv_baozhuangguige)).setText(i + "");

                    } catch (Exception e) {

                    }
                }
            });
            productView.findViewById(R.id.tv_guojia).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShowSelect((TextView) view, new SiteAdapter(SellActivity.this, sellIndexBean.getSite_list()), new GetChildSelecter() {

                        @Override
                        public void getChildIndex(String index) {
                            productView.findViewById(R.id.tv_changhao).setTag(null);
                            ((TextView) productView.findViewById(R.id.tv_changhao)).setText(getString(R.string.qingxuanze));
                            showProgress(SellActivity.this, getString(R.string.jiazaizhong_dotdotdot));
                            Map<String, Object> params = new HashMap<>();
                            params.put("model", "seller_offer");
                            params.put("action", "get_brand_list");
                            params.put("sid", index);
//                            params.put("brand_sn", index);
                            params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller_offer", "get_brand_list")));
                            HttpUtils.sendPost(params, new HttpRequestListener() {
                                @Override
                                public void success(String response, int id) {
                                    dismissProgress(SellActivity.this);
                                    sellChanghaoBean = new Gson().fromJson(response, SellChanghaoBean.class);
                                    ((TextView) productView.findViewById(R.id.tv_changhao)).setText(getString(R.string.qingxuanze));
                                    ((TextView) productView.findViewById(R.id.tv_changhao)).setTag(null);
                                    productView.findViewById(R.id.tv_changhao).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            ShowSelect((TextView) view, new ChangHaoAdapter(SellActivity.this, sellChanghaoBean.getBrand_list()), null, "changhao", (TextView) productView.findViewById(R.id.tv_guojia));
                                        }
                                    });
                                }

                                @Override
                                public void failByOther(String fail, int id) {
                                    SellActivity.this.failByOther(fail, id);
                                }

                                @Override
                                public void fail(String fail, int id) {
                                    SellActivity.this.fail(fail, id);
                                }
                            }, 1001);
                        }
                    });
                }
            });

            productView.findViewById(R.id.tv_chanpinfenlei).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShowSelect((TextView) view, new CatListAdapter(SellActivity.this, sellIndexBean.getCat_list()), new GetChildSelecter() {
                        @Override
                        public void getChildIndex(String index) {
                            productView.findViewById(R.id.tv_chanpinmingcheng).setTag(null);
                            ((TextView) productView.findViewById(R.id.tv_chanpinmingcheng)).setText(getString(R.string.qingxuanze));
                            showProgress(SellActivity.this, getString(R.string.jiazaizhong_dotdotdot));
                            Map<String, Object> params = new HashMap<>();
                            params.put("model", "seller_offer");
                            params.put("action", "get_goods_name");
                            params.put("cat_id", index);
//                            params.put("goods_name", index);
                            params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller_offer", "get_goods_name")));
                            HttpUtils.sendPost(params, new HttpRequestListener() {
                                @Override
                                public void success(String response, int id) {
                                    dismissProgress(SellActivity.this);
                                    sellChanpinmingchengBean = new Gson().fromJson(response, SellChanpinmingchengBean.class);
                                    ((TextView) productView.findViewById(R.id.tv_chanpinmingcheng)).setText(getString(R.string.qingxuanze));
                                    ((TextView) productView.findViewById(R.id.tv_chanpinmingcheng)).setTag(null);
                                    productView.findViewById(R.id.tv_chanpinmingcheng).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            ShowSelect((TextView) view, new ChanPinMingChengAdapter(SellActivity.this, sellChanpinmingchengBean.getGoods_name_list()), null, "shangpin", (TextView) productView.findViewById(R.id.tv_chanpinfenlei));
                                        }
                                    });
                                }

                                @Override
                                public void failByOther(String fail, int id) {
                                    SellActivity.this.failByOther(fail, id);
                                }

                                @Override
                                public void fail(String fail, int id) {
                                    SellActivity.this.fail(fail, id);
                                }
                            }, 1002);
                        }
                    });
                }
            });

        }
        View view = findViewById(R.id.view_sell_qihuo);
        view.findViewById(R.id.tv_baojialeixing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowSelect((TextView) view, new OfferTypeListAdapter(SellActivity.this, sellIndexBean.getOffer_type()), null);
            }
        });

        view.findViewById(R.id.tv_daodagangkou).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowSelect((TextView) view, new PortListAdapter(SellActivity.this, sellIndexBean.getPort()), null);
            }
        });

        View view_sell = findViewById(R.id.view_sell);
        view_sell.findViewById(R.id.tv_yufutiaokuan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowSelect((TextView) view, new PrepayListAdapter(SellActivity.this, sellIndexBean.getPrepay()), null);
            }
        });


        addTimeSelecter(tvYujidaogangqi);
        addTimeSelecter(tvZhuangchuanqi);
        adapter = new imgAdapter();
        gvImages.setAdapter(adapter);

    }

    private boolean checkValueArray(View v, String info, String type) {
        if (!params.containsKey(type)) {
            params.put(type, new ArrayList<String>());
        }
        if (v instanceof EditText) {
            if ("".equals(info)) {
                ((ArrayList<String>) params.get(type)).add(((EditText) v).getText().toString());
                return true;
            }
            if (((EditText) v).getText().toString().isEmpty() || ((EditText) v).getText().toString().equals(getString(R.string.qingshuru))) {
                ToastUtil.showToast(info);
                return false;
            } else {
                ((ArrayList<String>) params.get(type)).add(((EditText) v).getText().toString());
                return true;
            }
        }
        if (v instanceof TextView) {
            if (v.getTag() == null || v.getTag().toString().isEmpty()) {
                ToastUtil.showToast(info);
                return false;
            } else {
                ((ArrayList<String>) params.get(type)).add((v.getTag().toString()));
                return true;
            }
        }
        return false;
    }

    private boolean checkValue(View v, String info, String type) {
        if (v instanceof EditText) {
            if ("".equals(info)) {
                params.put(type, ((EditText) v).getText());
                return true;
            }
            if (((EditText) v).getText().toString().isEmpty() || ((EditText) v).getText().toString().equals(getString(R.string.qingshuru))) {
                ToastUtil.showToast(info);
                return false;
            } else {
                params.put(type, ((EditText) v).getText());
                return true;
            }
        }
        if (v instanceof TextView) {
            if (v.getTag() == null || v.getTag().toString().isEmpty()) {
                ToastUtil.showToast(info);
                return false;
            } else {
                params.put(type, v.getTag());
                return true;
            }
        }
        return false;
    }

    @OnClick(R.id.bt_commit)
    public void checkValue() {


        params = new HashMap<>();
        params.clear();

        if (rbSellTimeQihuo.isChecked()) {
            //期货
            if (checkValue(tvYujidaogangqi, getString(R.string.qingxuanze)+getString(R.string.yujidaogang), "arrive_date") &&
                    checkValue(tvZhuangchuanqi, getString(R.string.qingxuanze)+getString(R.string.zhuangchuanriqi), "lading_date") &&
                    checkValue(tvDaodagangkou, getString(R.string.qingxuanze)+getString(R.string.daodaguangkou), "port") &&
                    checkValue(tvBaojialeixing,getString(R.string.qingxuanze)+getString(R.string.baojialeixing), "offer_type") &&
                    checkValue(etChangshangdingdanhao, getString(R.string.qingxuanze)+getString(R.string.changshangdingdanhao), "goods_sn")) {
                L.e("期货属性检查完毕");
                params.put("goods_type", "6");

            } else {
                L.e("期货属性检查不通过");
                return;
            }
        } else {
            params.put("goods_type", "7");
        }
        if (rbSellTypeLingshou.isChecked()) {
            //零售
            View v1 = productViews.get(0);

            if (checkValue(v1.findViewById(R.id.tv_guojia), getString(R.string.qingxuanze)+getString(R.string.guojia), "region_id") &&
                    checkValue(v1.findViewById(R.id.tv_changhao), getString(R.string.qingxuanze)+getString(R.string.changhao), "brand_id") &&
                    checkValue(v1.findViewById(R.id.tv_chanpinfenlei), getString(R.string.qingxuanze)+getString(R.string.chanpinfenlei), "cat_id") &&
                    checkValue(v1.findViewById(R.id.tv_chanpinmingcheng), getString(R.string.qingxuanze)+getString(R.string.chanpinmingcheng), "base_id") &&
                    checkValue(v1.findViewById(R.id.tv_shengchanriqi), getString(R.string.qingxuanze)+getString(R.string.shengchanriqi), "make_date") &&
                    checkValue(v1.findViewById(R.id.et_beizhu), "", "spec_txt") &&
                    checkValue(v1.findViewById(R.id.et_baozhuangguige), getString(R.string.qingshuru)+getString(R.string.baozhuangguige), "spec_1")) {
                L.e("零售属性检查完毕");
                checkValue(v1.findViewById(R.id.et_baozhuangguige), "", "goods_weight");
                params.put("pack", ((RadioButton) v1.findViewById(R.id.rb_dingzhuang)).isChecked() ? "8" : "9");
                params.put("sell_type", "4");
                params.put("shop_price_unit", ((RadioButton) v1.findViewById(R.id.rb_jian)).isChecked() ? "1" : "0");
            } else {
                L.e("零售属性检查不通过");
                return;
            }
        } else {
            //整柜
            params.put("sell_type", "5");
            for (View v1 : productViews) {
                if (v1.getVisibility() == View.VISIBLE) {
                    if (checkValueArray(v1.findViewById(R.id.tv_guojia), getString(R.string.qingxuanze)+getString(R.string.guojia), "region") &&
                            checkValueArray(v1.findViewById(R.id.tv_changhao), getString(R.string.qingxuanze)+getString(R.string.changhao), "brand_ids") &&
                            checkValueArray(v1.findViewById(R.id.tv_chanpinfenlei), getString(R.string.qingxuanze)+getString(R.string.chanpinfenlei), "cat_ids") &&
                            checkValueArray(v1.findViewById(R.id.tv_chanpinmingcheng), getString(R.string.qingxuanze)+getString(R.string.chanpinmingcheng), "base_ids") &&
                            checkValueArray(v1.findViewById(R.id.tv_shengchanriqi), getString(R.string.qingxuanze)+getString(R.string.shengchanriqi), "make_dates") &&
                            checkValueArray(v1.findViewById(R.id.et_beizhu), "", "spec_txts") &&
                            checkValueArray(v1.findViewById(R.id.et_baozhuangguige), getString(R.string.qingshuru)+getString(R.string.baozhuangguige), "specs_1") &&
                            checkValueArray(v1.findViewById(R.id.et_baozhuangguige2), getString(R.string.qingshuru)+getString(R.string.baozhuangguige), "specs_3")
                            ) {

                        if (!params.containsKey("packs")) {
                            params.put("packs", new ArrayList<String>());
                        }
                        ((ArrayList<String>) params.get("packs")).add(((RadioButton) v1.findViewById(R.id.rb_dingzhuang)).isChecked() ? "8" : "9");
                        L.e("整柜属性检查完毕");
                    } else {
                        L.e("整柜属性检查不通过");
                        return;
                    }
                }
            }
        }
        params.put("offer", rbSellPriceZhengjia.isChecked() ? "10" : "11");
        params.put("is_on_sale", rbShangjia.isChecked() ? "1" : "0");

        if (rbRenminbi.isChecked()) {
            params.put("currency", "1");
        } else if (rbMeiyuan.isChecked()) {
            params.put("currency", "2");
        } else {
            params.put("currency", "3");
        }

        if (checkValue(etJinE, getString(R.string.qingshuru)+getString(R.string.jine), "currency_money") &&
                checkValue(etKuncun, getString(R.string.qingshuru)+getString(R.string.kucun), "goods_number") &&
                checkValue(etBeizhu, "", "goods_txt") &&
                (rbSellTimeQihuo.isChecked() ? true : checkValue(etHuowusuozaidi,  getString(R.string.qingshuru)+getString(R.string.suozaidi), "goods_local")) &&
                checkValue(tvYufutiaokuan, getString(R.string.qingxuanze)+getString(R.string.yufutiaokuan), "prepay")) {
            L.e("付款属性检查完毕");
        } else {
            L.e("付款属性检查不通过");
            return;
        }
        uploadImage();
    }

    private void uploadImage() {
        //图片上传
        if (!coverImgs.isEmpty()) {
            params.put("picture_list", new ArrayList<String>());
            String image = new String(coverImgs.get(0));
            coverImgs.remove(0);
            if(image.startsWith("http")){
                success(image,104);
            }else {
                uploadImg(image, 104);
            }
        } else if (!"".equals(coverImg)) {
            if(coverImg.startsWith("http")) {
                params.put("good_face", coverImg);
                coverImg = "";
                submit();
            }else{
                uploadImg(coverImg, 101);
            }
        } else {
            submit();
        }
    }

    private void uploadImg(String path, int code) {
        showProgress(SellActivity.this, getString(R.string.jiazaizhong_dotdotdot));
        Map<String, Object> params = new HashMap<>();
        params.clear();
        params.put("model", "seller_offer");
        params.put("action", "upload_image");
        params.put("user_id", MySPEdit.getInstance(this).getUserId());
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller_offer", "upload_image")));
        HttpUtils.sendPost(params, "upload_img", path, this, true, code);

    }

    private void submit() {
        params.put("model", "seller_offer");
        params.put("action",getIntent().getBooleanExtra("is_edit",false)? "offer_edit":"offer_add");
        params.put("user_id", MySPEdit.getInstance(this).getUserId());
        if(getIntent().getBooleanExtra("is_edit",false)){
            params.put("goods_id", getIntent().getStringExtra("goods_id"));
        }
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller_offer", getIntent().getBooleanExtra("is_edit",false)? "offer_edit":"offer_add")));
        HttpUtils.sendPost(params, this, 102);
    }

    @Override
    public void failByOther(String fail, int id) {
        ToastUtil.showToast(fail);
        finish();
    }

    @Override
    public void fail(String fail, int id) {
        T.showShort(this, R.string.jianchawangluo);
    }

    @Override
    public void success(String response, int id) {
        dismissProgress(this);
        if (id == 104) {
            ((ArrayList<String>) params.get("picture_list")).add(response);
            if (coverImgs.isEmpty()) {
                uploadImage();
            } else {
                String image = new String(coverImgs.get(0));
                coverImgs.remove(0);
                if(image.startsWith("http")){
                    success(image,104);
                }else {
                    uploadImg(image, 104);
                }
            }
        }
        if (id == 1) {
            sellIndexBean = new Gson().fromJson(response, SellIndexBean.class);
            enableSelecter();
            setUserInfo();
            if(getIntent().getBooleanExtra("is_edit",false)||getIntent().getBooleanExtra("is_copy",false)){
                initData(getIntent().getStringExtra("goods_id"));
            }
            if(getIntent().getBooleanExtra("is_edit",false)){
                tvTitle.setText(getString(R.string.bianjibaopan));
            }
        }
        //封面上传完成
        if (id == 101) {
            params.put("good_face", response);
            coverImg = "";
            submit();
        }
        //多图上传完成
        if (id == 103) {
            ToastUtil.showToast(response);

        }
        if (id == 102) {
            if(getIntent().getBooleanExtra("is_edit",false)) {
                ToastUtil.showToast(getString(R.string.bianjibaopanchenggong));
            }
            else {
                ToastUtil.showToast(getString(R.string.fabubaopanchenggong));
            }
            finish();
        }

    }

    private void setUserInfo() {
        etLianxirendianhua.setText(sellIndexBean.getUser_info().getMobile_phone());
        etLianxirenxingimng.setText(sellIndexBean.getUser_info().getAlias());
        etLianxirendianhua.setEnabled(sellIndexBean.getUser_info().getMobile_phone().equals("") ? true : false);
        etLianxirenxingimng.setEnabled(sellIndexBean.getUser_info().getAlias().equals("") ? true : false);
    }

    private void addImageToList() {
        Album.image(this) // 选择图片。
                .multipleChoice()
                .requestCode(200)
                .camera(true)
                .columnCount(2)
                .selectCount(99)
                .listener(new AlbumListener<ArrayList<AlbumFile>>() {
                    @Override
                    public void onAlbumResult(int requestCode, @NonNull ArrayList<AlbumFile> result) {
                        for (AlbumFile albumFile : result) {
                            coverImgs.add(albumFile.getPath());
                        }
                        adapter.notifyDataSetChanged();
                        setListViewHeightBasedOnChildren(gvImages);
                    }

                    @Override
                    public void onAlbumCancel(int requestCode) {
                    }
                })
                .start();
    }

    public String getYuanSlashDun() {
        if(rbOuyuan.isChecked()) {
            return getString(R.string.ouyuan)+"/" ;
        }
        else if(rbMeiyuan.isChecked()) {
            return getString(R.string.meiyuan)+"/";
        }
        else{
            return  getString(R.string.yuan)+"/";
        }
    }

    class imgAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return coverImgs.size() + 1;
        }

        @Override
        public Object getItem(int i) {
            return coverImgs.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View item = LayoutInflater.from(SellActivity.this).inflate(R.layout.list_images, null);
            if (i != coverImgs.size()) {
                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        coverImgs.remove(i);
                        adapter.notifyDataSetChanged();
                    }
                });
                if(coverImgs.get(i).startsWith("http")) {
                    ((SimpleDraweeView) item.findViewById(R.id.iv_add_image)).setImageURI(coverImgs.get(i));
                }else{
                    ((SimpleDraweeView) item.findViewById(R.id.iv_add_image)).setImageURI(Uri.parse("file://" + coverImgs.get(i)));
                }
            } else {
                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addImageToList();
                    }
                });
                ((SimpleDraweeView) item.findViewById(R.id.iv_add_image)).setBackground(getDrawable(R.mipmap.icon_addphoto));
            }
            return item;
        }
    }
}
