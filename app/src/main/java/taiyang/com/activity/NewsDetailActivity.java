package taiyang.com.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import butterknife.InjectView;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;

/**
 * 新闻详情页面
 */
public class NewsDetailActivity extends BaseActivity {

    @InjectView(R.id.webView)
    WebView webView;

    @InjectView(R.id.progress_bar)
    ProgressBar progressBar;

    private String webUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_news_detail);
//        ButterKnife.inject(this);
        mTitle.setText("文章详情");
        webUrl = getIntent().getStringExtra("webUrl");
//        webUrl="http://mp.weixin.qq.com/s?__biz=MTA3NDM1MzUwMQ==&idx=4&mid=2651929764&sn=a86ee979ea2beb1a9dc84f7fe01903be";
//        webUrl="http://tydpb2b.bizsoho.com/mobile/article.php?id=36";
//        webUrl="http://zhixunol.net/api/information/myInfo.do?appid=rrzxapp20160101&info_id=29d0f8e65280433cb99e392e976a1cfe";
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setAppCacheEnabled(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setSupportZoom(true);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);   // 默认使用缓存
//        settings.setAppCacheMaxSize(8*1024*1024);   //缓存最多可以有8M
//        settings.setAllowFileAccess(true);   // 可以读取文件缓存(manifest生效)
        webView.setWebChromeClient(new ChromeClient());
        webView.setWebViewClient(new Client());
        webView.loadUrl(webUrl);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_news_detail;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

    private class ChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if(progressBar!=null){
                progressBar.setProgress(newProgress);
                if (newProgress == 100) {
                    progressBar.setVisibility(View.INVISIBLE);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                }
            }

            super.onProgressChanged(view, newProgress);
        }

        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
        }
    }

    private class Client extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url != null) view.loadUrl(url);
            return true;
        }
    }
}
