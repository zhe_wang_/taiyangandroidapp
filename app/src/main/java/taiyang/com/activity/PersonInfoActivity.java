package taiyang.com.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.InjectView;
import taiyang.com.adapter.PersonInfoAdapter;
import taiyang.com.entity.GoosPerson;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.T;

/*
    取货人信息
 */
public class PersonInfoActivity extends BaseActivity implements View.OnClickListener {

    @InjectView(R.id.bt_personinfo_addperson)
    Button bt_personinfo_addperson;
    @InjectView(R.id.rv_personinfo)
    RecyclerView rv_personinfo;
    private Context mCtx;
    private List<GoosPerson> mDatas;
    private PersonInfoAdapter personInfoAdapter;
    @InjectView(R.id.scrollview)
    ScrollView scrollView;
    @InjectView(R.id.ll_noaddress_layout)
    LinearLayout ll_noaddress_layout;

    @Override
    protected void onResume() {
        super.onResume();
        if (myApplication.isAddressChange()) {
            showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
            Map<String, Object> params = new HashMap<>();
            params.put("model", "user");
            params.put("action", "get_address_list");
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "get_address_list")));
            params.put("user_id", mySPEdit.getUserId());
            params.put("type",type);
            params.put("token", mySPEdit.getToken());
            HttpUtils.sendPost(params, this);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle.setText(getString(R.string.dizhi));
        initListeren();
        initData();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_personinfo;
    }


    private void initListeren() {
        bt_personinfo_addperson.setOnClickListener(this);
    }

    public int type;
    public int flag;
    private void initData() {
        type = getIntent().getIntExtra("seller",0);
        flag = getIntent().getIntExtra("flag",0);
        mDatas = new ArrayList<>();
        bt_personinfo_addperson.setText(type==0?getString(R.string.tianjiaquhuorenxinxi):getString(R.string.tianjia));
        personInfoAdapter = new PersonInfoAdapter(mDatas, this, bt_personinfo_addperson,type);
        personInfoAdapter.setFlag(false);
        rv_personinfo.setAdapter(personInfoAdapter);
        personInfoAdapter.notifyDataSetChanged();
        rv_personinfo.setLayoutManager(new LinearLayoutManager(PersonInfoActivity.this));
        personInfoAdapter.setOnItemClickLitener(new PersonInfoAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_personinfo_addperson:
                Intent i = new Intent(this, AddPersonActivity.class);
                i.putExtra("seller",type);
                startActivity(i);
                break;
            default:
                break;
        }
    }

    @Override
    public void success(String response, int id) {
        dismissProgress(this);
        mDatas.clear();
        List<GoosPerson> goosPersonLists = new Gson().fromJson(response, new TypeToken<List<GoosPerson>>() {
        }.getType());
        for (int i = 0; i < goosPersonLists.size(); i++) {
            mDatas.add(goosPersonLists.get(i));
        }
        if(mDatas.size()==0){
            scrollView.setVisibility(View.GONE);
            ll_noaddress_layout.setVisibility(View.VISIBLE);
        }else{
            scrollView.setVisibility(View.VISIBLE);
            ll_noaddress_layout.setVisibility(View.GONE);
        }
        personInfoAdapter.notifyDataSetChanged();
    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(this);
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(this);
        T.showShort(this, R.string.jianchawangluo);
    }
}
