package taiyang.com.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.bigkoo.pickerview.OptionsPickerView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.entity.CityModel;
import taiyang.com.entity.ProvinceModel;
import taiyang.com.entity.UserModel;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.ACache;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.T;
import taiyang.com.utils.ToastUtil;

/**
 * Created by zhew on 9/16/17.
 * Copyright © 2006-2016 365jia.cn 安徽大尺度网络传媒有限公司版权所有
 **/

public class RuzhuActivity extends BaseActivity {

    @InjectView(R.id.et_email)
    EditText etEmail;
    @InjectView(R.id.et_address)
    EditText etAddress;
    @InjectView(R.id.et_shop_info)
    EditText etShopInfo;
    @InjectView(R.id.cb_zhu)
    CheckBox cbZhu;
    @InjectView(R.id.cb_niu)
    CheckBox cbNiu;
    @InjectView(R.id.cb_yang)
    CheckBox cbYang;
    @InjectView(R.id.cb_qinlei)
    CheckBox cbQinlei;
    @InjectView(R.id.cb_shuichan)
    CheckBox cbShuichan;
    OptionsPickerView pvOptions;
    //地址
    @InjectView(R.id.address_layout)
    RelativeLayout addressLayout;
    //    @InjectView(R.id.tv_location)
    TextView tvLocation;
    @InjectView(R.id.et_username)
    EditText et_username;
    //真实姓名
    @InjectView(R.id.et_alias)
    EditText et_alias;
    @InjectView(R.id.et_shop_name)
    EditText et_shop_name;
    //保存信息
    @InjectView(R.id.tv_save)
    Button tv_save;
    private int firstOptions1;
    private int firstOption2;
    //省市二级联动
    private ArrayList<ProvinceModel> options1Items = new ArrayList<>();
    private ArrayList<ArrayList<CityModel>> options2Items = new ArrayList<>();
    private String userName;
    private UserModel userModel;
    private ACache mCache;//主要用于缓存地址
    private boolean first = true;
    private List<CityModel> mCityList;
    private ArrayList<CityModel> options2Items_child;
    //已经存在省份市区
    private String hasProvinceId;
    private String hasCityId;
    private String provice;
    private String city;
    private String provinceId;
    private String cityId;

    @OnClick(R.id.address_layout)
    public void addressLayout(View v) {
        pvOptions.show();
    }

    @OnClick(R.id.tv_save)
    public void save() {

        showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
        Map<String, Object> params = new HashMap<>();
        params.put("model", "seller");
        params.put("action", "ruzhu");
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller", "ruzhu")));
        params.put("user_id", mySPEdit.getUserId());
        params.put("shop_name", et_shop_name.getText().toString());
        params.put("real_name", et_alias.getText().toString());
        params.put("address", etAddress.getText().toString());
        StringBuffer product = new StringBuffer();
        if (cbZhu.isChecked()) {
            product.append(" 猪");
        }
        if (cbNiu.isChecked()) {
            product.append(" 牛");
        }
        if (cbYang.isChecked()) {
            product.append(" 羊");
        }
        if (cbShuichan.isChecked()) {
            product.append(" 水产");
        }
        if (cbQinlei.isChecked()) {
            product.append(" 禽类");
        }
        params.put("product", product.toString().trim());
        params.put("email", etEmail.getText().toString());
        params.put("shop_info", etShopInfo.getText().toString());
        params.put("province", provinceId);
        params.put("city", cityId);
        L.e(JSON.toJSONString(params));
        HttpUtils.sendPost(params, this, 101);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCache = ACache.get(this);
        tvLocation = (TextView) findViewById(R.id.tv_location);
        userModel = new Gson().fromJson(mySPEdit.getUserInfo(), UserModel.class);
        hasProvinceId = userModel.getProvince();
        hasCityId = userModel.getCity();
        initView();
        initData();
    }

    //初始化数据
    private void initData() {
        //选项选择器
        pvOptions = new OptionsPickerView(this);
        if (TextUtils.isEmpty(mCache.getAsString("location"))) {
            HttpUtils.sendPost(this, 100, first);
        } else {
            String location = mCache.getAsString("location");
            initLocation(location);
        }
    }

    private void initView() {

        userName = userModel.getUser_name();
        et_username.setText(userName);
        if (hasProvinceId.equals("0") && hasCityId.equals("0")) {
            tvLocation.setText(getString(R.string.qingxuanze));
            mySPEdit.setFirstOptions1(0);
            mySPEdit.setFirstOption2(0);
            mySPEdit.setIsAddress(false);
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_ruzhu;
    }

    @Override
    public void success(String response, int id) {

        if (id == 100) {
            mCache.put("location", response, 6 * ACache.TIME_DAY);
            initLocation(response);

        }
        if (id == 101) {
            dismissProgress(this);
            ToastUtil.showToast("申请成功");
            finish();
        }

    }

    private void initLocation(String locationStr) {
        Type listType = new TypeToken<LinkedList<ProvinceModel>>() {
        }.getType();
        Gson gson = new Gson();
        LinkedList<ProvinceModel> provinceListModel = gson.fromJson(locationStr, listType);
        for (Iterator iterator = provinceListModel.iterator(); iterator.hasNext(); ) {
            ProvinceModel provinceModel = (ProvinceModel) iterator.next();
            if (hasProvinceId.equals(provinceModel.getId())) {
                provice = provinceModel.getName();
                mySPEdit.setFirstOptions1(firstOptions1);
            }
            options1Items.add(provinceModel);
            mCityList = provinceModel.getCity_list();
            options2Items_child = new ArrayList<>();
            for (int i = 0; i < mCityList.size(); i++) {
                options2Items_child.add(mCityList.get(i));
                if (hasCityId.equals(mCityList.get(i).getId())) {
                    city = mCityList.get(i).getName();
                    firstOption2 = i;
                    mySPEdit.setFirstOption2(firstOption2);
                }
            }
            options2Items.add(options2Items_child);
            firstOptions1++;
        }
        if (provice != null && city != null) {
            tvLocation.setText(provice + city);
        }

        //三级联动效果
//        pvOptions.setPicker(options1Items, options2Items, options3Items,true);
        pvOptions.setPicker(options1Items, options2Items, true);
        //设置选择的三级单位
//        pwOptions.setLabels("省", "市", "区");
        pvOptions.setTitle("选择城市");
        pvOptions.setCyclic(false);
        //设置默认选中的三级项目
        //监听确定选择按钮
        pvOptions.setSelectOptions(mySPEdit.getFirstOptions1(), mySPEdit.getFirstOption2());
        pvOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {

            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
//                    L.e("options1:"+options1);
//                    L.e("option2:"+option2);
                firstOptions1 = options1;
                firstOption2 = option2;
                //返回的分别是三个级别的选中位置
                String tx = options1Items.get(options1).getPickerViewText()
                        + options2Items.get(options1).get(option2).getPickerViewText();
                provinceId = options1Items.get(options1).getId();
                cityId = options2Items.get(options1).get(option2).getId();
//                    tvLocation.setText(tx+"//"+provinceId+"//"+cityId);
                tvLocation.setText(tx);
            }
        });
    }

    @Override
    public void failByOther(String fail, int id) {
        ToastUtil.showToast(fail);
        dismissProgress(this);
    }

    @Override
    public void fail(String fail, int id) {
        ToastUtil.showToast(fail);
        dismissProgress(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (pvOptions.isShowing()) {
                pvOptions.dismiss();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }


}
