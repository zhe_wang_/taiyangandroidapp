package taiyang.com.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.view.ContextThemeWrapper;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;

import com.facebook.drawee.view.SimpleDraweeView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.tydp_b.KProgressActivity;
import taiyang.com.tydp_b.R;

/**
 * 编辑卖家界面
 */
public class EditSellerActivity extends KProgressActivity {
    private String fileName;
    @InjectView(R.id.back_layout)
    LinearLayout backLayout;

    @OnClick(R.id.back_layout)
    public void back(View v) {
        finish();
    }

    @InjectView(R.id.im_personlogo)
    SimpleDraweeView personLogo;
    private static String path = "/sdcard/myHead/";// sd路径
    private Dialog dialog_pic;
    @InjectView(R.id.head_layout)
    RelativeLayout headLayout;

    @OnClick(R.id.head_layout)
    public void headLayout(View v) {
        openPictureSelectDialog();
    }

    /**
     * 打开对话框
     **/
    private void openPictureSelectDialog() {
        //自定义Context,添加主题
        Context dialogContext = new ContextThemeWrapper(this, android.R.style.Theme_Light);
        String[] choiceItems = new String[2];
        choiceItems[0] = "相机拍摄";  //拍照
        choiceItems[1] = "本地相册";  //从相册中选择
        ListAdapter adapter = new ArrayAdapter<String>(dialogContext, android.R.layout.simple_list_item_1, choiceItems);
        //对话框建立在刚才定义好的上下文上
        AlertDialog.Builder builder = new AlertDialog.Builder(dialogContext);
        builder.setTitle("选择图片");
        builder.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:  //相机
//                        doTakePhoto();
                        Intent intent2 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent2.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(
                                Environment.getExternalStorageDirectory(), "head.jpg")));
                        startActivityForResult(intent2, 2);// 采用ForResult打开
                        break;
                    case 1:  //从图库相册中选取
//                        doPickPhotoFromGallery();
                        Intent intent1 = new Intent(Intent.ACTION_PICK, null);
                        intent1.setDataAndType(
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                        startActivityForResult(intent1, 1);
                        break;
                }
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_seller);
        ButterKnife.inject(this);
//        Uri uri=Uri.parse("file://"+path+"head.jpg");
//        personLogo.setImageURI(uri);
//        personLogo.setImageURI(path+"head.jpg");
        String pathStr = Environment.getExternalStorageDirectory() + File.separator + "myHead/head.jpg";
//        Uri uri=Uri.parse("file://"+pathStr);
//        Uri.parseFromFile(new File(path));
        Bitmap bimap = BitmapFactory.decodeFile(pathStr);
//        String fileName = path + "head.jpg";
//        Bitmap bt = BitmapFactory.decodeFile(path + "head.jpg");// 从Sd中找头像，转换成Bitmap
        Bitmap bt = BitmapFactory.decodeFile(pathStr);// 从Sd中找头像，转换成Bitmap
        Uri uri = Uri.fromFile(new File(pathStr));
//        personLogo.setImageBitmap(bt);
//        personLogo.setImageURI(uri);
//        personLogo.setImageURI(uri);
//        personLogo.setimage
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    cropPhoto(data.getData());// 裁剪图片
                }
                break;
            case 2:
                if (resultCode == RESULT_OK) {
                    File temp = new File(Environment.getExternalStorageDirectory()
                            + "/head.jpg");
                    cropPhoto(Uri.fromFile(temp));// 裁剪图片
                }

                break;
            case 3:
                if (data != null) {
                    Bundle extras = data.getExtras();
//                    Uri uri = (Uri) extras.getParcelable(MediaStore.EXTRA_OUTPUT);
//                    Log.e("ceshi", "onActivityResult: "+(uri==null) );
                    Bitmap head = extras.getParcelable("data");
//                    personLogo.setImageBitmap(head);

                    if (head != null) {
                        /**
                         * 上传服务器代码
                         */
                        if (fileName != null) {
                            delFile(fileName);
                        }
                        setPicToView(head);// 保存在SD卡中
//                        ivHead.setImageBitmap(head);// 用ImageView显示出来
                        Uri uri = Uri.fromFile(new File(fileName));
                        personLogo.setImageURI(uri);
                    }
                }
                break;
            default:
                break;

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 调用系统的裁剪
     *
     * @param uri
     */
    public void cropPhoto(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, 3);
    }

    private void setPicToView(Bitmap mBitmap) {
        String sdStatus = Environment.getExternalStorageState();
        if (!sdStatus.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
            return;
        }
        FileOutputStream b = null;
        File file = new File(path);
        file.mkdirs();// 创建文件夹
        fileName = path + UUID.randomUUID() + ".jpg";// 图片名字
        try {
            b = new FileOutputStream(fileName);
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, b);// 把数据写入文件
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                // 关闭流
                b.flush();
                b.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    //删除文件
    public void delFile(String fileName) {
        File file = new File(fileName);
        if (file.isFile()) {
            file.delete();
        }
        file.exists();
    }
}
