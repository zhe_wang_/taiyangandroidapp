package taiyang.com.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.umeng.analytics.MobclickAgent;

import cn.magicwindow.MLinkAPIFactory;
import cn.magicwindow.mlink.YYBCallback;
import cn.magicwindow.mlink.annotation.MLinkDefaultRouter;
import taiyang.com.tydp_b.BuildConfig;
import taiyang.com.tydp_b.MainActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.L;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.T;
import taiyang.com.utils.ToastUtil;

@MLinkDefaultRouter
public class SplashActivity extends AppCompatActivity {
    private Handler mHandler = new Handler();
    private MySPEdit mySPEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash);

//跳转router调用
        if (getIntent().getData()!=null) {
            MLinkAPIFactory.createAPI(this).router(getIntent().getData());
            //跳转后结束当前activity
            finish();
        } else {

            MLinkAPIFactory.createAPI(this).checkYYB(SplashActivity.this, new YYBCallback() {
                @Override
                public void onFailed(Context context) {
                    goHome();
                }

                @Override
                public void onSuccess() {
                    finish();
                }
            });
        }

    }

    private void goHome() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    SplashActivity.this.finish();
            }
        }, 1000);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!BuildConfig.DEBUG) {
            MobclickAgent.onResume(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (!BuildConfig.DEBUG) {
            MobclickAgent.onResume(this);
        }
    }
}
