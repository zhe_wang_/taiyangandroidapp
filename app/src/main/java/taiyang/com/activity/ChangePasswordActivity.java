package taiyang.com.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.HashMap;
import java.util.Map;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.T;

/**
 * 修改密码界面
 */
public class ChangePasswordActivity extends BaseActivity {

    @InjectView(R.id.et_oldpassword)
    EditText oldPassword;
    @InjectView(R.id.et_newpassword)
    EditText newPassword;
    @InjectView(R.id.et_newrpassword)
    EditText newrPassword;

    @InjectView(R.id.et_oldpass_bg)
    View oldPasswordBg;
    @InjectView(R.id.et_newpassword_bg)
    View newPasswordBg;
    @InjectView(R.id.et_newrpassword_bg)
    View newrPasswordBg;
//    @InjectView(R.id.back_layout)
//    LinearLayout backLayout;
//
//    @OnClick(R.id.back_layout)
//    public void back(View v) {
//        finish();
//    }
    @InjectView(R.id.bt_save)
    Button bt_save;
    @OnClick(R.id.bt_save)
    public void save(View v){
        String oldPasswordStr=oldPassword.getText().toString();
        String newPasswordStr1=newPassword.getText().toString();
        String newPasswordStr2=newrPassword.getText().toString();
        if (!TextUtils.isEmpty(oldPasswordStr)&&!TextUtils.isEmpty(newPasswordStr1)&&!TextUtils.isEmpty(newPasswordStr2)){
            if (newPasswordStr1.equals(newPasswordStr2)){
                showProgress(this,getString(R.string.jiazaizhong_dotdotdot));
                Map<String,Object> params=new HashMap<>();
                params.put("model","user");
                params.put("action","edit_password");
                params.put("sign", MD5Utils.encode(MD5Utils.getSign("user","edit_password")));
                params.put("user_id",mySPEdit.getUserId());
                params.put("password",oldPasswordStr);
                params.put("new_password",newPasswordStr1);
                params.put("token",mySPEdit.getToken());
                HttpUtils.sendPost(params,this);
            }else {
                T.showShort(this,"两次输入密码不一致!");
            }
        }else {
            T.showShort(this,"请输入完整信息!");
        }

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_change_password);
//        ButterKnife.inject(this);
        mTitle.setText(R.string.xiugaidenglumima);
        initListener();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_change_password;
    }

    private void initListener() {
        oldPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                changeBg(oldPasswordBg, hasFocus);
            }
        });
        newPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                changeBg(newPasswordBg, hasFocus);
            }
        });
        newrPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                changeBg(newrPasswordBg, hasFocus);
            }
        });
    }

    private void changeBg(View v, boolean hasFocus) {
        if (hasFocus) {
            v.setBackgroundColor(getResources().getColor(R.color.bg_bluecolor));
        } else {
            v.setBackgroundColor(getResources().getColor(R.color.bg_color));
        }
    }

    @Override
    public void success(String response, int id) {
        dismissProgress(this);
        T.showShort(this,"修改成功!");
        finish();
    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(this);
        T.showShort(this,fail);
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(this);
        T.showShort(this,R.string.jianchawangluo);
    }
}
