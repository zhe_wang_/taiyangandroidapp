package taiyang.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.kymjs.kjframe.http.HttpConfig;

import java.io.File;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.cache.DataCleanManager;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.ACache;
import taiyang.com.utils.Constants;
import taiyang.com.utils.FileUtil;
import taiyang.com.utils.L;
import taiyang.com.utils.MethodsCompat;
import taiyang.com.utils.MyApplication;
import taiyang.com.utils.T;

/**
 * 设置界面
 */
public class SettingActivity extends BaseActivity {
    private MyApplication application = MyApplication.getInstance();
    @InjectView(R.id.logout)
    Button logoutButton;

    @OnClick(R.id.logout)
    public void logout(View v) {
        mySPEdit.setIsLogin(false);
//        application.setCurrentUser(null);
        mySPEdit.setToken(null);
        myApplication.setPersonChange(true);
        myApplication.setAddressChange(true);
        //数据是使用Intent返回
        Intent intent = new Intent();
        //设置返回数据
        SettingActivity.this.setResult(Constants.LOGOOUT, intent);
        //关闭Activity
        SettingActivity.this.finish();
    }

//    @InjectView(R.id.back_layout)
//    LinearLayout backLayout;
//
//    @OnClick(R.id.back_layout)
//    public void back(View v) {
//        finish();
//    }

    @InjectView(R.id.changepas_layout)
    RelativeLayout changepasLayout;

    @OnClick(R.id.changepas_layout)
    public void changepasLayout(View v) {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        startActivity(intent);
    }

    @InjectView(R.id.about_layout)
    RelativeLayout aboutLayout;

    @OnClick(R.id.about_layout)
    public void about(View v) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    //缓存大小
    @InjectView(R.id.tv_cachesize)
    TextView tv_cachesize;

    //清除缓存
    @InjectView(R.id.clear_layout)
    RelativeLayout clearLayout;

    @OnClick(R.id.clear_layout)
    public void clearLayout(View v) {
//        Intent intent=new Intent(this,AboutActivity.class);
//        startActivity(intent);
//        ImagePipeline imagePipeline = Fresco.getImagePipeline();
//        imagePipeline.clearCaches();
        DataCleanManager.clearAllCache(this);
        L.e("成功!"+mCache.remove("location"));
        T.showShort(this, getString(R.string.qinglichenggong));
        tv_cachesize.setText("0.00KB");
    }
    private ACache mCache;//主要用于缓存地址
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_setting);
//        ButterKnife.inject(this);
        mTitle.setText(R.string.shezhi);
        mCache=ACache.get(this);
////            tv_cachesize.setText(DataCleanManager.getTotalCacheSize(this));
        caculateCacheSize();
    }

    /**
     * 计算缓存的大小
     */
    private void caculateCacheSize() {
        long fileSize = 0;
        String cacheSize = "0KB";
        File filesDir = getFilesDir();
        File cacheDir = getCacheDir();

        fileSize += FileUtil.getDirSize(filesDir);
        fileSize += FileUtil.getDirSize(cacheDir);
        // 2.2版本才有将应用缓存转移到sd卡的功能
        if (MyApplication.isMethodsCompat(android.os.Build.VERSION_CODES.FROYO)) {
            File externalCacheDir = MethodsCompat
                    .getExternalCacheDir(this);
            fileSize += FileUtil.getDirSize(externalCacheDir);
            fileSize += FileUtil.getDirSize(new File(
                    org.kymjs.kjframe.utils.FileUtils.getSDCardPath()
                            + File.separator + HttpConfig.CACHEPATH));
        }
        if (fileSize > 0)
            cacheSize = FileUtil.formatFileSize(fileSize);
        tv_cachesize.setText(cacheSize);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_setting;
    }

    @Override
    public void success(String response, int id) {

    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

}
