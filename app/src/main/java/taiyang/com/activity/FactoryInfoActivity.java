package taiyang.com.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.hankkin.gradationscroll.GradationScrollView;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.utils.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import cn.magicwindow.mlink.annotation.MLinkRouter;
import taiyang.com.adapter.FactoryInfoAdapter;
import taiyang.com.entity.FactorGoodsListModel;
import taiyang.com.entity.FactorGoodsModel;
import taiyang.com.entity.SellerModel;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.ToastUtil;

/**
 * 店铺详情界面
 */

@MLinkRouter(keys={"taiyanggo_shop"})
public class FactoryInfoActivity extends BaseActivity implements GradationScrollView.ScrollViewListener, HttpRequestListener {

    private ListView listView;
    private RelativeLayout ivBanner;
    private FactoryInfoAdapter mAdapter;
    LinearLayout  search02, top_layout;
    @InjectView(R.id.iv_factoryinfo_back)
    ImageView iv_factoryinfo_back;

    @OnClick(R.id.iv_factoryinfo_back)
    public void back() {
        finish();
    }

    @InjectView(R.id.et_content)
    EditText et_content;
    @InjectView(R.id.iv_search)
    ImageView iv_search;

    @InjectView(R.id.iv_factoryinfo_share)
    ImageView iv_factoryinfo_share;

    @OnClick(R.id.iv_factoryinfo_share)
    public void share(View v) {
        L.e("点击了分享按钮");
        new ShareAction(FactoryInfoActivity.this).setDisplayList(SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE)
                .withTitle(tv_factoryinfo_shopname.getText().toString())
                .withText("首农冻品" + "")
                .withTargetUrl("http://www.taiyanggo.com/mobile/share.php?act=shop&id="+shop_id)
                .withMedia(new UMImage(FactoryInfoActivity.this, mSellerModel.getUser_face()))
                .open();
    }

    private String content;

    @OnClick(R.id.iv_search)
    public void search(View v) {
        mAdapter.getmList().clear();
        content = et_content.getText().toString();
        page = 1;
        index = 3;
        rb_firm_all_hide.setChecked(true);
        loadSearchResult();
    }

    @OnClick(R.id.tv_info)
    public void showInfo(View v) {
       initPopwindow();
    }
    @OnClick(R.id.tv_call)
    public void call(View v) {
        Intent intent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+mSellerModel.getService_hotline()));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
    private void resetAll(){
        resetFirstClass();
        resetSecondClass();
        resetBaseLine();

    }

    private void resetBaseLine() {
        rbLingshouBg.setVisibility(View.INVISIBLE);
        rbZhengguiBg.setVisibility(View.INVISIBLE);
        rbZhunxianhuoBg.setVisibility(View.INVISIBLE);
        rbQihuoBg.setVisibility(View.INVISIBLE);
        rbXianhuoBg.setVisibility(View.INVISIBLE);
        if(goods_type.equals("8")){
            rbZhunxianhuoBg.setVisibility(View.VISIBLE);
        }
        if(goods_type.equals("7")){
            rbXianhuoBg.setVisibility(View.VISIBLE);}
        if(goods_type.equals("6")){
            rbQihuoBg.setVisibility(View.VISIBLE);}
        if(sell_type.equals("5")){
            rbZhengguiBg.setVisibility(View.VISIBLE);}
        if(sell_type.equals("4")){
            rbLingshouBg.setVisibility(View.VISIBLE);}
    }

    private void resetSecondClass() {
        rbLingshou.setChecked(false);
        rbZhunxianhuo.setChecked(false);
        rbXianhuo.setChecked(false);
        rbQihuo.setChecked(false);
        rbZhenggui.setChecked(false);
    }

    private void resetFirstClass() {
        rb_firm_all_hide.setChecked(false);
        rb_firm_new_hide.setChecked(false);
        rb_firm_best_hide.setChecked(false);
    }

    private void loadSearchResult() {
        resetAll();
        showProgress(this,getResources().getString(R.string.jiazaizhong_dotdotdot));
        if (!TextUtils.isEmpty(content)) {
            Map<String, Object> params = new HashMap<>();
            params.put("model", "goods");
            params.put("action", "get_list");
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("goods", "get_list")));
            params.put("user_id", user_id);
            params.put("keywords", content);
            params.put("page", page);
            HttpUtils.sendPost(params, this, 104);
        }
    }

    private String shop_id;
    private int index;

    private void changeTab(int i) {
        goods_type = "";
        sell_type = "";
        mAdapter.getmList().clear();
        et_content.clearFocus();
        et_content.setText("");
        page = 1;
        index = i;
        resetSecondClass();
        resetBaseLine();
    }

    private SellerModel mSellerModel;
    RadioButton rbZhunxianhuo;

    RadioButton rbXianhuo;

    RadioButton rbQihuo;

    RadioButton rbZhenggui;

    RadioButton rbLingshou;

    View rbZhunxianhuoBg;

    View rbXianhuoBg;

    View rbQihuoBg;

    View rbZhengguiBg;

    View rbLingshouBg;
    TextView tvGuanzhuCount;

    Button btnGuanzhu;

    SimpleDraweeView iv_factoryinfo_logo;

    TextView tv_factoryinfo_shopname;

    TextView tv_factoryinfo_location;

    RadioButton rb_firm_all_hide;

    RadioButton rb_firm_new_hide;

    RadioButton rb_firm_best_hide;

    View lvHeader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.inject(this);
        lvHeader = this.getLayoutInflater().inflate(R.layout.activity_factoryinfo_header,null);
        rbZhunxianhuo = (RadioButton) lvHeader.findViewById(R.id.rb_zhunxianhuo);
        rbXianhuo = (RadioButton) lvHeader.findViewById(R.id.rb_xianhuo);
        rbQihuo = (RadioButton) lvHeader.findViewById(R.id.rb_qihuo);
        rbZhenggui = (RadioButton) lvHeader.findViewById(R.id.rb_zhenggui);
        rbLingshou = (RadioButton) lvHeader.findViewById(R.id.rb_lingshou);
        rbZhunxianhuoBg = lvHeader.findViewById(R.id.rb_zhunxianhuo_bg);
        rbXianhuoBg = lvHeader.findViewById(R.id.rb_xianhuo_bg);
        rbQihuoBg = lvHeader.findViewById(R.id.rb_qihuo_bg);
        tvGuanzhuCount = (TextView) lvHeader.findViewById(R.id.tv_guanzhu_count);
        rbZhengguiBg = lvHeader.findViewById(R.id.rb_zhenggui_bg);
        rbLingshouBg = lvHeader.findViewById(R.id.rb_lingshou_bg);
        btnGuanzhu = (Button) lvHeader.findViewById(R.id.btn_guanzhu);
        iv_factoryinfo_logo = (SimpleDraweeView) lvHeader.findViewById(R.id.iv_factoryinfo_logo);
        tv_factoryinfo_shopname = (TextView) lvHeader.findViewById(R.id.tv_factoryinfo_shopname);
        tv_factoryinfo_location = (TextView) lvHeader.findViewById(R.id.tv_factoryinfo_location);
        rb_firm_all_hide = (RadioButton) lvHeader.findViewById(R.id.rb_firm_all_hide);
        rb_firm_all_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTab(0);
                initAllData();
            }
        });

        rb_firm_new_hide = (RadioButton) lvHeader.findViewById(R.id.rb_firm_new_hide);
        rb_firm_new_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTab(1);
                initNewData();
            }
        });
        rb_firm_best_hide = (RadioButton) lvHeader.findViewById(R.id.rb_firm_best_hide);
        rb_firm_best_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTab(2);
                initBestData();
            }
        });

        listView = (ListView) findViewById(R.id.listview);
        listView.addHeaderView(lvHeader);
        ivBanner = (RelativeLayout) findViewById(R.id.iv_banner);
        ivBanner.setFocusable(true);
        ivBanner.setFocusableInTouchMode(true);
        ivBanner.requestFocus();
        search02 = (LinearLayout) findViewById(R.id.search02);
        top_layout = (LinearLayout) findViewById(R.id.top_layout);
        initData();
        resetBaseLine();
        resetSecondClass();
        rbZhunxianhuo.setOnClickListener(clickListener);
        rbXianhuo.setOnClickListener(clickListener);
        rbQihuo.setOnClickListener(clickListener);
        rbLingshou.setOnClickListener(clickListener);
        rbZhenggui.setOnClickListener(clickListener);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_factoryinfo;
    }


    private PopupWindow window;

    private void initPopwindow() {

        // 利用layoutInflater获得View
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_factory_info_, null);

        // 下面是两种方法得到宽度和高度 getWindow().getDecorView().getWidth()

        ((TextView)view.findViewById(R.id.tv_gongsimingcheng)).setText(mSellerModel.getShop_name());
        ((TextView)view.findViewById(R.id.tv_guanzhurenshu)).setText(mSellerModel.getFollow_count()+(getString(R.string.yiguanzhu)));
        ((TextView)view.findViewById(R.id.tv_dianpujianjie)).setText(mSellerModel.getShop_info());
        ((TextView)view.findViewById(R.id.tv_suozaidiqu)).setText(mSellerModel.getAddress());
        ((TextView)view.findViewById(R.id.tv_kaidianshijian)).setText(mSellerModel.getCreated());



        window = new PopupWindow(view,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.alpha = 0f;

        getWindow().setAttributes(params);
        // 设置popWindow弹出窗体可点击，这句话必须添加，并且是true
        window.setFocusable(true);

        // 必须要给调用这个方法，否则点击popWindow以外部分，popWindow不会消失
        // window.setBackgroundDrawable(new BitmapDrawable());

        window.setBackgroundDrawable(new BitmapDrawable());
        // 在参照的View控件下方显示
        // window.showAsDropDown(MainActivity.this.findViewById(R.id.start));

        // 设置popWindow的显示和消失动画
        window.setAnimationStyle(R.style.mypopwindow_anim_style);
        // 在底部显示
        window.showAtLocation(FactoryInfoActivity.this.findViewById(R.id.tv_info),
                Gravity.CENTER, 0, 0);
        // 这里检验popWindow里的button是否可以点击
        view.findViewById(R.id.ll_all).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                window.dismiss();
            }
        });


        // popWindow消失监听方法
        window.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                System.out.println("popWindow消失");
                WindowManager.LayoutParams params = getWindow().getAttributes();
                params.alpha = 1f;
                getWindow().setAttributes(params);
            }
        });
    }

    private String user_id;
    private int page = 1;
    private int pageCount;

    private void initData() {
        showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
        shop_id = getIntent().getStringExtra("shop_id");
        Map<String, Object> params = new HashMap<>();
        params.put("model", "user");
        params.put("action", "get_shop");
        params.put("user_id", MySPEdit.getInstance(this).getUserId());
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "get_shop")));
        params.put("shop_id", shop_id);
        HttpUtils.sendPost(params, this, 100);
        mAdapter = new FactoryInfoAdapter(new ArrayList<FactorGoodsModel>(),this);
        listView.setAdapter(mAdapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount > 0) {
                    onLoadMore();
                }
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(FactoryInfoActivity.this, OfferInfoActivity.class);
                intent.putExtra("Goods_id", view.findViewById(R.id.imageView).getTag().toString());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onScrollChanged(GradationScrollView scrollView, int x, int y, int oldx, int oldy) {
        // TODO Auto-generated method stub
    }

    private FactorGoodsListModel mFactorGoodsListModel;

    @Override
    public void success(String response, int id) {
        dismissProgress(this);
        switch (id) {
            case 100:
                mSellerModel = new Gson().fromJson(response, SellerModel.class);
                user_id = mSellerModel.getUser_id();
                tv_factoryinfo_shopname.setText(mSellerModel.getShop_name());
                MobclickAgent.onEvent(FactoryInfoActivity.this,mSellerModel.getShop_name());
                tvGuanzhuCount.setText(mSellerModel.getFollow_count()+getString(R.string.yiguanzhu));
                tv_factoryinfo_location.setText(mSellerModel.getProvince() + " " + mSellerModel.getCity());
                iv_factoryinfo_logo.setImageURI(Uri.parse(mSellerModel.getUser_face()));
                initAllData();
                if(mSellerModel.getIs_follow()==1){
                    btnGuanzhu.setText(getString(R.string.yiguanzhu));

                }else{
                    btnGuanzhu.setText(getString(R.string.guanzhu));
                }
                btnGuanzhu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(MySPEdit.getInstance(FactoryInfoActivity.this).getIsLogin()) {
                            boolean action = btnGuanzhu.getText().toString().equals(getString(R.string.yiguanzhu));
                            Map<String, Object> params = new HashMap<>();
                            params.put("model", "user");
                            params.put("action", action ? "unfollow_store" : "follow_store");
                            params.put("user_id", MySPEdit.getInstance(FactoryInfoActivity.this).getUserId());
                            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", action ? "unfollow_store" : "follow_store")));
                            params.put("shop_id", shop_id);
                            HttpUtils.sendPost(params, FactoryInfoActivity.this, action ? 106 : 105);
                        }else{
                            ToastUtil.showToast(getResources().getString(R.string.qingxiandengku));
                        }
                    }
                });
                break;
            case 101:
            case 102:
            case 103:
            case 104:
                setData(response);
                break;
            case 105:
                btnGuanzhu.setText(getString(R.string.yiguanzhu));
                break;
            case 106:
                btnGuanzhu.setText(getString(R.string.guanzhu));
                break;
        }

    }

    private void setData(String response) {
        mFactorGoodsListModel = new Gson().fromJson(response, FactorGoodsListModel.class);
        pageCount = mFactorGoodsListModel.getTotal().getPage_count();
        if(page==1){
            mAdapter.getmList().clear();
        }
        mAdapter.getmList().addAll(mFactorGoodsListModel.getList());
        mAdapter.notifyDataSetChanged();
    }

    //初始化所有数据
    private void initAllData() {
        Map<String, Object> params = new HashMap<>();
        params.put("user_id", user_id);
        params.put("model", "goods");
        params.put("action", "get_list");
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("goods", "get_list")));
        params.put("page", page);
        addSecondClass(params);
        HttpUtils.sendPost(params, this, 101);
    }

    //初始化上新
    private void initNewData() {
        Map<String, Object> params = new HashMap<>();
        params.put("user_id", user_id);
        params.put("model", "goods");
        params.put("action", "get_list");
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("goods", "get_list")));
        params.put("page", page);
        params.put("size", 20);
        addSecondClass(params);
        HttpUtils.sendPost(params, this, 102);
    }

    private void addSecondClass(Map<String, Object> params) {
        if(!sell_type.equals("")){
            params.put("sell_type",sell_type);
        }
        if(!goods_type.equals("")){
            params.put("goods_type",goods_type);
        }
    }

    //初始化促销
    private void initBestData() {
        Map<String, Object> params = new HashMap<>();
        params.put("user_id", user_id);
        params.put("model", "goods");
        params.put("action", "get_list");
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("goods", "get_list")));
        params.put("page", page);
        params.put("type", "best");
        addSecondClass(params);
        HttpUtils.sendPost(params, this, 103);
    }

    @Override
    public void failByOther(String fail, int id) {
        ToastUtil.showToast(fail);
        dismissProgress(this);
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(this);
    }

    public void onLoadMore() {
        if(index == 1){
            return;
        }
        if (page >= pageCount) {
            return;
        }
        page++;
        if (index == 0) {
            initAllData();
        } else if (index == 1) {
            initNewData();
        } else if (index == 2) {
            initBestData();
        } else {
            loadSearchResult();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /** attention to this below ,must add this**/
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
        Log.d("result", "onActivityResult");
    }
    private String goods_type="";
    private String sell_type="";


    public View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(((RadioButton)view).getText().equals(getString(R.string.zhunxianhuo))){
                goods_type = "8";
            }
            if(((RadioButton)view).getText().equals(getString(R.string.xianhuo))){
                goods_type = "7";
            }
            if(((RadioButton)view).getText().equals(getString(R.string.qihuo))){
                goods_type = "6";
            }
            if(((RadioButton)view).getText().equals(getString(R.string.guishou))){
                sell_type = "5";
            }
            if(((RadioButton)view).getText().equals(getString(R.string.dunshou))){
                sell_type = "4";
            }
            showProgress(FactoryInfoActivity.this,getString(R.string.jiazaizhong_dotdotdot));
            page=1;
            resetBaseLine();
            if (index == 0) {
                initAllData();
            } else if (index == 1) {
                initNewData();
            } else if (index == 2) {
                initBestData();
            }
        }
    };

}
