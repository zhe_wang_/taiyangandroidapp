package taiyang.com.activity;


import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.adapter.NewsAdapter;
import taiyang.com.entity.InfoModel;
import taiyang.com.entity.NewsModel;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.T;

/*import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;*/

/*import taiyang.com.tydp_b.R;*/

/**
 * 资讯
 * Created by hoo on 2016/7/5.
 */
public class InfoActivity extends BaseActivity implements HttpRequestListener {

    @InjectView(R.id.news_recyclerView)
    RecyclerView mRecyclerView;
    @InjectView(R.id.rg_info)
    RadioGroup rg_info;
    //    @InjectView(R.id.ll_info)
//    LinearLayout ll_info;
    private RadioButton rbt_notice, rbt_marketinfo;
    private InfoModel infoModel;
    private List<NewsModel> mList;
    private NewsAdapter mAdapter;
    private NewsModel newsModel;
    @InjectView(R.id.progress_layout)
    RelativeLayout progerss_layout;

    @InjectView(R.id.tv_title)
    TextView tv_title;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);

        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mList = new ArrayList<>();
        mAdapter = new NewsAdapter(this, mList);

        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickLitener(new NewsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        initData();
    }


    private void initView() {
        tv_title.setText(getString(R.string.hangyezixun));
        RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(0,
                RadioGroup.LayoutParams.MATCH_PARENT);
        params.weight = 1;
        if (infoModel.getCategory().size() == 1) {
            L.e("0000");
        } else if (infoModel.getCategory().size() == 2) {
            L.e("1111");
            rbt_notice = new RadioButton(this);
//            L.e("id:"+infoModel.getCategory().get(0).getCat_id());
//            rbt_marketinfo.setId(Integer.parseInt(infoModel.getCategory().get(0).getCat_id()));
            rbt_notice.setButtonDrawable(new StateListDrawable());
            rbt_notice.setBackgroundResource(R.drawable.notice_selector);
            rbt_notice.setTextColor(getResources().getColorStateList(R.color.info_tab_textcolor));
            rbt_notice.setGravity(Gravity.CENTER);
            rbt_notice.setText(infoModel.getCategory().get(0).getCat_name());
//            rbt_notice.setChecked(true);
            rg_info.addView(rbt_notice, params);
            rbt_notice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showProgress(InfoActivity.this,getString(R.string.jiazaizhong_dotdotdot));
                    Map<String, Object> params = new HashMap<>();
                    params.put("model", "other");
                    params.put("action", "get_article");
                    params.put("id", infoModel.getCategory().get(0).getCat_id());
                    params.put("sign", MD5Utils.encode(MD5Utils.getSign("other", "get_article")));
                    HttpUtils.sendPost(params, InfoActivity.this, 101);
                }
            });

            rbt_marketinfo = new RadioButton(this);
//            rbt_marketinfo.setId(Integer.parseInt(infoModel.getCategory().get(1).getCat_id()));
            rbt_marketinfo.setButtonDrawable(new StateListDrawable());
            rbt_marketinfo.setBackgroundResource(R.drawable.news_selector);
            rbt_marketinfo.setTextColor(getResources().getColorStateList(R.color.info_tab_textcolor));
            rbt_marketinfo.setText(infoModel.getCategory().get(1).getCat_name());
            rbt_marketinfo.setGravity(Gravity.CENTER);
            rbt_marketinfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showProgress(InfoActivity.this,getString(R.string.jiazaizhong_dotdotdot));
                    Map<String, Object> params = new HashMap<>();
                    params.put("model", "other");
                    params.put("action", "get_article");
                    params.put("id", infoModel.getCategory().get(1).getCat_id());
                    params.put("sign", MD5Utils.encode(MD5Utils.getSign("other", "get_article")));
                    HttpUtils.sendPost(params, InfoActivity.this, 101);
                }
            });
            rg_info.addView(rbt_marketinfo, params);
            rg_info.requestLayout();
        } else {
            L.e("2222");
            for (int i = 0; i < infoModel.getCategory().size(); i++) {
                if (i == 0) {
                    rbt_notice = new RadioButton(this);
                    rbt_notice.setId(Integer.parseInt(infoModel.getCategory().get(0).getCat_id()));
                    rbt_notice.setButtonDrawable(new StateListDrawable());
                    rbt_notice.setBackgroundResource(R.drawable.notice_selector);
                    rbt_notice.setTextColor(getResources().getColorStateList(R.color.info_tab_textcolor));
                    rbt_notice.setGravity(Gravity.CENTER);
                    rbt_notice.setText(infoModel.getCategory().get(0).getCat_name());
                    rbt_notice.setChecked(true);
                    rg_info.addView(rbt_notice, params);
                } else if (i == infoModel.getCategory().size() - 1) {
                    rbt_marketinfo = new RadioButton(this);
                    rbt_marketinfo.setId(Integer.parseInt(infoModel.getCategory().get(i).getCat_id()));
                    rbt_marketinfo.setButtonDrawable(new StateListDrawable());
                    rbt_marketinfo.setBackgroundResource(R.drawable.news_selector);
                    rbt_marketinfo.setTextColor(getResources().getColorStateList(R.color.info_tab_textcolor));
                    rbt_marketinfo.setText(infoModel.getCategory().get(i).getCat_name());
                    rbt_marketinfo.setGravity(Gravity.CENTER);
                    rg_info.addView(rbt_marketinfo, params);
                } else {
                    RadioButton radioButton = new RadioButton(this);
                    radioButton.setId(Integer.parseInt(infoModel.getCategory().get(i).getCat_id()));
                    radioButton.setButtonDrawable(new StateListDrawable());
                    radioButton.setBackgroundResource(R.drawable.info_selector);
                    radioButton.setTextColor(getResources().getColorStateList(R.color.info_tab_textcolor));
                    radioButton.setText(infoModel.getCategory().get(i).getCat_name());
                    radioButton.setGravity(Gravity.CENTER);
                    rg_info.addView(rbt_marketinfo, params);
                }
            }
        }
        mAdapter.notifyDataSetChanged();
//        rg_info.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
//                RadioButton radioButton=(RadioButton)rg_info.findViewById(checkedId);
//                L.e("checkedId :"+checkedId);
//            }
//        });
    }

    private void initData() {
        rg_info.removeAllViews();
        Map<String, Object> params = new HashMap<>();
        params.put("model", "other");
        params.put("action", "get_article");
        params.put("id", 4);
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("other", "get_article")));
        HttpUtils.sendPost(params, this, 100);

    }

    private boolean flag = true;

    @Override
    public void success(String response, int id) {
        switch (id) {
            case 100:
                progerss_layout.setVisibility(View.GONE);
                mList.clear();
                infoModel = new Gson().fromJson(response, InfoModel.class);
                for (int i = 0; i < infoModel.getList().size(); i++) {
                    newsModel = infoModel.getList().get(i);
                    mList.add(newsModel);
                }
                if (flag) {
                    initView();
                    rbt_notice.setChecked(true);
                    flag = false;
                } else {
                    mAdapter.notifyDataSetChanged();
                }
                mRecyclerView.scrollToPosition(0);

                break;
            case 101:
                mList.clear();
                dismissProgress(this);
//                rbt_notice.setChecked(false);
                infoModel = new Gson().fromJson(response, InfoModel.class);
                for (int i = 0; i < infoModel.getList().size(); i++) {
                    newsModel = infoModel.getList().get(i);
                    mList.add(newsModel);
                }
                mAdapter.notifyDataSetChanged();
                mRecyclerView.scrollToPosition(0);
                break;
            case 102:

                break;
        }
//        L.e(response);


    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {
        progerss_layout.setVisibility(View.GONE);
        T.showShort(this, R.string.jianchawangluo);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_info;
    }
}
