package taiyang.com.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.cundong.recyclerview.CustRecyclerView;
import com.cundong.recyclerview.HeaderAndFooterRecyclerViewAdapter;
import com.cundong.recyclerview.LoadingFooter;
import com.cundong.recyclerview.RecyclerOnScrollListener;
import com.cundong.recyclerview.RecyclerViewStateUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.InjectView;
import taiyang.com.adapter.SellerDickerAdapter;
import taiyang.com.entity.SellerDickerListModel;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.T;
import taiyang.com.view.ErrorLayout;

/**
 * 我的还价
 */
public class SellerDickerActivity extends BaseActivity {
    @InjectView(R.id.recycler_view)
    CustRecyclerView mRecyclerView;
    @InjectView(R.id.error_layout)
    ErrorLayout mErrorLayout;
    private SellerDickerAdapter mAdapter;
    protected HeaderAndFooterRecyclerViewAdapter mRecyclerViewAdapter;
    protected int mCurrentPage = 1;
    protected int totalPage = 0;
    private SellerDickerListModel mDickerListModel;
    public List<SellerDickerListModel.ListBean> mDatas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_dicker);
//        ButterKnife.inject(this);
        mTitle.setText(getString(R.string.yijialiebiao));
        mDatas = new ArrayList<>();
        mAdapter = new SellerDickerAdapter(this);
        mAdapter.setDataList(mDatas);
        mRecyclerViewAdapter = new HeaderAndFooterRecyclerViewAdapter(this,mAdapter);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
        mRecyclerView.setIsRefreshing(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addOnScrollListener(mOnScrollListener);
        mErrorLayout.setOnLayoutClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mCurrentPage = 1;
                mErrorLayout.setErrorType(ErrorLayout.NETWORK_LOADING);
                requestData();
            }
        });

        requestData();
    }
    protected RecyclerOnScrollListener mOnScrollListener = new RecyclerOnScrollListener() {

        @Override
        public void onScrolled(int dx, int dy) {
            super.onScrolled(dx, dy);

        }

        @Override
        public void onScrollUp() {

        }

        @Override
        public void onScrollDown() {
        }

        @Override
        public void onBottom() {
            L.e("onBottom mCurrentPage = " + mCurrentPage);
            LoadingFooter.State state = RecyclerViewStateUtils.getFooterViewState(mRecyclerView);
            if(state == LoadingFooter.State.Loading) {
                return;
            }

            if (mCurrentPage <= totalPage) {
                // loading more
                RecyclerViewStateUtils.setFooterViewState(SellerDickerActivity.this, mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
                requestData();
            } else {

                RecyclerViewStateUtils.setFooterViewState(SellerDickerActivity.this, mRecyclerView, totalPage, LoadingFooter.State.TheEnd, null);
            }
        }


    };
    public void requestData() {
        Map<String, Object> params = new HashMap<>();
        params.put("model", "seller");
        params.put("action", "getHucksterList");
        params.put("user_id", mySPEdit.getUserId());
        params.put("token", mySPEdit.getToken());
        params.put("page", mCurrentPage);
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller", "getHucksterList")));
        HttpUtils.sendPost(params, this);
    }



    @Override
    protected int getLayout() {
        return R.layout.activity_dicker;
    }

    @Override
    public void success(String response, int id) {
        dismissProgress(this);


        mCurrentPage++;
        mDickerListModel = new Gson().fromJson(response, SellerDickerListModel.class);
        totalPage = mDickerListModel.getTotal().getPage_count();
        mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
        L.e("縂頁數:"+totalPage);
//                mRecyclerView.loadMoreComplete();
        for (int i = 0; i < mDickerListModel.getList().size(); i++) {
            mDatas.add(mDickerListModel.getList().get(i));
        }
//
        if (mCurrentPage == 1) {
            L.e("数据的大小:"+mDatas.size());
            mAdapter.setDataList(mDatas);
        } else {
            L.e("设置数据:");
            RecyclerViewStateUtils.setFooterViewState(mRecyclerView, LoadingFooter.State.Normal);
//            mAdapter.addAll(mDatas);
            mAdapter.setDataList(mDatas);
        }
        mRecyclerView.refreshComplete();
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(this);
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(this);
        T.showShort(this,R.string.jianchawangluo);
        if (mCurrentPage == 1) {
            mErrorLayout.setErrorType(ErrorLayout.NETWORK_ERROR);
        } else {
            mCurrentPage--;
            mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
            RecyclerViewStateUtils.setFooterViewState(SellerDickerActivity.this, mRecyclerView, totalPage, LoadingFooter.State.NetWorkError, mFooterClick);
            mAdapter.notifyDataSetChanged();
        }
    }
    private View.OnClickListener mFooterClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RecyclerViewStateUtils.setFooterViewState(SellerDickerActivity.this, mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
            requestData();
        }
    };
}
