package taiyang.com.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.view.ContextThemeWrapper;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bigkoo.pickerview.OptionsPickerView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.entity.CityModel;
import taiyang.com.entity.ProvinceModel;
import taiyang.com.entity.UserModel;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.ACache;
import taiyang.com.utils.Constants;
import taiyang.com.utils.DataCleanManager;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.RegularUtils;
import taiyang.com.utils.T;
import taiyang.com.utils.ToastUtil;
import taiyang.com.view.MyDialog;

/**
 * 编辑资料界面
 */
public class EditBuyerActivity extends BaseActivity {

    private int firstOptions1;
    private int firstOption2;
    private TimeCount time;
    private Button bt_code;

    private String phoneNUmber;

    //省市二级联动
    private ArrayList<ProvinceModel> options1Items = new ArrayList<>();
    private ArrayList<ArrayList<CityModel>> options2Items = new ArrayList<>();
    OptionsPickerView pvOptions;

    private boolean flag ;//判断是否修改头像
    @InjectView(R.id.tv_verify)
    TextView tv_verify;

    //发送验证码
    @OnClick(R.id.tv_verify)
    public void sendVerify(View v) {
        phoneNUmber = noPhone.getText().toString();
        if (RegularUtils.isMobileExact(phoneNUmber)) {
            View view = LayoutInflater.from(this).inflate(R.layout.verifylayout, null);
            TextView tv_quit = (TextView) view.findViewById(R.id.tv_quit);
            TextView tv_confirm = (TextView) view.findViewById(R.id.tv_confirm);
            TextView tv_phone = (TextView) view.findViewById(R.id.tv_phone);
            tv_phone.setText(phoneNUmber.substring(phoneNUmber.length() - 4));
            bt_code = (Button) view.findViewById(R.id.bt_code);
            final MyDialog dialog = new MyDialog(this, view, R.style.dialog);
            time = new TimeCount(110000, 1000);
            time.start();
            dialog.show();
            dialog.setCancelable(false);
            tv_quit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (time != null) {
                        time.cancel();
                    }
                    dialog.dismiss();
                }
            });
            tv_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            bt_code.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    time.start();
                }
            });
        } else {
            Toast.makeText(this, "请输入正确的电话号码", Toast.LENGTH_LONG).show();
            return;
        }


    }

    @InjectView(R.id.im_personlogo)
    SimpleDraweeView personLogo;
    private String fileName;
    private static String path = "/sdcard/myHead/";// sd路径

    //选择头像
    @InjectView(R.id.head_layout)
    RelativeLayout headLayout;

    @OnClick(R.id.head_layout)
    public void headLayout(View v) {
        openPictureSelectDialog();
    }

    //地址
    @InjectView(R.id.address_layout)
    RelativeLayout addressLayout;

    @OnClick(R.id.address_layout)
    public void addressLayout(View v) {
        pvOptions.show();
    }

    //    @InjectView(R.id.tv_location)
    TextView tvLocation;

    //性别
    @InjectView(R.id.rb_man)
    RadioButton rb_man;
    @InjectView(R.id.rb_woman)
    RadioButton rb_woman;

    //修改电话
    @InjectView(R.id.phone_layout)
    RelativeLayout phoneLayout;
    @InjectView(R.id.et_username)
    EditText et_username;
    private String userName;

    @OnClick(R.id.phone_layout)
    public void change(View v) {
        String phone=hashPhone.getText().toString();
        if (!TextUtils.isEmpty(phone)){
            Intent intent = new Intent(this, ChangePhoneActivity.class);
            intent.putExtra("phone",phone);
            startActivityForResult(intent, Constants.REQUESTCODE);
        }

    }
    @InjectView(R.id.et_nophone)
    EditText noPhone;


    //真实姓名
    @InjectView(R.id.et_alias)
    EditText et_alias;
    private String mAlias;
    //电话号码
    @InjectView(R.id.tv_hasphone)
    TextView hashPhone;
//    private String phoneNumber;

    @InjectView(R.id.et_shop_name)
    EditText et_shop_name;
    @InjectView(R.id.et_shop_info)
    EditText et_shop_info;
    //保存信息
    @InjectView(R.id.tv_save)
    TextView tv_save;

    @InjectView(R.id.iv_verify)
    ImageView iv_verify;

    @OnClick(R.id.tv_save)
    public void save() {
        changeUserName=et_username.getText().toString();
        changeAlias=et_alias.getText().toString();
        changePhone=hashPhone.getText().toString();
        changeAddress=tvLocation.getText().toString();

        String sex;
        mAlias = et_alias.getText().toString();

        if (rb_man.isChecked()) {
            sex = "1";
            changeSex="男";
        } else {
            sex = "2";
            changeSex="女";
        }
        if (TextUtils.isEmpty(changeUserName)||TextUtils.isEmpty(changeAlias)||TextUtils.isEmpty(changePhone)||TextUtils.isEmpty(changeAddress)){
            T.showShort(this,getString(R.string.qingjiangxinxibuchongwanzheng));
            return;
        }
        if(TextUtils.isEmpty(hasProvinceId)||TextUtils.isEmpty(hasCityId)||"0".equals(hasCityId)||"0".equals(hasProvinceId)){
            T.showShort(this,getString(R.string.qingjiangxinxibuchongwanzheng));
            return;
        }

            showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
            Map<String, Object> params = new HashMap<>();
            params.put("model", "user");
            params.put("action", "edit_info");
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "edit_info")));
            params.put("user_id", mySPEdit.getUserId());
            params.put("alias", mAlias);
            params.put("user_name", changeUserName);
            params.put("sex", sex);
//        params.put("shop_name", shopName);
            params.put("mobile_phone", changePhone);
            params.put("email", "");
//        params.put("address", tvLocation.getText().toString());
            //sex 1 男 2 女
            params.put("token", mySPEdit.getToken());
            params.put("province", hasProvinceId);
            params.put("city", hasCityId);

            if(type ==1){
                params.put("shop_name", et_shop_name.getText().toString());
                params.put("shop_info", et_shop_info.getText().toString());
            }
            L.e(JSON.toJSONString(params));
            HttpUtils.sendPost(params, "head_img", fileName, this, flag, 101);

    }


    private UserModel userModel;

    private String originalUserName, changeUserName;
    private String originalAlias, changeAlias;
    private String originalShopName, changeShopName;
    private String originalPhone, changePhone;
    private String originalAddress, changeAddress;
    private String originalSex, changeSex;

    private ACache mCache;//主要用于缓存地址
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_edit_buyer);
//        ButterKnife.inject(this);
        mCache = ACache.get(this);
        tvLocation = (TextView) findViewById(R.id.tv_location);
        userModel = new Gson().fromJson(mySPEdit.getUserInfo(), UserModel.class);
        hasProvinceId = userModel.getProvince();
        hasCityId = userModel.getCity();
        initView();
        initData();
    }

    private void initOrigalData() {
        if (et_username==null||et_alias==null||hashPhone==null||tvLocation==null){
            return;
        }
        originalUserName=et_username.getText().toString();
        originalAlias=et_alias.getText().toString();
        originalPhone=hashPhone.getText().toString();
        originalAddress=tvLocation.getText().toString();
        originalSex="男";
    }

    private boolean first = true;


    int type = 0;
    //初始化数据
    private void initData() {
        type =getIntent().getIntExtra("seller",0);
        //卖家
        if(type ==1){
            findViewById(R.id.shop_layout_info).setVisibility(View.VISIBLE);
            findViewById(R.id.shop_layout).setVisibility(View.VISIBLE);
        }else{
            findViewById(R.id.shop_layout_info).setVisibility(View.GONE);
            findViewById(R.id.shop_layout).setVisibility(View.GONE);
        }
        //选项选择器
        pvOptions = new OptionsPickerView(this);
        if (TextUtils.isEmpty(mCache.getAsString("location"))){
            HttpUtils.sendPost(this, 100, first);
        }else {
            String location=mCache.getAsString("location");
            initLocation(location);
        }


    }

    private void initView() {
        mTitle.setText(R.string.bianjiziliao);
        if (userModel.getUser_face() != null) {
            personLogo.setImageURI(Uri.parse(userModel.getUser_face()));
        }
        userName = userModel.getUser_name();
        if (!TextUtils.isEmpty(userName)) {
            et_username.setText(userName);
            et_username.setEnabled(false);
        } else {
            et_username.setEnabled(true);
        }
        mAlias = et_alias.getText().toString();
        mAlias = userModel.getAlias();
        if (mAlias != null) {
            et_alias.setText(userModel.getAlias());
        }
            et_shop_name.setText(userModel.getShop_name());
            et_shop_info.setText(userModel.getShop_info());
        phoneNUmber = userModel.getMobile_phone();
        if (!TextUtils.isEmpty(phoneNUmber)) {
            noPhone.setVisibility(View.GONE);
            tv_verify.setVisibility(View.GONE);
            hashPhone.setVisibility(View.VISIBLE);
            iv_verify.setVisibility(View.VISIBLE);
            hashPhone.setText(userModel.getMobile_phone());
        } else {
            noPhone.setVisibility(View.VISIBLE);
            tv_verify.setVisibility(View.VISIBLE);
            hashPhone.setVisibility(View.GONE);
            iv_verify.setVisibility(View.GONE);
        }


        if (hasProvinceId.equals("0") && hasCityId.equals("0")) {
            tvLocation.setText(R.string.qingxuanze);
            mySPEdit.setFirstOptions1(0);
            mySPEdit.setFirstOption2(0);
            mySPEdit.setIsAddress(false);
        } else {
//            tvLocation.setText(application.getCurrentUser().getAddress());
        }
        if (userModel.getSex().equals("2")) {
            rb_woman.setChecked(true);
        } else {
            rb_man.setChecked(true);
        }

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_edit_buyer;
    }


    private Bitmap head;
    private File temp;

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    cropPhoto(data.getData());// 裁剪图片
//                    File temp = new File(Environment.getExternalStorageDirectory()
//                            + "/head.jpg");
//                    cropPhoto(Uri.fromFile(temp));// 裁剪图片
                }
                break;
            case 2:
                if (resultCode == RESULT_OK) {
                    temp = new File(Environment.getExternalStorageDirectory()
                            + "/head.jpg");
//                     temp=new File(path+"/head.jpg");
                    cropPhoto(Uri.fromFile(temp));// 裁剪图片
                }

                break;
            case 3:
                if (data != null) {
                    Bundle extras = data.getExtras();
//                    Uri uri = (Uri) extras.getParcelable(MediaStore.EXTRA_OUTPUT);
//                    Log.e("ceshi", "onActivityResult: "+(uri==null) );
                    head = extras.getParcelable("data");
//                    personLogo.setImageBitmap(head);

                    if (head != null) {
                        flag = true;
                        /**
                         * 上传服务器代码
                         */
//                        if (fileName != null) {
//                            delFile(fileName);
//                        }

                        setPicToView(head);// 保存在SD卡中
//                        ivHead.setImageBitmap(head);// 用ImageView显示出来
                        L.e(fileName);
                        Uri uri = Uri.fromFile(new File(fileName));
                        L.e("uri" +uri);
                        personLogo.setImageURI(uri);
                    }
                }
                break;
            case Constants.REQUESTCODE:
                if (resultCode==Constants.CHANGEPHONE){
                    L.e("修改手机号成功");
                    String newphone = data.getStringExtra("newphone");
                    hashPhone.setText(newphone);
                }
                break;
            default:
                break;

        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private List<CityModel> mCityList;
    private ArrayList<CityModel> options2Items_child;

    //已经存在省份市区
    private String hasProvinceId;
    private String hasCityId;
    private String provice;
    private String city;

    @Override
    public void success(String response, int id) {

        if (id == 100) {
            mCache.put("location",response,6* ACache.TIME_DAY);
//            mCache.put("location",response,30);
            initLocation(response);

        }
        if (id == 101) {
            if (TextUtils.isEmpty(response)) {
                dismissProgress(this);
                Toast.makeText(this, "修改成功", Toast.LENGTH_SHORT).show();
                mySPEdit.setFirstOptions1(firstOptions1);
                mySPEdit.setFirstOption2(firstOption2);

                mySPEdit.setIsAddress(true);
                if(fileName!=null)
                {
                    DataCleanManager.deleteFilesByDirectory(new File(fileName));
                }

                myApplication.setPersonChange(true);
                finish();
            } else {
                 userModel = new Gson().fromJson(response, UserModel.class);
                initView();
            }
        }

    }

    private void initLocation(String locationStr) {
        Type listType = new TypeToken<LinkedList<ProvinceModel>>() {
        }.getType();
        Gson gson = new Gson();
        LinkedList<ProvinceModel> provinceListModel = gson.fromJson(locationStr, listType);
        for (Iterator iterator = provinceListModel.iterator(); iterator.hasNext(); ) {
            ProvinceModel provinceModel = (ProvinceModel) iterator.next();
//                System.out.println(user.getName());
//                System.out.println(user.getId());
            if (hasProvinceId.equals(provinceModel.getId())) {
                provice = provinceModel.getName();
                mySPEdit.setFirstOptions1(firstOptions1);
            }
            options1Items.add(provinceModel);
            mCityList = provinceModel.getCity_list();
            options2Items_child = new ArrayList<>();
            for (int i = 0; i < mCityList.size(); i++) {
                options2Items_child.add(mCityList.get(i));
                if (hasCityId.equals(mCityList.get(i).getId())) {
                    city = mCityList.get(i).getName();
                    firstOption2 = i;
                    mySPEdit.setFirstOption2(firstOption2);
                }
            }
            options2Items.add(options2Items_child);
            firstOptions1++;
        }
        if (provice != null && city != null) {
            tvLocation.setText(provice + city);
        }
        initOrigalData();
//            L.e("获取的数据为空:" + (provinceListModel == null));
        //选项1
//            options1Items.add(new ProvinceBean(0, "广东", "广东省，以岭南东道、广南东路得名", "其他数据"));
//            options1Items.add(new ProvinceBean(1, "湖南", "湖南省地处中国中部、长江中游，因大部分区域处于洞庭湖以南而得名湖南", "芒果TV"));
//            options1Items.add(new ProvinceBean(3, "广西", "嗯～～", ""));


        //三级联动效果
//        pvOptions.setPicker(options1Items, options2Items, options3Items,true);
        pvOptions.setPicker(options1Items, options2Items, true);
        //设置选择的三级单位
//        pwOptions.setLabels("省", "市", "区");
        pvOptions.setTitle("选择城市");
        pvOptions.setCyclic(false);
        //设置默认选中的三级项目
        //监听确定选择按钮
        pvOptions.setSelectOptions(mySPEdit.getFirstOptions1(), mySPEdit.getFirstOption2());
        pvOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {

            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
//                    L.e("options1:"+options1);
//                    L.e("option2:"+option2);
                firstOptions1 = options1;
                firstOption2 = option2;
                //返回的分别是三个级别的选中位置
                String tx = options1Items.get(options1).getPickerViewText()
                        + options2Items.get(options1).get(option2).getPickerViewText();
                hasProvinceId = options1Items.get(options1).getId();
                hasCityId = options2Items.get(options1).get(option2).getId();
//                    tvLocation.setText(tx+"//"+provinceId+"//"+cityId);
                tvLocation.setText(tx);
            }
        });
    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(this);
        ToastUtil.showToast(fail);
    }
    @Override
    public void fail(String fail, int id) {
        dismissProgress(this);
        ToastUtil.showToast(fail);
    }

    /**
     * 验证码倒计时
     */
    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {// 计时完毕
            bt_code.setText(R.string.chongfa);
            bt_code.setTextColor(getResources().getColor(R.color.message_bg));
            bt_code.setClickable(true);
        }

        @Override
        public void onTick(long millisUntilFinished) {// 计时过程
            bt_code.setClickable(false);//防止重复点击
            bt_code.setTextColor(getResources().getColor(R.color.message_bg2));
            bt_code.setText(millisUntilFinished / 1000 + getString(R.string.s_houchongfa));
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (pvOptions.isShowing()) {
                pvOptions.dismiss();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 打开对话框
     **/
    private void openPictureSelectDialog() {
        //自定义Context,添加主题
        Context dialogContext = new ContextThemeWrapper(this, android.R.style.Theme_Light);
        String[] choiceItems = new String[2];
        choiceItems[0] = getString(R.string.xiangjipaishe);  //拍照
        choiceItems[1] = getString(R.string.bendixiangce);  //从相册中选择
        ListAdapter adapter = new ArrayAdapter<String>(dialogContext, android.R.layout.simple_list_item_1, choiceItems);
        //对话框建立在刚才定义好的上下文上
        AlertDialog.Builder builder = new AlertDialog.Builder(dialogContext);
        builder.setTitle(getString(R.string.xuanzetupian));
        builder.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:  //相机
//                        doTakePhoto();
                        Intent intent2 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent2.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "head.jpg")));
                        startActivityForResult(intent2, 2);// 采用ForResult打开
                        break;
                    case 1:  //从图库相册中选取
//                        doPickPhotoFromGallery();
                        Intent intent1 = new Intent(Intent.ACTION_PICK, null);
                        intent1.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                        startActivityForResult(intent1, 1);
                        break;
                }
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    /**
     * 调用系统的裁剪
     *
     * @param uri
     */
    public void cropPhoto(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, 3);
    }

    //将图片存储至sd卡
    private void setPicToView(Bitmap mBitmap) {
        String sdStatus = Environment.getExternalStorageState();
        if (!sdStatus.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
            return;
        }
        FileOutputStream b = null;
        File file = new File(path);
        file.mkdirs();// 创建文件夹
        fileName = path + UUID.randomUUID() + ".jpg";// 图片名字
        try {
            b = new FileOutputStream(fileName);
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, b);// 把数据写入文件
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                // 关闭流
                b.flush();
                b.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
