package taiyang.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.InjectView;
import taiyang.com.adapter.BankAccountAdapter;
import taiyang.com.entity.BankAccountModel;
import taiyang.com.tydp_b.BaseActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.T;
import taiyang.com.utils.ToastUtil;

/*
    资金账户管理
 */
public class AccountActivity extends BaseActivity implements View.OnClickListener {

    @InjectView(R.id.bt_personinfo_addperson)
    Button bt_personinfo_addperson;
    @InjectView(R.id.rv_personinfo)
    RecyclerView rv_personinfo;
    @InjectView(R.id.tv_yuejiaoyi)
    TextView tvYuejiaoyi;
    @InjectView(R.id.tv_zhanghushu)
    TextView tvZhanghushu;
    private List<BankAccountModel.BankListBean> mDatas;
    private BankAccountAdapter bankAccountAdapter;
    @InjectView(R.id.scrollview)
    ScrollView scrollView;
    BankAccountModel mBankAccountModel;


    @InjectView(R.id.tv_xianhuo_account)
    TextView tv_xianhuo_account;

    @InjectView(R.id.tv_qihuo_account)
    TextView tv_qihuo_account;

    @Override
    protected void onResume() {
        super.onResume();
        refreshData();
    }
public void setData(String chang_type,String changId){
    showProgress(this,getString(R.string.jiazaizhong_dotdotdot));
    Map<String,Object> params=new HashMap<>();
    params.put("model","seller");
    params.put("action","defaultBank");
    params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller","defaultBank")));
    params.put("user_id", mySPEdit.getUserId());
    params.put("token",mySPEdit.getToken());
    params.put("bank_id", changId);
    params.put("good_type",chang_type);
    HttpUtils.sendPost(params, new HttpRequestListener() {
        @Override
        public void success(String response, int id) {
            refreshData();
            dismissProgress(AccountActivity.this);
            ToastUtil.showToast(getString(R.string.caozuochenggong));
        }

        @Override
        public void failByOther(String fail, int id) {

        }

        @Override
        public void fail(String fail, int id) {

        }
    });
}

    public void refreshData() {
        dismissProgress(this);
        showProgress(this, getString(R.string.jiazaizhong_dotdotdot));
        Map<String, Object> params = new HashMap<>();
        params.put("model", "seller");
        params.put("action", "getShopBankList");
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller", "getShopBankList")));
        params.put("user_id", mySPEdit.getUserId());
        HttpUtils.sendPost(params, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle.setText(getString(R.string.zhanghuzijin));
        initListeren();
        initData();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_account_info;
    }


    private void initListeren() {
        bt_personinfo_addperson.setOnClickListener(this);
        tv_qihuo_account.setOnClickListener(this);
        account_type =1;
        tv_qihuo_account.setTextColor(getResources().getColor(R.color.colorPrimary));
        tv_xianhuo_account.setTextColor(getResources().getColor(R.color.color_text_normal));
        tv_xianhuo_account.setOnClickListener(this);
    }



    int account_type = 1;
    private void initData() {
        mDatas = new ArrayList<>();
        bankAccountAdapter = new BankAccountAdapter(mDatas, this, bt_personinfo_addperson,account_type);
            bankAccountAdapter.setFlag(true);
        rv_personinfo.setAdapter(bankAccountAdapter);
        bankAccountAdapter.notifyDataSetChanged();
        rv_personinfo.setLayoutManager(new LinearLayoutManager(AccountActivity.this));
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.bt_personinfo_addperson:
                Intent i = new Intent(this, AddAccountActivity.class);
                startActivity(i);
                break;
            case R.id.tv_qihuo_account:
                account_type =1;
                tv_qihuo_account.setTextColor(getResources().getColor(R.color.colorPrimary));
                tv_xianhuo_account.setTextColor(getResources().getColor(R.color.color_text_normal));
                bankAccountAdapter.setType(account_type);
                refreshData();
                break;
            case R.id.tv_xianhuo_account:
                account_type =2;
                tv_qihuo_account.setTextColor(getResources().getColor(R.color.color_text_normal));
                tv_xianhuo_account.setTextColor(getResources().getColor(R.color.colorPrimary));
                bankAccountAdapter.setType(account_type);
                refreshData();
                break;

        }
    }

    @Override
    public void success(String response, int id) {
        dismissProgress(this);
        mDatas.clear();
        mBankAccountModel = new Gson().fromJson(response, BankAccountModel.class);
        if(mBankAccountModel.getShop().getBank_switch().equals("0")){
            ToastUtil.showToast(getString(R.string.yinhangzhuhuweikaitong));
            this.finish();

        }
        tvZhanghushu.setText(mBankAccountModel.getBank_count()+"");
        tvYuejiaoyi.setText(mBankAccountModel.getGoods_amount_count()+"");
        for (int i = 0; i < mBankAccountModel.getBank_list().size(); i++) {
            mDatas.add(mBankAccountModel.getBank_list().get(i));
        }
        if (mDatas.size() == 0) {
            scrollView.setVisibility(View.GONE);
        } else {
            scrollView.setVisibility(View.VISIBLE);
        }
        bankAccountAdapter.notifyDataSetChanged();
    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(this);
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(this);
        T.showShort(this, R.string.jianchawangluo);
    }
}
