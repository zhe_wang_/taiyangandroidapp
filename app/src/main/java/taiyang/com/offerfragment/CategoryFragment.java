package taiyang.com.offerfragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.activity.InquiryListActivity;
import taiyang.com.adapter.EListAdapter;
import taiyang.com.entity.OfferFragmentTextBean;
import taiyang.com.entity.ZhuFragmentCDContentBean;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.ToastUtil;

/**
 * 猪fragment
 * Created by hoo on 2016/7/5.
 */
public class CategoryFragment extends TypeFragment implements View.OnClickListener {

    @InjectView(R.id.et_zhu_inquiry)
    EditText et_zhu_inquiry;
    @InjectView(R.id.iv_zhu_inquiry_search)
    ImageView iv_zhu_inquiry_search;

    @OnClick(R.id.iv_zhu_inquiry_search)
    public void search(View v) {
        String result=et_zhu_inquiry.getText().toString().trim();
        if(!TextUtils.isEmpty(result)){
            Intent i = new Intent(getActivity(), InquiryListActivity.class);
            i.putExtra("relut",result);
            getActivity().startActivity(i);
            et_zhu_inquiry.setText("");
        }else {
           ToastUtil.showToast("请输入查询内容!");
        }
    }

    public int getType() {
        return type;
    }

    public TypeFragment setType(int type) {
        this.type = type;
        return this;
    }

    private int type;

    @InjectView(R.id.tv_zhu_confirm)
    TextView tv_zhu_confirm;
    @InjectView(R.id.tv_zhu_name)
    TextView tv_zhu_name;
    @InjectView(R.id.bt_zhu_ok)
    Button bt_zhu_ok;
    @InjectView(R.id.rl_zhu_cd)
    RelativeLayout rl_zhu_cd;

    @InjectView(R.id.rl_zhu_szd)
    RelativeLayout rl_zhu_szd;
    @InjectView(R.id.rl_zhu_cp)
    RelativeLayout rl_zhu_cp;
    @InjectView(R.id.rl_zhu_ch)
    RelativeLayout rl_zhu_ch;
    @InjectView(R.id.rl_zhu_gk)
    RelativeLayout rl_zhu_gk;
    @InjectView(R.id.rl_zhu_lx)
    RelativeLayout rl_zhu_lx;
    @InjectView(R.id.rl_zhu_qh)
    RelativeLayout rl_zhu_qh;
    @InjectView(R.id.rl_zhu_sell_type)
    RelativeLayout rl_zhu_sell_type;
    @InjectView(R.id.rl_zhu_time)
    RelativeLayout rl_zhu_time;
    @InjectView(R.id.ll_zhu1)
    LinearLayout ll_zhu1;
    @InjectView(R.id.ll_zhu2)
    LinearLayout ll_zhu2;
    @InjectView(R.id.ll_zhu3)
    LinearLayout ll_zhu3;
    @InjectView(R.id.elv_zhu)
    ExpandableListView elv_zhu;
    @InjectView(R.id.tv_zhu2_confirm)
    TextView tv_zhu2_confirm;
    @InjectView(R.id.rv_zhu2_inquiry)
    RecyclerView rv_zhu2_inquiry;
    @InjectView(R.id.tv_zhuischecked_cd)
    TextView tv_zhuischecked_cd;
    @InjectView(R.id.tv_zhuischecked_ch)
    TextView tv_zhuischecked_ch;
    @InjectView(R.id.tv_zhuischecked_cp)
    TextView tv_zhuischecked_cp;
    @InjectView(R.id.tv_zhuischecked_gk)
    TextView tv_zhuischecked_gk;
    @InjectView(R.id.tv_zhuischecked_szd)
    TextView tv_zhuischecked_szd;
    @InjectView(R.id.tv_zhuischecked_bp)
    TextView tv_zhuischecked_bp;
    @InjectView(R.id.tv_zhuischecked_qh)
    TextView tv_zhuischecked_qh;
    @InjectView(R.id.tv_zhuischecked_sell_type)
    TextView tv_zhuischecked_sell_type;
    @InjectView(R.id.tv_zhuischecked_time)
    TextView tv_zhuischecked_time;




    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_zhu, container, false);
        ButterKnife.inject(this, view);

        initView();
        initListeren();
        initData();
        return view;
    }



    private void initListeren() {
        bt_zhu_ok.setOnClickListener(this);
        rl_zhu_cd.setOnClickListener(this);
        rl_zhu_ch.setOnClickListener(this);
        rl_zhu_cp.setOnClickListener(this);
        rl_zhu_gk.setOnClickListener(this);
        rl_zhu_lx.setOnClickListener(this);
        rl_zhu_qh.setOnClickListener(this);
        rl_zhu_sell_type.setOnClickListener(this);
        rl_zhu_szd.setOnClickListener(this);
        rl_zhu_time.setOnClickListener(this);
        tv_zhu_confirm.setOnClickListener(this);
        tv_zhu2_confirm.setOnClickListener(this);
    }
    protected int getTypeId(){
        return getType();
    }

    @Override
    void setGkShow(boolean show) {

    }

    @Override
    void setBpShow(boolean show) {

    }

    @Override
    void setDgShow(boolean show) {

    }

    @Override
    void setSzdShow(boolean show) {

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.bt_zhu_ok://提交筛选数据

                    sendDataToInquiryListActivity();

                break;

            case R.id.tv_zhu_confirm://是否已选
                ll_zhu1.setVisibility(View.VISIBLE);
                ll_zhu2.setVisibility(View.GONE);
                ll_zhu3.setVisibility(View.GONE);

                isCheckedAllList();

                break;

            case R.id.tv_zhu2_confirm:
                ll_zhu1.setVisibility(View.VISIBLE);
                ll_zhu2.setVisibility(View.GONE);
                ll_zhu3.setVisibility(View.GONE);

                int cp = 0;
                //产品 已选显示
                if(cpCheckedList != null){
                    for (int a = 0; a <cpCheckedList.size(); a++){
                        List<Boolean> booleen = cpCheckedList.get(a);
                        for (int i=0; i<booleen.size(); i++){

                               if(booleen.get(i)==true){
                                  tv_zhuischecked_cp.setVisibility(View.VISIBLE);
                                  cp = 1;
                               }
                        }
                    }
                }
                if (cp == 0)tv_zhuischecked_cp.setVisibility(View.GONE);
                break;

            case R.id.rl_zhu_cd:
                ll_zhu1.setVisibility(View.GONE);
                ll_zhu2.setVisibility(View.VISIBLE);
                ll_zhu3.setVisibility(View.GONE);
                tv_zhu_name.setText(R.string.chandi);

                if(contentBean == null)return;
                site_list = contentBean.getSite_list();
                if (site_list == null) return;

                if (cdCheckedList == null || cdCheckedList.size() == 0) {
                    cdCheckedList = new ArrayList<Boolean>();

                    for (int i = 0; i < site_list.size(); i++)
                        cdCheckedList.add(false);
                }
                listss.clear();

                for (int i = 0; i < site_list.size(); i++) {
                    offerFragmentTextBean = new OfferFragmentTextBean();
                    offerFragmentTextBean.setBrand_id(site_list.get(i).getId());
                    offerFragmentTextBean.setname(site_list.get(i).getSite_name());
                    listss.add(offerFragmentTextBean);
                }
                setAdapter(cdCheckedList);

                break;

            case R.id.rl_zhu_ch:
                ll_zhu1.setVisibility(View.GONE);
                ll_zhu2.setVisibility(View.VISIBLE);
                ll_zhu3.setVisibility(View.GONE);
                tv_zhu_name.setText(R.string.changhao);

                //选择了产地 数据
                int isChecekCD = 0;
                if(cdCheckedList!=null){
                    for (int i = 0; i <cdCheckedList.size(); i++) {
                        if (cdCheckedList.get(i) == true) {
                            isChecekCD = 1;
                        }
                    }
                }

                if(isChecekCD == 1){
                 initCHListData();

                //原始数据
                }else {
                    if(contentBean == null)return;
                    brandListBean = contentBean.getBrand_list();
                    if (brandListBean == null) {
                        return;
                    }
                    listss.clear();

                    if (chCheckedList == null || chCheckedList.size() == 0) {
                        chCheckedList = new ArrayList<Boolean>(brandListBean.size());

                        for (ZhuFragmentCDContentBean.BrandListBean bean : brandListBean)
                            chCheckedList.add(false);
                    }
                    for (int i = 0; i < brandListBean.size(); i++) {
                        offerFragmentTextBean = new OfferFragmentTextBean();
                        offerFragmentTextBean.setBrand_id(brandListBean.get(i).getBrand_id());
                        offerFragmentTextBean.setname(brandListBean.get(i).getBrand_sn());
                        listss.add(offerFragmentTextBean);
                    }
                    setAdapter(chCheckedList);

                }


                break;
            case R.id.rl_zhu_cp:

                ll_zhu1.setVisibility(View.GONE);
                ll_zhu2.setVisibility(View.GONE);
                ll_zhu3.setVisibility(View.VISIBLE);
                tv_zhu_name.setText(R.string.chanpin);

                if(cat_data == null || cat_data.size() ==0) {
                    if(contentBean == null)return;
                    cat_data = contentBean.getCat_data();
                    elv_zhu.setGroupIndicator(null);
                    cpCheckedList = new ArrayList<List<Boolean>> ();
                    for(ZhuFragmentCDContentBean.CatDataBean catBean : cat_data) {
                        smalcpCheckedList = new ArrayList<>();
                        for(ZhuFragmentCDContentBean.CatDataBean.CatListBean bean : catBean.getCat_list()) {
                            smalcpCheckedList.add(false);
                        }
                        cpCheckedList.add(smalcpCheckedList);
                    }
                    mAdapter = new EListAdapter(getActivity(), cat_data, cpCheckedList);
                    elv_zhu.setAdapter(mAdapter);
                }else {
                    mAdapter.notifyDataSetChanged();
                }

                break;
            case R.id.rl_zhu_szd:
                ll_zhu1.setVisibility(View.GONE);
                ll_zhu2.setVisibility(View.VISIBLE);
                ll_zhu3.setVisibility(View.GONE);
                tv_zhu_name.setText(R.string.suozaidi);
                if(contentBean == null)return;
                szd_data = contentBean.getGoods_local();
                if (szd_data == null) {
                    return;
                }
                listss.clear();
                if (szdCheckedList == null || szdCheckedList.size() == 0) {
                    szdCheckedList = new ArrayList<Boolean>();

                    for (int i = 0; i < szd_data.size(); i++)
                        szdCheckedList.add(false);
                }

                for (int i = 0; i < szd_data.size(); i++) {
                    offerFragmentTextBean = new OfferFragmentTextBean();
                    offerFragmentTextBean.setBrand_id(i+"");
                    offerFragmentTextBean.setname(szd_data.get(i));
                    listss.add(offerFragmentTextBean);
                }
                setAdapter(szdCheckedList);

                break;
            case R.id.rl_zhu_gk:
                ll_zhu1.setVisibility(View.GONE);
                ll_zhu2.setVisibility(View.VISIBLE);
                ll_zhu3.setVisibility(View.GONE);
                tv_zhu_name.setText(R.string.gangkou);
                if(contentBean == null)return;
                sel_list = contentBean.getSel_list();
                if (sel_list == null) {
                    return;
                }
                listss.clear();
                if (ggCheckedList == null || ggCheckedList.size() == 0) {
                    ggCheckedList = new ArrayList<Boolean>();

                    for (int i = 0; i < sel_list.size(); i++)
                        ggCheckedList.add(false);
                }

                for (int i = 0; i < sel_list.size(); i++) {
                    offerFragmentTextBean = new OfferFragmentTextBean();
                    offerFragmentTextBean.setBrand_id(sel_list.get(i).getId());
                    offerFragmentTextBean.setname(sel_list.get(i).getVal());
                    listss.add(offerFragmentTextBean);
                }
                setAdapter(ggCheckedList);

                break;


            case R.id.rl_zhu_lx:
                ll_zhu1.setVisibility(View.GONE);
                ll_zhu2.setVisibility(View.VISIBLE);
                ll_zhu3.setVisibility(View.GONE);
                tv_zhu_name.setText(getString(R.string.baopanleixing));

                listss.clear();
                if (bpCheckedList == null || bpCheckedList.size() == 0) {
                    bpCheckedList = new ArrayList<Boolean>();

                    for (int i = 0; i < bp_list.size(); i++)
                        bpCheckedList.add(false);
                }

                for (Map.Entry entry : bp_list.entrySet()) {
                    Object key = entry.getKey();
                    Object value = entry.getValue();
                    offerFragmentTextBean = new OfferFragmentTextBean();
                    offerFragmentTextBean.setBrand_id((String) key);
                    offerFragmentTextBean.setname((String) value);
                    listss.add(offerFragmentTextBean);
                }
                setAdapter(bpCheckedList);

                break;

            case R.id.rl_zhu_qh:
                ll_zhu1.setVisibility(View.GONE);
                ll_zhu2.setVisibility(View.VISIBLE);
                ll_zhu3.setVisibility(View.GONE);
                tv_zhu_name.setText("准/期/现货");

                listss.clear();
                if (qhCheckedList == null || qhCheckedList.size() == 0) {
                    qhCheckedList = new ArrayList<Boolean>();

                    for (int i = 0; i < qh_list.size(); i++)
                        qhCheckedList.add(false);
                }

                for (Map.Entry entry : qh_list.entrySet()) {
                    Object key = entry.getKey();
                    Object value = entry.getValue();
                    offerFragmentTextBean = new OfferFragmentTextBean();
                    offerFragmentTextBean.setBrand_id((String) key);
                    offerFragmentTextBean.setname((String) value);
                    listss.add(offerFragmentTextBean);
                }
                setAdapter(qhCheckedList);

                break;
            case R.id.rl_zhu_sell_type:
                ll_zhu1.setVisibility(View.GONE);
                ll_zhu2.setVisibility(View.VISIBLE);
                ll_zhu3.setVisibility(View.GONE);
                tv_zhu_name.setText(getString(R.string.dunshou)+"/"+getString(R.string.guishou));
                listss.clear();
                if (sell_typeCheckedList == null || sell_typeCheckedList.size() == 0) {
                    sell_typeCheckedList = new ArrayList<Boolean>();

                    for (int i = 0; i < sell_type_list.size(); i++)
                        sell_typeCheckedList.add(false);
                }

                for (Map.Entry entry : sell_type_list.entrySet()) {
                    Object key = entry.getKey();
                    Object value = entry.getValue();
                    offerFragmentTextBean = new OfferFragmentTextBean();
                    offerFragmentTextBean.setBrand_id((String) key);
                    offerFragmentTextBean.setname((String) value);
                    listss.add(offerFragmentTextBean);
                }
                setAdapter(sell_typeCheckedList);

                break;
            case R.id.rl_zhu_time:
                ll_zhu1.setVisibility(View.GONE);
                ll_zhu2.setVisibility(View.VISIBLE);
                ll_zhu3.setVisibility(View.GONE);
                tv_zhu_name.setText("预计到港时间");

                listss.clear();
                if (timeCheckedList == null || timeCheckedList.size() == 0) {
                    timeCheckedList = new ArrayList<Boolean>();

                    for (int i = 0; i < time_list.size(); i++)
                        timeCheckedList.add(false);
                }

                for (Map.Entry entry : time_list.entrySet()) {
                    Object key = entry.getKey();
                    Object value = entry.getValue();
                    offerFragmentTextBean = new OfferFragmentTextBean();
                    offerFragmentTextBean.setBrand_id((String) key);
                    offerFragmentTextBean.setname((String) value);
                    listss.add(offerFragmentTextBean);
                }
                setAdapter(timeCheckedList);

                break;

            default:
                break;

        }
    }



    //所有列表是否选择过
    private void isCheckedAllList(){
        cd = 0;
        ch = 0;
        gk = 0;
        szd = 0;
        bp = 0;
        qh = 0;
        time = 0;
        sell_type = 0;
        //产地 已选显示
        if(cdCheckedList !=null){
            for (int i = 0; i <cdCheckedList.size(); i++){
                if(cdCheckedList.get(i)==true){
                    tv_zhuischecked_cd.setVisibility(View.VISIBLE);
                    cd = 1;
                }
            }
        }

        //厂号 已选显示
        if(chCheckedList!=null){
            for (int i = 0; i <chCheckedList.size(); i++){
                if(chCheckedList.get(i)==true){
                    tv_zhuischecked_ch.setVisibility(View.VISIBLE);
                    ch = 1;
                }
            }
        }

        //港口 已选显示
        if(ggCheckedList!=null){
            for (int i = 0; i <ggCheckedList.size(); i++){
                if(ggCheckedList.get(i)==true){
                    tv_zhuischecked_gk.setVisibility(View.VISIBLE);
                    gk = 1;
                }
            }
        }
        //所在地 已选显示
        if(szdCheckedList!=null){
            for (int i = 0; i <szdCheckedList.size(); i++){
                if(szdCheckedList.get(i)==true){
                    tv_zhuischecked_szd.setVisibility(View.VISIBLE);
                    szd = 1;
                }
            }
        }
        //报盘 已选显示
        if(bpCheckedList!=null){
            for (int i = 0; i <bpCheckedList.size(); i++){
                if(bpCheckedList.get(i)==true){
                    tv_zhuischecked_bp.setVisibility(View.VISIBLE);
                    bp = 1;
                }
            }
        }
        //期货 已选显示
        if(qhCheckedList!=null){
            for (int i = 0; i <qhCheckedList.size(); i++){
                if(qhCheckedList.get(i)==true){
                    tv_zhuischecked_qh.setVisibility(View.VISIBLE);
                    qh = 1;
                }
            }
        }
        //期货 已选显示
        if(sell_typeCheckedList!=null){
            for (int i = 0; i <sell_typeCheckedList.size(); i++){
                if(sell_typeCheckedList.get(i)==true){
                    tv_zhuischecked_sell_type.setVisibility(View.VISIBLE);
                    sell_type = 1;
                }
            }
        }
        //到港时间 已选显示
        if(timeCheckedList!=null){
            for (int i = 0; i <timeCheckedList.size(); i++){
                if(timeCheckedList.get(i)==true){
                    tv_zhuischecked_time.setVisibility(View.VISIBLE);
                    time = 1;
                }
            }
        }

        if (cd == 0)tv_zhuischecked_cd.setVisibility(View.GONE);
        if (ch == 0)tv_zhuischecked_ch.setVisibility(View.GONE);
        if (gk == 0)tv_zhuischecked_gk.setVisibility(View.GONE);
        if (bp == 0)tv_zhuischecked_bp.setVisibility(View.GONE);
        if (szd == 0)tv_zhuischecked_szd.setVisibility(View.GONE);
        if (qh == 0)tv_zhuischecked_qh.setVisibility(View.GONE);
        if (time == 0)tv_zhuischecked_time.setVisibility(View.GONE);
        if (sell_type == 0)tv_zhuischecked_sell_type.setVisibility(View.GONE);

    }


    public RecyclerView getRv() {
        return rv_zhu2_inquiry;
    }


}
