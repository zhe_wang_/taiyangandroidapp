package taiyang.com.offerfragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import taiyang.com.activity.InquiryListActivity;
import taiyang.com.adapter.EListAdapter;
import taiyang.com.adapter.ZhuFragmentCDRvAdapter;
import taiyang.com.entity.BrandListBean;
import taiyang.com.entity.OfferFragmentTextBean;
import taiyang.com.entity.ZhuFragmentCDContentBean;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;

/**
 * 猪fragment
 * Created by hoo on 2016/7/5.
 */
public abstract class TypeFragment extends Fragment {

    protected EListAdapter mAdapter;
    protected ZhuFragmentCDContentBean contentBean;
    protected List<BrandListBean> newBrandListBean;
    protected ZhuFragmentCDRvAdapter zhuFragmentCDRvAdapter;
    protected OfferFragmentTextBean offerFragmentTextBean;
    protected List<ZhuFragmentCDContentBean.CatDataBean> cat_data;
    protected List<String> szd_data;

    protected List<Boolean> szdCheckedList;
    protected List<OfferFragmentTextBean> listss;
    protected List<Boolean> cdCheckedList;
    protected List<Boolean> chCheckedList;
    protected List<List<Boolean>> cpCheckedList;
    protected List<Boolean> ggCheckedList;

    protected List<Boolean> bpCheckedList;
    protected Map<String, String> bp_list;
    protected List<Boolean> qhCheckedList;
    protected List<Boolean> sell_typeCheckedList;
    protected Map<String, String> qh_list;
    protected List<Boolean> timeCheckedList;
    protected Map<String, String> time_list;
    protected Map<String, String> sell_type_list;
    protected List<Boolean> smalcpCheckedList;
    protected List<ZhuFragmentCDContentBean.SiteListBean> site_list;
    protected int cd;
    protected int ch;
    protected int gk;
    protected int szd;
    protected int bp;
    protected int qh;
    protected int time;
    protected int sell_type;
    protected Map<String, Object> params;
    protected List<ZhuFragmentCDContentBean.BrandListBean> brandListBean;
    protected List<ZhuFragmentCDContentBean.SelListBean> sel_list;


    abstract int getTypeId();

    abstract void setGkShow(boolean show);

    abstract void setBpShow(boolean show);

    abstract void setDgShow(boolean show);

    abstract void setSzdShow(boolean show);

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return null;
    }

    protected void initView() {
        listss = new ArrayList<>();
        //报盘 数据
        bp_list = new LinkedHashMap<String, String>();
        bp_list.put("1", "CIF");
        bp_list.put("2", "FOB");
        bp_list.put("3", "DDP");
        bp_list.put("4", "CFR");
        //期现货 数据
        qh_list = new LinkedHashMap<String, String>();
        qh_list.put("1", getActivity().getString(R.string.zhunxianhuo));
        qh_list.put("2", getActivity().getString(R.string.qihuo));
        qh_list.put("3", getActivity().getString(R.string.xianhuo));
        //到港时间 数据
        time_list = new LinkedHashMap<String, String>();
        time_list.put("1", "15天");
        time_list.put("2", "30天");
        time_list.put("3", "45天");
        time_list.put("4", "60天");

        //零售/整柜 数据
        sell_type_list = new LinkedHashMap<String, String>();
        sell_type_list.put("1", getActivity().getString(R.string.dunshou));
        sell_type_list.put("2", getActivity().getString(R.string.guishou));

    }


    protected void initData() {
        params = new HashMap<>();
        params.put("model", "goods");
        params.put("action", "get_category");
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("goods", "get_category")));
        params.put("id", getTypeId());
        HttpUtils.sendPost(params, new HttpRequestListener() {

            @Override
            public void success(String response, int id) {

                contentBean = new Gson().fromJson(response, ZhuFragmentCDContentBean.class);
                contentBean.getGoods_local().toString();

            }

            @Override
            public void failByOther(String fail, int id) {

            }

            @Override
            public void fail(String fail, int id) {

            }
        });
    }


    //提交所有选择
    protected void sendDataToInquiryListActivity() {

        //获取选择产地id
        if (cdCheckedList != null) {
            String region_ids = "";
            for (int a = 0; a < cdCheckedList.size(); a++) {
                if (cdCheckedList.get(a) == true) {
                    String id = site_list.get(a).getId();
                    if (region_ids.length() == 0) {
                        region_ids += id;
                    } else {
                        region_ids += "," + id;
                    }
                }
            }
            params.put("region_ids", region_ids);
            // L.e("产地"+region_ids);
        }

        //获取选择厂号id
        if (chCheckedList != null) {
            String brand_ids = "";
            //默认厂号
            if (brandListBean != null) {
                for (int a = 0; a < chCheckedList.size(); a++) {
                    if (chCheckedList.get(a) == true) {
                        String id = brandListBean.get(a).getBrand_id();
                        if (brand_ids.length() == 0) {
                            brand_ids += id;
                        } else {
                            brand_ids += "," + id;
                        }
                    }
                }
                //产地关联厂号
            } else {
                for (int a = 0; a < chCheckedList.size(); a++) {
                    if (chCheckedList.get(a) == true) {
                        String id = newBrandListBean.get(a).getBrand_id();
                        if (brand_ids.length() == 0) {
                            brand_ids += id;
                        } else {
                            brand_ids += "," + id;
                        }
                    }
                }
            }
            params.put("brand_ids", brand_ids);
            //  L.e("厂号"+brand_ids);
        }

        //获取选择产品id
        if (cpCheckedList != null) {
            String base_id = "";
            for (int a = 0; a < cpCheckedList.size(); a++) {
                List<Boolean> booleen = cpCheckedList.get(a);
                for (int i = 0; i < booleen.size(); i++) {
                    if (booleen.get(i) == true) {
                        String id = cat_data.get(a).getCat_list().get(i).getGoods_id();
                        if (base_id.length() == 0) {
                            base_id += id;
                        } else {
                            base_id += "," + id;
                        }
                    }
                }

            }
            params.put("base_ids", base_id);
            //L.e("产品"+base_id);
        }

        //获取选择港口id
        if (ggCheckedList != null) {
            String ports = "";
            for (int a = 0; a < ggCheckedList.size(); a++) {
                if (ggCheckedList.get(a) == true) {
                    String id = sel_list.get(a).getId();
                    if (ports.length() == 0) {
                        ports += id;
                    } else {
                        ports += "," + id;
                    }
                }
            }
            params.put("ports", ports);
            //L.e("港口"+ports);
        }
        //获取选择港口id
        if (szdCheckedList != null) {
            String ports = "";
            for (int a = 0; a < szdCheckedList.size(); a++) {
                if (szdCheckedList.get(a) == true) {
                    String id = szd_data.get(a);
                    if (ports.length() == 0) {
                        ports += id;
                    } else {
                        ports += "," + id;
                    }
                }
            }
            params.put("goods_local", ports);
            //L.e("港口"+ports);
        }
        //获取报盘类型id
        if (bpCheckedList != null) {
            String offer_types = "";
            for (int a = 0; a < bpCheckedList.size(); a++) {
                if (bpCheckedList.get(a) == true) {
                    for (Map.Entry entry : bp_list.entrySet()) {
                        Integer key = Integer.parseInt((String) entry.getKey());
                        if (key - 1 == a) {
                            if (offer_types.length() == 0) {
                                offer_types += key;
                            } else {
                                offer_types += "," + key;
                            }
                        }
                        ;
                    }
                }
            }
            params.put("offer_types", offer_types.replace("4","34"));
            // L.e("报盘"+offer_types);
        }

        //获取准/期/现货id
        if (qhCheckedList != null) {
            String goods_type = "";
            for (int a = 0; a < qhCheckedList.size(); a++) {
                if (qhCheckedList.get(a) == true) {
                    for (Map.Entry entry : qh_list.entrySet()) {
                        Integer key = Integer.parseInt((String) entry.getKey());
                        if (key - 1 == a) {
                            if (goods_type.length() == 0) {
                                goods_type += key;
                            } else {
                                goods_type += "," + key;
                            }
                        }
                    }
                }
            }
            goods_type = goods_type.replace('1', '8').replace('3', '7').replace('2', '6');
            params.put("goods_type", goods_type);
            //L.e("现货"+goods_type);
        }
        //获取零售.整柜
        if (sell_typeCheckedList != null) {
            String goods_type = "";
            for (int a = 0; a < sell_typeCheckedList.size(); a++) {
                if (sell_typeCheckedList.get(a) == true) {
                    for (Map.Entry entry : sell_type_list.entrySet()) {
                        Integer key = Integer.parseInt((String) entry.getKey());
                        if (key - 1 == a) {
                            if (goods_type.length() == 0) {
                                goods_type += key;
                            } else {
                                goods_type += "," + key;
                            }
                        }
                    }
                }
            }
            goods_type = goods_type.replace('1', '4').replace('2', '5');
            params.put("sell_type", goods_type);
            //L.e("现货"+goods_type);
        }


        //获取到港时间id
        if (timeCheckedList != null) {
            String arrive_date_types = "";
            for (int a = 0; a < timeCheckedList.size(); a++) {
                if (timeCheckedList.get(a) == true) {
                    for (Map.Entry entry : time_list.entrySet()) {
                        Integer key = Integer.parseInt((String) entry.getKey());
                        if (key - 1 == a) {
                            if (arrive_date_types.length() == 0) {
                                arrive_date_types += key;
                            } else {
                                arrive_date_types += "," + key;
                            }
                        }
                        ;
                    }
                }
            }
            params.put("arrive_date_types", arrive_date_types);
        }
        params.put("model", "goods");
        params.put("cat_id", getTypeId());
        params.put("action", "get_list");
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("goods", "get_list")));
        Intent i = new Intent(getActivity(), InquiryListActivity.class);
        i.putExtra("offerFragment", (Serializable) params);
        i.putExtra("baopan", "baopan");
        getActivity().startActivity(i);
    }


    //选择厂号后的数据
    protected void initCHListData() {

        //获取选择产地id
        String region_id = "";
        for (int a = 0; a < cdCheckedList.size(); a++) {
            if (cdCheckedList.get(a) == true) {
                String id = site_list.get(a).getId();

                if (region_id.length() == 0) {
                    region_id += id;
                } else {
                    region_id += "," + id;
                }
            }
        }


        Map<String, Object> params = new HashMap<>();
        params.put("model", "goods");
        params.put("action", "get_brand");
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("goods", "get_brand")));
        params.put("region_id", region_id);
        HttpUtils.sendPost(params, new HttpRequestListener() {
            @Override
            public void success(String response, int id) {
                Gson gson = new Gson();
                newBrandListBean = gson.fromJson(response, new TypeToken<List<BrandListBean>>() {
                }.getType());
                if (newBrandListBean == null) {
                    return;
                }
                listss.clear();

                if (chCheckedList == null || chCheckedList.size() == 0) {
                    chCheckedList = new ArrayList<Boolean>(newBrandListBean.size());

                    for (BrandListBean bean : newBrandListBean)
                        chCheckedList.add(false);
                }
                for (int i = 0; i < newBrandListBean.size(); i++) {
                    offerFragmentTextBean = new OfferFragmentTextBean();
                    offerFragmentTextBean.setBrand_id(newBrandListBean.get(i).getBrand_id());
                    offerFragmentTextBean.setname(newBrandListBean.get(i).getBrand_sn());
                    listss.add(offerFragmentTextBean);
                }
                setAdapter(chCheckedList);
            }

            @Override
            public void failByOther(String fail, int id) {

            }

            @Override
            public void fail(String fail, int id) {

            }
        });
    }

    protected void setAdapter(final List<Boolean> checkedList) {
        if (zhuFragmentCDRvAdapter == null) {
            zhuFragmentCDRvAdapter = new ZhuFragmentCDRvAdapter(listss, getActivity(), checkedList);
            getRv().setAdapter(zhuFragmentCDRvAdapter);
        } else {
            zhuFragmentCDRvAdapter.setCheckedList(checkedList);
            zhuFragmentCDRvAdapter.notifyDataSetChanged();
        }
        if (checkedList.size() < 7) {
            getRv().setLayoutManager(new LinearLayoutManager(getActivity()));
        } else {
            getRv().setLayoutManager(new GridLayoutManager(getActivity(), 3));
        }
    }

    public RecyclerView getRv() {
        return null;
    }


}
