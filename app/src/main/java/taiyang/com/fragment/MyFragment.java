package taiyang.com.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.jauker.widget.BadgeView;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.activity.AccountActivity;
import taiyang.com.activity.AfterSaleActivity;
import taiyang.com.activity.AttentionActivity;
import taiyang.com.activity.BaopanguanliActivity;
import taiyang.com.activity.DickerActivity;
import taiyang.com.activity.EditBuyerActivity;
import taiyang.com.activity.MyBuyActivity;
import taiyang.com.activity.OrderActivity;
import taiyang.com.activity.PersonInfoActivity;
import taiyang.com.activity.QiugouListActivity;
import taiyang.com.activity.RuzhuActivity;
import taiyang.com.activity.SellerDickerActivity;
import taiyang.com.activity.SellerOrderActivityPage;
import taiyang.com.activity.SettingActivity;
import taiyang.com.entity.DickerListModel;
import taiyang.com.entity.UserModel;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.Constants;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MyApplication;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.T;
import taiyang.com.utils.ToastUtil;


/**
 * 我的fragment
 * Created by hoo on 2016/7/5.
 */
public class MyFragment extends Fragment implements HttpRequestListener {
    private boolean isClick;

    //关注
    @InjectView(R.id.attention_layout)
    RelativeLayout attentionLayout;

    @OnClick(R.id.attention_layout)
    public void attentionLayout(View v) {
            Intent intent = new Intent(getActivity(), AttentionActivity.class);
            startActivity(intent);
    }
    @OnClick(R.id.tv_attaction)
    public void attaction(View v) {
            Intent intent = new Intent(getActivity(),  AttentionActivity.class);
            startActivity(intent);
    }
    @OnClick(R.id.tv_seller_guanzhu)
    public void attactionSeller(View v) {
        Intent intent = new Intent(getActivity(),  AttentionActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tv_all_order)
    public void orderLayout(View v) {
        if (isClick) {
            Intent intent = new Intent(getActivity(), OrderActivity.class);
            startActivity(intent);
        }

    }
    @OnClick(R.id.tv_dingdanguanli)
    public void dingdanguanlit(View v) {
        if (isClick) {
            Intent intent = new Intent(getActivity(), SellerOrderActivityPage.class);
            startActivity(intent);
        }

    }
    @OnClick(R.id.tv_qiugou_more)
    public void qiugou(View v) {
        Intent i = new Intent(getActivity(), QiugouListActivity.class);
        getActivity().startActivity(i);
    }

    @InjectView(R.id.arrivalnumber_layout)
    FrameLayout arrivalnumber_layout;

    @InjectView(R.id.checkout_layout)
    LinearLayout checkoutLayout;

    @OnClick(R.id.checkout_layout)
    public void checkoutLayout(View v) {
        if (isClick) {
            Intent intent = new Intent(getActivity(), OrderActivity.class);
            intent.putExtra("tab", "已付款");
            getActivity().startActivity(intent);
        }

    }

    @InjectView(R.id.checkoutnumber_layout)
    FrameLayout checkoutnumber_layout;

    @InjectView(R.id.delivery_layout)
    LinearLayout deliveryLayout;

    @OnClick(R.id.delivery_layout)
    public void deliveryLayout(View v) {
        if (isClick) {
            Intent intent = new Intent(getActivity(), OrderActivity.class);
            intent.putExtra("tab", getString(R.string.yiwancheng));
            getActivity().startActivity(intent);
        }

    }

    //设置
    @InjectView(R.id.setting_layout)
    LinearLayout imSetting;

    @InjectView(R.id.iv_ruzhu)
    ImageView ivRuzhu;

    @OnClick(R.id.setting_layout)
    public void imSetting(View v) {
        if (isClick) {
            Intent intent = new Intent(getActivity(), SettingActivity.class);
            getActivity().startActivityForResult(intent, Constants.REQUESTCODE);
        }
    }
    private int type = 1;

    @InjectView(R.id.ll_buyer)
    LinearLayout llBuyer;


    @InjectView(R.id.ll_buyer_top)
    RelativeLayout llBuyerTop;


    @InjectView(R.id.ll_seller)
    LinearLayout llSeller;


    @InjectView(R.id.tv_title)
    TextView tvTitle;
    @InjectView(R.id.ll_seller_top)
    LinearLayout llSellerTop;

    @InjectView(R.id.tv_seller_yijia)
    TextView tvSellerYijia;

    @OnClick(R.id.tv_seller_yijia)
    public void showYijia(){
        if (isClick) {
            Intent intent = new Intent(getActivity(), SellerDickerActivity.class);
            startActivity(intent);
        }
    }
    @OnClick(R.id. tv_baopanguanli)
    public void baopanguanli(){
        if (isClick) {
            Intent intent = new Intent(getActivity(), BaopanguanliActivity.class);
            startActivity(intent);
        }
    }

    void setPageType(){
      if(type!=0){
          tvTitle.setText(R.string.mai_buyerjiazhongxin);
          llBuyer.setVisibility(View.VISIBLE);
          llBuyerTop.setVisibility(View.VISIBLE);
          llSeller.setVisibility(View.GONE);
          llSellerTop.setVisibility(View.GONE);
      }else{
          tvTitle.setText(R.string.mai_sellerjiazhongxin);
          llBuyer.setVisibility(View.GONE);
          llBuyerTop.setVisibility(View.GONE);
          llSeller.setVisibility(View.VISIBLE);
          llSellerTop.setVisibility(View.VISIBLE);
      }
    }

    @OnClick(R.id.ll_change_type)
    public void imNews(View v) {
        if(type==1){
            if(userModel.getUser_rank().equals("2")) {
                type=0;
                setPageType();
            }else if("1".equals(userModel.getJoin())){
                Intent i = new Intent(getActivity(),RuzhuActivity.class);
                startActivity(i);
            }else{
                ToastUtil.showToast(userModel.getJoin());
            }
        }else{
            type=1;
            setPageType();
        }
    }


    @OnClick(R.id.iv_ruzhu)
    public void ruzhu(){
        if(type==1){
            if(userModel.getUser_rank().equals("2")) {
                type=0;
                setPageType();
            }else if("1".equals(userModel.getJoin())){
                Intent i = new Intent(getActivity(),RuzhuActivity.class);
                startActivity(i);
            }else{
                ToastUtil.showToast(userModel.getJoin());
            }
        }else{
            type=1;
            setPageType();
        }
    }

    //收货人管理
    @InjectView(R.id.receive_layout)
    RelativeLayout receiveLayout;

    @OnClick(R.id.receive_layout)
    public void receiveLayout(View v) {
        if (isClick) {
            Intent intent = new Intent(getActivity(), PersonInfoActivity.class);
            intent.putExtra("seller",0);
            getActivity().startActivity(intent);
        }

    }

    @OnClick(R.id.tv_seller_fahuoguanli)
    public void tv_seller_fahuoguanli(){
        Intent intent = new Intent(getActivity(), PersonInfoActivity.class);
        intent.putExtra("seller",1);
        getActivity().startActivity(intent);
    }
    @OnClick(R.id.tv_seller_account)
    public void tv_seller_account(){
        Intent intent = new Intent(getActivity(), AccountActivity.class);
        getActivity().startActivity(intent);
    }


    //个人资料
    @InjectView(R.id.im_personlogo)
    SimpleDraweeView personLogo;

    //个人资料
    @InjectView(R.id.im_personlogo1)
    SimpleDraweeView personLogo1;

    @OnClick(R.id.im_personlogo)
    public void personLogo(View v) {
        if (isClick) {
            Intent intent = new Intent(getActivity(), EditBuyerActivity.class);
            intent.putExtra("seller",0);
            getActivity().startActivity(intent);
        }
    }
    @OnClick(R.id.im_personlogo1)
    public void personLogo1(View v) {
        if (isClick) {
            Intent intent = new Intent(getActivity(), EditBuyerActivity.class);
            intent.putExtra("seller",1);
            getActivity().startActivity(intent);
        }
    }

    //我的还价
    @InjectView(R.id.dicker_layout)
    TextView dickerLayout;

    @OnClick(R.id.dicker_layout)
    public void dickerLayout(View v) {
        if (isClick) {
            Intent intent = new Intent(getActivity(), DickerActivity.class);
            startActivity(intent);
        }

    }

    private BadgeView mBadgeView;

    //专属客服
    @InjectView(R.id.service_layout)
    RelativeLayout serviceLayout;

    @OnClick(R.id.call_layout)
    public void serviceLayout2(View v) {
        if (isClick) {
            Intent intent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+userModel.getCustomer_service()));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
    @OnClick(R.id.service_layout)
    public void serviceLayout(View v) {
        if (isClick) {
            Intent intent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+userModel.getCustomer_service()));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @OnClick(R.id.tv_ask)
    public void ask(View v) {
        if (isClick) {
            Intent intent = new Intent(getActivity(), MyBuyActivity.class);
            startActivity(intent);
        }
    }


    //售后服务
    @InjectView(R.id.after_layout)
    RelativeLayout afterLayout;

    @OnClick(R.id.after_layout)
    public void afterLayout(View v) {
        if (isClick) {
            Intent intent = new Intent(getActivity(), AfterSaleActivity.class);
            startActivity(intent);
        }

    }
    @OnClick(R.id.tv_seller_feedback)
    public void tvSellerFeedback(View v) {
        if (isClick) {
            Intent intent = new Intent(getActivity(), AfterSaleActivity.class);
            startActivity(intent);
        }
    }


    @InjectView(R.id.tv_personnick)
    TextView tv_personnick;

    @InjectView(R.id.tv_personnick1)
    TextView tv_personnick1;

    @InjectView(R.id.tv_balancecount)
    TextView tv_balancecount;

    private MySPEdit mySPEdit = MySPEdit.getInstance(getActivity());
    private MyApplication myApplication = MyApplication.getInstance();
    private String userInfo;

    private UserModel userModel;

    @Override
    public void onResume() {
        super.onResume();
        if (mySPEdit.getIsLogin() && myApplication.isPersonChange()) {
            Map<String, Object> params = new HashMap<>();
            params.put("model", "user");
            params.put("action", "info");
            params.put("user_id", mySPEdit.getUserId());
            params.put("token", mySPEdit.getToken());
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "info")));
            HttpUtils.sendPost(params, this, 100);
        }

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();


    }
    private void initView() {
        userModel = new Gson().fromJson(mySPEdit.getUserInfo(), UserModel.class);
        if (userModel!=null){
            tv_personnick.setText(userModel.getUser_name());
            tv_personnick1.setText(userModel.getUser_name());
            if (userModel.getUser_face() != null) {
                personLogo.setImageURI(Uri.parse(userModel.getUser_face()));
                personLogo1.setImageURI(Uri.parse(userModel.getUser_face()));
            }
            tv_balancecount.setText(userModel.getFormated_user_money());
        }
        if(!userModel.getUser_rank().equals("2")){
            ivRuzhu.setVisibility(View.VISIBLE);
        }else{
            ivRuzhu.setVisibility(View.GONE);
        }
        setPageType();
    }

    @Override
    public void success(String response, int id) {

        switch (id) {
            case 100:
                userInfo = response;
                mySPEdit.setUserInfo(userInfo);
                UserModel userModel = new Gson().fromJson(userInfo, UserModel.class);
                if (userModel.getUser_face() != null) {
                    personLogo.setImageURI(Uri.parse(userModel.getUser_face()));
                }
                initView();
                if(!userModel.getUser_rank().equals("2")){
                    type=1;
                    setPageType();
                }
                String str = "<font color='black'>"+getResources().getString(R.string.yijialiebiao)+"</font>" + "<font color= 'red'>("+userModel.getHuck_num()+")</font>";
                tvSellerYijia.setText(Html.fromHtml(str));
                isClick=true;
                break;
        }
    }

    @Override
    public void failByOther(String fail, int id) {
        isClick=true;
    }

    @Override
    public void fail(String fail, int id) {
        isClick=true;
        T.showShort(getActivity(), R.string.jianchawangluo);
    }
}
