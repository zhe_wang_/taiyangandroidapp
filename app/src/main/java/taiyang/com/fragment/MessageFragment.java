package taiyang.com.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSON;
import com.cundong.recyclerview.CustRecyclerView;
import com.cundong.recyclerview.HeaderAndFooterRecyclerViewAdapter;
import com.cundong.recyclerview.LoadingFooter;
import com.cundong.recyclerview.RecyclerOnScrollListener;
import com.cundong.recyclerview.RecyclerViewStateUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.adapter.AfterSaleAdapter;
import taiyang.com.entity.MessageListModel;
import taiyang.com.entity.MessageModel;
import taiyang.com.tydp_b.BaseFragment;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.T;
import taiyang.com.view.ErrorLayout;

/**
 * Created by heng on 2016/7/21.
 * 留言fragment
 */
public class MessageFragment extends BaseFragment implements HttpRequestListener {
    /*@InjectView(R.id.message_recyclerView)
    XRecyclerView mRecyclerView;*/
    private AfterSaleAdapter mAdapter;

    private String title;

    @InjectView(R.id.recycler_view)
    CustRecyclerView mRecyclerView;
    @InjectView(R.id.error_layout)
    ErrorLayout mErrorLayout;

    protected HeaderAndFooterRecyclerViewAdapter mRecyclerViewAdapter;

    protected int mCurrentPage = 1;
    protected int totalPage = 0;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message, container, false);
        ButterKnife.inject(this, view);
        title = getArguments().getString("title");
        return view;
    }

    private MySPEdit mySPEdit;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mySPEdit = MySPEdit.getInstance(getActivity());
        mCurrentPage = 1;
        mDatas=new ArrayList<>();
        mAdapter = new AfterSaleAdapter(getActivity());
        mAdapter.setOnItemClickLitener(new AfterSaleAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        mAdapter.setDataList(mDatas);
        mRecyclerViewAdapter = new HeaderAndFooterRecyclerViewAdapter(getActivity(),mAdapter);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.addOnScrollListener(mOnScrollListener);
        mRecyclerView.setIsRefreshing(false);
        mErrorLayout.setOnLayoutClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mCurrentPage = 1;
                mErrorLayout.setErrorType(ErrorLayout.NETWORK_LOADING);
                requestData();
            }
        });
        requestData();
    }
    private void requestData(){
        if ("全部".equals(title)) {
            showProgress(getActivity(),getString(R.string.jiazaizhong_dotdotdot));
            Map<String, Object> params = new HashMap<>();
            params.put("model", "user");
            params.put("action", "message_list");
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "message_list")));
            params.put("user_id", mySPEdit.getUserId());
            params.put("msg_type","");
            params.put("token", mySPEdit.getToken());
            params.put("size", 10);
            params.put("page", mCurrentPage);
            L.e("全部"+JSON.toJSONString(params));
            HttpUtils.sendPost(params, this, 100);
        } else if ("留言".equals(title)) {
            showProgress(getActivity(),getString(R.string.jiazaizhong_dotdotdot));
            Map<String, Object> params = new HashMap<>();
            params.put("model", "user");
            params.put("action", "message_list");
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "message_list")));
            params.put("user_id", mySPEdit.getUserId());
            params.put("msg_type", "0");
            params.put("token", mySPEdit.getToken());
            params.put("size", 10);
            params.put("page", mCurrentPage);
            L.e(JSON.toJSONString(params));
            L.e("留言"+JSON.toJSONString(params));
            HttpUtils.sendPost(params, this, 101);
        } else if ("投诉".equals(title)) {
            showProgress(getActivity(),getString(R.string.jiazaizhong_dotdotdot));
            Map<String, Object> params = new HashMap<>();
            params.put("model", "user");
            params.put("action", "message_list");
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "message_list")));
            params.put("user_id", mySPEdit.getUserId());
            params.put("msg_type", "1");
            params.put("token", mySPEdit.getToken());
            params.put("size", 10);
            params.put("page", mCurrentPage);
            L.e("投诉"+JSON.toJSONString(params));
            HttpUtils.sendPost(params, this, 102);
        } else {
            showProgress(getActivity(),getString(R.string.jiazaizhong_dotdotdot));
            Map<String, Object> params = new HashMap<>();
            params.put("model", "user");
            params.put("action", "message_list");
            params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "message_list")));
            params.put("user_id", mySPEdit.getUserId());
            params.put("msg_type", "2");
            params.put("token", mySPEdit.getToken());
            params.put("size", 10);
            params.put("page", mCurrentPage);
            L.e("售后"+JSON.toJSONString(params));
            HttpUtils.sendPost(params, this, 103);
        }
    }
    private MessageListModel messageListModel;
    private List<MessageModel> mDatas;

    @Override
    public void success(String response, int id) {
        mCurrentPage++;
        switch (id) {
            case 100:
                dataSuccess(response);
                break;
            case 101:
                dataSuccess(response);
                break;
            case 102:
                dataSuccess(response);
                break;
            case 103:
                dataSuccess(response);
                break;
        }
    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(getActivity());
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(getActivity());
        T.showShort(getActivity(),R.string.jianchawangluo);
        if (mCurrentPage == 1) {
            mErrorLayout.setErrorType(ErrorLayout.NETWORK_ERROR);
        } else {

            //在无网络时，滚动到底部时，mCurrentPage先自加了，然而在失败时却
            //没有减回来，如果刻意在无网络的情况下上拉，可以出现漏页问题
            //find by TopJohn
//            mCurrentPage--;

            mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
            RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, totalPage, LoadingFooter.State.NetWorkError, mFooterClick);
            mAdapter.notifyDataSetChanged();
        }
    }
    //數據請求 成功進行處理
    private void dataSuccess(String response) {
        dismissProgress(getActivity());
        messageListModel = new Gson().fromJson(response, MessageListModel.class);
        totalPage = messageListModel.getTotal().getPage_count();
        mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
        L.e("縂頁數:"+totalPage);
//                mRecyclerView.loadMoreComplete();
        for (int i = 0; i < messageListModel.getList().size(); i++) {
            mDatas.add(messageListModel.getList().get(i));
        }
        if (mCurrentPage == 1) {
            L.e("数据的大小:"+mDatas.size());
            mAdapter.setDataList(mDatas);
        } else {
            L.e("设置数据:");
            RecyclerViewStateUtils.setFooterViewState(mRecyclerView, LoadingFooter.State.Normal);
//            mAdapter.addAll(mDatas);
            mAdapter.setDataList(mDatas);
        }
        mRecyclerView.refreshComplete();
        mAdapter.notifyDataSetChanged();
    }
    private View.OnClickListener mFooterClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
            requestData();
        }
    };
    protected RecyclerOnScrollListener mOnScrollListener = new RecyclerOnScrollListener() {

        @Override
        public void onScrolled(int dx, int dy) {
            super.onScrolled(dx, dy);

//            if(null != headerView) {
//                if (dy == 0 || dy < headerView.getHeight()) {
//                    toTopBtn.setVisibility(View.GONE);
//                }
//            }


        }

        @Override
        public void onScrollUp() {
            // 滑动时隐藏float button
//            if (toTopBtn.getVisibility() == View.VISIBLE) {
//                toTopBtn.setVisibility(View.GONE);
//                animate(toTopBtn, R.anim.floating_action_button_hide);
//            }
        }

        @Override
        public void onScrollDown() {
//            if (toTopBtn.getVisibility() != View.VISIBLE) {
//                toTopBtn.setVisibility(View.VISIBLE);
//                animate(toTopBtn, R.anim.floating_action_button_show);
//            }
        }

        @Override
        public void onBottom() {
            LoadingFooter.State state = RecyclerViewStateUtils.getFooterViewState(mRecyclerView);
            if(state == LoadingFooter.State.Loading) {
                return;
            }
            L.e("當前頁:"+mCurrentPage);
            L.e("縂頁數:"+totalPage);
            if (mCurrentPage <=totalPage) {
                // loading more
                RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
                requestData();
            } else {
                //the end
                /*if (totalPage > 1){
                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, getPageSize(), LoadingFooter.State.TheEnd, null);
                }else {
                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, mListAdapter.getItemCount(), LoadingFooter.State.TheEnd, null);

                }*/
                RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, totalPage, LoadingFooter.State.TheEnd, null);
            }
        }


    };
}
