package taiyang.com.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.adapter.FactoryInfoAdapter;
import taiyang.com.tydp_b.R;

/**
 * Created by admin on 2016/8/2.
 * 厂商信息frgment
 */
public class FactoryInfoFragment extends Fragment {
    @InjectView(R.id.id_stickynavlayout_innerscrollview)
    ListView mListView;
    private FactoryInfoAdapter mAdapter;

    private List<String> mDatas = new ArrayList<String>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_factoryinfo, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        List<String> mDatas = new ArrayList<>();
//        mDatas.add("1");
//        mDatas.add("1");
//        mDatas.add("1");
//        mDatas.add("1");
//        mAdapter = new FactoryInfoAdapter(mDatas);
//        mRecyclerView.setAdapter(mAdapter);
//        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        for (int i = 0; i < 10; i++) {
            mDatas.add("测试" + " -> " + i);
        }
//        mAdapter=new FactoryInfoAdapter(getActivity(),mDatas);

        mListView.setAdapter(mAdapter);
    }
}
