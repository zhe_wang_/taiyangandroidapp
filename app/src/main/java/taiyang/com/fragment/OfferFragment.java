package taiyang.com.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.offerfragment.CategoryFragment;
import taiyang.com.tydp_b.R;



/**
 * 报盘fragment
 * Created by hoo on 2016/7/13.
 */
public class OfferFragment extends Fragment implements View.OnClickListener {

    private FragmentManager fManager;
    private CategoryFragment mZhuFragment;
    private CategoryFragment mNiuFragment;
    private CategoryFragment mYangFragment;
    private CategoryFragment mQinFragment;
    private CategoryFragment mWaterFragment;
    private CategoryFragment mOtherFragment;

    @InjectView(R.id.rb_left_zhu)
    RadioButton rb_left_zhu;
    @InjectView(R.id.rb_left_niu)
    RadioButton rb_left_niu;
    @InjectView(R.id.rb_left_yang)
    RadioButton rb_left_yang;
    @InjectView(R.id.rb_left_qin)
    RadioButton rb_left_qin;
    @InjectView(R.id.rb_left_water)
    RadioButton rb_left_water;
    @InjectView(R.id.rb_left_other)
    RadioButton rb_left_other;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_offer, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        changeToF1();

        initView();
        initData();
        initListener();
    }


    private void initView() {

    }

    private void initData() {

    }

    private void initListener() {
        rb_left_zhu.setOnClickListener(this);
        rb_left_niu.setOnClickListener(this);
        rb_left_yang.setOnClickListener(this);
        rb_left_qin.setOnClickListener(this);
        rb_left_water.setOnClickListener(this);
        rb_left_other.setOnClickListener(this);
    }

    /**
     * 通过点击rb切换fragment
     * @param v
     */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rb_left_zhu:
                changeToF1();
                break;
            case R.id.rb_left_niu:
                changeToF2();
                break;
            case R.id.rb_left_yang:
                changeToF3();
                break;
            case R.id.rb_left_qin:
                changeToF4();
                break;
            case R.id.rb_left_water:
                changeToF5();
                break;

            case R.id.rb_left_other:
                changeToF6();
                break;

            default:
                break;
        }
    }
    /**
     * 切换zhuFragment
     */
    private void changeToF1() {
        fManager =  getChildFragmentManager();
        FragmentTransaction transaction = fManager.beginTransaction();
        commonChange(transaction);

        rb_left_zhu.setChecked(true);
        if (mZhuFragment == null) {
            transaction.add(R.id.offer_container, new CategoryFragment().setType(1), "f1");
        } else {
            transaction.show(mZhuFragment);
        }
        transaction.commitAllowingStateLoss();

    }

    /**
     * 切换niuFragment
     */
    private void changeToF2() {
        fManager =  getChildFragmentManager();
        FragmentTransaction transaction = fManager.beginTransaction();
        commonChange(transaction);

        rb_left_niu.setChecked(true);
        if (mNiuFragment == null) {
            transaction.add(R.id.offer_container, new CategoryFragment().setType(2), "f2");
        } else {
            transaction.show(mNiuFragment);
        }
        transaction.commitAllowingStateLoss();
    }

    /**
     * 切换yangFragment
     */
    private void changeToF3() {
        fManager =  getChildFragmentManager();
        FragmentTransaction transaction = fManager.beginTransaction();
        commonChange(transaction);

        rb_left_yang.setChecked(true);
        if (mYangFragment == null) {
            transaction.add(R.id.offer_container, new CategoryFragment().setType(3), "f3");
        } else {
            transaction.show(mYangFragment);
        }
        transaction.commitAllowingStateLoss();
    }

    /**
     * 切换yangFragment
     */
    private void changeToF4() {
        fManager =  getChildFragmentManager();
        FragmentTransaction transaction = fManager.beginTransaction();
        commonChange(transaction);

        rb_left_qin.setChecked(true);
        if (mQinFragment == null) {
            transaction.add(R.id.offer_container, new CategoryFragment().setType(289), "f4");
        } else {
            transaction.show(mQinFragment);
        }
        transaction.commitAllowingStateLoss();
    }


    /**
     * 切换WaterFragment
     */
    private void changeToF5() {
        fManager =  getChildFragmentManager();
        FragmentTransaction transaction = fManager.beginTransaction();
        commonChange(transaction);

        rb_left_water.setChecked(true);
        if (mWaterFragment == null) {
            transaction.add(R.id.offer_container,  new CategoryFragment().setType(5), "f5");
        } else {
            transaction.show(mWaterFragment);
        }
        transaction.commitAllowingStateLoss();
    }

    /**
     * 切换otherFragment
     */
    private void changeToF6() {
        fManager =  getChildFragmentManager();
        FragmentTransaction transaction = fManager.beginTransaction();
        commonChange(transaction);

        rb_left_other.setChecked(true);
        if (mOtherFragment== null) {
            transaction.add(R.id.offer_container,  new CategoryFragment().setType(290), "f6");
        } else {
            transaction.show(mOtherFragment);
        }
        transaction.commitAllowingStateLoss();
    }


    /**
     * 每次切换时 隐藏以前打开的fragment 类似删除
     * @param transaction
     */
    private void commonChange(FragmentTransaction transaction) {
        mZhuFragment = (CategoryFragment) fManager.findFragmentByTag("f1");
        mNiuFragment = (CategoryFragment) fManager.findFragmentByTag("f2");
        mYangFragment = (CategoryFragment) fManager.findFragmentByTag("f3");
        mQinFragment = (CategoryFragment) fManager.findFragmentByTag("f4");
        mWaterFragment = (CategoryFragment) fManager.findFragmentByTag("f5");
        mOtherFragment = (CategoryFragment) fManager.findFragmentByTag("f6");

        if (mZhuFragment != null) {
            transaction.hide(mZhuFragment);
        }
        if (mNiuFragment != null) {
            transaction.hide(mNiuFragment);
        }
        if (mYangFragment != null) {
            transaction.hide(mYangFragment);
        }
        if (mQinFragment != null) {
            transaction.hide(mQinFragment);
        }
        if (mWaterFragment != null) {
            transaction.hide(mWaterFragment);
        }
        if (mOtherFragment != null) {
            transaction.hide(mOtherFragment);
        }
    }


}
