package taiyang.com.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.cundong.recyclerview.CustRecyclerView;
import com.cundong.recyclerview.HeaderAndFooterRecyclerViewAdapter;
import com.cundong.recyclerview.LoadingFooter;
import com.cundong.recyclerview.RecyclerOnScrollListener;
import com.cundong.recyclerview.RecyclerViewStateUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.adapter.SellerOrderAdapter;
import taiyang.com.entity.SellerBaopanListModel;
import taiyang.com.tydp_b.BaseFragment;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.T;
import taiyang.com.view.ErrorLayout;

/**
 * Created by heng on 2016/7/18.
 */
public class SellerOrderFragment extends BaseFragment implements HttpRequestListener {


    private SellerOrderAdapter mAdapter;
    private String tabs;
    @InjectView(R.id.ll_nodata_layout)
    LinearLayout ll_nodata_layout;

    @InjectView(R.id.recycler_view)
    CustRecyclerView mRecyclerView;
    @InjectView(R.id.error_layout)
    ErrorLayout mErrorLayout;

    @InjectView(R.id.recycler_view_layout)
    LinearLayout recycler_view_layout;
    protected HeaderAndFooterRecyclerViewAdapter mRecyclerViewAdapter;

    protected int mCurrentPage = 1;
    protected int totalPage = 0;
    private List<SellerBaopanListModel.ListBean> mDatas;
    private View order_empty_view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order, container, false);
        ButterKnife.inject(this, view);
        tabs = getArguments().getString("tabs");
//        mTabs.setText(tabs);
        return view;
    }

    private MySPEdit mySPEdit = MySPEdit.getInstance(getActivity());

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mCurrentPage = 1;

        mDatas=new ArrayList<>();
        mAdapter = new SellerOrderAdapter(getActivity());
        mAdapter.setDataList(mDatas);
        mRecyclerViewAdapter = new HeaderAndFooterRecyclerViewAdapter(getActivity(),mAdapter);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.addOnScrollListener(mOnScrollListener);
        mRecyclerView.setIsRefreshing(false);
//        mRecyclerView.setEmptyView(order_empty_view);
        mErrorLayout.setOnLayoutClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mCurrentPage = 1;
                mErrorLayout.setErrorType(ErrorLayout.NETWORK_LOADING);
                requestData();
            }
        });
        requestData();
    }


    //请求数据
    private void requestData(){
        showProgress(getActivity(), getString(R.string.jiazaizhong_dotdotdot));
        Map<String, Object> params = new HashMap<>();
        params.put("model", "seller");
        params.put("action", "order_list");
        params.put("user_id", mySPEdit.getUserId());
        params.put("token", mySPEdit.getToken());
        if (getString(R.string.quanbu).equals(tabs)) {
        } else if (getString(R.string.weishoukuan).equals(tabs)) {
            params.put("status", 0);
        } else if (getString(R.string.yishoukuan).equals(tabs)) {
            params.put("status", 1);
        } else if (getString(R.string.yiyufu).equals(tabs)) {
            params.put("status", 2);
        } else {
            params.put("status", 99);
        }
        params.put("page", mCurrentPage);
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller", "order_list")));
        HttpUtils.sendPost(params, this);
    }

    @Override
    public void success(String response, int id) {
        mCurrentPage++;
        dataSuccess(response);
    }


    private SellerBaopanListModel mSellerBaopanListModel;
    //數據請求 成功進行處理
    private void dataSuccess(String response) {
        dismissProgress(getActivity());

        mSellerBaopanListModel = new Gson().fromJson(response, SellerBaopanListModel.class);
        totalPage = mSellerBaopanListModel.getTotal().getPage_count();
        mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
        L.e("总页数:"+totalPage);
//                mRecyclerView.loadMoreComplete();
        for (int i = 0; i < mSellerBaopanListModel.getList().size(); i++) {
            mDatas.add(mSellerBaopanListModel.getList().get(i));
        }
        if (mDatas.size()==0){
            recycler_view_layout.setVisibility(View.GONE);
            ll_nodata_layout.setVisibility(View.VISIBLE);
        }
        if (mCurrentPage == 1) {
            L.e("数据的大小:"+mDatas.size());
            mAdapter.setDataList(mDatas);
        } else {
            RecyclerViewStateUtils.setFooterViewState(mRecyclerView, LoadingFooter.State.Normal);
//            mAdapter.addAll(mDatas);
            mAdapter.setDataList(mDatas);
        }
        mRecyclerView.refreshComplete();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void failByOther(String fail, int id) {
        dismissProgress(getActivity());
    }

    @Override
    public void fail(String fail, int id) {
        dismissProgress(getActivity());
        T.showShort(getActivity(), R.string.jianchawangluo);
        if (mCurrentPage == 1) {
            mErrorLayout.setErrorType(ErrorLayout.NETWORK_ERROR);
        } else {

            //在无网络时，滚动到底部时，mCurrentPage先自加了，然而在失败时却
            //没有减回来，如果刻意在无网络的情况下上拉，可以出现漏页问题
            //find by TopJohn
//            mCurrentPage--;

            mErrorLayout.setErrorType(ErrorLayout.HIDE_LAYOUT);
            RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, totalPage, LoadingFooter.State.NetWorkError, mFooterClick);
            mAdapter.notifyDataSetChanged();
        }
    }
    private View.OnClickListener mFooterClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
            requestData();
        }
    };
    protected RecyclerOnScrollListener mOnScrollListener = new RecyclerOnScrollListener() {

        @Override
        public void onScrolled(int dx, int dy) {
            super.onScrolled(dx, dy);

//            if(null != headerView) {
//                if (dy == 0 || dy < headerView.getHeight()) {
//                    toTopBtn.setVisibility(View.GONE);
//                }
//            }


        }

        @Override
        public void onScrollUp() {
            // 滑动时隐藏float button
//            if (toTopBtn.getVisibility() == View.VISIBLE) {
//                toTopBtn.setVisibility(View.GONE);
//                animate(toTopBtn, R.anim.floating_action_button_hide);
//            }
        }

        @Override
        public void onScrollDown() {
//            if (toTopBtn.getVisibility() != View.VISIBLE) {
//                toTopBtn.setVisibility(View.VISIBLE);
//                animate(toTopBtn, R.anim.floating_action_button_show);
//            }
        }

        @Override
        public void onBottom() {
            LoadingFooter.State state = RecyclerViewStateUtils.getFooterViewState(mRecyclerView);
            if(state == LoadingFooter.State.Loading) {
                return;
            }
            L.e("當前頁:"+mCurrentPage);
            L.e("縂頁數:"+totalPage);
            if (mCurrentPage <= totalPage) {
                // loading more
                RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, totalPage, LoadingFooter.State.Loading, null);
                requestData();
            } else {
                //the end
                /*if (totalPage > 1){
                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, getPageSize(), LoadingFooter.State.TheEnd, null);
                }else {
                    RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, mListAdapter.getItemCount(), LoadingFooter.State.TheEnd, null);

                }*/
                RecyclerViewStateUtils.setFooterViewState(getActivity(), mRecyclerView, totalPage, LoadingFooter.State.TheEnd, null);
            }
        }


    };

}
