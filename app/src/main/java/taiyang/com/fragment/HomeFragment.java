package taiyang.com.fragment;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import cn.magicwindow.Session;
import taiyang.com.activity.FactoryListActivity;
import taiyang.com.activity.InquiryListActivity;
import taiyang.com.activity.OfferInfoActivity;
import taiyang.com.activity.PureInquiryListActivity;
import taiyang.com.activity.QiugouInfoActivity;
import taiyang.com.activity.QiugouListActivity;
import taiyang.com.adapter.HomeDTLBAdapter;
import taiyang.com.adapter.HomeRvAdapter;
import taiyang.com.entity.ContentBean;
import taiyang.com.tydp_b.MainActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.Constants;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.ToastUtil;
import taiyang.com.view.MyLinearLayoutManager;
import taiyang.com.view.XCSlideView;

/**
 * HomeFragment
 * Created by hoo on 2016/7/13.
 */
public class HomeFragment extends Fragment implements OnClickListener {

    int a = 0;
    int s = 0;


    //-----------------------------------------------
    @InjectView(R.id.tv_miaosha_more)
    TextView tv_miaosha_more;

    @InjectView(R.id.tv_tejia_more)
    TextView tv_tejia_more;

    @InjectView(R.id.tv_qiugou_more)
    TextView tv_qiugou_more;

    @InjectView(R.id.tv_tuijian_more)
    TextView tv_tuijian_more;


    //------------------------------------------------

    @InjectView(R.id.ll_tejia1)
    LinearLayout ll_tejia1;
    @InjectView(R.id.riv_tejia1)
    SimpleDraweeView riv_tejia1;

    @InjectView(R.id.tv_tejia1_name)
    TextView tv_tejia1_name;

    @InjectView(R.id.tv_tejia1_price)
    TextView tv_tejia1_price;

    @InjectView(R.id.tv_tejia1_fake_price)
    TextView tv_tejia1_fake_price;

    @InjectView(R.id.ll_tejia2)
    LinearLayout ll_tejia2;
    @InjectView(R.id.riv_tejia2)
    SimpleDraweeView riv_tejia2;

    @InjectView(R.id.tv_tejia2_name)
    TextView tv_tejia2_name;

    @InjectView(R.id.tv_tejia2_price)
    TextView tv_tejia2_price;

    @InjectView(R.id.tv_tejia2_fake_price)
    TextView tv_tejia2_fake_price;

    //-----------------------------------------
    @InjectView(R.id.ll_miaosha1)
    LinearLayout ll_miaosha1;

    @InjectView(R.id.riv_miaosha1)
    SimpleDraweeView riv_miaosha1;

    @InjectView(R.id.tv_miaosha1_name)
    TextView tv_miaosha1_name;

    @InjectView(R.id.tv_miaosha1_price)
    TextView tv_miaosha1_price;

    @InjectView(R.id.tv_miaosha1_fake_price)
    TextView tv_miaosha1_fake_price;
    @InjectView(R.id.ll_miaosha2)
    LinearLayout ll_miaosha2;
    @InjectView(R.id.riv_miaosha2)
    SimpleDraweeView riv_miaosha2;

    @InjectView(R.id.tv_miaosha2_name)
    TextView tv_miaosha2_name;

    @InjectView(R.id.tv_miaosha2_price)
    TextView tv_miaosha2_price;

    @InjectView(R.id.tv_miaosha2_fake_price)
    TextView tv_miaosha2_fake_price;

    @InjectView(R.id.ll_miaosha3)
    LinearLayout ll_miaosha3;
    @InjectView(R.id.riv_miaosha3)
    SimpleDraweeView riv_miaosha3;

    @InjectView(R.id.tv_miaosha3_name)
    TextView tv_miaosha3_name;

    @InjectView(R.id.tv_miaosha3_price)
    TextView tv_miaosha3_price;

    @InjectView(R.id.tv_miaosha3_fake_price)
    TextView tv_miaosha3_fake_price;
    //--------------------------------


    @InjectView(R.id.ll_qiugou_list)
    LinearLayout ll_qiugou_list;
    @InjectView(R.id.ll_best_goods)
    LinearLayout ll_best_goods;

    @InjectView(R.id.ll_all)
    LinearLayout ll_all;
    @InjectView(R.id.tv_classify)
    TextView tv_classify;
    @InjectView(R.id.rl_home_qh)
    RelativeLayout rl_home_qh;
    @InjectView(R.id.rl_home_xh)
    RelativeLayout rl_home_xh;
    @InjectView(R.id.rl_home_zxh)
    RelativeLayout rl_home_zxh;
    @InjectView(R.id.rl_home_gs)
    RelativeLayout rl_home_gs;
    @InjectView(R.id.rl_home_ds)
    RelativeLayout rl_home_ds;
    @InjectView(R.id.info_recycleView)
    RecyclerView info_recycleView;
    @InjectView(R.id.vp_homepager)
    ViewPager vp_homepager;
    @InjectView(R.id.bt_home_choose_factory)
    TextView bt_home_choose_factory;
    @InjectView(R.id.bt_home_choose_goods)
    TextView bt_home_choose_goods;
    @InjectView(R.id.et_content)
    EditText et_home_choose;
    @InjectView(R.id.iv_search)
    ImageView iv_home_choose;
    @InjectView(R.id.rl_home_choose)
    RelativeLayout rl_home_choose;
    @InjectView(R.id.tv_pop_goods)
    TextView tv_pop_goods;
    @InjectView(R.id.tv_pop_factory)
    TextView tv_pop_factory;
    private HomeRvAdapter mAdapter;
    private List<ContentBean.BestGoodsBean> mList;
    private XCSlideView mSlideViewLEFT;
    private PopupWindow mPopWindow;
    private List<ContentBean.AppSlideBean> app_silde;
    private View popView;
    private Map<String, Object> params;
    private MySPEdit mySPEdit;
    //延迟滚动banner
    private Handler mainHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            vp_homepager.setCurrentItem(vp_homepager.getCurrentItem() + 1);// 设置下一页
            // 延时发送消息
            mainHandler.sendEmptyMessageDelayed(0, 4000);
        }

    };
    private ContentBean contentBean;

    public HomeFragment() {

    }

    @OnClick(R.id.change_lang)
    public void changLang(){

        if("en".equals(MySPEdit.getInstance(getContext()).getUserLang())){
            MySPEdit.getInstance(getContext()).setUserLang("ch");
        }else{
            MySPEdit.getInstance(getContext()).setUserLang("en");
        }

        Intent intent = new Intent(getActivity(),MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
// 杀掉进程
        Session.onKillProcess();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }
    //    public HomeFragment(View view) {
//        this.mSlideViewLEFT = (XCSlideView) view;
//    }
    public void setView(XCSlideView mSlideViewLEFT) {
        this.mSlideViewLEFT = mSlideViewLEFT;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.inject(this, view);
        return view;
    }
    @OnClick(R.id.tv_qiugou_more)
    public void qiugou(View v) {
        Intent i = new Intent(getActivity(), QiugouListActivity.class);
        getActivity().startActivity(i);
    }
    @OnClick(R.id.tv_miaosha_more)
    public void miaosha(View v) {
        showPorductList("hot",getResources().getString(R.string.xianshimiaosha));
    }
    @OnClick(R.id.tv_tejia_more)
    public void tejia(View v) {

        showPorductList("promote",getResources().getString(R.string.jinritejia));
    }
    @OnClick(R.id.tv_tuijian_more)
    public void tuijian(View v) {

        showPorductList("best",getResources().getString(R.string.tuijianchanpin));
    }
    protected boolean ch(){
        return Locale.getDefault().getLanguage().equals("ch");
    }
    private void showPorductList(String type,String title){
        Intent i = new Intent(getActivity(), PureInquiryListActivity.class);
        i.putExtra("type",type);
        i.putExtra("title",title);
        getActivity().startActivity(i);
    }

    @InjectView(R.id.refreshLayout)
    RefreshLayout refreshLayout;
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mySPEdit = MySPEdit.getInstance(getActivity());
        initView();
        initListeren();
        initData();
        refreshLayout.setRefreshHeader(new MaterialHeader(getContext()).setShowBezierWave(false));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refreshlayout.finishRefresh(2000/*,false*/);//传入false表示刷新失败
                initData();
            }
        });
    }

    private void initView() {

    }

    private void initListeren() {
        tv_classify.setOnClickListener(this);
        tv_pop_goods.setOnClickListener(this);
        tv_pop_factory.setOnClickListener(this);
        bt_home_choose_goods.setOnClickListener(this);
        iv_home_choose.setOnClickListener(this);
        bt_home_choose_factory.setOnClickListener(this);
        rl_home_qh.setOnClickListener(this);
        rl_home_xh.setOnClickListener(this);
        rl_home_zxh.setOnClickListener(this);
        ll_all.setOnClickListener(this);
        rl_home_gs.setOnClickListener(this);
        rl_home_ds.setOnClickListener(this);
        // viewpager监听 改变点
    }

    private void initData() {

        params = new HashMap<>();
        params.put("model", "other");
        params.put("action", "get_index");
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("other", "get_index")));
        HttpUtils.sendPost(params, new HttpRequestListener() {
            @Override
            public void success(String response, int id) {

                ContentBean contentBean = new Gson().fromJson(response, ContentBean.class);
                if (contentBean == null) {
                    return;
                }

                //首页轮播图
                if(app_silde!=null){
                    app_silde.clear();
                }
                app_silde = contentBean.getApp_slide();
                HomeDTLBAdapter homeDTLBAdapter = new HomeDTLBAdapter(getActivity(), app_silde);
                vp_homepager.setAdapter(homeDTLBAdapter);

                // 默认设置中间的某个item
                vp_homepager.setCurrentItem(app_silde.size() * 100000);
                // 延时发送消息
                if (mainHandler != null) {
                    mainHandler.removeCallbacksAndMessages(null);
                    mainHandler.sendEmptyMessageDelayed(0, 4000);
                }

                //首页大图
//                String dtUrl = contentBean.getApp_index_pic().getUrl();
//                iv_home_module1.setImageURI(Uri.parse(dtUrl));

                //首頁商品列表
                mList = contentBean.getBest_goods();

                try {
                    setTejiaGoodsView(contentBean);
                    setMiaoshaGoodsView(contentBean);
                    setQiugouGoodsView(contentBean);
                    setTuijianGoodsView(contentBean);
                } catch (Exception e) {

                }
                mAdapter = new HomeRvAdapter(mList, getActivity());
                info_recycleView.setAdapter(mAdapter);

                mAdapter.notifyDataSetChanged();
                info_recycleView.setLayoutManager(new MyLinearLayoutManager(getActivity()));
                mAdapter.setOnItemClickLitener(new HomeRvAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                    }
                });
            }


            @Override
            public void failByOther(String fail, int id) {

            }

            @Override
            public void fail(String fail, int id) {

            }
        });
    }

    private void setTuijianGoodsView(ContentBean contentBean) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        ll_best_goods.removeAllViews();
        for (ContentBean.BestGoodsBean bestGoodsBean : contentBean.getBest_goods()) {
            LinearLayout v = (LinearLayout) inflater
                    .inflate(R.layout.recyclerview_inquirylist_item, null);
            final String Goods_id = bestGoodsBean.getGoods_id();
            int goods_number = Integer.parseInt(bestGoodsBean.getGoods_number());
            //库存不为0
            if (goods_number  <= 0) {
                v.findViewById(R.id.rl_home_item).setOnClickListener(null);
                v.findViewById(R.id.rl_home_isover).setVisibility(View.VISIBLE);
            } else {
                v.findViewById(R.id.rl_home_item).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(getActivity(), OfferInfoActivity.class);
                        i.putExtra("Goods_id", Goods_id);
                        getActivity().startActivity(i);
                    }
                });
                v.findViewById(R.id.rl_home_isover).setVisibility(View.GONE);
            }
            ((SimpleDraweeView) v.findViewById(R.id.imageView)).setImageURI(Uri.parse(bestGoodsBean.getGoods_thumb()));
            ((TextView) v.findViewById(R.id.textView_chanpin)).setText(bestGoodsBean.getGoods_name());//名称
            ((TextView) v.findViewById(R.id.tv_chanpin_number)).setText(bestGoodsBean.getBrand_sn());//厂号
            ((TextView) v.findViewById(R.id.tv_ilaitem_location)).setText(bestGoodsBean.getGoods_local());//地点
                ((TextView) v.findViewById(R.id.tv_ilaitem_location)).setText(bestGoodsBean.getGoods_local()+" "+bestGoodsBean.getArrive_count());//地点

            ((TextView) v.findViewById(R.id.tv_ilaitem_price)).setText(bestGoodsBean.getShop_price());//价格

            ((TextView) v.findViewById(R.id.iv_homelist_guojia)).setText(bestGoodsBean.getRegion_name());
            ((TextView) v.findViewById(R.id.tv_ilaitem_unit)).setText("/" + bestGoodsBean.getUnit_name());//价格单位
            ((TextView) v.findViewById(R.id.tv_ilaitem_price2)).setText(bestGoodsBean.spec_1);//规格数量
            ((TextView) v.findViewById(R.id.tv_ilaitem_unit2)).setText(bestGoodsBean.spec_1_unit);//规格单位

            //还价
            if (bestGoodsBean.getOffer().equals("11")) {
                ((TextView)v.findViewById(R.id.tv_huan)).setVisibility(View.VISIBLE);
            } else {
                ((TextView)v.findViewById(R.id.tv_huan)).setVisibility(View.GONE);
            }
            //拼
            if (bestGoodsBean.getIs_pin() == 1) {
                ((TextView)v.findViewById(R.id.tv_pin)).setVisibility(View.VISIBLE);
            } else {
                ((TextView)v.findViewById(R.id.tv_pin)).setVisibility(View.GONE);
            }
            //商品类型  6=期货7=现货
            if (bestGoodsBean.getGoods_type().equals("6")) {
                ((TextView)v.findViewById(R.id.tv_xian_qi)).setText(getResources().getString(R.string.qi));
                ((TextView)v.findViewById(R.id.tv_xian_qi)).setBackgroundColor(getResources().getColor(R.color.qi));
            } else {
                ((TextView)v.findViewById(R.id.tv_xian_qi)).setText(getResources().getString(R.string.xian));
                ((TextView)v.findViewById(R.id.tv_xian_qi)).setBackgroundColor(getResources().getColor(R.color.xian));

            }
            //售卖类型 4零5整
            if (bestGoodsBean.getSell_type().equals("4")) {
                ((TextView)v.findViewById(R.id.tv_zheng_ling)).setText(getResources().getString(R.string.ling));
                ((TextView)v.findViewById(R.id.tv_zheng_ling)).setBackgroundColor(getResources().getColor(R.color.ling));
            } else {
                ((TextView)v.findViewById(R.id.tv_zheng_ling)).setText(getResources().getString(R.string.zheng));
                ((TextView)v.findViewById(R.id.tv_zheng_ling)).setBackgroundColor(getResources().getColor(R.color.zheng));
            }


            ll_best_goods.addView(v);
        }
    }

    private void setMiaoshaGoodsView(ContentBean contentBean) {

        final ContentBean.BestGoodsBean good1 = contentBean.getMiaosha_goods().get(0);
        riv_miaosha1.setImageURI(good1.getGoods_thumb());
        tv_miaosha1_name.setText(good1.getGoods_name());
        tv_miaosha1_price.setText(Html.fromHtml("<font color='#e64040'>" + good1.getShop_price() + "</font>/" + good1.getUnit_name()));
        tv_miaosha1_fake_price.setText(good1.getShop_price_fake() + "/"+good1.getUnit_name());
        tv_miaosha1_fake_price.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        ll_miaosha1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), OfferInfoActivity.class);
                i.putExtra("Goods_id", good1.getGoods_id());
                getActivity().startActivity(i);
            }
        });
        final ContentBean.BestGoodsBean good2 = contentBean.getMiaosha_goods().get(1);
        riv_miaosha2.setImageURI(good2.getGoods_thumb());
        tv_miaosha2_name.setText(good2.getGoods_name());
        tv_miaosha2_price.setText(Html.fromHtml("<font color='#e64040'>" + good2.getShop_price() + "</font>/" + good2.getUnit_name()));
        tv_miaosha2_fake_price.setText(good2.getShop_price_fake() + "/"+good2.getUnit_name());
        tv_miaosha2_fake_price.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        ll_miaosha2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), OfferInfoActivity.class);
                i.putExtra("Goods_id", good2.getGoods_id());
                getActivity().startActivity(i);
            }
        });
        final ContentBean.BestGoodsBean good3 = contentBean.getMiaosha_goods().get(2);
        riv_miaosha3.setImageURI(good3.getGoods_thumb());
        tv_miaosha3_name.setText(good3.getGoods_name());
        tv_miaosha3_price.setText(Html.fromHtml("<font color='#e64040'>" + good3.getShop_price() + "</font>/" + good3.getUnit_name()));
        tv_miaosha3_fake_price.setText(good3.getShop_price_fake() + "/"+good3.getUnit_name());
        tv_miaosha3_fake_price.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        ll_miaosha3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), OfferInfoActivity.class);
                i.putExtra("Goods_id", good3.getGoods_id());
                getActivity().startActivity(i);
            }
        });

    }

    private void setQiugouGoodsView(ContentBean contentBean) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        ll_qiugou_list.removeAllViews();
        for (final ContentBean.QiuGou qiuGou : contentBean.getPurchase_list()) {
            LinearLayout v = (LinearLayout) inflater
                    .inflate(R.layout.recyclerview_qiugou_list_item, null);
            v.findViewById(R.id.rl_home_item).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(),QiugouInfoActivity.class);
                    intent.putExtra("id",qiuGou.getId()+"");
                    startActivity(intent);
                }
            });
            ((SimpleDraweeView) v.findViewById(R.id.riv_qiugou1)).setImageURI(Uri.parse(qiuGou.getUser_face()));
            ((TextView) v.findViewById(R.id.tv_qiugou1_name)).setText(qiuGou.getUser_name().replaceAll("(\\d{3})\\d{4}(\\d{4})","$1****$2"));
            ((TextView) v.findViewById(R.id.tv_qiugou1_localtion)).setText(qiuGou.getAddress());
            ((TextView) v.findViewById(R.id.tv_qiugou1_time)).setText(qiuGou.getCreated_at());
            SpannableStringBuilder style2 = new SpannableStringBuilder(qiuGou.getGoods_name() + "    " +
                    qiuGou.getGoods_num() + "吨   " + qiuGou.getPrice_low() + "-" + qiuGou.getPrice_up() + "元");
            ((TextView) v.findViewById(R.id.tv_qiugou1_info)).setText(style2);
            v.findViewById(R.id.ll_more).setVisibility(View.GONE);
            ll_qiugou_list.addView(v);
        }
    }

    private void setTejiaGoodsView(ContentBean contentBean) {
        final ContentBean.BestGoodsBean good1 = contentBean.getTejia_goods().get(0);
        riv_tejia1.setImageURI(good1.getGoods_thumb());
        tv_tejia1_name.setText(good1.getGoods_name());
        tv_tejia1_price.setText(Html.fromHtml("<font color='#e64040'>" + good1.getShop_price() + "</font>/" + good1.getUnit_name()));
        tv_tejia1_fake_price.setText(good1.getShop_price_fake() + "/"+good1.getUnit_name());
        tv_tejia1_fake_price.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        ll_tejia1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), OfferInfoActivity.class);
                i.putExtra("Goods_id", good1.getGoods_id());
                getActivity().startActivity(i);
            }
        });
        final ContentBean.BestGoodsBean good2 = contentBean.getTejia_goods().get(1);
        riv_tejia2.setImageURI(good2.getGoods_thumb());
        tv_tejia2_name.setText(good2.getGoods_name());
        tv_tejia2_price.setText(Html.fromHtml("<font color='#e64040'>" + good2.getShop_price() + "</font>/" + good2.getUnit_name()));
        tv_tejia2_fake_price.setText(good2.getShop_price_fake() + "/"+good2.getUnit_name());
        tv_tejia2_fake_price.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        ll_tejia2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), OfferInfoActivity.class);
                i.putExtra("Goods_id", good2.getGoods_id());
                getActivity().startActivity(i);
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tv_classify:
                if (mSlideViewLEFT != null && !mSlideViewLEFT.isShow()) {
                    mSlideViewLEFT.show();
                } else {
                    mSlideViewLEFT.dismiss();
                }
                break;

            //店铺按钮
            case R.id.bt_home_choose_factory:

                //点击 店铺
                if (a == 0) {
                    rl_home_choose.setVisibility(View.VISIBLE);
                    rl_home_qh.setOnClickListener(null);
                    a = 1;
                    //未点击店铺
                } else {
                    rl_home_choose.setVisibility(View.GONE);
                    rl_home_qh.setOnClickListener(this);
                    a = 0;
                }

                break;

            //商品按钮
            case R.id.bt_home_choose_goods:

                //点击商品
                if (s == 0) {
                    rl_home_choose.setVisibility(View.VISIBLE);
                    rl_home_qh.setOnClickListener(null);
                    s = 1;
                    //未点击商品
                } else {
                    rl_home_choose.setVisibility(View.GONE);
                    rl_home_qh.setOnClickListener(this);
                    s = 0;
                }
                break;

            //下拉商品
            case R.id.tv_pop_goods:
                rl_home_choose.setVisibility(View.GONE);
                bt_home_choose_goods.setVisibility(View.VISIBLE);
                bt_home_choose_factory.setVisibility(View.GONE);
                s = 0;

                break;

            //下拉店铺
            case R.id.tv_pop_factory:
                rl_home_choose.setVisibility(View.GONE);
                bt_home_choose_goods.setVisibility(View.GONE);
                bt_home_choose_factory.setVisibility(View.VISIBLE);
                a = 0;

                break;

            //提交搜索结果
            case R.id.iv_search:

                String relut = et_home_choose.getText().toString();
                if (relut == null || relut.equals("")) return;
                if (bt_home_choose_factory.getVisibility() == View.VISIBLE) {
                    //搜索店铺
                    Intent a = new Intent(getActivity(), FactoryListActivity.class);
                    a.putExtra("relut", relut);
                    getActivity().startActivity(a);
                } else {
                    //搜索商品
                    Intent i = new Intent(getActivity(), InquiryListActivity.class);
                    i.putExtra("relut", relut);
                    getActivity().startActivityForResult(i, Constants.REQUESTCODE);

                }

                break;

            case R.id.rl_home_qh: //期货

                Intent qhInt = new Intent(getActivity(), InquiryListActivity.class);
                qhInt.putExtra("qhInt", "qhInt");
                getActivity().startActivityForResult(qhInt, Constants.REQUESTCODE);
                break;

            case R.id.rl_home_xh://现货

                Intent xhInt = new Intent(getActivity(), InquiryListActivity.class);
                xhInt.putExtra("xhInt", "xhInt");
                getActivity().startActivityForResult(xhInt, Constants.REQUESTCODE);

                break;
            case R.id.rl_home_zxh://现货

                Intent zxhInt = new Intent(getActivity(), InquiryListActivity.class);
                zxhInt.putExtra("zxhInt", "zxhInt");
                getActivity().startActivityForResult(zxhInt, Constants.REQUESTCODE);

                break;
            case R.id.rl_home_gs: {
                Intent intent = new Intent(getActivity(), InquiryListActivity.class);
                intent.putExtra("zgInt", "zgInt");
                getActivity().startActivityForResult(intent, Constants.REQUESTCODE);
            }
            break;

            case R.id.rl_home_ds: {
                Intent intent = new Intent(getActivity(), InquiryListActivity.class);
                intent.putExtra("retailInt", "retailInt");
                getActivity().startActivityForResult(intent, Constants.REQUESTCODE);
            }
            break;


            default:
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        vp_homepager.setCurrentItem(vp_homepager.getCurrentItem() + 1);// 设置下一页
        // 延时发送消息
        mainHandler.sendEmptyMessageDelayed(0, 4000);
    }

    @Override
    public void onStop() {
        super.onStop();
        // 清除所有消息和callback
        mainHandler.removeCallbacksAndMessages(null);
    }


}
