package taiyang.com.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import taiyang.com.activity.InfoActivity;
import taiyang.com.activity.PublishActivity;
import taiyang.com.activity.StorageServicectivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.ToastUtil;


/**
 * 服务fragment
 * Created by hoo on 2016/7/5.
 */
public class ServiceFragment extends Fragment {

    @InjectView(R.id.agency_layout)
    View agencyLayout;


    @OnClick(R.id.agency_layout)
    public void agencyLayout(View v) {
        Intent intent = new Intent(getActivity(), StorageServicectivity.class);
        intent.putExtra("title", getString(R.string.dailibaoguan));
        startActivity(intent);
    }

    @InjectView(R.id.logistics_layout)
    LinearLayout logisticsLayout;

    @OnClick(R.id.logistics_layout)
    public void logisticsLayout(View v) {
        Intent intent = new Intent(getActivity(), InfoActivity.class);
        startActivity(intent);

    }

    @InjectView(R.id.storage_layout)
    View storageLayout;

    @OnClick(R.id.storage_layout)
    public void storageLayout(View v) {
        Intent intent = new Intent(getActivity(), StorageServicectivity.class);
        intent.putExtra("title", getString(R.string.cangcufuwu));
        startActivity(intent);
    }

    @InjectView(R.id.financing_layout)
    View financingLayout;

    @OnClick(R.id.financing_layout)
    public void financingLayout(View v) {
        Intent intent = new Intent(getActivity(), StorageServicectivity.class);
        intent.putExtra("title",getString(R.string.daiyahuokuan));
        startActivity(intent);
    }

    //询盘服务
    @InjectView(R.id.buy_layout)
    LinearLayout buyLayout;

    @OnClick(R.id.buy_layout)
    public void buyLayout(View v) {
//        Intent intent=new Intent(getActivity(), AllBuyActivity.class);
        Intent intent = new Intent(getActivity(), PublishActivity.class);
        startActivity(intent);
    }

    //市场价格
    @InjectView(R.id.price_layout)
    View priceLayout;

    @OnClick(R.id.price_layout)
    public void priceLayout(View v) {
        ToastUtil.showToast("敬请期待");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void initView() {

    }

    private void initData() {

    }

}
