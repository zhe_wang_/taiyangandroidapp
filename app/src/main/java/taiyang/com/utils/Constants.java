package taiyang.com.utils;

/**
 * Created by admin on 2016/8/23.
 */
public class Constants {
    public static final int REQUESTCODE = 0;//请求码
    public static final int LOGOOUT = 200;//登出
    public static final int CHANGEPHONE = 201;//修改手机号
    public static final int BACKSELECT = 202;//跳转到报盘
    public static final String STROAGEACTIVITYACTION="com.tydp.activity.StorageServicectivity";//仓储服务的action
    public static final String PUBLISHACTIVITYACTION="com.tydp.activity.PublishActivity";//询盘服务的action
    public static final String OFFERINFOACTION="com.tydp.activity.OfferInfoActivity";//报盘信息的action
}
