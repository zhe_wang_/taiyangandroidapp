package taiyang.com.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 *  存取 SP 中值
 * @author Administrator
 *
 */
public class SPUtils {
	private static final String SP_NAME = "sp_name";
	private static SharedPreferences sp;

	/**
	 * 取布尔值
	 * 
	 * @param context
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static boolean getBoolean(Context context, String key,
			boolean defValue) {
		if (sp == null) {
			sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
		}

		return sp.getBoolean(key, defValue);
	}

	/**
	 * 存布尔值
	 * 
	 * @param context
	 * @param key
	 * @param value
	 */
	public static void putBoolean(Context context, String key, boolean value) {
		if (sp == null) {
			sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
		}
		sp.edit().putBoolean(key, value).commit();

	}
	
	
	/**
	 * 存取字符串
	 * @param context
	 * @param key
	 * @param value
	 */
	public static void putString(Context context, String key, String value){
		if (sp == null) {
			sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
		}
		sp.edit().putString(key, value).commit();
	}
	
	public static String getString(Context context, String key, String defValue){
		if (sp == null) {
			sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
		}
		return sp.getString(key, defValue);
	}
	
	/**
	 * 清除所有 SharedPreferences
	 */
	public static void  clearSP(){
		sp.edit().clear().commit();
		//清楚指定键
		//editor.remove("password_field");
	}
	/**
	 * 清除所有 SharedPreferences
	 */
	public static void  clearSP_one(String key){
		sp.edit().remove(key).commit();
		//清楚指定键
		//editor.remove("password_field");
	}
}
