package taiyang.com.utils;

import android.widget.Toast;



public class ToastUtil {
	private static Toast toast;
	/**
	 * 能够连续弹吐司，不用等上个消失
	 * @param text
	 */
	public static void showToast(String text){
		if(toast==null){
			toast = Toast.makeText(MyApplication.getContext(), text, Toast.LENGTH_LONG);
		}
		toast.setText(text);
		toast.show();
	}
}
