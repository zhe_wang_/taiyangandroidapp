package taiyang.com.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.PushAgent;
import com.umeng.socialize.PlatformConfig;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import cn.magicwindow.Session;
import okhttp3.OkHttpClient;
import taiyang.com.entity.UserModel;

/**
 * Created by heng on 2016/7/15.
 */
public class MyApplication extends Application {

    private static Context context;//全局Context变量
    private static MyApplication myApplication;
    private UserModel currentUser;

    private boolean addressChange = true;//取货人信息是否修改
    private boolean personChange = true;//个人信息是否修改

    @Override
    public void onCreate() {
        super.onCreate();
        final PushAgent mPushAgent = PushAgent.getInstance(this);
//注册推送服务，每次调用register方法都会回调该接口
        mPushAgent.register(new IUmengRegisterCallback() {

            @Override
            public void onSuccess(String deviceToken) {
                MySPEdit.getInstance(context).setDeviceToken(deviceToken);
            }

            @Override
            public void onFailure(String s, String s1) {

            }
        });
        //初始化各个平台
        initShare();
        context = this;
        myApplication = this;
        //magin window
        Session.setAutoSession(this);
        //图片加载
        Fresco.initialize(this);
        //网络请求
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
//              .addInterceptor(new LoggerInterceptor("TAG"))
                .connectTimeout(10000L, TimeUnit.MILLISECONDS)
                .readTimeout(10000L, TimeUnit.MILLISECONDS)
                //其他配置
                .build();
        OkHttpUtils.initClient(okHttpClient);
        if("en".equals(MySPEdit.getInstance(context).getUserLang())){
            Locale.setDefault(Locale.ENGLISH);
            Configuration config = getBaseContext().getResources().getConfiguration();
            config.locale = Locale.ENGLISH;
            getBaseContext().getResources().updateConfiguration(config
                    , getBaseContext().getResources().getDisplayMetrics());
        }else{
            Locale.setDefault(Locale.CHINESE);
            Configuration config = getBaseContext().getResources().getConfiguration();
            config.locale = Locale.CHINESE;
            getBaseContext().getResources().updateConfiguration(config
                    , getBaseContext().getResources().getDisplayMetrics());
        }
    }

    private void initShare() {
        //微信 wx12342956d1cab4f9,a5ae111de7d9ea137e88a5e02c07c94d
        PlatformConfig.setWeixin("wxbcce9bddfe71700f", "d613b222d75ea05042498cad770dd5f1");

    }

    public static MyApplication getInstance() {
        return myApplication;
    }

    public UserModel getCurrentUser() {
        return currentUser;
    }

    public static Context getContext() {
        return context;
    }

    public void setCurrentUser(UserModel currentUser) {
        this.currentUser = currentUser;
    }

    private List<Activity> activityList = new ArrayList<>();

    //添加Activity 到容器中
    public void addActivity(Activity activity) {
        activityList.add(activity);
    }

    //移除容器中的Activity
    public void removeActivity(Activity activity) {
        activityList.remove(activity);
    }

    public void finishAll() {
        for (Activity activity : activityList) {
            if (!activity.isFinishing()) {
                activity.finish();
            }
        }
    }

    /**
     * 判断当前版本是否兼容目标版本的方法
     *
     * @param VersionCode
     * @return
     */
    public static boolean isMethodsCompat(int VersionCode) {
        int currentVersion = android.os.Build.VERSION.SDK_INT;
        return currentVersion >= VersionCode;

    }

    public boolean isAddressChange() {
        return addressChange;
    }

    public void setAddressChange(boolean addressChange) {
        this.addressChange = addressChange;
    }

    public boolean isPersonChange() {
        return personChange;
    }

    public void setPersonChange(boolean personChange) {
        this.personChange = personChange;
    }
}
