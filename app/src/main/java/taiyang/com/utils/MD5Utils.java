package taiyang.com.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 
 * @ClassName: MD5Utils 
 * @author: guzd
 * @Description:MD5加密(32位)
 * @date: 2014年10月9日 上午9:55:50
 */
public class MD5Utils {

	/*
	 * MD5加密(32位)
	 */
	public static String encode(String context) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(context.getBytes());
			byte[] encryContext = md.digest();

			int i;
			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < encryContext.length; offset++) {
				i = encryContext[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			return buf.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return context;
		}
	}

	public static String getSign(String model,String action){
		StringBuilder params=new StringBuilder();
		params.append(model);
		params.append(action);
		params.append("76sRx2VW@i4uJleBd4X9MoFfgWdGqbe1");
		return params.toString();
	}
	public static String getMobileSign(String phone){
		StringBuilder params=new StringBuilder();
		params.append(phone);
		params.append("76sRx2VW@i4uJleBd4X9MoFfgWdGqbe1");
		return params.toString();
	}
//	mobile_sign

	public static void main(String[] args) {
		System.out.println(MD5Utils.encode("123"));;
	}
}
