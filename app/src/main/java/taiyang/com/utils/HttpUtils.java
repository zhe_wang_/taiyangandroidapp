package taiyang.com.utils;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import okhttp3.Call;

/**
 * Created by heng on 2016/7/28.
 */
public class HttpUtils {
//    private static String url = "http://test.taiyanggo.com/app/api.php";
    private static String url = "http://www.taiyanggo.com/app/api.php";

    private static Map<String, Object> addLang(Map<String, Object> map){
        map.put("lang",Locale.getDefault().getLanguage());
        return map;

    }

    public static void sendPost(Map<String, Object> map, final HttpRequestListener listener) {
        OkHttpUtils.post()
                .url(url)
                .addParams("post_data", JSON.toJSONString(addLang(map)))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        listener.fail(e.getMessage(), id);
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        try {
                            L.e("110000");
                            Log.e("ceshi", response);
                            JSONObject jsonObject = new JSONObject(response);
                            Log.e("ceshi", jsonObject.optString("error"));
                            if ("0".equals(jsonObject.optString("error"))) {
                                L.e("回掉结果0: ");
                                listener.success(jsonObject.optString("content"), id);
                            } else {
                                L.e("回掉结果非0: ");
                                listener.failByOther(jsonObject.optString("message"), id);
                            }
                        } catch (JSONException e) {
                            L.e("出异常了");
                            e.printStackTrace();
                        }
                    }
                });
    }

    public static void sendPost(Map<String, Object> map, final HttpRequestListener listener, int id) {
        OkHttpUtils.post()
                .url(url)
                .id(id)
                .addParams("post_data", JSON.toJSONString(addLang(map)))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        listener.fail(e.getMessage(), id);
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        try {
                            L.e(response);
                            L.e("id: " + id);
                            JSONObject jsonObject = new JSONObject(response);
                            Log.e("ceshi", jsonObject.optString("error"));
                            if ("0".equals(jsonObject.optString("error"))) {
                                L.e("ceshi", "回掉结果0: ");
                                L.e("ceshi", "id: " + id);
                                listener.success(jsonObject.optString("content"), id);
                            } else {
//                                Log.e("ceshi", "回掉结果非0: " );
                                L.e("ceshi", "回掉结果非0: ");
                                listener.failByOther(jsonObject.optString("message"), id);
                            }
                        } catch (JSONException e) {
                            Log.e("ceshi", "出异常了");
                            e.printStackTrace();
                        }
                    }
                });
    }

    //上传图片使用
    public static void sendPost(Map<String, Object> map, String name, String fileName, final HttpRequestListener listener, boolean flag, int id) {
        if (flag) {
            OkHttpUtils.post()
                    .addFile(name, fileName, new File(fileName))
                    .id(id)
                    .url(url)
                    .addParams("post_data", JSON.toJSONString(addLang(map)))
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.fail(e.getMessage(), id);
                        }

                        @Override
                        public void onResponse(String response, int id) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if ("0".equals(jsonObject.optString("error"))) {
                                    L.e("回掉结果0: ");
                                    listener.success(jsonObject.optString("content"), id);
                                } else {
                                    L.e("回掉结果非0: ");
                                    listener.failByOther(jsonObject.optString("message"), id);
                                }
                            } catch (JSONException e) {
                                L.e("出异常了");
                                e.printStackTrace();
                            }
                        }
                    });
        } else {
            sendPost(map, listener, id);
        }

//        OkHttpUtils.post()
//                .url(url)
//                .addParams("post_data", JSON.toJSONString(map))
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        listener.fail(e.getMessage(),id);
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        try {
//                            L.e("110000");
//                            Log.e("ceshi",response);
//                            JSONObject jsonObject=new JSONObject(response);
//                            Log.e("ceshi", jsonObject.optString("error"));
//                            if ("0".equals(jsonObject.optString("error"))){
//                                L.e( "回掉结果0: " );
//                                listener.success(jsonObject.optString("content"),id);
//                            }else {
//                                L.e("回掉结果非0: " );
//                                listener.failByOther(jsonObject.optString("message"),id);
//                            }
//                        } catch (JSONException e) {
//                            L.e( "出异常了");
//                            e.printStackTrace();
//                        }
//                    }
//                });
    }

    //获取所有省份使用
    public static void sendPost(final HttpRequestListener listener, int id, boolean flag) {
        if (flag) {
            Map<String, Object> map = new HashMap<>();
            map.put("model", "other");
            map.put("action", "get_city");
            map.put("sign", MD5Utils.encode(MD5Utils.getSign("other", "get_city")));
            OkHttpUtils.post()
                    .url(url)
                    .id(id)
                    .addParams("post_data", JSON.toJSONString(addLang(map)))
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            L.e("失败" + id);
                            listener.fail(e.getMessage(), id);
                        }

                        @Override
                        public void onResponse(String response, int id) {
                            L.e("成功" + id);
//                            L.e(response);
                            listener.success(response, id);
                        }
                    });
        }

    }
}
