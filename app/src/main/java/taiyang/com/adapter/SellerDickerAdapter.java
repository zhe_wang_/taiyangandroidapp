package taiyang.com.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.activity.OfferInfoActivity;
import taiyang.com.activity.SellerDickerActivity;
import taiyang.com.entity.SellerDickerListModel;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MyApplication;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.ToastUtil;

/**
 * Created by admin on 2016/7/18.
 * 我的还价适配器
 */
public class SellerDickerAdapter extends ListBaseAdapter<SellerDickerListModel.ListBean> {
    private Context mCtx;

    public SellerDickerAdapter(Context mCtx) {
        this.mCtx = mCtx;

    }

    @Override
    public SellerDickerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.seller_dicker_item, parent, false);
        SellerDickerAdapter.ViewHolder vh = new SellerDickerAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        SellerDickerAdapter.ViewHolder viewHolder= (SellerDickerAdapter.ViewHolder) holder;
        final String Goods_id = mDatas.get(position).getGoods_id();

                viewHolder.rl_home_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i =new Intent(MyApplication.getContext(), OfferInfoActivity.class);
                        i.putExtra("Goods_id",Goods_id);
                        mCtx.startActivity(i);
                    }
                });

        viewHolder.imageView.setImageURI(Uri.parse(mDatas.get(position).getGoods_thumb()));
        viewHolder.iv_homelist_guojia.setText(mDatas.get(position).getRegion_name());
        viewHolder.textView_chanpin.setText(mDatas.get(position).getGoods_name());//名称
        viewHolder.tv_chanpin_number.setText(mDatas.get(position).getBrand_sn());//厂号
        viewHolder.tv_ilaitem_price.setText(mDatas.get(position).getShop_price());//价格
        //还价
        if (mDatas.get(position).getOffer().equals("11")) {
            viewHolder.huan.setVisibility(View.VISIBLE);
        } else {
            viewHolder.huan.setVisibility(View.GONE);
        }

        viewHolder.pin.setVisibility(View.GONE);
        //商品类型  6=期货7=现货
        if (mDatas.get(position).getGoods_type().equals("6")) {
            viewHolder.xianqi.setText(mCtx.getString(R.string.qi));
            viewHolder.xianqi.setBackgroundColor(mCtx.getResources().getColor(R.color.qi));
        } else {
            viewHolder.xianqi.setText(mCtx.getString(R.string.xian));
            viewHolder.xianqi.setBackgroundColor(mCtx.getResources().getColor(R.color.xian));

        }
        //售卖类型 4零5整
        if (mDatas.get(position).getSell_type().equals("4")) {
            viewHolder.zhengling.setText(mCtx.getString(R.string.ling));
            viewHolder.zhengling.setBackgroundColor(mCtx.getResources().getColor(R.color.ling));
        } else {
            viewHolder.zhengling.setText(mCtx.getString(R.string.zheng));
            viewHolder.zhengling.setBackgroundColor(mCtx.getResources().getColor(R.color.zheng));
        }

        viewHolder.ll_chujia_list.removeAllViews();
        for (final SellerDickerListModel.ListBean.HuckListBean huckListBean : mDatas.get(position).getHuck_list()) {
            View chujia = LayoutInflater.from(mCtx).inflate(R.layout.item_chujia_list_item, null, false);
            ((TextView)chujia.findViewById(R.id.tv_ilaitem_price)).setText(huckListBean.getPrice());
            ((TextView)chujia.findViewById(R.id.tv_time)).setText(huckListBean.getCreated_at());
            ((TextView)chujia.findViewById(R.id.tv_msg)).setText(mCtx.getString(R.string.liuyan)+":"+huckListBean.getMessage());
            if(huckListBean.getPrice().equals(mDatas.get(position).getShop_price())){
                ((TextView)chujia.findViewById(R.id.tv_caiyong)).setText(R.string.yicaiyong);
            }else{
            ((TextView)chujia.findViewById(R.id.tv_caiyong)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    huanjiaAction("-1",huckListBean.getId(),mDatas.get(position).getGoods_id());
                }
            });}
            viewHolder.ll_chujia_list.addView(chujia);


        }

        viewHolder.tv_zidingjia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText editText = new EditText(mCtx);
                AlertDialog.Builder inputDialog =
                        new AlertDialog.Builder(mCtx);
                inputDialog.setTitle(R.string.qingshuruzidingjia).setView(editText);
                inputDialog.setPositiveButton(mCtx.getString(R.string.queding),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                huanjiaAction(editText.getText().toString(),"-1",mDatas.get(position).getGoods_id());
                            }
                        }).show();
            }
        });
    }

    private void huanjiaAction(String zidingjia,String caiyong,String goodsid){
        Map<String, Object> params = new HashMap<>();
        if(!zidingjia.equals("-1")){
            params.put("price",zidingjia);
        }if(!caiyong.equals("-1")){
            params.put("huck_id",caiyong);
        }
        params.put("goods_id",goodsid);
        params.put("model", "seller");
        params.put("action", "saveHuckster");
        params.put("user_id", MySPEdit.getInstance(mCtx).getUserId());
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller", "saveHuckster")));
        HttpUtils.sendPost(params, new HttpRequestListener() {
            @Override
            public void success(String response, int id) {
                ToastUtil.showToast(mCtx.getString(R.string.caozuochenggong));
                ((SellerDickerActivity)mCtx).finish();
                Intent intent = new Intent(mCtx, SellerDickerActivity.class);
                mCtx.startActivity(intent);

            }
            @Override
            public void failByOther(String fail, int id) {
                ToastUtil.showToast(fail);
            }

            @Override
            public void fail(String fail, int id) {

            }
        });

    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @InjectView(R.id.imageView)
        SimpleDraweeView imageView;

        @InjectView(R.id.iv_homelist_guojia)
        TextView iv_homelist_guojia;
        @InjectView(R.id.textView_chanpin)
        TextView textView_chanpin;
        @InjectView(R.id.tv_chanpin_number)
        TextView tv_chanpin_number;
        @InjectView(R.id.tv_ilaitem_price)
        TextView tv_ilaitem_price;
        @InjectView(R.id.tv_ilaitem_unit)
        TextView tv_ilaitem_unit;
        @InjectView(R.id.tv_ilaitem_price2)
        TextView tv_ilaitem_price2;
        @InjectView(R.id.tv_ilaitem_unit2)
        TextView tv_ilaitem_unit2;
        @InjectView(R.id.tv_xian_qi)
        TextView xianqi;
        @InjectView(R.id.tv_zheng_ling)
        TextView zhengling;
        @InjectView(R.id.tv_pin)
        TextView pin;
        @InjectView(R.id.tv_huan)
        TextView huan;
        @InjectView(R.id.rl_home_isover)
        ImageView rl_home_isover;
        @InjectView(R.id.rl_home_item)
        RelativeLayout rl_home_item;
        @InjectView(R.id.tv_zidingjia)
        TextView tv_zidingjia;
        @InjectView(R.id.ll_chujia_list)
        LinearLayout ll_chujia_list;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.inject(this,itemView);

        }
    }

}
