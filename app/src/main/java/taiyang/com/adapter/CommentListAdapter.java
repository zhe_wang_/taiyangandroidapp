package taiyang.com.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import taiyang.com.entity.CommentListBean;
import taiyang.com.tydp_b.R;

/**
 * Created by zhew on 9/9/17.
 * Copyright © 2006-2016 365jia.cn 安徽大尺度网络传媒有限公司版权所有
 **/

public class CommentListAdapter extends BaseAdapter {

    public List<CommentListBean.CommentBean> getmList() {
        return mList;
    }

    public void setmList(List<CommentListBean.CommentBean> mList) {
        this.mList = mList;
    }

    private List<CommentListBean.CommentBean> mList;
    private Context mCtx;

    public CommentListAdapter(List<CommentListBean.CommentBean> mList, Context mCtx) {
        this.mList = mList;
        this.mCtx = mCtx;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return Long.parseLong(getmList().get(i).getId());
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = LayoutInflater.from(mCtx).inflate(R.layout.recyclerview_comment_list_item, null, false);
        CommentListBean.CommentBean commentBean = (CommentListBean.CommentBean) getItem(i);
        ((SimpleDraweeView) v.findViewById(R.id.riv_qiugou1)).setImageURI(Uri.parse(commentBean.getUser_face()));
        ((TextView) v.findViewById(R.id.tv_qiugou1_name)).setText(
                commentBean.getUser_name()!=null?commentBean.getUser_name().replaceAll("(\\d{3})\\d{4}(\\d{4})","$1****$2"):commentBean.getUser_name());
        ((TextView) v.findViewById(R.id.tv_qiugou1_localtion)).setText(commentBean .getAdd_time());
        ((TextView) v.findViewById(R.id.tv_qiugou1_text)).setText(commentBean.getContent());
        ((TextView) v.findViewById(R.id.tv_qiugou1_text)).setVisibility(commentBean.getContent().isEmpty()?View.GONE:View.VISIBLE);

        return v;
    }
}
