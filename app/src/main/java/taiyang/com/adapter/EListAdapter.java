package taiyang.com.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import taiyang.com.entity.ZhuFragmentCDContentBean;
import taiyang.com.tydp_b.R;
import taiyang.com.view.MyGridView;

public class EListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<ZhuFragmentCDContentBean.CatDataBean> groups;
    private MyGridView gridview;
    private LayoutInflater mInflater;
    private List<List<Boolean>> mCheckedList;

    public EListAdapter(Context context, List<ZhuFragmentCDContentBean.CatDataBean> groups, List<List<Boolean>> checkedList) {
        this.context = context;
        this.groups = groups;
        this.mCheckedList = checkedList;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public Object getChild(int groupPosition, int childPosition) {
//        return groups.get(groupPosition).getCat_list().get(childPosition);
        return groups.get(groupPosition).getCat_list();
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    public int getChildrenCount(int groupPosition) {
//        return groups.get(groupPosition).getCat_list().size();
        return 1;
    }

    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    public int getGroupCount() {
        return groups.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    /**
     * 設定 Group 資料
     */
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
//        ZhuFragmentCDContentBean.CatDataBean group = (ZhuFragmentCDContentBean.CatDataBean) getGroup(groupPosition);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_produts_one, null);
        }

        Holder holder = Holder.getHolder(convertView);
        holder.tv_produts_name.setText(((ZhuFragmentCDContentBean.CatDataBean) getGroup(groupPosition)).getCat_name());

        if (isExpanded) {
            // 大组展开
            holder.iv_produts_jt.setBackgroundResource(R.mipmap.leftclassify_icon_up);
            holder.tv_produts_name.setTextColor(Color.rgb(230, 64, 64));

        } else {
            // 大组关闭
            holder.iv_produts_jt.setBackgroundResource(R.mipmap.leftclassify_icon_down);
            holder.tv_produts_name.setTextColor(Color.rgb(58, 58, 58));
        }
        return convertView;
    }

    /**
     * 二级
     * 設定 Children 資料
     */
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
//        L.e("1"+ groups.size());
//        L.e("2"+ groups.get(groupPosition).getCat_list().size());
        if (convertView == null) { convertView = mInflater.inflate(R.layout.item_produts_two, null); }
        if(groups.get(groupPosition).getCat_list().size() !=0){

            ZhuFragmentCDContentBean.CatDataBean.CatListBean catBean = groups.get(groupPosition).getCat_list().get(childPosition);
            gridview = (MyGridView) convertView.findViewById(R.id.gridview);
            GridTextAdapter mAdapter = new GridTextAdapter(groupPosition, groups.get(groupPosition).getCat_list());
            gridview.setAdapter(mAdapter);
        }

        return convertView;
    }

    static class Holder {
        TextView tv_produts_name;
        ImageView iv_produts_jt;

        public Holder(View convertView) {
            iv_produts_jt = (ImageView) convertView.findViewById(R.id.iv_produts_jt);
            tv_produts_name = (TextView) convertView.findViewById(R.id.tv_produts_name);
        }

        public static Holder getHolder(View convertView) {
            Holder holder = (Holder) convertView.getTag();
            if (holder == null) {
                holder = new Holder(convertView);
                convertView.setTag(holder);
            }
            return holder;
        }
    }

    //二级点击事件
    class GridTextAdapter extends BaseAdapter {
        private List<ZhuFragmentCDContentBean.CatDataBean.CatListBean> childs;
        private int groupPosition;

        public GridTextAdapter(int groupPosition, List<ZhuFragmentCDContentBean.CatDataBean.CatListBean> childs) {
            this.groupPosition = groupPosition;
            this.childs = childs;
        }

        @Override
        public int getCount() {
            return childs.size();
        }

        @Override
        public Object getItem(int position) {
            return childs.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflater.inflate(
                        R.layout.item_products_gridchild, null);
            }

            final ViewHodler holder = ViewHodler.getHolder(convertView);
            ZhuFragmentCDContentBean.CatDataBean.CatListBean catBean = groups.get(groupPosition).getCat_list().get(position);

            holder.mTvType.setText(catBean.getGoods_name());
            boolean checked = mCheckedList.get(groupPosition).get(position);
            holder.mTvType.setChecked(checked);

            holder.mTvType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean oriChecked = mCheckedList.get(groupPosition).get(position);
                    holder.mTvType.setChecked(!oriChecked);
                    mCheckedList.get(groupPosition).set(position, !oriChecked);
                    notifyDataSetChanged();
                }
            });

            return convertView;

        }

    }

    static class ViewHodler {
        private CheckBox mTvType;

        ViewHodler(View convertView) {
            mTvType = (CheckBox) convertView
                    .findViewById(R.id.cb_item_goods_type);
        }

        public static ViewHodler getHolder(View convertView) {
            ViewHodler holder = (ViewHodler) convertView.getTag();
            if (holder == null) {
                holder = new ViewHodler(convertView);
                convertView.setTag(holder);
            }

            return holder;
        }

    }

}
