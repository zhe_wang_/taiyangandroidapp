package taiyang.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.activity.OrderDetailActivity;
import taiyang.com.entity.GoodsModel;
import taiyang.com.entity.ListBean;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.L;

/**
 * Created by admin on 2016/7/18.
 * 订单适配器
 */
public class OrderAdapter extends ListBaseAdapter<ListBean> {
    private GoodsModel mGoodsModel;;
    private String statues;
    private Context mContext;
    private String pay_type_id;

    public OrderAdapter(Context mContext){
        this.mContext=mContext;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {


        final ViewHolder viewHolder= (ViewHolder) holder;
        if(mOnItemClickLitener!=null){
            viewHolder.pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos=viewHolder.getLayoutPosition();
                    mOnItemClickLitener.onItemClick(viewHolder.pay, pos);
                }
            });
        }
        String time=mDatas.get(position).getAdd_time();
        mGoodsModel=mDatas.get(position).getGoods();
        viewHolder.im_orderlogo.setImageURI(Uri.parse(mGoodsModel.getGoods_thumb()));
        viewHolder.order.setText(mDatas.get(position).getOrder_sn());
        viewHolder.ordername.setText(mGoodsModel.getGoods_name()+"   "+mGoodsModel.getBrand_sn());
        viewHolder.orderprice.setText(mGoodsModel.getFormated_goods_price()+"/吨");
        viewHolder.time.setText(time.substring(0,time.length()-3));


        viewHolder.tv_info.setText("   "+
                (mDatas.get(position).getGoods().getIs_retail().equals("1")?mDatas.get(position).getGoods().getPart_number():mDatas.get(position).getGoods().getPart_weight())+
                ""+mDatas.get(position).getGoods().getPart_unit()+"    "+
                mDatas.get(position).getGoods().getGoods_number()+mDatas.get(position).getGoods().getMeasure_unit());




        viewHolder.orderstate.setText(mDatas.get(position).getOrder_status_format());
        if (mOnItemClickLitener!=null){
            viewHolder.ll_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContext, OrderDetailActivity.class);
                    intent.putExtra("order_id",mDatas.get(position).getOrder_id());
                    L.e("详情 -平台");
                    mContext.startActivity(intent);
                }
            });
        }
    }



    class ViewHolder extends RecyclerView.ViewHolder{
        @InjectView(R.id.tv_order)
        TextView order;
        @InjectView(R.id.tv_orderstate)
        TextView orderstate;
        @InjectView(R.id.tv_ordername)
        TextView ordername;
        @InjectView(R.id.tv_orderprice)
        TextView orderprice;
        @InjectView(R.id.tv_pay)
        TextView pay;
        @InjectView(R.id.tv_time)
        TextView time;
        @InjectView(R.id.tv_info)
        TextView tv_info;

        @InjectView(R.id.ll_order)
        View ll_order;

        @InjectView(R.id.im_orderlogo)
        SimpleDraweeView im_orderlogo;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
//        void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
    public void setStatues(String statues){
        this.statues=statues;
    }
}
