package taiyang.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.activity.PublishActivity;
import taiyang.com.entity.InquiryModel;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.L;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.T;

/**
 * Created by admin on 2016/7/18.
 * 求购适配器
 */
public class BuyAdapter extends ListBaseAdapter<InquiryModel> implements HttpRequestListener {
    private Context mContext;
    private boolean flag = true;
    private MySPEdit mySPEdit;

    public BuyAdapter(Context mContext) {
        this.mContext = mContext;
        mySPEdit = MySPEdit.getInstance(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.buy_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder viewHolder= (ViewHolder) holder;
        final InquiryModel inquiryModel = mDatas.get(position);
        L.e("所有的id-------"+inquiryModel.getId());
        viewHolder.tv_type.setText("【" + inquiryModel.getType() + "】");
        viewHolder.tv_title.setText(inquiryModel.getTitle());
        viewHolder.tv_location.setText(inquiryModel.getAddress());
        viewHolder.tv_price.setText(inquiryModel.getPrice_low() + "元-" + inquiryModel.getPrice_up() + "元");
        viewHolder.tv_time.setText(inquiryModel.getCreated());
        viewHolder.tv_show.setText(inquiryModel.getMemo());
        viewHolder.tv_hide.setText(inquiryModel.getMemo());
        viewHolder.tv_title.setText(inquiryModel.getTitle());
        if (inquiryModel.getMemo().length() < 28) {
            viewHolder.iv_more.setVisibility(View.GONE);
        }
        if (mOnItemClickLitener != null) {
            viewHolder.iv_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    int pos = holder.getLayoutPosition();
//                    mOnItemClickLitener.onItemClick(holder.more, pos);
                    if (flag) {
                        viewHolder.tv_show.setVisibility(View.GONE);
                        viewHolder.tv_hide.setVisibility(View.VISIBLE);
                        viewHolder.iv_more.setImageResource(R.mipmap.buy_icon_up);
                        flag = false;
                    } else {
                        viewHolder.tv_show.setVisibility(View.VISIBLE);
                        viewHolder.tv_hide.setVisibility(View.GONE);
                        viewHolder.iv_more.setImageResource(R.mipmap.buy_icon_down);
                        flag = true;
                    }
                }
            });
            viewHolder.tv_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = viewHolder.getLayoutPosition()-1;
//                    L.e("pos:"+pos+"個");
                    Intent intent = new Intent(mContext, PublishActivity.class);
                    Bundle data = new Bundle();
                    data.putSerializable("data", mDatas.get(pos));
                    intent.putExtras(data);
                    mContext.startActivity(intent);
                }
            });
            viewHolder.tv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = viewHolder.getLayoutPosition()-1;
//                    pos=position;
//                    mOnItemClickLitener.onItemClick(view,pos);
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put("model", "user");
                    params.put("action", "del_purchase");
                    params.put("sign", MD5Utils.encode(MD5Utils.getSign("user", "del_purchase")));
                    params.put("id", mDatas.get(pos).getId());
                    params.put("token",mySPEdit.getToken() );
                    L.e("第:"+pos+"個");
                    L.e("要删除的id:"+mDatas.get(pos).getId());
                    params.put("user_id", mySPEdit.getUserId());
                    HttpUtils.sendPost(params, BuyAdapter.this);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }
    private int pos;
    private int removePos;
    @Override
    public void success(String response, int id) {
        removeData(pos);
        T.showShort(mContext,"删除成功");
    }

    @Override
    public void failByOther(String fail, int id) {

    }

    @Override
    public void fail(String fail, int id) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.tv_type)
        TextView tv_type;
        @InjectView(R.id.tv_title)
        TextView tv_title;
        @InjectView(R.id.tv_location)
        TextView tv_location;
        @InjectView(R.id.tv_price)
        TextView tv_price;
        @InjectView(R.id.tv_show)
        TextView tv_show;
        @InjectView(R.id.tv_hide)
        TextView tv_hide;
        @InjectView(R.id.tv_time)
        TextView tv_time;
        @InjectView(R.id.tv_edit)
        TextView tv_edit;
        @InjectView(R.id.tv_delete)
        TextView tv_delete;
        @InjectView(R.id.iv_more)
        ImageView iv_more;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }

    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);

//        void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
    //移除动画
    public void removeData(int position) {
        mDatas.remove(position);
        notifyItemRemoved(position);
    }
}
