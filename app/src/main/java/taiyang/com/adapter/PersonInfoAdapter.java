package taiyang.com.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.activity.AddPersonActivity;
import taiyang.com.activity.EditPersonActivity;
import taiyang.com.entity.GoosPerson;
import taiyang.com.tydp_b.R;

/**
 * PersonInfoActivity  Adapter
 * Created by heng on 2016/7/14.
 */
public class PersonInfoAdapter extends RecyclerView.Adapter<PersonInfoAdapter.ViewHolder> {
    private List<GoosPerson> mDatas;
//    private int count = 4;
    private boolean flag = true;


    private int type = 0;
    private Context mCtx;
    //用于记录每个RadioButton的状态，并保证只可选一个
    HashMap<String, Boolean> states = new HashMap<String, Boolean>();

    private boolean flagCheck;

    private Button confirm;

    private boolean intentFlag;

    public PersonInfoAdapter(List<GoosPerson> mDatas, Context mCtx, Button confirm,int type) {
        this.mDatas = mDatas;
        this.mCtx = mCtx;
        this.confirm = confirm;
        this.type = type;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_person, parent, false);

        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tv_personinfoitem_name.setText(mDatas.get(position).getName());
        holder.tv_personinfoitem_phone.setText(mDatas.get(position).getMobile());
        String idNumber=mDatas.get(position).getId_number();
        holder.tv_personinfoitem_number.setText(idNumber.substring(0,5)+"****"+idNumber.substring(idNumber.length()-6,idNumber.length()));
        if (flagCheck) {
            holder.rb_personinfo_ischeck.setVisibility(View.GONE);
        } else {
            holder.rb_personinfo_ischeck.setTag(position);
            if (mOnItemClickLitener != null) {
                //单选收货地址
                holder.rb_personinfo_ischeck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //重置，确保最多只有一项被选中
                        for (String key : states.keySet()) {
                            states.put(key, false);
                        }
                        states.put(String.valueOf(position), holder.rb_personinfo_ischeck.isChecked());
                        if (holder.rb_personinfo_ischeck.isChecked() == true) {
                            intentFlag = true;
                            confirm.setText("确定");
                        } else {
                            intentFlag = false;
                            confirm.setText(type==0?"添加取货人":"添加发货人");
                        }
                        PersonInfoAdapter.this.notifyDataSetChanged();
                    }
                });

                boolean res = false;
                if (states.get(String.valueOf(position)) == null || states.get(String.valueOf(position)) == false) {
                    res = false;
                    states.put(String.valueOf(position), false);
//
//                    L.e("更新选择状态1");
                } else {
                    res = true;
//                    L.e("更新选择状态2");
//
                }
                holder.rb_personinfo_ischeck.setChecked(res);

            }

        }
        //编辑收货地址
        holder.iv_personinfo_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                    L.e("点击了编辑资料");
                Intent i = new Intent(mCtx, EditPersonActivity.class);
                i.putExtra("goodsPerson", (Serializable) mDatas.get(position));
                mCtx.startActivity(i);
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (intentFlag) {
                    if(Activity.class.isInstance(mCtx))
                    {
                        // 转化为activity，然后finish就行了
                        Activity activity = (Activity)mCtx;
                        Intent a = new Intent(mCtx, AddPersonActivity.class);
                        a.putExtra("address_id",mDatas.get(position).getId());
                        a.putExtra("seller",type);
                        activity.setResult(10,a);
                        activity.finish();
                    }
                } else {
                    Intent i = new Intent(mCtx, AddPersonActivity.class);
                    i.putExtra("seller",type);
                    mCtx.startActivity(i);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.iv_personinfo_ischeck)
        CheckBox rb_personinfo_ischeck;
        @InjectView(R.id.iv_personinfo_edit)
        ImageView iv_personinfo_edit;
        @InjectView(R.id.tv_personinfoitem_name)
        TextView tv_personinfoitem_name;
        @InjectView(R.id.tv_personinfoitem_phone)
        TextView tv_personinfoitem_phone;
        @InjectView(R.id.tv_personinfoitem_number)
        TextView tv_personinfoitem_number;


        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }

    }

    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
//      void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    public void setFlag(boolean flag) {
        this.flagCheck = flag;
    }
}
