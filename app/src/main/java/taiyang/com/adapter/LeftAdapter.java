package taiyang.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import taiyang.com.activity.InquiryListActivity;
import taiyang.com.entity.HomeCityBean;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.MyApplication;
import taiyang.com.view.XCSlideView;

/**
 * Created by admin on 2016/7/20.
 * 左侧选择ListView适配器
 */
public class LeftAdapter  extends BaseExpandableListAdapter {

    //设置组视图的显示文字
    private String[] generalsTypes = new String[] { "猪", "牛", "羊", "水产", "鸡", "鸭", "鹅" };
    //子视图显示文字
//    private String[][] generals = new String[][] {
//            { "全部", "猪肩胛 (肩部)", "猪腿", "猪尾", "猪腹肌", "猪骨", "猪内脏" },
//            {  "全部", "牛肩胛 (肩部)", "牛腿", "牛尾", "牛腹肌", "牛骨", "牛内脏" },
//            {  "全部", "羊肩胛 (肩部)", "羊腿", "羊尾", "羊腹肌", "羊骨", "羊内脏" },
//            {  "全部", "水产", "羊腿", "羊尾", "羊腹肌", "羊骨", "羊内脏" },
//            {  "全部", "鸡肩胛 (肩部)", "鸡腿", "鸡尾", "鸡腹肌", "鸡骨", "鸡内脏" },
//            {  "全部", "鸭肩胛 (肩部)", "鸭腿", "鸭尾", "鸭腹肌", "鸭骨", "鸭内脏" },
//            {  "全部", "鹅肩胛 (肩部)", "鹅腿", "鹅尾", "鹅腹肌", "鹅骨", "鹅内脏" },
//
//    };

    //子视图图片
//        public int[][] generallogos = new int[][] {
//                { R.drawable.xiahoudun, R.drawable.zhenji,
//                        R.drawable.xuchu, R.drawable.guojia,
//                        R.drawable.simayi, R.drawable.yangxiu },
//                { R.drawable.machao, R.drawable.zhangfei,
//                        R.drawable.liubei, R.drawable.zhugeliang,
//                        R.drawable.huangyueying, R.drawable.zhaoyun },
//                { R.drawable.lvmeng, R.drawable.luxun, R.drawable.sunquan,
//                        R.drawable.zhouyu, R.drawable.sunshangxiang } };
    private List<HomeCityBean> contentBean;
    private Context mContext;
    private XCSlideView mSlideViewLEFT;

    public LeftAdapter(Context mContext, List<HomeCityBean> contentBean, XCSlideView mSlideViewLEFT) {
        this.mContext=mContext;
        this.contentBean = contentBean;
        this.mSlideViewLEFT = mSlideViewLEFT;
    }
    @Override
    public int getGroupCount() {
       // return generalsTypes.length;
        return contentBean.size();

    }

    @Override
    public int getChildrenCount(int groupPosition) {
        //return generals[groupPosition].length;
        return contentBean.get(groupPosition).getSon().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        //return generalsTypes[groupPosition];
        return contentBean.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        //return generals[groupPosition][childPosition];
        return contentBean.get(groupPosition).getSon().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    //一级
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,View convertView, ViewGroup parent) {

        if(convertView==null){
            LayoutInflater inflater = LayoutInflater.from(MyApplication.getContext());
            convertView = inflater.inflate(R.layout.item_leftview_one, null);
        }
        Holder holder = Holder.getHolder(convertView);
        holder.tv_leftview_one.setText(contentBean.get(groupPosition).getCat_name());

        if (isExpanded){
            // 大组展开
            holder.iv_leftview_one.setImageURI(Uri.parse(contentBean.get(groupPosition).getUrl()));
            holder.iv_leftviewcheck_one.setBackgroundResource(R.mipmap.leftclassify_icon_up);
            holder.tv_leftview_one.setTextColor(Color.rgb(230, 64, 64));

        }else{
            // 大组关闭
            holder.iv_leftview_one.setImageURI(Uri.parse(contentBean.get(groupPosition).getUrl()));
            holder.iv_leftviewcheck_one.setBackgroundResource(R.mipmap.leftclassify_icon_down);
            holder.tv_leftview_one.setTextColor(Color.rgb(58, 58, 58));
        }

        return convertView;
    }

    //二级
    @Override
    public View getChildView(final int groupPosition, final int childPosition,boolean isLastChild, View convertView, ViewGroup parent) {

        if(convertView==null){
            LayoutInflater inflater = LayoutInflater.from(MyApplication.getContext());
            convertView = inflater.inflate(R.layout.item_leftview_tow, null);
        }
        Holder holder = Holder.getHolder(convertView);
        //holder.tv_leftview_two.setText(getChild(groupPosition, childPosition).toString());
        holder.tv_leftview_two.setText(contentBean.get(groupPosition).getSon().get(childPosition).getCat_name());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(mContext, InquiryListActivity.class);
                intent.putExtra("cat_id", contentBean.get(groupPosition).getSon().get(childPosition).getCat_id());
                //mContext.startActivityForResult(intent, Constants.REQUESTCODE);
                mContext.startActivity(intent);
                mSlideViewLEFT.dismiss();
            }
        });

        return convertView;
    }


    
    

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    static class Holder {
        TextView tv_leftview_one,tv_leftview_two;
        SimpleDraweeView iv_leftview_one,iv_leftviewcheck_one;

        public Holder(View convertView) {
            iv_leftview_one = (SimpleDraweeView) convertView.findViewById(R.id.iv_leftview_one);
            iv_leftviewcheck_one = (SimpleDraweeView) convertView.findViewById(R.id.iv_leftviewcheck_one);
            tv_leftview_two = (TextView) convertView.findViewById(R.id.tv_leftview_two);
            tv_leftview_one = (TextView) convertView.findViewById(R.id.tv_leftview_one);
        }

        public static Holder getHolder(View convertView) {
            Holder holder = (Holder) convertView.getTag();
            if (holder == null) {
                holder = new Holder(convertView);
                convertView.setTag(holder);
            }
            return holder;
        }
    }

}

