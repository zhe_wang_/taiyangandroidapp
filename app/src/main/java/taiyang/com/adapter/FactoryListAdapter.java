package taiyang.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.activity.FactoryInfoActivity;
import taiyang.com.activity.OfferInfoActivity;
import taiyang.com.entity.FactoryListBean;
import taiyang.com.tydp_b.R;

/**
 * PersonInfoActivity  Adapter
 * Created by heng on 2016/8/26.
 */
public class FactoryListAdapter extends RecyclerView.Adapter<FactoryListAdapter.ViewHolder> {
    private List<FactoryListBean> factoryListBean;
    private Context mCtx;
    private List<FactoryListBean.GoodListBean> good_list;
    private GridTextAdapter gridTextAdapternew;

    public FactoryListAdapter(List<FactoryListBean> factoryListBean, Context mCtx) {
        this.factoryListBean = factoryListBean;
        this.mCtx = mCtx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_factorylist, parent, false);

        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        good_list = factoryListBean.get(position).getGood_list();
        String uri =factoryListBean.get(position).getUser_face();
        if(uri.equals("")){
            holder.iv_factorylist_logo.setImageResource(R.mipmap.shop_nophoto1);
        }else {
            holder.iv_factorylist_logo.setImageURI(Uri.parse(uri));
        }

        holder.tv_factorylist_number.setText("订单成交量"+factoryListBean.get(position).getOrder_num());
        holder.tv_factorylist_name.setText(factoryListBean.get(position).getShop_name());
        holder.bt_factorylist_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Intent i = new Intent(mCtx, FactoryInfoActivity.class);
               i.putExtra("shop_id",factoryListBean.get(position).getShop_id());
               mCtx.startActivity(i);
            }
        });

        gridTextAdapternew = new GridTextAdapter(good_list);
        holder.gv_factorylist_im.setAdapter(gridTextAdapternew);
        //点击gridview 图片不同id
        holder.gv_factorylist_im.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

               // String id = good_list.get(i).getGoods_id();
                //成员变量的good_list position是初始化数据 ,代表屏幕遍历完后最后一条item的position
                //  而   factoryListBean.get(position).getGood_list() 是当前点击position
                String id = factoryListBean.get(position).getGood_list().get(i).getGoods_id();
                Intent a =new Intent(mCtx, OfferInfoActivity.class);
                a.putExtra("Goods_id",id);
                mCtx.startActivity(a);
            }
        });

    }
    @Override
    public int getItemCount() {
        return factoryListBean.size();
    }

    class GridTextAdapter extends BaseAdapter {
        private List<FactoryListBean.GoodListBean> goodList;

        public GridTextAdapter(List<FactoryListBean.GoodListBean> goodList) {
            this.goodList = goodList;
        }

        @Override
        public int getCount() {
            return goodList.size();
        }

        @Override
        public Object getItem(int position) {
            return goodList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_factorylist_gridview, null);
            }

            ViewHodler holder = ViewHodler.getHolder(convertView);
            holder.iv_factorylist_child.setImageURI(Uri.parse(goodList.get(position).getGoods_thumb()));


            return convertView;

        }

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.iv_factorylist_logo)
        SimpleDraweeView iv_factorylist_logo;
        @InjectView(R.id.gv_factorylist_im)
        GridView gv_factorylist_im;

        @InjectView(R.id.tv_factorylist_number)
        TextView tv_factorylist_number;
        @InjectView(R.id.tv_factorylist_name)
        TextView tv_factorylist_name;
        @InjectView(R.id.bt_factorylist_go)
        Button bt_factorylist_go;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }


    }

    //gv 子布局ViewHodler
    static class ViewHodler {
        private SimpleDraweeView iv_factorylist_child;

        ViewHodler(View convertView) {
            iv_factorylist_child = (SimpleDraweeView) convertView.findViewById(R.id.iv_factorylist_child);
        }

        public static ViewHodler getHolder(View convertView) {
            ViewHodler holder = (ViewHodler) convertView.getTag();
            if (holder == null) {
                holder = new ViewHodler(convertView);
                convertView.setTag(holder);
            }

            return holder;
        }

    }



    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
//      void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
}
