package taiyang.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.activity.OfferInfoActivity;
import taiyang.com.entity.DickerModel;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.L;

/**
 * Created by admin on 2016/7/18.
 * 我的还价适配器
 */
public class DickerAdapter extends ListBaseAdapter<DickerModel> {
    private Context mContext;
    private boolean flag;

    public DickerAdapter(Context mContext) {
        this.mContext = mContext;
//        this.mDatas = mDatas;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.dicker_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder= (ViewHolder) holder;
        final DickerModel dickerModel = mDatas.get(position);
        viewHolder.shop_logo.setImageURI(Uri.parse(dickerModel.getGoods_thumb()));
        viewHolder.tv_shopname.setText(dickerModel.getGoods_name());
        viewHolder.tv_chanpin_number.setText(mContext.getString(R.string.changhao)+":" + dickerModel.getBrand_sn());
        viewHolder.tv_ilaitem_price.setText("¥"+dickerModel.getLast_price());
        viewHolder.tv_originalprice.setText("¥"+dickerModel.getLast_price());
        viewHolder.tv_myprice.setText("¥"+dickerModel.getPrice());
        viewHolder.tv_msg_time.setText(mContext.getString(R.string.shijian)+":"+dickerModel.getCreated_at());
        viewHolder.tv_msg_txt.setText(mContext.getString(R.string.liuyan)+":"+dickerModel.getMessage());

        //还价
        if (mDatas.get(position).getOffer().equals("11")) {
            viewHolder.huan.setVisibility(View.VISIBLE);
        } else {
            viewHolder.huan.setVisibility(View.GONE);
        }
        //拼
        if (mDatas.get(position).getIs_pin() == 1) {
            viewHolder.pin.setVisibility(View.VISIBLE);
        } else {
            viewHolder.pin.setVisibility(View.GONE);
        }
        //商品类型  6=期货7=现货
        if (mDatas.get(position).getGoods_type().equals("6")) {
            viewHolder.xianqi.setText(mContext.getString(R.string.qi));
            viewHolder.xianqi.setBackgroundColor(mContext.getResources().getColor(R.color.qi));

        } else {
            viewHolder.xianqi.setText(mContext.getString(R.string.xian));
            viewHolder.xianqi.setBackgroundColor(mContext.getResources().getColor(R.color.xian));


        }
        //售卖类型 4零5整
        if (mDatas.get(position).getSell_type().equals("4")) {
            viewHolder.zhengling.setText(mContext.getString(R.string.ling));
            viewHolder.zhengling.setBackgroundColor(mContext.getResources().getColor(R.color.ling));
        } else {
            viewHolder.zhengling.setText(mContext.getString(R.string.zheng));
            viewHolder.zhengling.setBackgroundColor(mContext.getResources().getColor(R.color.zheng));
        }

        //0  等待  1 失败  2 成功
        if (dickerModel.getStatus().equals("0")) {
            viewHolder.iv_result.setImageResource(R.mipmap.pic_wait);
            viewHolder.bt_dicker.setBackgroundColor(mContext.getResources().getColor(R.color.message_bg2));
            viewHolder.bt_dicker.setEnabled(false);
        } else {
            viewHolder.iv_result.setImageResource(R.mipmap.bargin_icon_success);
            viewHolder.bt_dicker.setBackgroundColor(mContext.getResources().getColor(R.color.change_bg));
            viewHolder.bt_dicker.setEnabled(true);
        }
        if (mOnItemClickLitener != null) {
            viewHolder.bt_dicker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!dickerModel.getStatus().equals("0")) {

                        Intent intent = new Intent(mContext, OfferInfoActivity.class);
                        intent.putExtra("Goods_id", dickerModel.getGoods_id());
                        mContext.startActivity(intent);
                    }
                }
            });
            viewHolder.bt_buy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, OfferInfoActivity.class);
                    intent.putExtra("Goods_id", dickerModel.getGoods_id());
                    mContext.startActivity(intent);
                }
            });

        }
    }



    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //        @InjectView(R.id.tv_order)

        @InjectView(R.id.shop_logo)
        SimpleDraweeView shop_logo;
        @InjectView(R.id.tv_shopname)
        TextView tv_shopname;
        @InjectView(R.id.tv_chanpin_number)
        TextView tv_chanpin_number;
        @InjectView(R.id.tv_ilaitem_price)
        TextView tv_ilaitem_price;
        @InjectView(R.id.tv_originalprice)
        TextView tv_originalprice;
        @InjectView(R.id.tv_myprice)
        TextView tv_myprice;
        @InjectView(R.id.tv_location)
        TextView tv_location;


        @InjectView(R.id.tv_msg_time)
        TextView tv_msg_time;
        @InjectView(R.id.tv_msg_txt)
        TextView tv_msg_txt;
        @InjectView(R.id.iv_result)
        ImageView iv_result;
        @InjectView(R.id.tv_xian_qi)
        TextView xianqi;
        @InjectView(R.id.tv_zheng_ling)
        TextView zhengling;
        @InjectView(R.id.tv_pin)
        TextView pin;
        @InjectView(R.id.tv_huan)
        TextView huan;

        @InjectView(R.id.bt_dicker)
        Button bt_dicker;
        @InjectView(R.id.bt_buy)
        Button bt_buy;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }

    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);

//        void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
}
