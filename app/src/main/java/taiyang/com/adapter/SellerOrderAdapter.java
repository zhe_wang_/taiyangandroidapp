package taiyang.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.activity.OrderDetailSellerActivity;
import taiyang.com.activity.SellerDickerActivity;
import taiyang.com.entity.SellerBaopanListModel;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MyApplication;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.ToastUtil;

/**
 * Created by admin on 2016/7/18.
 * 我的订单管理适配器
 */
public class SellerOrderAdapter extends ListBaseAdapter<SellerBaopanListModel.ListBean> {
    private Context mCtx;

    public SellerOrderAdapter(Context mCtx) {
        this.mCtx = mCtx;

    }

    @Override
    public SellerOrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.seller_baopan_item, parent, false);
        SellerOrderAdapter.ViewHolder vh = new SellerOrderAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        SellerOrderAdapter.ViewHolder viewHolder= (SellerOrderAdapter.ViewHolder) holder;
        final String order_id = mDatas.get(position).getOrder_id();
                viewHolder.rl_home_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent2 = new Intent(MyApplication.getContext(),OrderDetailSellerActivity.class);
                        intent2.putExtra("order_id",order_id);
                        mCtx.startActivity(intent2);
                    }
                });

        viewHolder.imageView.setImageURI(Uri.parse(mDatas.get(position).getGoods().getGoods_thumb()));
        viewHolder.iv_homelist_guojia.setText(mDatas.get(position).getGoods().getRegion_name());
        viewHolder.textView_chanpin.setText(mDatas.get(position).getGoods().getGoods_name());//名称
        viewHolder.tv_chanpin_number.setText(mDatas.get(position).getGoods().getBrand_sn());//厂号
        viewHolder.tv_ilaitem_price.setText(mDatas.get(position).getGoods().getFormated_goods_price()+"/"+mCtx.getString(R.string.dun));//价格

            viewHolder.tv_info.setText("   "+
                    (mDatas.get(position).getGoods().getIs_retail().equals("1")?mDatas.get(position).getGoods().getPart_number():mDatas.get(position).getGoods().getPart_weight())+
                    ""+mDatas.get(position).getGoods().getPart_unit()+"    "+
                    mDatas.get(position).getGoods().getGoods_number()+mDatas.get(position).getGoods().getMeasure_unit());




        viewHolder.tv_time.setText(mDatas.get(position).getAdd_time());
        viewHolder.tv_title.setText(mDatas.get(position).getOrder_sn());
            viewHolder.tv_states.setText(mDatas.get(position).getOrder_status_format());
        viewHolder.bt_action.setText(mDatas.get(position).getTag_status());
    }

    private void huanjiaAction(String zidingjia,String caiyong,String goodsid){
        Map<String, Object> params = new HashMap<>();
        if(!zidingjia.equals("-1")){
            params.put("price",zidingjia);
        }if(!caiyong.equals("-1")){
            params.put("huck_id",caiyong);
        }
        params.put("goods_id",goodsid);
        params.put("model", "seller");
        params.put("action", "saveHuckster");
        params.put("user_id", MySPEdit.getInstance(mCtx).getUserId());
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller", "saveHuckster")));
        HttpUtils.sendPost(params, new HttpRequestListener() {
            @Override
            public void success(String response, int id) {
                ToastUtil.showToast(mCtx.getString(R.string.caozuochenggong));
                ((SellerDickerActivity)mCtx).finish();
                Intent intent = new Intent(mCtx, SellerDickerActivity.class);
                mCtx.startActivity(intent);

            }
            @Override
            public void failByOther(String fail, int id) {
                ToastUtil.showToast(fail);
            }

            @Override
            public void fail(String fail, int id) {

            }
        });

    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @InjectView(R.id.imageView)
        SimpleDraweeView imageView;

        @InjectView(R.id.iv_homelist_guojia)
        TextView iv_homelist_guojia;
        @InjectView(R.id.textView_chanpin)
        TextView textView_chanpin;
        @InjectView(R.id.tv_chanpin_number)
        TextView tv_chanpin_number;
        @InjectView(R.id.tv_ilaitem_price)
        TextView tv_ilaitem_price;
        @InjectView(R.id.tv_ilaitem_price2)
        TextView tv_ilaitem_price2;
        @InjectView(R.id.tv_ilaitem_unit2)
        TextView tv_ilaitem_unit2;
        @InjectView(R.id.tv_ilaitem_location)
        TextView tv_ilaitem_location;
        @InjectView(R.id.tv_info)
        TextView tv_info;

        @InjectView(R.id.tv_title)
        TextView tv_title;
        @InjectView(R.id.tv_states)
        TextView tv_states;

        @InjectView(R.id.rl_home_isover)
        ImageView rl_home_isover;
        @InjectView(R.id.rl_home_item)
        RelativeLayout rl_home_item;

        @InjectView(R.id.bt_action)
        Button bt_action;
        @InjectView(R.id.tv_time)
        TextView tv_time;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.inject(this,itemView);

        }
    }

}
