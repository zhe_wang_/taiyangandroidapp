package taiyang.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.activity.NewsDetailActivity;
import taiyang.com.cache.CacheUtils;
import taiyang.com.entity.NewsModel;
import taiyang.com.tydp_b.R;

/**
 * Created by admin on 2016/7/18.
 * 我的消息配器
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {
    private List<NewsModel> mDatas;
    private Context mContext;
    private final String URL="http://test.taiyanggo.com/mobile/";
    private CacheUtils mCacheManager;
    public NewsAdapter(Context mContext, List<NewsModel> mDatas) {
        this.mContext = mContext;
        this.mDatas = mDatas;
        mCacheManager=new CacheUtils(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.news_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.title.setText(mDatas.get(position).getTitle());
        holder.time.setText(mDatas.get(position).getAdd_time());
        holder.im_news.setImageURI(Uri.parse(mDatas.get(position).getArc_img()));
//        holder.content.setText(mDatas.get(position).getShort_title());
        if (mOnItemClickLitener != null) {
            holder.news_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos=holder.getLayoutPosition();
                    String webUrl=URL+mDatas.get(pos).getUrl()+"&from=app";
                    Intent intent=new Intent(mContext,NewsDetailActivity.class);
                    intent.putExtra("webUrl",webUrl);
                    mContext.startActivity(intent);
//                    mOnItemClickLitener.onItemClick(holder.pay, pos);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.tv_title)
        TextView title;
        @InjectView(R.id.tv_time)
        TextView time;
        @InjectView(R.id.news_layout)
        RelativeLayout news_layout;
        @InjectView(R.id.im_news)
        SimpleDraweeView im_news;

//        @InjectView(R.id.tv_content)
//        TextView content;

        //        @InjectView(R.id.tv_orderprice)
//        TextView orderprice;
//        @InjectView(R.id.tv_ordercount)
//        TextView ordercount;
//        @InjectView(R.id.tv_pay)
//        TextView pay;
//        @InjectView(R.id.tv_time)
//        TextView time;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }

    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);

//        void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
}
