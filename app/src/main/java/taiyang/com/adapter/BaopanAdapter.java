package taiyang.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.activity.BaopanguanliActivity;
import taiyang.com.activity.OfferInfoActivity;
import taiyang.com.activity.SellActivity;
import taiyang.com.entity.BaopanListModel;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MyApplication;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.ToastUtil;

/**
 * Created by admin on 2016/7/18.
 * 我的还价适配器
 */
public class BaopanAdapter extends ListBaseAdapter<BaopanListModel.ListBean> {
    private Context mCtx;

    public BaopanAdapter(Context mCtx) {
        this.mCtx = mCtx;

    }

    @Override
    public BaopanAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.baopan_item, parent, false);
        BaopanAdapter.ViewHolder vh = new BaopanAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        BaopanAdapter.ViewHolder viewHolder= (BaopanAdapter.ViewHolder) holder;
        final String Goods_id = mDatas.get(position).getGoods_id();


        int goods_number = Integer.parseInt(mDatas.get(position).getGoods_number());
        //库存不为0
        if(goods_number <=0){
            viewHolder.rl_home_isover.setVisibility(View.VISIBLE);
            viewHolder.rl_home_item.setOnClickListener(null);
        }else {
            viewHolder.rl_home_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i =new Intent(mCtx, OfferInfoActivity.class);
                    i.putExtra("Goods_id",Goods_id);
                    mCtx.startActivity(i);
                }
            });
            viewHolder.rl_home_isover.setVisibility(View.GONE);
        }
        viewHolder.imageView.setImageURI(Uri.parse(mDatas.get(position).getGoods_thumb()));
        viewHolder.iv_homelist_guojia.setText(mDatas.get(position).getRegion_name());
        viewHolder.textView_chanpin.setText(mDatas.get(position).getGoods_name());//名称
        viewHolder.tv_chanpin_number.setText(mDatas.get(position).getBrand_sn());//厂号
        viewHolder.tv_ilaitem_price.setText(mDatas.get(position).getShop_price());//价格
        viewHolder.tv_ilaitem_unit.setText("/ "+mDatas.get(position).getShop_price_unit());//价格
        viewHolder.tv_info.setText("   "+mDatas.get(position).getSpec_2()+mDatas.get(position).getSpec_2_unit()+"    "+
                mDatas.get(position).getGoods_number()+mDatas.get(position).getMeasure_unit());

        if(mDatas.get(position).getSell_type().equals("4")){
            viewHolder.tv_info.setText("   "+mDatas.get(position).getSpec_2()+mDatas.get(position).getSpec_2_unit()+"    "+
                    mDatas.get(position).getGoods_number()+mDatas.get(position).getMeasure_unit());
        }else{
            viewHolder.tv_info.setText("   "+mDatas.get(position).getGoods_weight()+mDatas.get(position).getSpec_1_unit()+"    "+
                    mDatas.get(position).getGoods_number()+mDatas.get(position).getMeasure_unit());
        }
        viewHolder.tv_time.setText(mDatas.get(position).getLast_update_time());
        viewHolder.tv_title.setText(mDatas.get(position).getSku());
        if(mDatas.get(position).getIs_check().equals("0")){
            viewHolder.tv_states.setText(R.string.weishenhe);
        }
        if(mDatas.get(position).getIs_check().equals("-1")){
            viewHolder.tv_states.setText(R.string.shenhebutongguo);
        }
        if(mDatas.get(position).getIs_check().equals("1")){
            viewHolder.tv_states.setText(R.string.shenhetongguo);
        }
        //还价
        if (mDatas.get(position).getOffer().equals("11")) {
            viewHolder.huan.setVisibility(View.VISIBLE);
        } else {
            viewHolder.huan.setVisibility(View.GONE);
        }
        //拼
        if (mDatas.get(position).getIs_pin() == 1) {
            viewHolder.pin.setVisibility(View.VISIBLE);
        } else {
            viewHolder.pin.setVisibility(View.GONE);
        }
        //商品类型  6=期货7=现货
        if (mDatas.get(position).getGoods_type().equals("6")) {
            viewHolder.xianqi.setText(mCtx.getString(R.string.qi));
            viewHolder.xianqi.setBackgroundColor(mCtx.getResources().getColor(R.color.qi));

        } else {
            viewHolder.xianqi.setText(mCtx.getString(R.string.xian));
            viewHolder.xianqi.setBackgroundColor(mCtx.getResources().getColor(R.color.xian));


        }
        //售卖类型 4零5整
        if (mDatas.get(position).getSell_type().equals("4")) {
            viewHolder.zhengling.setText(mCtx.getString(R.string.ling));
            viewHolder.zhengling.setBackgroundColor(mCtx.getResources().getColor(R.color.ling));
        } else {
            viewHolder.zhengling.setText(mCtx.getString(R.string.zheng));
            viewHolder.zhengling.setBackgroundColor(mCtx.getResources().getColor(R.color.zheng));
        }
        viewHolder.bt_jia.setText(mDatas.get(position).getIs_on_sale().equals("1") ?R.string.xiajia:R.string.shangjia);
        viewHolder.bt_jia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shangxiajiaAction(((TextView)view).getText().toString(),Goods_id);
            }
        });
        viewHolder.bt_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mCtx, SellActivity.class);
                i.putExtra("goods_id",Goods_id);
                i.putExtra("is_edit",true);
                mCtx.startActivity(i);
            }
        });
        viewHolder.bt_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mCtx, SellActivity.class);
                i.putExtra("goods_id",Goods_id);
                i.putExtra("is_copy",true);
                mCtx.startActivity(i);
            }
        });
    }

    private void shangxiajiaAction(String on_sale,String goods_id){
        Map<String, Object> params = new HashMap<>();
        params.put("is_on_sale",on_sale.equals(mCtx.getString(R.string.shangjia))?1:0);
        params.put("model", "seller_offer");
        params.put("goods_id", goods_id);
        params.put("action", "offer_edit_on_sale");
        params.put("user_id", MySPEdit.getInstance(mCtx).getUserId());
        params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller_offer", "offer_edit_on_sale")));
        HttpUtils.sendPost(params, new HttpRequestListener() {
            @Override
            public void success(String response, int id) {
                ToastUtil.showToast(mCtx.getString(R.string.bianjibaopanchenggong));
                ((BaopanguanliActivity)mCtx).initData();
            }
            @Override
            public void failByOther(String fail, int id) {
                ToastUtil.showToast(fail);
            }

            @Override
            public void fail(String fail, int id) {

            }
        });

    }
    class ViewHolder extends RecyclerView.ViewHolder{
        @InjectView(R.id.imageView)
        SimpleDraweeView imageView;
        @InjectView(R.id.iv_homelist_guojia)
        TextView iv_homelist_guojia;
        @InjectView(R.id.textView_chanpin)
        TextView textView_chanpin;
        @InjectView(R.id.tv_chanpin_number)
        TextView tv_chanpin_number;
        @InjectView(R.id.tv_ilaitem_price)
        TextView tv_ilaitem_price;
        @InjectView(R.id.tv_ilaitem_unit)
        TextView tv_ilaitem_unit;
        @InjectView(R.id.tv_ilaitem_price2)
        TextView tv_ilaitem_price2;
        @InjectView(R.id.tv_ilaitem_unit2)
        TextView tv_ilaitem_unit2;
        @InjectView(R.id.tv_ilaitem_location)
        TextView tv_ilaitem_location;
        @InjectView(R.id.tv_info)
        TextView tv_info;
        @InjectView(R.id.tv_title)
        TextView tv_title;
        @InjectView(R.id.tv_states)
        TextView tv_states;
        @InjectView(R.id.tv_xian_qi)
        TextView xianqi;
        @InjectView(R.id.tv_zheng_ling)
        TextView zhengling;
        @InjectView(R.id.tv_pin)
        TextView pin;
        @InjectView(R.id.tv_huan)
        TextView huan;
        @InjectView(R.id.rl_home_isover)
        ImageView rl_home_isover;
        @InjectView(R.id.rl_home_item)
        RelativeLayout rl_home_item;

        @InjectView(R.id.tv_time)
        TextView tv_time;

        @InjectView(R.id.bt_jia)
        TextView bt_jia;

        @InjectView(R.id.bt_edit)
        TextView bt_edit;

        @InjectView(R.id.bt_copy)
        TextView bt_copy;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.inject(this,itemView);

        }
    }

}
