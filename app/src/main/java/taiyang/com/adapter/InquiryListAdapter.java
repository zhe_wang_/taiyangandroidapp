package taiyang.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.activity.OfferInfoActivity;
import taiyang.com.entity.InquiryListContentBean;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.MyApplication;

/**
 * 筛选InquiryList 推荐列表Adapter
 * Created by heng on 2016/7/14.
 */
public class InquiryListAdapter extends ListBaseAdapter<InquiryListContentBean.ListBean> {
    private Context mCtx;

    public InquiryListAdapter(Context mCtx) {
        this.mCtx = mCtx;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_inquirylist_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolder viewHolder= (ViewHolder) holder;
        final String Goods_id = mDatas.get(position).getGoods_id();
        int goods_number = Integer.parseInt(mDatas.get(position).getGoods_number());

        if(mOnItemClickLitener != null){
            //库存不为0
            if(goods_number ==0 ||goods_number<0){
                viewHolder.rl_home_isover.setVisibility(View.VISIBLE);

                viewHolder.rl_home_item.setOnClickListener(null);
            }else {
                viewHolder.rl_home_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i =new Intent(MyApplication.getContext(), OfferInfoActivity.class);
                        i.putExtra("Goods_id",Goods_id);
                        mCtx.startActivity(i);
                    }
                });
                viewHolder.rl_home_isover.setVisibility(View.GONE);
            }

        }

        viewHolder.imageView.setImageURI(Uri.parse(mDatas.get(position).getGoods_thumb()));
        viewHolder.iv_homelist_guojia.setText(mDatas.get(position).getRegion_name_ch());
        viewHolder.textView_chanpin.setText(mDatas.get(position).getGoods_name());//名称
        viewHolder.tv_chanpin_number.setText(mDatas.get(position).getBrand_sn());//厂号
            viewHolder.tv_ilaitem_location.setText(mDatas.get(position).getGoods_local()+" "+mDatas.get(position).getArrive_count());//地点
        viewHolder.tv_ilaitem_price.setText(mDatas.get(position).getShop_price());//价格
        viewHolder.tv_ilaitem_unit.setText("/" + mDatas.get(position).getUnit_name());//价格单位
        viewHolder.tv_ilaitem_price2.setText(mDatas.get(position).getSpec_1());//规格数量
        viewHolder.tv_ilaitem_unit2.setText(mDatas.get(position).getSpec_1_unit());//规格单位
        //还价
        if (mDatas.get(position).getOffer().equals("11")) {
            viewHolder.huan.setVisibility(View.VISIBLE);
        } else {
            viewHolder.huan.setVisibility(View.GONE);
        }
        //拼
        if (mDatas.get(position).getIs_pin() == 1) {
            viewHolder.pin.setVisibility(View.VISIBLE);
        } else {
            viewHolder.pin.setVisibility(View.GONE);
        }
        //商品类型  6=期货7=现货
        if (mDatas.get(position).getGoods_type().equals("6")) {
            viewHolder.xianqi.setText(mCtx.getString(R.string.qi));
            viewHolder.xianqi.setBackgroundColor(mCtx.getResources().getColor(R.color.qi));
        } else {
            viewHolder.xianqi.setText(mCtx.getString(R.string.xian));
            viewHolder.xianqi.setBackgroundColor(mCtx.getResources().getColor(R.color.xian));

        }
        //售卖类型 4零5整
        if (mDatas.get(position).getSell_type().equals("4")) {
            viewHolder.zhengling.setText(mCtx.getString(R.string.ling));
            viewHolder.zhengling.setBackgroundColor(mCtx.getResources().getColor(R.color.ling));
        } else {
            viewHolder.zhengling.setText(mCtx.getString(R.string.zheng));
            viewHolder.zhengling.setBackgroundColor(mCtx.getResources().getColor(R.color.zheng));
        }
    }




    class ViewHolder extends RecyclerView.ViewHolder{
        @InjectView(R.id.imageView)
        SimpleDraweeView imageView;

        @InjectView(R.id.iv_homelist_guojia)
        TextView iv_homelist_guojia;
        @InjectView(R.id.textView_chanpin)
        TextView textView_chanpin;
        @InjectView(R.id.tv_chanpin_number)
        TextView tv_chanpin_number;
        @InjectView(R.id.tv_ilaitem_price)
        TextView tv_ilaitem_price;
        @InjectView(R.id.tv_ilaitem_unit)
        TextView tv_ilaitem_unit;
        @InjectView(R.id.tv_ilaitem_price2)
        TextView tv_ilaitem_price2;
        @InjectView(R.id.tv_ilaitem_unit2)
        TextView tv_ilaitem_unit2;
        @InjectView(R.id.tv_ilaitem_location)
        TextView tv_ilaitem_location;
        @InjectView(R.id.tv_xian_qi)
        TextView xianqi;
        @InjectView(R.id.tv_zheng_ling)
        TextView zhengling;
        @InjectView(R.id.tv_pin)
        TextView pin;
        @InjectView(R.id.tv_huan)
        TextView huan;
        @InjectView(R.id.rl_home_isover)
        ImageView rl_home_isover;
        @InjectView(R.id.rl_home_item)
        RelativeLayout rl_home_item;


        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.inject(this,itemView);

        }

    }



    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
}
