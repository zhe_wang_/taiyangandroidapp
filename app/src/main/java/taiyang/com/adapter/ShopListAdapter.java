package taiyang.com.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.activity.FactoryInfoActivity;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.MyApplication;

/**
 * Created by admin on 2016/8/1.
 * 店铺列表适配器
 */
public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.ViewHolder> {
    private List<String> mDatas;
    public ShopListAdapter(List<String> mDatas) {
        this.mDatas = mDatas;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_shoplist, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (mOnItemClickLitener != null) {
            holder.bt_shoplist_shop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(MyApplication.getContext(),FactoryInfoActivity.class);
                    MyApplication.getContext().startActivity(i);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.iv_shoplist_logo)
        ImageView iv_shoplist_logo;
        @InjectView(R.id.iv_shoplist_rz)
        ImageView iv_shoplist_rz;
        @InjectView(R.id.iv_shoplist_bz)
        ImageView iv_shoplist_bz;
        @InjectView(R.id.tv_shoplist_name)
        TextView tv_shoplist_name;
        @InjectView(R.id.tv_shoplist_order)
        TextView tv_shoplist_order;
        @InjectView(R.id.bt_shoplist_shop)
        View bt_shoplist_shop;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }

    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);

//        void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
}
