package taiyang.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import taiyang.com.entity.QiugouListBean;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.DensityUtil;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.ToastUtil;

/**
 * Created by zhew on 9/9/17.
 * Copyright © 2006-2016 365jia.cn 安徽大尺度网络传媒有限公司版权所有
 **/

public class QiugouListAdapter extends BaseAdapter {

    public List<QiugouListBean.ListBean> getmList() {
        return mList;
    }

    public void setmList(List<QiugouListBean.ListBean> mList) {
        this.mList = mList;
    }

    private List<QiugouListBean.ListBean> mList;
    private Context mCtx;

    public QiugouListAdapter(List<QiugouListBean.ListBean> mList, Context mCtx) {
        this.mList = mList;
        this.mCtx = mCtx;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return Long.parseLong(mList.get(i).getId());
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = LayoutInflater.from(mCtx).inflate(R.layout.recyclerview_qiugou_list_item, null, false);
        int dp = DensityUtil.dip2px(mCtx,10);
        v.setPadding(dp,dp,dp,dp);
        final QiugouListBean.ListBean qiuGou = (QiugouListBean.ListBean) getItem(i);
        ((SimpleDraweeView) v.findViewById(R.id.riv_qiugou1)).setImageURI(Uri.parse(qiuGou.getUser_face()));
        ((TextView) v.findViewById(R.id.tv_qiugou1_name)).setText(qiuGou.getUser_name().replaceAll("(\\d{3})\\d{4}(\\d{4})","$1****$2"));
        ((TextView) v.findViewById(R.id.tv_qiugou1_localtion)).setText(qiuGou.getAddress());
        SpannableStringBuilder style2 = new SpannableStringBuilder(qiuGou.getGoods_name() + "    " +
                qiuGou.getGoods_num() + mCtx.getString(R.string.dun)+"   " + qiuGou.getPrice_low() + "-" + qiuGou.getPrice_up() + mCtx.getString(R.string.yuan));
        ((TextView) v.findViewById(R.id.tv_qiugou1_info)).setText(style2);
        ((TextView) v.findViewById(R.id.tv_qiugou1_text)).setText(qiuGou.getMemo());
        ((TextView) v.findViewById(R.id.tv_qiugou1_time)).setText(qiuGou.getCreated_at());
        ((TextView) v.findViewById(R.id.tv_qiugou1_text)).setVisibility(qiuGou.getMemo().isEmpty()?View.GONE:View.VISIBLE);

        v.findViewById(R.id.btn_call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MySPEdit.getInstance(mCtx).getIsLogin()) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + qiuGou.getUser_phone()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mCtx.startActivity(intent);
                }else{
                    ToastUtil.showToast("登录后可联系对方");
                }
            }
        });

        return v;
    }
}
