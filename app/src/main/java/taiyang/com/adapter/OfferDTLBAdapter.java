package taiyang.com.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import taiyang.com.entity.OfferInfoBean;
import taiyang.com.tydp_b.R;

public class OfferDTLBAdapter extends PagerAdapter {

	private List<OfferInfoBean.PictureListBean> app_silde;
	private LayoutInflater mInflater;

	public OfferDTLBAdapter(Context ctx, List<OfferInfoBean.PictureListBean> app_silde) {
		this.app_silde = app_silde;
		mInflater = LayoutInflater.from(ctx);
	}

	@Override
	public int getCount() {
		return app_silde.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == arg1;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		View view = mInflater.inflate(R.layout.fragment_homedtlb, null);
		com.facebook.drawee.view.SimpleDraweeView iv = (com.facebook.drawee.view.SimpleDraweeView) view.findViewById(R.id.iv_homefragment_dtlb);
		String picUrl = app_silde.get(position % app_silde.size()).getImg_url();
		iv.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		iv.setImageURI(Uri.parse(picUrl));
		container.addView(view);
		return view;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

}
