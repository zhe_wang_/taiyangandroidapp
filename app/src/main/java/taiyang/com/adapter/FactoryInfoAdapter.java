package taiyang.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.activity.OfferInfoActivity;
import taiyang.com.entity.FactorGoodsModel;
import taiyang.com.tydp_b.R;

/**
 * Created by admin on 2016/8/4.
 * 厂商信息适配器
 */
public class FactoryInfoAdapter extends BaseAdapter {
    public List<FactorGoodsModel> getmList() {
        return mList;
    }

    public void setmList(List<FactorGoodsModel> mList) {
        this.mList = mList;
    }

    private List<FactorGoodsModel> mList;
    private Context mCtx;

    public FactoryInfoAdapter(ArrayList<FactorGoodsModel> mList, Context mCtx) {
        this.mList = mList;
        this.mCtx = mCtx;
    }

    @Override
    public View getView(int position, View v, ViewGroup viewGroup) {

        View view = LayoutInflater.from(mCtx).inflate(R.layout.recyclerview_inquirylist_item, null, false);

        FactoryInfoAdapter.ViewHolder holder = new FactoryInfoAdapter.ViewHolder(view);
        final String Goods_id = mList.get(position).getGoods_id();
        int goods_number = Integer.parseInt(mList.get(position).getGoods_number());

            //库存不为0
            if (goods_number <= 0) {
                holder.rl_home_isover.setVisibility(View.VISIBLE);
                holder.rl_home_item.setOnClickListener(null);
            } else {
                holder.rl_home_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(mCtx, OfferInfoActivity.class);
                        i.putExtra("Goods_id", Goods_id);
                        mCtx.startActivity(i);
                    }
                });
                holder.rl_home_isover.setVisibility(View.GONE);
            }


        holder.imageView.setImageURI(Uri.parse(mList.get(position).getGoods_thumb()));
        holder.imageView.setTag(mList.get(position).getGoods_id());
        holder.iv_homelist_guojia.setText(mList.get(position).getRegion_name_ch());
        holder.textView_chanpin.setText(mList.get(position).getGoods_name());//名称
        holder.tv_chanpin_number.setText(mList.get(position).getBrand_sn());//厂号
        holder.tv_ilaitem_location.setText(mList.get(position).getGoods_local());//地点


            holder.tv_ilaitem_location.setText(mList.get(position).getGoods_local()+" "+mList.get(position).getArrive_count());//地点

        holder.tv_ilaitem_price.setText(mList.get(position).getShop_price());//价格
        holder.tv_ilaitem_unit.setText("/" + mList.get(position).getUnit_name());//价格单位
        holder.tv_ilaitem_price2.setText(mList.get(position).spec_1);//规格数量
        holder.tv_ilaitem_unit2.setText("/"+mList.get(position).spec_1_unit);//规格单位
        //还价
        if (mList.get(position).getOffer().equals("11")) {
            holder.huan.setVisibility(View.VISIBLE);
        } else {
            holder.huan.setVisibility(View.GONE);
        }
        //拼
        if (mList.get(position).getIs_pin() == 1) {
            holder.pin.setVisibility(View.VISIBLE);
        } else {
            holder.pin.setVisibility(View.GONE);
        }
        //商品类型  6=期货7=现货
        if (mList.get(position).getGoods_type().equals("6")) {
            holder.xianqi.setText(mCtx.getString(R.string.qi));
            holder.xianqi.setBackgroundColor(mCtx.getResources().getColor(R.color.qi));
        } else {
            holder.xianqi.setText(mCtx.getString(R.string.xian));
            holder.xianqi.setBackgroundColor(mCtx.getResources().getColor(R.color.xian));

        }
        //售卖类型 4零5整
        if (mList.get(position).getSell_type().equals("4")) {
            holder.zhengling.setText(mCtx.getString(R.string.ling));
            holder.zhengling.setBackgroundColor(mCtx.getResources().getColor(R.color.ling));
        } else {
            holder.zhengling.setText(mCtx.getString(R.string.zheng));
            holder.zhengling.setBackgroundColor(mCtx.getResources().getColor(R.color.zheng));
        }

        return view;

    }


    @Override
    public int getCount() {return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return Long.parseLong(mList.get(i).getGoods_id());
    }



    class ViewHolder extends RecyclerView.ViewHolder{
        @InjectView(R.id.imageView)
        SimpleDraweeView imageView;
        @InjectView(R.id.textView_chanpin)
        TextView textView_chanpin;
        @InjectView(R.id.iv_homelist_guojia)
        TextView iv_homelist_guojia;
        @InjectView(R.id.tv_chanpin_number)
        TextView tv_chanpin_number;
        @InjectView(R.id.tv_ilaitem_price)
        TextView tv_ilaitem_price;
        @InjectView(R.id.tv_ilaitem_unit)
        TextView tv_ilaitem_unit;
        @InjectView(R.id.tv_ilaitem_price2)
        TextView tv_ilaitem_price2;
        @InjectView(R.id.tv_ilaitem_unit2)
        TextView tv_ilaitem_unit2;
        @InjectView(R.id.tv_ilaitem_location)
        TextView tv_ilaitem_location;
        @InjectView(R.id.tv_xian_qi)
        TextView xianqi;
        @InjectView(R.id.tv_zheng_ling)
        TextView zhengling;
        @InjectView(R.id.tv_pin)
        TextView pin;
        @InjectView(R.id.tv_huan)
        TextView huan;
        @InjectView(R.id.rl_home_isover)
        ImageView rl_home_isover;
        @InjectView(R.id.rl_home_item)
        RelativeLayout rl_home_item;


        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.inject(this,itemView);

        }

    }


    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

}
