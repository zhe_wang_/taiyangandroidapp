package taiyang.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.entity.MessageModel;
import taiyang.com.tydp_b.R;

/**
 * Created by admin on 2016/7/21.
 * 售后适配器
 */
public class AfterSaleAdapter extends ListBaseAdapter<MessageModel>{
    private Context mContext;
    private boolean flag=true;
    public AfterSaleAdapter(Context mContext) {
        this.mContext=mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.message_item2, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final ViewHolder viewHolder= (ViewHolder) holder;
        MessageModel messageModel=mDatas.get(position);
        viewHolder.tv_title.setText(messageModel.getMsg_title());
        viewHolder.tv_contentshow.setText(messageModel.getMsg_content());
        viewHolder.tv_contenthide.setText(messageModel.getMsg_content());
        viewHolder.tv_time.setText(messageModel.getMsg_time());
//        holder.tv_title.setText(messageModel.getMsg_title());
        if (viewHolder.tv_contentshow.getText().length()<20){
            viewHolder.more.setVisibility(View.GONE);
        }
        if (mOnItemClickLitener != null) {
            viewHolder.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = viewHolder.getLayoutPosition();
                    mOnItemClickLitener.onItemClick(viewHolder.more, pos);
//                    if(flag){
                    viewHolder.showLayout.setVisibility(View.GONE);
                    viewHolder.hideLayout.setVisibility(View.VISIBLE);
//                        holder.more.setImageResource(R.mipmap.buy_icon_up);
//                        flag=false;
//                    }else {
//                        holder.show.setVisibility(View.VISIBLE);
//                        holder.hide.setVisibility(View.GONE);
//                        holder.more.setImageResource(R.mipmap.buy_icon_down);
//                        flag=true;
//                    }
                }
            });
            viewHolder.hide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = holder.getLayoutPosition();
                    mOnItemClickLitener.onItemClick(viewHolder.more, pos);
//                    if(flag){
                    viewHolder.showLayout.setVisibility(View.VISIBLE);
                    viewHolder.hideLayout.setVisibility(View.GONE);
//                        holder.more.setImageResource(R.mipmap.buy_icon_up);
//                        flag=false;
//                    }else {
//                        holder.show.setVisibility(View.VISIBLE);
//                        holder.hide.setVisibility(View.GONE);
//                        holder.more.setImageResource(R.mipmap.buy_icon_down);
//                        flag=true;
//                    }
                }
            });
        }
    }

   /* public void onBindViewHolder(final ViewHolder holder, int position) {
        MessageModel messageModel=mDatas.get(position);
        holder.tv_title.setText(messageModel.getMsg_title());
        holder.tv_contentshow.setText(messageModel.getMsg_content());
        holder.tv_contenthide.setText(messageModel.getMsg_content());
        holder.tv_time.setText(messageModel.getMsg_time());
//        holder.tv_title.setText(messageModel.getMsg_title());
        if (holder.tv_contentshow.getText().length()<20){
            holder.more.setVisibility(View.GONE);
        }
        if (mOnItemClickLitener != null) {
            holder.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = holder.getLayoutPosition();
                    mOnItemClickLitener.onItemClick(holder.more, pos);
//                    if(flag){
                        holder.showLayout.setVisibility(View.GONE);
                        holder.hideLayout.setVisibility(View.VISIBLE);
//                        holder.more.setImageResource(R.mipmap.buy_icon_up);
//                        flag=false;
//                    }else {
//                        holder.show.setVisibility(View.VISIBLE);
//                        holder.hide.setVisibility(View.GONE);
//                        holder.more.setImageResource(R.mipmap.buy_icon_down);
//                        flag=true;
//                    }
                }
            });
            holder.hide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = holder.getLayoutPosition();
                    mOnItemClickLitener.onItemClick(holder.more, pos);
//                    if(flag){
                    holder.showLayout.setVisibility(View.VISIBLE);
                    holder.hideLayout.setVisibility(View.GONE);
//                        holder.more.setImageResource(R.mipmap.buy_icon_up);
//                        flag=false;
//                    }else {
//                        holder.show.setVisibility(View.VISIBLE);
//                        holder.hide.setVisibility(View.GONE);
//                        holder.more.setImageResource(R.mipmap.buy_icon_down);
//                        flag=true;
//                    }
                }
            });
        }
    }*/

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //        @InjectView(R.id.tv_order)
//        TextView order;
//        @InjectView(R.id.tv_orderstate)
//        TextView orderstate;
//        @InjectView(R.id.tv_ordername)
//        TextView ordername;
//        @InjectView(R.id.tv_orderprice)
//        TextView orderprice;
//        @InjectView(R.id.tv_ordercount)
//        TextView ordercount;
        @InjectView(R.id.tv_title)
        TextView tv_title;
        @InjectView(R.id.tv_contentshow)
        TextView tv_contentshow;
        @InjectView(R.id.tv_time)
        TextView tv_time;
        @InjectView(R.id.tv_contenthide)
        TextView tv_contenthide;
        @InjectView(R.id.im_more)
        LinearLayout more;
        @InjectView(R.id.im_more_hide)
        LinearLayout hide;
        @InjectView(R.id.show_layout)
        RelativeLayout showLayout;
        @InjectView(R.id.hide_layout)
        RelativeLayout hideLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }

    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);

//        void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
}
