package taiyang.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.entity.OfferFragmentTextBean;
import taiyang.com.tydp_b.R;

/**
 * OfferFragment CategoryFragment 产地 厂号 港口 RecyClerView Adapter
 * Created by heng on 2016/8/9.
 */
public class ZhuFragmentCDRvAdapter extends RecyclerView.Adapter<ZhuFragmentCDRvAdapter.ViewHolder> {
    private List<OfferFragmentTextBean> lists;
    private Context mCtx;
    public boolean isShow = false;
    private List<Boolean> checkedList;

    public ZhuFragmentCDRvAdapter(List<OfferFragmentTextBean> lists, Context mCtx, List<Boolean> checkedList) {
        this.lists = lists;
        this.mCtx = mCtx;
        this.checkedList = checkedList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_zhufragment_cd, parent, false);

        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tv_zhufragment_sitename.setText(lists.get(position).getname());
        holder.position = position;

        holder.rl_zhufragment_site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < checkedList.size(); i++) {
                    checkedList.set(i,false);
                }
                holder.checkBox.toggle();
                checkedList.set(position, holder.checkBox.isChecked());
                notifyDataSetChanged();
            }
        });

        holder.checkBox.setChecked(checkedList.get(position));
        holder.checkBox.setVisibility(checkedList.get(position) ? View.VISIBLE : View.GONE);

    }

    @Override
    public int getItemCount() {

        return lists.size();
    }

    public void setCheckedList(List<Boolean> checkedList) {
        this.checkedList = checkedList;
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

        @InjectView(R.id.tv_zhufragment_sitename)
        TextView tv_zhufragment_sitename;
        @InjectView(R.id.iv_zhufragmentcd_ico)
        CheckBox checkBox;
        @InjectView(R.id.rl_zhufragment_site)
        RelativeLayout rl_zhufragment_site;
        private int position;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            checkBox.setOnCheckedChangeListener(this);
            rl_zhufragment_site.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);
                    onItemClickListener.setOnItemClick(position, false);
                } else {
                    checkBox.setChecked(true);
                    onItemClickListener.setOnItemClick(position, true);
                }
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (onItemClickListener != null) {
                onItemClickListener.setOnItemCheckedChanged(position, isChecked);
            }
        }
    }

    public interface OnItemClickListener {
        void setOnItemClick(int position, boolean isCheck);

        void setOnItemCheckedChanged(int position, boolean isCheck);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

}
