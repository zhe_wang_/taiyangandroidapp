package taiyang.com.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.activity.AccountActivity;
import taiyang.com.activity.AddAccountActivity;
import taiyang.com.activity.EditAccountActivity;
import taiyang.com.entity.BankAccountModel;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.HttpRequestListener;
import taiyang.com.utils.HttpUtils;
import taiyang.com.utils.MD5Utils;
import taiyang.com.utils.MySPEdit;
import taiyang.com.utils.ToastUtil;

/**
 * PersonInfoActivity  Adapter
 * Created by heng on 2016/7/14.
 */
public class BankAccountAdapter extends RecyclerView.Adapter<BankAccountAdapter.ViewHolder> {
    private List<BankAccountModel.BankListBean> mDatas;
//    private int count = 4;
    private boolean flag = true;
    private Context mCtx;
    //用于记录每个RadioButton的状态，并保证只可选一个
    HashMap<String, Boolean> states = new HashMap<String, Boolean>();
    private boolean flagCheck ;

    private Button confirm;

    private boolean intentFlag;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    private int type =0;

    public BankAccountAdapter(List<BankAccountModel.BankListBean> mDatas, Context mCtx, Button confirm,int type) {
        this.mDatas = mDatas;
        this.mCtx = mCtx;
        this.confirm = confirm;
        this.type = type;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_account, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tv_personinfoitem_name.setText(mDatas.get(position).getUsername());
        holder.tv_personinfoitem_phone.setText(mDatas.get(position).getDeposit_bank());
        holder.tv_personinfoitem_number.setText(mDatas.get(position).getAccount());
        holder.rb_personinfo_ischeck.setChecked(false);
        if(type==2){
            holder.rb_personinfo_ischeck.setChecked(mDatas.get(position).getIs_spot().equals("1"));
        }
        if(type==1){
            holder.rb_personinfo_ischeck.setChecked(mDatas.get(position).getIs_future().equals("1"));
        }
        holder.rb_personinfo_ischeck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!((CheckBox)view).isChecked()){
                    ((CheckBox) view).setChecked(true);
                    return;
                }else{
                    ((AccountActivity)mCtx).setData(type==1?"future":"spot",mDatas.get(position).getId());

                }
            }
        });
        holder.iv_personinfo_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mCtx, EditAccountActivity.class);
                i.putExtra("bankListBean", (Serializable) mDatas.get(position));
                mCtx.startActivity(i);
            }
        });
        holder.iv_personinfo_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Map<String,Object> params=new HashMap<>();
                params.put("model","seller");
                params.put("action","delBank");
                params.put("sign", MD5Utils.encode(MD5Utils.getSign("seller","delBank")));
                params.put("user_id", MySPEdit.getInstance(mCtx).getUserId());
                params.put("token",MySPEdit.getInstance(mCtx).getToken());
                params.put("bank_id", mDatas.get(position).getId());
                HttpUtils.sendPost(params, new HttpRequestListener() {
                    @Override
                    public void success(String response, int id) {
                        ((AccountActivity)mCtx).refreshData();
                        ToastUtil.showToast("删除成功");
                    }

                    @Override
                    public void failByOther(String fail, int id) {

                    }

                    @Override
                    public void fail(String fail, int id) {

                    }
                }, 101);
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (intentFlag) {

                    if(Activity.class.isInstance(mCtx))
                    {
                        // 转化为activity，然后finish就行了
                        Activity activity = (Activity)mCtx;
                        Intent a = new Intent(mCtx, AddAccountActivity.class);
                        a.putExtra("address_id",mDatas.get(position).getId());
                        activity.setResult(10,a);
                        activity.finish();
                    }
                } else {
                    Intent i = new Intent(mCtx, AddAccountActivity.class);
                    mCtx.startActivity(i);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.iv_personinfo_ischeck)
        CheckBox rb_personinfo_ischeck;
        @InjectView(R.id.iv_personinfo_edit)
        ImageView iv_personinfo_edit;
        @InjectView(R.id.iv_personinfo_delete)
        ImageView iv_personinfo_delete;
        @InjectView(R.id.tv_personinfoitem_name)
        TextView tv_personinfoitem_name;
        @InjectView(R.id.tv_personinfoitem_phone)
        TextView tv_personinfoitem_phone;
        @InjectView(R.id.tv_personinfoitem_number)
        TextView tv_personinfoitem_number;


        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }

    }



    public void setFlag(boolean flag) {
        this.flagCheck = flag;
    }
}
