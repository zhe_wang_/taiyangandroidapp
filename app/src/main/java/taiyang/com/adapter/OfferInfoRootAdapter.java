package taiyang.com.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.entity.HuckListBean;
import taiyang.com.tydp_b.R;
import taiyang.com.utils.MyApplication;

/**
 * 商品详情 出价底部 RecyClerView Adapter
 * Created by heng on 2016/7/14.
 */
public class OfferInfoRootAdapter extends RecyclerView.Adapter<OfferInfoRootAdapter.ViewHolder> {
    private List<HuckListBean> mList;

    public OfferInfoRootAdapter(List<HuckListBean> mList) {
        this.mList = mList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_offerinforoot, parent, false);

        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if(position ==0){
            holder.tv_offerinforoot_name.setTextColor(MyApplication.getContext().getResources().getColor(R.color.change_bg));
            holder.tv_offerinforoot_tima.setTextColor(MyApplication.getContext().getResources().getColor(R.color.change_bg));
            holder.tv_offerinforoot_danwei.setTextColor(MyApplication.getContext().getResources().getColor(R.color.change_bg));
        }
        holder.tv_offerinforoot_name.setText(mList.get(position).getUser_name());
        holder.tv_offerinforoot_tima.setText(mList.get(position).getCreated_at());
        holder.tv_offerinforoot_danwei.setText(mList.get(position).getFormated_price()+mList.get(position).getUnit());
    }

    @Override
    public int getItemCount() {

        return mList.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder{
        @InjectView(R.id.tv_offerinforoot_name)
        TextView tv_offerinforoot_name;
        @InjectView(R.id.tv_offerinforoot_tima)
        TextView tv_offerinforoot_tima;
        @InjectView(R.id.tv_offerinforoot_danwei)
        TextView tv_offerinforoot_danwei;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.inject(this,itemView);
        }

    }

    // 点击事件的回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
//      void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
}
