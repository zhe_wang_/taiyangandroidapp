package taiyang.com.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import taiyang.com.entity.OfferInfoBean;
import taiyang.com.tydp_b.R;

/**
 * OfferInfoHistory底部RecyClerView Adapter
 * Created by heng on 2016/7/18.
 */
public class OfferInfoHistoryRvAdapter extends RecyclerView.Adapter<OfferInfoHistoryRvAdapter.ViewHolder> {
    private List<OfferInfoBean.BpListBean> mList;
    public OfferInfoHistoryRvAdapter(List<OfferInfoBean.BpListBean> mList) {
        this.mList = mList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_offerinfohistorylist_item, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tv_offerinfohistory_time.setText(mList.get(position).getAdd_time());
        holder.tv_offerinfohistory_money.setText("¥"+mList.get(position).getGoods_price());
//        holder.pb_offerinfostory.setProgress(mList.get(position).getRate());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @InjectView(R.id.tv_offerinfohistory_time)
        TextView tv_offerinfohistory_time;
        @InjectView(R.id.tv_offerinfohistory_money)
        TextView tv_offerinfohistory_money;



        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.inject(this,itemView);

        }

    }
}
